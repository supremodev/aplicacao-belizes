<?php

namespace App\Controller;

use App\Model\Admin;
use App\Model\Cliente;
use App\Model\Funcionario;
use App\Model\Pesquisa;
use App\Model\Agenda;
use App\Controller\NivelController;



class AdminController
{
    public function __construct()
    {
        (new LoginController)->usuarioLongado();
    }

    public function header()
    {
        if ($_SESSION['funcNivel'] == "Profissional") {
            require APP . 'view/templates/header-profissional.php';
        } elseif($_SESSION['funcNivel'] == "Atendimento"){
            require APP . 'view/templates/header-atendente.php';
        } elseif($_SESSION['funcNivel'] == "Admin"){
            require APP . 'view/templates/header.php';
        }   
    }

    public function index()
    {
        
        $Admin          = new Admin();
        $Clientes       = new Cliente();
        $qtdClientes    = $Clientes->getAmountOfCliente();
        
        $Funcionario    = new Funcionario();
        $qtdfuncionario = $Funcionario->getAmountOfFuncionario();
		
		$MediaPesquisa  = new Pesquisa();
        $mediaPesquisa	= $MediaPesquisa->listaPesquisa24H();

        $Compromisso  = new Agenda();
        $listaCompromisso = $Compromisso->listaTodosAgendamento();
        $qtdCompromisso = $Compromisso->qtdAgendamento();
        $qtdCompromissoMes = $Compromisso->qtdAgendamentoMes();
        
	
        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/admin/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function cliente($id)
    {
		$MediaPesquisa  = new Pesquisa();
        $mediaPesquisa	= $MediaPesquisa->listaPesquisa24H();
        
        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/admin/index.php';
        require APP . 'view/templates/footer.php';
    }
}
