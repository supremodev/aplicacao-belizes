<?php
namespace App\Controller;


use App\Model\Agenda;
use App\Model\Cliente;
use App\Model\Funcionario;
use App\Model\Servicos;
use App\Model\Pacote;


class AgendaController
{
	private $idDoAgendamento;
	private $agendaData;
	private $agendaHora;
	private $idUnidade;
	private $agendaFormaPG;
	private $idFunc;
	private $idPacoteServico;
	private $agendaStatus;
	
    public function __construct()
    {
    	//(new LoginController)->usuarioLongado();
	}
	
	public function header()
    {
        if ($_SESSION['funcNivel'] == "Profissional") {
            require APP . 'view/templates/header-profissional.php';
        } elseif($_SESSION['funcNivel'] == "Atendimento"){
            require APP . 'view/templates/header-atendente.php';
        } elseif($_SESSION['funcNivel'] == "Admin"){
            require APP . 'view/templates/header.php';
        }   
    }

/*################## LISTAR TODOS OS DADOS DA AGENDA ################################# */
    public function index()
    {
        $Agendamento 	= new Agenda();
		$agendaLista 	= $Agendamento->listaTodosAgendamento();

		$Funcionario 			= new Funcionario();
        
        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/agenda/index.php';
        require APP . 'view/templates/footer.php';
	}
	
	public function listafiltro($filtro)
    {
		$Agendamento 	= new Agenda();
		
		$Funcionario 			= new Funcionario();
		
		if ($filtro == "dia") {
			$agendaLista 	= $Agendamento->agendamentoDia();
		} else if($filtro == "semana") {
			$agendaLista 	= $Agendamento->agendamentoSemana();
		} else {
			$agendaLista 	= $Agendamento->agendamentoMes();
		}
		
		require APP . 'view/templates/head.php';
        $this->header(); 
        require APP . 'view/agenda/index.php';
        require APP . 'view/templates/footer.php';
	}
	
	public function filtroCompromisso($filtro)
    {
		$Funcionario = new Funcionario();
        $profissionaisLista = $Funcionario->getPerfilProfissional($_GET['idusuario']);
		$especialidadeLista = $Funcionario->getPerfilEspecialidade($_GET['idusuario']);
		
        $Agendamento 	= new Agenda();
		
		if ($filtro == "dia") {
			$agendaLista 	= $Agendamento->compromissoDia($_GET['idusuario']);
		} else if($filtro == "semana") {
			$agendaLista 	= $Agendamento->compromissoSemana($_GET['idusuario']);
		} else {
			$agendaLista 	= $Agendamento->compromissoMes($_GET['idusuario']);
		}
		
		require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/funcionario/perfil.php';
        require APP . 'view/templates/footer.php';

    }
	
/*################## CARREGAR PÁGINA PARA UM NOVO REGISTRO NA AGENDA ################################# */
	public function novo()
    {
		// CADASTRAR O ID DO NOVO REGISTRO
		$Agendamento 			= new Agenda();		
		$idDoAgendamento		= $Agendamento->idAgendamento;
		$Cliente				= new Cliente();
		$listaCliente			= $Cliente->listaTodos();
		$Servicos				= new Servicos();
		$listaServicos			= $Servicos->listaTodos(1);
		$Funcionario			= new Funcionario();
		$listaFuncionario		= $Funcionario->todosProfissionais();
		$Pacote					= new Pacote();
		$listaPacote			= $Pacote->listaTodos(1); 

        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/agenda/novo.php';
        require APP . 'view/templates/footer.php';
	}

	public function visualizar($id)
    {
        $Agenda = new Agenda();
		$AgendaLista = $Agenda->lista($id);
		
		$Funcionario			= new Funcionario();
		$listaFuncionario		= $Funcionario->todosProfissionais();

		$Servicos				= new Servicos();
		$listaServicos			= $Servicos->agendamentoServicos($id);

		$Pacote					= new Servicos();
		$listaPacotes			= $Pacote->agendamentoPacotes($id);

		$Tratamentos			= new Servicos();
		$tratamentoQtd			= $Tratamentos->tratamentoQtd($AgendaLista[0]->idTratamento);

		if (!empty($tratamentoQtd)) {

			if (!empty($tratamentoQtd[0]->idFichaAnamneseCriolipolise)) {
				$fichaTratamento = "Tratamento Anamnese Criolipolise";
				$tratamentototal = $Tratamentos->tratamentoCriolipolise($tratamentoQtd[0]->idFichaAnamneseCriolipolise);
				$tratamentototal = $tratamentototal[0]->fichaAnamneseQtdSessoes;
			} else if(!empty($tratamentoQtd[0]->idFichaFacil)) {
				$fichaTratamento = "Tratamento Facial";
				$tratamentototal = $Tratamentos->tratamentoFacial($tratamentoQtd[0]->idFichaFacil);
				$tratamentototal = $tratamentototal[0]->fichaFacilQtdSessoes;
			} else {
				$fichaTratamento = "Tratamento Corporal";
				$tratamentototal = $Tratamentos->tratamentoCorporal($tratamentoQtd[0]->idFichaCorporal);
				$tratamentototal = $tratamentototal[0]->fichaCorporalQtdSessoes;
			}
			
		}

        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/agenda/visualizar.php';
        require APP . 'view/templates/footer.php';
	}
	
	public function visualizarCliente($id)
    {
        $AgendaLista = new Agenda();
		$AgendaLista = $AgendaLista->lista($id);
		
		$Servicos				= new Servicos();
		$listaServicos			= $Servicos->agendamentoServicos($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header-cliente.php';
        require APP . 'view/agenda/visualizarcliente.php';
        require APP . 'view/templates/footer.php';
	}
	
	public function visualizarDatas()
    {
        $AgendaLista = new Agenda();
		$AgendaLista = $AgendaLista->visualizardata($_POST['idFunc'],$_POST['dataAgenda']);

        require APP . 'view/agenda/visualizardata.php';
    }

	
/*################## CARREGAR PÁGINA PARA EDITAR O REGISTRO DA AGENDA ################################# */
	public function editar($id)
    {
        $AgendaLista = new Agenda();
		$AgendaLista = $AgendaLista->lista($id);
		
		$Funcionario			= new Funcionario();
		$listaFuncionario		= $Funcionario->todosProfissionais();

		$Servicos				= new Servicos();
		$listaServicos			= $Servicos->agendamentoServicos($id);

		$Pacote					= new Pacote();
		$listaPacote			= $Pacote->listaAgendamento($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/agenda/editar.php';
        require APP . 'view/templates/footer.php';
    }
	
	public function tratamento()
    {
        $Tratamento = new Agenda();

		$listafacial = $Tratamento->ExiteFichaFacial($_POST['idCliente'],$_POST['idFunc']);
		$listacorporal = $Tratamento->ExiteFichaCorporal($_POST['idCliente'],$_POST['idFunc']);
		$listacriolipolise = $Tratamento->ExiteFichaCriolipolise($_POST['idCliente'],$_POST['idFunc']);

        require APP . 'view/agenda/lista-tratamento.php';
	}
/*################## DESATIVAR REGISTRO DA AGENDA ################################# */

	
	
/*################## ATUALIZAR REGISTRO DA AGENDA ################################# */
	public function atualizar() 
    {
        $Agenda 			= new Agenda();

		if (isset($_POST['idTratamento'])) {
		
			$Sessoesfeitas	= $Agenda->qtdTratamento($_POST['idTratamento']);

			if ($_POST['existetratamento'] == 1) 
			{
				$realizarTratamento	= $Agenda->atualizarTratamento($_POST['idTratamento'],++$Sessoesfeitas[0]->tratamentoQtd);
			}
		}	

        $realizarRegistro	= $Agenda->atualizarRegistro(	$_POST['idAgenda'], 
														 	$_POST['agendaData'],
															$_POST['agendaHora'],
															$_POST['idCliente'],
															$_POST['agendaFormaPG'],
															$_POST['idFunc'],
															$_POST['agendaStatus'],
															$_POST['agendaMensagem']);
        echo json_encode($realizarRegistro);
    }
	

	
/*################## CONSULTAR AGENDA POR NOME DO CLIENTE ################################# */
    public function inserir()
    {
        $Agenda 		= new Agenda();
        $agendamentos 	= $Agenda->inserir($_POST['agendaData'], 
											$_POST['agendaHora'], 
											$_POST['idCliente'], 
											$_POST['agendaFormaPG'], 
											$_POST['idFunc'], 
											$_POST['agendaMensagem'],
											true);
        echo json_encode($agendamentos);
	}
	
	public function inserirServico()
    {
		$Agenda 		= new Agenda();

		if (isset($_POST['idServico'])) {

			$idsservicos = array();
			$idServico = $_POST['idServico'];
			foreach ($idServico as $linha) {
				array_push($idsservicos, $Agenda->retornaIdServico($linha));
			}
	
			foreach ($idsservicos as $linhaa) {
				$agendamentos = $Agenda->inserirServico($_POST['idAgenda'],$linhaa[0]->idServico);
			}
	
			echo json_encode($agendamentos);
			
		} else if(isset($_POST['idPacote'])) {

			$idsservicos = array();
			$idServico = $_POST['idPacote'];
			foreach ($idServico as $linha) {
				array_push($idsservicos, $Agenda->retornaIdPacote($linha));
			}
	
			foreach ($idsservicos as $linhaa) {
				$agendamentos = $Agenda->inserirPacote($_POST['idAgenda'],$linhaa[0]->idPacoteServico);
			}
	
			echo json_encode($agendamentos);
			
		} else {

			$servicoOuPacote = $Agenda->retornaIdServicoEIdPacote($_POST['idCliente'], $_POST['idFunc'], $_POST['idTratamento']);
			
			if ($servicoOuPacote[0]->idServico == 0) {
		
				foreach ($servicoOuPacote as $linhaa) {
					$agendamentos = $Agenda->inserirPacote($_POST['idAgenda'],$linhaa->idPacoteServico);
				}

			} else {

				foreach ($servicoOuPacote as $linhaa) {
					$agendamentos = $Agenda->inserirServico($_POST['idAgenda'],$linhaa->idServico);
				}

			}

			$agendamentos 	= $Agenda->atualizarTratamentoo($_POST['idAgenda'], $_POST['idTratamento']);
        	echo json_encode($agendamentos);
			
		}

    }

}

