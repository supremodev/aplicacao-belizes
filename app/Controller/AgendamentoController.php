<?php

namespace App\Controller;

class AgendamentoController
{
    public function index()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");       

        require APP . 'view/site/agendamento.php';
    }
	
	public function inserir()
    {
        $Funcionario = new Funcionario();
        $funcionarios = $Funcionario->inserir(	$_POST['funcNome'], 
												$_POST['funcCpf'], 
												$_POST['funcRg'], 
												$_POST['funcEmail'], 
												$_POST['funcSenha'], 
												$_POST['funcFone'], 
												$_POST['funcCel'], 
												$_POST['funcNasci'], 
												$_POST['funcSexo'], 
												$_POST['funcCargo'], 
												$_POST['funcEnd'], 
												$_POST['funcNumero'], 
												$_POST['funcEndComp'], 
												$_POST['funcCidade'], 
												$_POST['funcCEP'], 
												$_POST['funcNivel'], 
												"user.jpg", 
												"assinatura.png", 
												$_POST['funcBiografia'], 
												true);
        echo json_encode($funcionarios);
    }
}
