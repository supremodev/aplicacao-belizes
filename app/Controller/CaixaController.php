<?php
namespace App\Controller;

use App\Controller\EmailController;
use App\Model\Caixa;
use App\Model\Cliente;
use App\Model\Funcionario;



class CaixaController
{
	private $tipo;
	
    public function __construct()
    {
        (new LoginController)->usuarioLongado();
    }

    public function header()
    {
        if ($_SESSION['funcNivel'] == "Profissional") {
            require APP . 'view/templates/header-profissional.php';
        } elseif($_SESSION['funcNivel'] == "Atendimento"){
            require APP . 'view/templates/header-atendente.php';
        } elseif($_SESSION['funcNivel'] == "Admin"){
            require APP . 'view/templates/header.php';
        }   
    }

    public function index()
    {
        $CaixaLista 		= new Caixa();
		$caixaLista 		= $CaixaLista->listaTodosCaixa();
        $qtdCaixa 			= $CaixaLista->qtdOfCaixa();
        $caixaEntrada 		= $CaixaLista->totalEntradaCaixa();
        $caixaSaida 		= $CaixaLista->totalSaidaCaixa();
		
        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/caixa/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function listafiltro($filtro)
    {
        $Caixa 	= new Caixa();

        $caixaLista 		= $Caixa->listaTodosCaixa();
        $qtdCaixa 			= $Caixa->qtdOfCaixa();
        $caixaEntrada 		= $Caixa->totalEntradaCaixa();
        $caixaSaida 		= $Caixa->totalSaidaCaixa();
		
		if ($filtro == "dia") {
			$caixaLista 	= $Caixa->caixaDia();
		} else if($filtro == "semana") {
			$caixaLista 	= $Caixa->caixaSemana();
		} else if($filtro == "mes") {
			$caixaLista 	= $Caixa->caixaMes();
		} else {
            $caixaLista 	= $Caixa->caixaSemestre();
        }
		
		require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/caixa/index.php';
        require APP . 'view/templates/footer.php';

	}

    public function novo()
    {
        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/caixa/novo.php';
        require APP . 'view/templates/footer.php';
    }
    
    public function pagarComissao()
    {
        require APP . 'view/caixa/comissao.php';
    }

    public function comissaoAgendamento()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data 				= date_create();
        $DataAtual 			= date_format($Data,"d-m-Y");
		
		$Caixa 			= new Caixa();
        $inserirCaixa 	= $Caixa->comissaoAgendamento(	$_POST['idAgenda'],
                                                        $_POST['caixaCategoria'], 
                                                        $_POST['caixaTipo'], 
                                                        $DataAtual, 
                                                        $_POST['caixaVencimento'], 
                                                        $_POST['caixaValor'], 
                                                        $_POST['caixaObs'],  
                                                        "1",
                                                        $_POST['caixaDataPagamento']);
        echo json_encode($inserirCaixa);
    }
	
	public function consultaTipoEntrada()
    {
        $nome = $_POST['tipo'];

        if(!empty($tipo))
        {
            $consultaTipoEntrada 	= new Caixa();
            $consultaEntrada 		= $consultaTipoEntrada->consultaTipoEntrada($tipo);

            if (!empty($consultaEntrada)) {
                foreach ($consultaEntrada as $linha) {     
                    
                }
            } else {
                echo "<div class='nomeescolhido'><div class='form-check'>Nenhum nome encontrado</div></div>";
            } 
        } 
    }

    
    public function editar($id)
    {
        $CaixaLista 	= new Caixa();
        $caixaLista 	= $CaixaLista->lista($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/caixa/editar.php';
        require APP . 'view/templates/footer.php';

    }

    public function desativar()
    {
		$id 			= $_POST['id'];
        $Caixa 			= new Caixa();
        $caixaDesativar = $Caixa->desativar($id);
        echo json_decode($caixaDesativar);
    }

    public function atualizar($id)
    {
        $Caixa 			= new Caixa();
        $caixaLista 	= $Caixa->lista($id);
        $msgModal 		= $Caixa->atualizar($id,
											$_POST['caixaCategoria'],
											$_POST['caixaTipo'],
											$_POST['caixaVencimento'],
											$_POST['caixaValor'],
											$_POST['caixaObs'],
											$_POST['caixaDataPagamento']);
        echo json_encode($msgModal);    
    }


    public function inserir()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data 				= date_create();
        $DataAtual 			= date_format($Data,"d-m-Y");
		
		
		$Caixa 			= new Caixa();
        $inserirCaixa 	= $Caixa->inserir(		$_POST['caixaCategoria'], 
												$_POST['caixaTipo'], 
												$DataAtual, 
												$_POST['caixaVencimento'], 
												$_POST['caixaValor'], 
												$_POST['caixaObs'],  
												"1",
										 		$_POST['caixaDataPagamento']);
        echo json_encode($inserirCaixa);
    }
	
	
	/* ############################ PERFIL Caixa ########################### */	
	public function perfil($id)
    {
        $perfilCaixa = new Caixa();
        $perfilLista = $perfilCaixa->getPerfilCaixa($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/Caixa/perfil.php';
        require APP . 'view/templates/footer.php';
    }

}

