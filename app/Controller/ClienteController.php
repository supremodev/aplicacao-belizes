<?php

namespace App\Controller;

use App\Model\Cliente;
use App\Model\Agenda;
use App\Model\Prontuario;
use App\Model\Depoimento;
use App\Model\Ficha;
use App\Model\Pesquisa;
use App\Model\Contrato;

class ClienteController
{
    private $alerta;
    private $controller;
    public  $permitirAtualizacaoGeral;
    private $permitirAtualizacaoConvid;

    public function __construct()
    {
        /*if (isset($_SESSION['idUsuario'])) {
            (new LoginController)->usuarioLongado();
        } else {
            (new LoginController)->clienteLongado();
        }*/
        
        //$this->controller =  get_class($this);
    }

    public function header()
    {
        if ($_SESSION['funcNivel'] == "Profissional") {
            require APP . 'view/templates/header-profissional.php';
        } elseif($_SESSION['funcNivel'] == "Atendimento"){
            require APP . 'view/templates/header-atendente.php';
        } elseif($_SESSION['funcNivel'] == "Admin"){
            require APP . 'view/templates/header.php';
        } else {
            require APP . 'view/templates/header-cliente.php';
        }
    }

    public function index()
    {
        $controller 	=  $this->controller;
        $clienteLista   = new Cliente();
        $qtdCliente     = $clienteLista->getAmountOfCliente();
        $clienteLista   = $clienteLista->listaTodos();
		
		$MediaPesquisa  = new Pesquisa();
        $mediaPesquisa	= $MediaPesquisa->mediaPesquisa();

        $msgErro = $this->alerta;

        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/cliente/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function listadeclienteprofissional()
    {
        $controller 	=  $this->controller;
        $clienteLista   = new Cliente();
        $qtdCliente     = $clienteLista->getAmountOfCliente();
        $clienteLista   = $clienteLista->listaTodos();
		
		$MediaPesquisa  = new Pesquisa();
        $mediaPesquisa	= $MediaPesquisa->mediaPesquisa();

        $msgErro = $this->alerta;

        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/cliente/lista-cliente-profissional.php';
        require APP . 'view/templates/footer.php';
    }

    public function listafiltro($cpf)
    {
        $controller 	=  $this->controller;
        $clienteLista   = new Cliente();
        $qtdCliente     = $clienteLista->getAmountOfCliente();
        $clienteLista   = $clienteLista->listafiltrocpf($cpf);

        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/cliente/lista-cliente-profissional.php';
        require APP . 'view/templates/footer.php';
    }

    public function desativado()
    {
        $controller 	=  $this->controller;
        $clienteLista   = new Cliente();
        $clienteLista   = $clienteLista->listaDesativados();

        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/cliente/desativados.php';
        require APP . 'view/templates/footer.php';
    }

    public function corpo()
    {
        $x = 700;
        $y = 300;

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/corpo/homem.php';
        require APP . 'view/templates/footer.php';
    }

    public function novo()
    {
        $controller =  $this->controller;
        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/cliente/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        $controller =  $this->controller;
        $clienteLista = new Cliente();
        $clienteLista = $clienteLista->lista($id);

        require APP . 'view/templates/head.php';
        
        $this->header();

        require APP . 'view/cliente/editar.php';
        require APP . 'view/templates/footer.php';
    }

    public function ativarDesativar($boleano)
    {
        $id = $_POST['id'];
        $cliente = new Cliente();
        $cliente = $cliente->ativarDesativar($id,$boleano);
        echo json_decode($cliente);
    }

    public function permissaoAtualizarGeral()
    {
        $id = $_POST['id'];
        $boleano = $_POST['boleano'];
        $cliente = new Cliente();
        $cliente = $cliente->permissaoAtualizarGeral($id,$boleano);
        echo json_decode($cliente);
    }

    public function permissaoAtualizarCovid()
    {
        $id = $_POST['id'];
        $boleano = $_POST['boleano'];
        $cliente = new Cliente();
        $cliente = $cliente->permissaoAtualizarCovid($id,$boleano);
        echo json_decode($cliente);
    }

    public function atualizar($id)
    {
			
            $imagem = $_FILES['cliFoto'];
            $cliente = new Cliente();
            $cliente = $cliente->lista($id);

            $imgBanco = $cliente[0]->cliFoto;
            $webBanco = $cliente[0]->cliWebcam;
            $handle = new \Verot\Upload\Upload($imagem);
            $imgInput = $handle->file_src_name;

            if (!empty($_POST['cliWebcam'])) { //Se tiver imagem input

                $img = "";
                $webcam = $_POST['cliWebcam'];

            } elseif(!empty($imgInput)  ) {
                
                $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/cliente');
                $handle = new \Verot\Upload\Upload($imagem);

                if ($handle->uploaded) {
                    $handle->image_resize         = true;
                    $handle->image_x              = 100;
                    $handle->image_ratio_y        = true;
                    $handle->file_safe_name = false;
                    $handle->file_name_body_add = '_mini';
                    $handle->process($diretorio_destino);
                }

                if ($handle->uploaded) {
                    $handle->image_resize         = true;
                    $handle->image_x              = 680;
                    $handle->image_ratio_y        = true;
                    $handle->process($diretorio_destino);
                }

                $img = $handle->file_src_name;
                $webcam = "";
            }else{

                $img    = $imgBanco;
				$webcam = $webBanco;
            }
            
            if(!empty($_POST['cliSenha'])){
                $senhadHash = password_hash($_POST['cliSenha'], PASSWORD_DEFAULT);
            } else {
                $senhadHash = $cliente[0]->cliSenha;
            }

            $Cliente        = new Cliente();
            $inserirCliente = $Cliente->atualizar(	$id,
													$_POST['cliNome'],
													$_POST['cliCPF'],
													$_POST['cliRG'],
													$_POST['cliEstadoCivil'],
													$_POST['cliEmail'],
													$senhadHash,
													$_POST['cliFone'],
													$_POST['cliCel'],
													$_POST['cliNasci'],
													$_POST['cliSexo'],
													$_POST['cliEnd'],
													$_POST['cliEndNumero'],
													$_POST['cliEndComp'],
													$_POST['cliCEP'],
													$_POST['cliEstado'],
													$_POST['cliCidade'],
													$img,
													$webcam,
													$_POST['cliComoConheceu'],
													$_POST['cliTratamento'],
													$_POST['cliAssinatura'],
													"1");
            echo json_encode($inserirCliente); 
    }

    public function inserir()
    {
		$imagem 		= $_FILES['cliFoto'];
		

            if (isset($imagem)) {

                $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/cliente');
                $handle = new \Verot\Upload\Upload($imagem);

                //$ext = pathinfo($imagem, PATHINFO_EXTENSION);
                //$nome = $imagem['name'];

                if ($handle->uploaded) {
                    $handle->image_resize         	= true;
                    $handle->image_x              	= 100;
                    $handle->image_ratio_y        	= true;
                    $handle->file_safe_name 		= false;
                    $handle->file_name_body_add 	= '_mini';
                    $handle->process($diretorio_destino);
                }

                if ($handle->uploaded) {
                    $handle->image_resize         = true;
                    $handle->image_x              = 680;
                    $handle->image_ratio_y        = true;
                    $handle->process($diretorio_destino);
                }

                $img = $handle->file_src_name;
            } else {
                $img = "";
            }

            $senhadHash = password_hash($_POST['cliSenha'], PASSWORD_DEFAULT);

            if (isset($_POST['cliAssinatura'])) {
                $assinatura = $_POST['cliAssinatura'];
            } else {
                $assinatura = null;
            }
            
            $Cliente         = new Cliente();
            $inserirCliente = $Cliente->inserir(
                $_POST['cliNome'],
                $_POST['cliCPF'],
                $_POST['cliRG'],
                $_POST['cliEstadoCivil'],
                $_POST['cliEmail'],
                $senhadHash,
                $_POST['cliFone'],
                $_POST['cliCel'],
                $_POST['cliNasci'],
                $_POST['cliSexo'],
                $_POST['cliProfissao'],
                $_POST['cliEnd'],
                $_POST['cliEndNumero'],
                $_POST['cliEndComp'],
                $_POST['cliCEP'],
                $_POST['cliEstado'],
                $_POST['cliCidade'],
                $img,
                $_POST['cliWebcam'],
                $_POST['cliComoConheceu'],
                $_POST['cliTratamento'],
                $assinatura,
                "1");
            echo json_encode($inserirCliente);
        
    }

    /* ############################ PERFIL CLIENTE ########################### */
    public function perfil($id)
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"d/m/Y");
        $HoraAtual = date_format($Data,"H:i:s");

        $perfilCliente = new Cliente();
        $perfilLista = $perfilCliente->getPerfilCliente($id);

        $depoimentoLista = new Depoimento();
        $depoimentoLista = $depoimentoLista->listaIdUsuario($id);

        $Agendamento 	= new Agenda();
        $agendaLista 	= $Agendamento->listaIdCliente($id);
        
        $Contrato = new Contrato();
        $contratoListaData 	= $Contrato->listaContratoCliente($id); 

        $FichaGeral             	= new Ficha();
        $FichaGeralLista        	= $FichaGeral->listaFichaGeralCliente($id, $DataAtual);
        $FichaGeralListaData    	= $FichaGeral->listaFichaGeralClienteData($id);
        $FichaCorporalListaData    	= $FichaGeral->listaFichaCorporalClienteData($id);
        $FichaFacialListaData    	= $FichaGeral->listaFichaFacialClienteData($id);
        $FichaAnamneseListaData    	= $FichaGeral->listaFichaAnamneseClienteData($id);
        
        $FichaCovid              = new Ficha();
        $fichaCovidLista         = $FichaCovid->getFichaAnamneseCovid19Cliente($id);
        $fichaCovidListaData     = $FichaCovid->getFichaAnamneseCovid19ClienteData($id);

        require APP . 'view/templates/head.php'; 
        $this->header();
        require APP . 'view/cliente/perfil.php'; 
        require APP . 'view/templates/footer.php'; 
    }

    public function perfilhistorico($id)
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"d/m/Y");
        $HoraAtual = date_format($Data,"H:i:s");

        $Prontuario = new Prontuario();
        $listaProntuario = $Prontuario->listaProntuarioIdCliente($id);

        $perfilCliente = new Cliente();
        $perfilLista = $perfilCliente->getPerfilCliente($id);

        $depoimentoLista = new Depoimento();
        $depoimentoLista = $depoimentoLista->listaIdUsuario($id);

        $Agendamento 	= new Agenda();
        $agendaLista 	= $Agendamento->listaIdCliente($id);
        
        $Contrato = new Contrato();
        $contratoListaData 	= $Contrato->listaContratoCliente($id); 

        $FichaGeral             	= new Ficha();
        $FichaGeralLista        	= $FichaGeral->listaFichaGeralCliente($id, $DataAtual);
        $FichaGeralListaData    	= $FichaGeral->listaFichaGeralClienteData($id);
        $FichaCorporalListaData    	= $FichaGeral->listaFichaCorporalClienteData($id);
        $FichaFacialListaData    	= $FichaGeral->listaFichaFacialClienteData($id);
        $FichaAnamneseListaData    	= $FichaGeral->listaFichaAnamneseClienteData($id);
        
        $FichaCovid              = new Ficha();
        $fichaCovidLista         = $FichaCovid->getFichaAnamneseCovid19Cliente($id);
        $fichaCovidListaData     = $FichaCovid->getFichaAnamneseCovid19ClienteData($id);

        require APP . 'view/templates/head.php'; 
        $this->header();
        require APP . 'view/cliente/perfil-historico.php';
        require APP . 'view/templates/footer.php'; 
    }

    public function formGeralHistorico($idFicha)
    {
        $FichaGeral = new Ficha();
        $FichaGeralLista = $FichaGeral->listaFichaGeralClienteHistorico($idFicha);
        require APP . 'view/cliente/form-geral-atualizacao.php';
    }

    public function visualizarcorporal($idFicha) 
    {
        $FichaGeral = new Ficha();

        $fichacorporal = $FichaGeral->FichaCorporal($idFicha);
        $fichacorporalfoto = $FichaGeral->FichaCorporalFoto($idFicha);
        $fichacorporalaperimetria = $FichaGeral->FichaCorporalPerimetria($idFicha);
        $fichacorporaladipometria = $FichaGeral->FichaCorporalAdipometria($idFicha);

        $FichaGeralLista = $FichaGeral->FichaCorporalCliente($idFicha);
        $listaMarcacaoCorporal 	= $FichaGeral->FichaCorporalMarcacao($idFicha); 

        require APP . 'view/ficha/fichacorporal.php';
        //require APP . 'view/corpo/visualizar-corpo.php';   
    }

    public function visualizarfacial($idFicha)
    {
        $FichaGeral = new Ficha();

        $FichaFacialLista = $FichaGeral->FichaFacial($idFicha);
        $FichaGeralLista = $FichaGeral->FichaFaciall($idFicha);
        $listaMarcacao = $FichaGeral->FichaFacialMarcacao($idFicha);

        //require APP . 'view/corpo/visualizar-facial.php'; 
        require APP . 'view/ficha/fichafacial.php';   
    }

    public function visualizarAnamneseCriolipolise($idFicha)
    {
        $FichaGeral = new Ficha();

        $listaAnamnese = $FichaGeral->listaIdFichaAnamnese($idFicha);

        $FichaGeralLista = $FichaGeral->sexoFichaAnamnese($idFicha);

        $listaMarcacao = $FichaGeral->FichaAnamneseMarcacao($idFicha);

        require APP . 'view/ficha/visualizarFichaAnamneseCriolipolise.php'; 
    }

    public function formGeral($id)
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"d/m/Y");
        $HoraAtual = date_format($Data,"H:i:s");

        $FichaGeral = new Ficha();
        $FichaGeralLista = $FichaGeral->listaFichaGeralCliente($id, $DataAtual);

        $perfilCliente = new Cliente();
        $perfilLista = $perfilCliente->getPerfilCliente($id);
        //$corpoMarcacao = $FichaGeral->listaMarcacaoCorpo($id);

        if(empty($FichaGeralLista)) {
            
            require APP . 'view/cliente/form-geral.php';
            $this->permitirAtualizacaoGeral = false;
            
        } else if ($perfilLista->cliPermitirAtualizacaoGeral) {

            $this->permitirAtualizacaoGeral = true;
            require APP . 'view/cliente/form-geral.php';

        } else { 

            $this->permitirAtualizacaoGeral = false;
            require APP . 'view/cliente/form-geral-atualizacao.php';

        };
    }

    public function formCovid($id)
    {
        $FichaCovid = new Ficha();
        $fichaCovidLista = $FichaCovid->getFichaAnamneseCovid19Cliente($id);

        $perfilCliente = new Cliente();
        $perfilLista = $perfilCliente->getPerfilCliente($id);
        //require APP . 'view/cliente/form-covid-atualizacao-copy.php';


        if ($perfilLista->cliPermitirAtualizacaoCovid) {
            require APP . 'view/cliente/form-covid19.php';
        } else { 
            require APP . 'view/cliente/form-covid-atualizacao-copy.php';
        };
    }

    public function formCovidHistorico($idFicha)
    {
        $FichaCovid = new Ficha();
        $fichaCovidLista = $FichaCovid->getFichaAnamneseCovid19ClienteHistorico($idFicha);
        require APP . 'view/cliente/form-covid-atualizacao-copy.php';
    }
	
	public function depoimento()
    {
            $Cliente         	= new Cliente();
            $inserirDepo 		= $Cliente->inserirDepo($_POST['idCliente'],
														$_POST['depoimentoTexto'],
														$_POST['depoimentoStatus']);
            echo json_encode($inserirDepo);        
    }

    public function existeCpf()
    {
        $Cliente         	= new Cliente();
        $cpf 		= $Cliente->existeCPF($_POST['cpf']);
        echo json_encode($cpf);        
    }

    public function enviocontato()
    {
        $Cliente        = new EmailController($_POST['nome'],$_POST['email'],$_POST['assunto'],"anexo",$_POST['msg']);
        $cliente 		= $Cliente->envioContato();
        echo json_encode($cliente);        
    }

    public function idNome()
    {
        $Cliente        = new Cliente();
        $cliente 		= $Cliente->idNome($_POST['cpf']);
        echo json_encode($cliente);        
    }
    

    
	
	
}