<?php

namespace App\Controller;

use App\Model\Cliente;
use App\Model\Agenda;

class ClienteController
{
    private $alerta;
    private $controller;

    public function __construct()
    {
        (new LoginController)->usuarioLongado();
        $this->controller =  get_class($this);
    }

    public function index()
    {
        $controller 	=  $this->controller;
        $clienteLista   = new Cliente();
        $qtdCliente     = $clienteLista->getAmountOfCliente();
        $clienteLista   = $clienteLista->listaTodos();

        $msgErro = $this->alerta;

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function desativado()
    {
        $controller 	=  $this->controller;
        $clienteLista   = new Cliente();
        $clienteLista   = $clienteLista->listaDesativados();

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/desativados.php';
        require APP . 'view/templates/footer.php';
    }

    public function corpo()
    {
        $x = 700;
        $y = 300;

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/corpo/homem.php';
        require APP . 'view/templates/footer.php';
    }

    public function novo()
    {
        $controller =  $this->controller;
        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        $controller =  $this->controller;
        $clienteLista = new Cliente();
        $clienteLista = $clienteLista->lista($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/editar.php';
        require APP . 'view/templates/footer.php';
    }

    public function ativarDesativar($boleano)
    {
        $id = $_POST['id'];
        $cliente = new Cliente();
        $cliente = $cliente->ativarDesativar($id,$boleano);
        echo json_decode($cliente);
    }

    public function permissaoAtualizar()
    {
        $id = $_POST['id'];
        $boleano = $_POST['boleano'];
        $cliente = new Cliente();
        $cliente = $cliente->permissaoAtualizar($id,$boleano);
        echo json_decode($cliente);
    }

    public function atualizar($id)
    {
			
            $imagem = $_FILES['cliFoto'];
            $cliente = new Cliente();
            $cliente = $cliente->lista($id);

            $imgBanco = $cliente[0]->cliFoto;
            $webBanco = $cliente[0]->cliWebcam;
            $handle = new \Verot\Upload\Upload($imagem);
            $imgInput = $handle->file_src_name;

            if (!empty($_POST['cliWebcam'])) { //Se tiver imagem input

                $img = "";
                $webcam = $_POST['cliWebcam'];

            } elseif(!empty($imgInput)  ) {
                
                $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/cliente');
                $handle = new \Verot\Upload\Upload($imagem);

                if ($handle->uploaded) {
                    $handle->image_resize           = true;
                    $handle->image_x                = 100;
                    $handle->image_ratio_y          = true;
                    $handle->file_safe_name         = false;
                    $handle->image_convert       	= 'jpg';
                    $handle->file_name_body_add     = '_mini';
                    $handle->process($diretorio_destino);
                }

                if ($handle->uploaded) {
                    $handle->image_resize           = true;
                    $handle->image_x                = 680;
                    $handle->image_ratio_y          = true;
                    $handle->image_convert          = 'jpg';
                    $handle->process($diretorio_destino);
                }

                $img = $handle->file_src_name;
                $webcam = "";
            }else{

                $img    = $imgBanco;
				$webcam = $webBanco;
			}

            $Cliente        = new Cliente();
            $inserirCliente = $Cliente->atualizar(	$id,
													$_POST['cliNome'],
													$_POST['cliCPF'],
													$_POST['cliRG'],
													$_POST['cliEstadoCivil'],
													$_POST['cliEmail'],
													$_POST['cliSenha'],
													$_POST['cliFone'],
													$_POST['cliCel'],
													$_POST['cliNasci'],
													$_POST['cliSexo'],
													$_POST['cliEnd'],
													$_POST['cliEndNumero'],
													$_POST['cliEndComp'],
													$_POST['cliCEP'],
													$_POST['cliEstado'],
													$_POST['cliCidade'],
													$img,
													$webcam,
													$_POST['cliComoConheceu'],
													$_POST['cliTratamento'],
													$_POST['cliAssinatura'],
													"1");
            echo json_encode($inserirCliente); 
    }

    public function inserir()
    {
		$imagem 		= $_FILES['cliFoto'];
		

            if (isset($imagem)) {

                $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/cliente');
                $handle = new \Verot\Upload\Upload($imagem);

                //$ext = pathinfo($imagem, PATHINFO_EXTENSION);
                //$nome = $imagem['name'];

                if ($handle->uploaded) {
                    $handle->image_resize         	= true;
                    $handle->image_x              	= 100;
                    $handle->image_ratio_y        	= true;
                    $handle->file_safe_name 		= false;
                    $handle->file_name_body_add 	= '_mini';
                    $handle->process($diretorio_destino);
                }

                if ($handle->uploaded) {
                    $handle->image_resize         = true;
                    $handle->image_x              = 680;
                    $handle->image_ratio_y        = true;
                    $handle->process($diretorio_destino);
                }

                $img = $handle->file_src_name;
            } else {
                $img = "";
            }

            $Cliente         = new Cliente();
            $inserirCliente = $Cliente->inserir(
                $_POST['cliNome'],
                $_POST['cliCPF'],
                $_POST['cliRG'],
                $_POST['cliEstadoCivil'],
                $_POST['cliEmail'],
                $_POST['cliSenha'],
                $_POST['cliFone'],
                $_POST['cliCel'],
                $_POST['cliNasci'],
                $_POST['cliSexo'],
                $_POST['cliProfissao'],
                $_POST['cliEnd'],
                $_POST['cliEndNumero'],
                $_POST['cliEndComp'],
                $_POST['cliCEP'],
                $_POST['cliEstado'],
                $_POST['cliCidade'],
                $img,
                $_POST['cliWebcam'],
                $_POST['cliComoConheceu'],
                $_POST['cliTratamento'],
                $_POST['cliAssinatura'],
                "1");
            echo json_encode($inserirCliente);
        
    }

    /* ############################ PERFIL CLIENTE ########################### */
    public function perfil($id)
    {
        $perfilCliente = new Cliente();
        $perfilLista = $perfilCliente->getPerfilCliente($id);

        $Agendamento 	= new Agenda();
		$agendaLista 	= $Agendamento->listaIdCliente($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/perfil.php';
        require APP . 'view/templates/footer.php';
    }
	
	public function depoimento()
    {
            $Cliente         	= new Cliente();
            $inserirDepo 		= $Cliente->inserirDepo($_POST['idCliente'],
														$_POST['depoimentoTexto'],
														$_POST['depoimentoStatus']);
            echo json_encode($inserirDepo);        
    }
	
	
}