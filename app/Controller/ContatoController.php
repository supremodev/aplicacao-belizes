<?php

namespace App\Controller;

use App\Model\Cliente;
use App\Controller\EmailController;

class ContatoController
{
     
    public function index()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");       

		require APP . 'view/site/head.php';
        require APP . 'view/templates/header-front.php';
        require APP . 'view/site/contato.php';
		require APP . 'view/templates/footer-front.php';
    }

}
