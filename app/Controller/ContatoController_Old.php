<?php

namespace App\ Controller;

use App\ Model\ Paginas;
use App\ Controller\ EmailController;

class ContatoController {
    
    public function index() {
        date_default_timezone_set( 'America/Sao_paulo' );
        $Data = date_create();
        $DataAtual = date_format( $Data, "Y-m-d" );

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/email/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function enviar() {
        
        $nome       =  $_POST['nome'];
        $email      =  $_POST['email'];
        $assunto    =  $_POST['assunto'];
        $anexo      =  $_FILES['anexo'];
        $msg        =  $_POST['msg'];
        
                
        $emailClass = new EmailController($nome, $email, $assunto, $anexo, $msg);        
        echo json_decode($emailClass->novoClienteCadastrado());


        }

}