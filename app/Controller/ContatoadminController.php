<?php

namespace App\Controller;

use App\Model\Cliente;
use App\Controller\EmailController;

class ContatoadminController
{
    public function __construct()
    {
        (new LoginController)->usuarioLongado();
    }
    
    public function index()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");       
		
        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/email/index.php';
        require APP . 'view/templates/footer.php';
		
    }

    public function formContato($idCliente)
    {
        $clienteLista   = new Cliente();
        $Cliente     = $clienteLista->lista($idCliente);

        require APP . 'view/email/form-contato.php';
    }

    public function enviar()
    {
        $emailClass = new EmailController("icaro",$_POST['email'],$_POST['assunto'],$_FILES['anexo'],$_POST['msg']);

        echo json_decode($emailClass->novoClienteCadastrado());

    }

}
