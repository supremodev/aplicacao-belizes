<?php
namespace App\Controller;

use App\Model\Depoimento;
use App\Controller\NivelController;
use App\Model\Usuario;
use App\Controller\LoginController;
use Verot\Upload;

class DepoimentoController
{

    public function __construct()
    {
        //(new LoginController)->usuarioLongado();
                        
        //$nivelAcesso = new NivelController();
        //$nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {

        $depoimento = new Depoimento();
        $depoimentoLista = $depoimento->listaTodos();

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/depoimento/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function analisar($id)
    {
        $depoimentoLista = new Depoimento();
        $depoimentoLista = $depoimentoLista->lista($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/depoimento/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        $depoimentoLista = new Depoimento();
        $depoimentoLista = $depoimentoLista->lista($id);

        require APP . 'view/depoimento/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/depoimento/editar.php';
        require APP . 'view/templates/footer.php';
    }

    public function deletar($id)
    {
        $depoimento = new Depoimento();
        $depoimento = $depoimento->deletar($id);
        echo json_decode($depoimento);
    }

    public function atualizarEstatus($id)
    {
        $depoimento = new Depoimento();
        $depoimento = $depoimento->atualizarEstatus($id, $_POST['depoimentoStatus']);
        echo json_decode($depoimento);
    }
    
    public function atualizar($id)
    {
        (new LoginController)->usuarioLongado();

        $imagem = $_FILES['imagem'];

        $depoimento = new Depoimento();
        $depoimento = $depoimento->lista($id);

        $imgBanco = $depoimento[0]->img;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;
    
        if (!empty($imgInput)) { //Se tiver imagem input
            
            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/depoimento');    
            $handle = new \Verot\Upload\Upload($imagem);
    
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 100;
                $handle->image_ratio_y        = true;
                $handle->file_safe_name = false;
                $handle->file_name_body_add = '_mini';
                $handle->process($diretorio_destino);
        
            }
        
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 680;
                $handle->image_ratio_y        = true;
                $handle->process($diretorio_destino);
        
            }

            $img = $handle->file_src_name;
            
        } else {
            $img = $imgBanco;
        } 
        
        $depoimento = new Depoimento();
        $msgModal = $depoimento->atualizar($id, $_POST["nome"], $_POST["profissao"], $_POST["depoimento"], $_POST["avaliacao"], $img, $_POST["alt"]);

        echo json_encode($msgModal);    

    }

    public function inserir()
    {
        $Depoimento 		= new Depoimento();
        $inserirDepoimento 	= $Depoimento->inserir(	$_POST['idCliente'], 
													$_POST['depoimentoTexto'], 
													true);
        echo json_encode($inserirDepoimento);
    }

    public function lixeira()
    {

        $DepoimentoLista = new Depoimento();
        $DepoimentoLista = $DepoimentoLista->listaTodos();

        require APP . 'view/depoimento/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/depoimento/lixeira.php';
        require APP . 'view/templates/footer.php';
    }

}

