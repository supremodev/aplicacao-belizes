<?php

namespace App\Controller;

use PHPMailer;
use SplFileInfo;

class EmailController
{
    private $template;
    private $email;
    private $assunto;
    private $nome;
    private $anexo;
    private $msg;

    public function __construct($nome,$email,$assunto,$anexo,$msg)
    {
        $this->nome=$nome;
        $this->email=$email;
        $this->assunto=$assunto;
        $this->anexo=$anexo;
        $this->msg=$msg;
    }

    public function recuperarSenha()
    {
        $phpmail = new PHPMailer();

        $phpmail->isSMTP(); // envia por SMTP

        $phpmail->SMTPDebug = 0;
        $phpmail->Debugoutput = 'html';

        $phpmail->Host = "smtp.gmail.com"; // SMTP servers         
        $phpmail->Port = 587; // Porta SMTP do GMAIL

        $phpmail->SMTPSecure = 'tls'; // Autenticação SMTP
        $phpmail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação   

        $phpmail->Username = "belizestimesistema@gmail.com"; // SMTP username         
        $phpmail->Password = "Sup*Dig*84"; // SMTP password

        $nomeRemetente = '=?UTF-8?B?'.base64_encode('Supremo').'?=';

        $phpmail->IsHTML(true);
       
        $phpmail->setFrom($this->email, $nomeRemetente); // E-mail do remetende enviado pelo method post  

        $phpmail->addAddress($this->email, $this->nome); // E-mail do destinatario/*  

        $phpmail->Subject = $this->assunto; // Assunto do remetende enviado pelo method post

        $body = str_replace(
            array(
              '[codigo]',
              '[nome]'
            ),
            array(
                $this->msg,
                $this->nome
            ),
            file_get_contents(APP . 'view/email/recuperar-senha.php')
        );
        //$body = file_get_contents(APP . 'view/email/recuperar-senha.php');

        $phpmail->msgHTML($body);

        if ($phpmail->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function novoClienteCadastrado()
    {
        $phpmail = new PHPMailer();

        $phpmail->isSMTP(); // envia por SMTP

        $phpmail->SMTPDebug = 0;
        $phpmail->Debugoutput = 'html';

        $phpmail->Host = "smtp.gmail.com"; // SMTP servers         
        $phpmail->Port = 587; // Porta SMTP do GMAIL

        $phpmail->SMTPSecure = 'tls'; // Autenticação SMTP
        $phpmail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação   

        $phpmail->Username = "smpsistema@gmail.com"; // SMTP username         
        $phpmail->Password = "SenacSMP"; // SMTP password

        $nomeRemetente = '=?UTF-8?B?'.base64_encode('Supremo').'?=';

        $ext = pathinfo($this->anexo['tmp_name'], PATHINFO_EXTENSION);

        $info = new SplFileInfo($this->anexo['file']);
        var_dump($info->getExtension());

        $filename = $this->anexo['file']['name'];

        $phpmail->AddAttachment($this->anexo['tmp_name'],'teste.'.$info->getExtension());

        //$phpmail->AddAttachment($this->anexo['file']['tmp_name'], $this->anexo['file']['name']);

        $phpmail->IsHTML(true);
       
        $phpmail->setFrom($this->email, $nomeRemetente); // E-mail do remetende enviado pelo method post  

        $phpmail->addAddress($this->email, $this->nome); // E-mail do destinatario/*  

        $phpmail->Subject = $this->assunto; // Assunto do remetende enviado pelo method post

        $body = str_replace(
            array(
              '[codigo]',
              '[nome]'
            ),
            array(
                $this->msg,
               $this->nome
            ),
            file_get_contents(APP . 'view/email/novo-cliente.php')
        );
        //$body = file_get_contents(APP . 'view/email/recuperar-senha.php');

        $phpmail->msgHTML($body);

        if ($phpmail->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function usuarioCadastrado()
    {
        $phpmail = new PHPMailer();

        $phpmail->isSMTP(); // envia por SMTP

        $phpmail->SMTPDebug = 0;
        $phpmail->Debugoutput = 'html';

        $phpmail->Host = "smtp.gmail.com"; // SMTP servers         
        $phpmail->Port = 587; // Porta SMTP do GMAIL

        $phpmail->SMTPSecure = 'tls'; // Autenticação SMTP
        $phpmail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação   

        $phpmail->Username = "goldtimegestao@gmail.com"; // SMTP username         
        $phpmail->Password = "Sup*Dig*84"; // SMTP password

        $nomeRemetente = '=?UTF-8?B?'.base64_encode('GoldTime Gestão').'?=';

        $phpmail->IsHTML(true);
       
        $phpmail->setFrom($this->email, $nomeRemetente); // E-mail do remetende enviado pelo method post  

        $phpmail->addAddress($this->email, $this->nome); // E-mail do destinatario/*  

        $phpmail->Subject = $this->assunto; // Assunto do remetende enviado pelo method post

        $body = str_replace(
            array(
              '[link]',
              '[senha]',
              '[nome]'
            ),
            array(
                $this->destino,
                $this->senha,
                $this->nome
            ),
            file_get_contents(APP . 'view/email/novo-usuario.php')
        );
        //$body = file_get_contents(APP . 'view/email/recuperar-senha.php');

        $phpmail->msgHTML($body);

        if ($phpmail->send()) {
            return true;//echo json_encode(1);
        } else {
            return false;//echo json_encode(0);
        }
    }

    public function envioContato()
    {
        $phpmail = new PHPMailer();

        $phpmail->isSMTP(); // envia por SMTP

        $phpmail->SMTPDebug = 0;
        $phpmail->Debugoutput = 'html';

        $phpmail->Host = "smtp.gmail.com"; // SMTP servers         
        $phpmail->Port = 587; // Porta SMTP do GMAIL

        $phpmail->SMTPSecure = 'tls'; // Autenticação SMTP
        $phpmail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação   

        $phpmail->Username = "supremodigitaldev@gmail.com"; // SMTP username         
        $phpmail->Password = "Sup*Dig*84"; // SMTP password

        $nomeRemetente = '=?UTF-8?B?'.base64_encode('Belizes').'?=';

        $phpmail->IsHTML(true);
       
        $phpmail->setFrom($this->email, $nomeRemetente); // E-mail do remetende enviado pelo method post  

        $phpmail->addAddress($this->email, $this->nome); // E-mail do destinatario/*  

        $phpmail->Subject = $this->assunto; // Assunto do remetende enviado pelo method post

        $body = str_replace(
            array(
              '[codigo]',
              '[nome]'
            ),
            array(
                $this->msg,
                $this->nome
            ),
            file_get_contents(APP . 'view/email/novo-cliente.php')
        );
        //$body = file_get_contents(APP . 'view/email/recuperar-senha.php');

        $phpmail->msgHTML($body);

        return $phpmail->send();
    }

}
