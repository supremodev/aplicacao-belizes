<?php
namespace App\Controller;

use App\Controller\EmailController;
use App\Model\Estoque;



class EstoqueController
{
	private $tipo;
	
    public function __construct()
    {
        (new LoginController)->usuarioLongado();
    }

    public function header()
    {
        if ($_SESSION['funcNivel'] == "Profissional") {
            require APP . 'view/templates/header-profissional.php';
        } elseif($_SESSION['funcNivel'] == "Atendimento"){
            require APP . 'view/templates/header-atendente.php';
        } elseif($_SESSION['funcNivel'] == "Admin"){
            require APP . 'view/templates/header.php';
        }   
    }

    public function index()
    {
        $EstoqueLista 		= new Estoque();
		$estoqueLista 		= $EstoqueLista->listaTodosEstoque();
        $qtdEstoque 		= $EstoqueLista->qtdOfEstoque();
        $qtdEstoqueML 		= $EstoqueLista->totalEstoqueML();
        $qtdEstoqueG 		= $EstoqueLista->totalEstoqueGrama();
		
        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/estoque/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function listafiltro($nome)
    {
        $Estoque 		= new Estoque();
		
        $estoqueLista 	= $Estoque->produtoNome($nome); 
        $qtdEstoque 		= $Estoque->qtdOfEstoque();
        $qtdEstoqueML 		= $Estoque->totalEstoqueML();
        $qtdEstoqueG 		= $Estoque->totalEstoqueGrama();
		
		require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/estoque/index.php';
        require APP . 'view/templates/footer.php';
	}

    public function novo()
    {
        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/estoque/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        $EstoqueLista 		= new Estoque();
        $estoqueLista 		= $EstoqueLista->lista($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/estoque/editar.php';
        require APP . 'view/templates/footer.php';

    }

    public function desativar()
    {
		$id 				= $_POST['id'];
        $Estoque			= new Estoque();
        $estoqueDesativar 	= $Estoque->desativar($id);
        echo json_decode($estoqueDesativar);
    }
	

    public function atualizar($id)
    {
		
        $Estoque 			= new Estoque();
        $estoqueLista 		= $Estoque->lista($id);
        $atualizarEstoque 	= $Estoque->atualizar(	$id,
													$_POST['estoqueProduto'], 
													$_POST['estoqueTipo'], 
													$_POST['estoqueQtd'], 
													$_POST['estoqueUsar15Dias'], 
													$_POST['estoqueMedida'], 
													$_POST['estoqueQtdUnidade'], 
													$_POST['estoqueVencimento'],
													"1");
        echo json_encode($atualizarEstoque);    
    }


    public function inserir()
    {		
		$Estoque 		= new Estoque();
        $inserirEstoque = $Estoque->inserir($_POST['estoqueProduto'],
											$_POST['estoqueTipo'],
											$_POST['estoqueQtd'], 
											$_POST['estoqueUsar15Dias'], 
											$_POST['estoqueMedida'], 
											$_POST['estoqueQtdUnidade'], 
											$_POST['estoqueVencimento'],
											"1");
        echo json_encode($inserirEstoque);
    }

}