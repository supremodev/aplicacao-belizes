<?php

namespace App\Controller;


use App\Model\Ficha;
use App\Model\Funcionario;
use App\Model\Servicos;
use App\Model\Pacote;
use Respect\Validation\Rules\Length;
use Verot\Upload;

class FichaController
{
/*	
#####################################################################
#############											#############
#############				FICHA GERAL					#############
#############											#############
#####################################################################
*/ 
    public function __construct()
    {
        //(new LoginController)->usuarioLongado();
    }

    public function header()
    {
        if ($_SESSION['funcNivel'] == "Profissional") {
            require APP . 'view/templates/header-profissional.php';
        } elseif($_SESSION['funcNivel'] == "Atendimento"){
            require APP . 'view/templates/header.php';
        } elseif($_SESSION['funcNivel'] == "Admin"){
            require APP . 'view/templates/header.php';
        } else {
            require APP . 'view/templates/header-cliente.php';
        }   
    }
	
/* ############ CARREGAR PÁGINA FICHA GERAL LISTANDO TODOS OS DADOS DO BANCO E A QUANTIDADE DE ITENS ################*/
    public function fichaGeral()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y/m/d");

		$FichaGeral 		= new Ficha();    
        $qtdFichaGeral 		= $FichaGeral->dadosQtdFichaGeral();

        if ($_SESSION['funcNivel'] == "Profissional") {

            $fichaGeral 		= $FichaGeral->agendaProfissional($_SESSION['idUsuario'], "$DataAtual");

        } elseif($_SESSION['funcNivel'] == "Atendimento"){

            $fichaGeral 		= $FichaGeral->todosFichaGeral();

        } elseif($_SESSION['funcNivel'] == "Admin"){
            $fichaGeral 		= $FichaGeral->todos();
        }

        //$fichaGeral 		= $FichaGeral->agendaProfissional(41, $DataAtual);
        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/ficha/fichaGeral.php';
        require APP . 'view/templates/footer.php';
    }

    public function carregarCorpo()
    {
        if ($_POST['sexo'] == "Masculino") {
            $x = 700;
            $y = 300;
            require APP . 'view/corpo/homem.php';
        } else {
            $x = 700;
            $y = 300;
            require APP . 'view/corpo/mulher.php';
        }
    }

    public function carregarFacial()
    {
        if ($_POST['sexo'] == "Masculino") {
            $x = 700;
            $y = 300;
            require APP . 'view/corpo/rostoHomem.php';
        } else {
            $x = 700;
            $y = 300;
            require APP . 'view/corpo/rostoMulher.php';
        }
    } 
	
/*  ############ CARREGAR PÁGINA CADASTRO DE UMA NOVA FICHA GERAL ################*/
    public function novoFichaGeral($id)
    {
        //$nivelAcesso = new NivelController();
        //$nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
        //$teste = $nivelAcesso;
    
        $CPF = $id;

        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/ficha/novoFichaGeral.php';
        require APP . 'view/templates/footer.php';
        echo "<script>$(document).ready(function () {
            ajax();});</script>";
    }

    public function consultaCpfFichaGeral()
    {
        $cpf = $_POST['cpf'];
        $FichaGeral 		= new Ficha();
        $fichaGeral 		= $FichaGeral->consultaCpfFichaGeral($cpf);

        echo json_encode($fichaGeral);  
    }

    public function consultaCpfNecessario()
    {
        $cpf = $_POST['cpf'];
        $FichaGeral 		= new Ficha();
        $fichaGeral 		= $FichaGeral->consultaCpfNecessario($cpf);

        echo json_encode($fichaGeral);  
    }
	
/* ############ CARREGAR PÁGINA EDITAR OS DADOS DA FICHA GERAL E CARREGANDO SO DADOS DO BANCO COM BASE NO ID ################*/
    public function editarFichaGeral($id)
    {
        $FichaGeral = new Ficha();
        $FichaGeralLista = $FichaGeral->listaFichaGeral($id);
        $corpoMarcacao = $FichaGeral->listaMarcacaoCorpo($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/ficha/editarFichaGeral.php';
        require APP . 'view/templates/footer.php';
    }
	
/* ############ DESATIVAR (ALTERAR O STATUS) FICHA GERAL COM BASE NO ID ################*/
    public function desativarFichaGeral($id)
    {
        $FichaGeral = new Ficha();
        $desativarFichaGeral = $FichaGeral->desativarFichaGeral($id);
        echo json_decode($desativarFichaGeral);
    }

/* ############ ATUALIZAR FICHA GERAL COM BASE NO ID ################*/
    public function atualizarFichaGeral($id)
    {
        $imagem = $_FILES['imagem'];

        $destaque = new Funcionario();
        $destaque = $destaque->Funcionario($id);

        $imgBanco = $destaque[0]->img;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;
    
        if (!empty($imgInput)) { //Se tiver imagem input
            
            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/Funcionario');    
            $handle = new \Verot\Upload\Upload($imagem);
    
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 100;
                $handle->image_ratio_y        = true;
                $handle->file_safe_name = false;
                $handle->file_name_body_add = '_mini';
                $handle->process($diretorio_destino);
        
            }
        
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 300;
                $handle->image_ratio_y        = true;
                $handle->process($diretorio_destino);
        
            }

            $img = $handle->file_src_name;
            
        } else {
            $img = $imgBanco;
        } 

        $servicos = serialize($_POST['servico']);
        
        $destaque = new Funcionario();
        $msgModal = $destaque->atualizar($id, $_POST["nome"], $_POST["email"], $img, $servicos);

        echo json_encode($msgModal);    
    }

/*  ############ INSERIR FICHA GERAL LISTANDO TODOS OS DADOS DO BANCO E A QUANTIDADE DE ITENS ################*/
    public function inserirAvaliacaoGeral()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"d/m/Y");
        $HoraAtual = date_format($Data,"H:i:s");

        $Ficha 					= new Ficha();
        $inserirAvaliacaoGeral 	= $Ficha->addFichaGeral(
                                                    $_POST['idCliente'],												 
													$_POST['fichaHistoricoDeDermatiteOuCancerDePele'],
													$_POST['fichaEmQualLocalDoCorpo'],
													$_POST['fichaFazQuantoTempo'],
													$_POST['fichaFoiLiberadoPeloMedicoParaTratamentoEstetico'],
													$_POST['fichaHipertireoidismo'],
													$_POST['fichaTratamentoHipertireoidismo'], 
													$_POST['fichaHipotireoidismo'],
													$_POST['fichaTratamentoHipotireoidismo'], 
													$_POST['fichaDiabetes'], 
													$_POST['fichaTratamentoDiabetes'], 
													$_POST['fichaHipertensao'], 
													$_POST['fichaTratamentoHipertensao'],
													$_POST['fichaEplepsia'], 
													$_POST['fichaTratamentoEplepsia'], 
													$_POST['fichaPsoriase'], 
													$_POST['fichaTratamentoPsoriase'], 
													$_POST['fichaOutrasDoencas'],
													$_POST['fichaTemImplantesMetalicos'], 
													$_POST['fichaLocalTemImplantesMetalicos'], 
													$_POST['fichaPortadorDeMarcapasso'], 
													$_POST['fichaQuantoTempoPortadorDeMarcapasso'], 
													$_POST['fichaPortadorDeHIV'], 
													$_POST['fichaTratamentoPortadorDeHIV'], 
													$_POST['fichaPortadorDeHepatite'], 
													$_POST['fichaQualPortadorDeHepatite'], 
													$_POST['fichaAlteracaoHormonal'], 
													$_POST['fichaQualAlteracaoHormonal'], 
													$_POST['fichaAlergiaAlgumAlimento'], 
													$_POST['fichaQualAlergiaAlgumAlimento'],
													$_POST['fichaAlergiaAlgumMedicamento'], 
													$_POST['fichaQualAlergiaAlgumMedicamento'], 
													$_POST['fichaFazUsoDeMedicamento'], 
													$_POST['fichaQualFazUsoDeMedicamento'], 
													$_POST['fichaPresencaDeQueloides'], 
													$_POST['fichaQueLocalPresencaDeQueloides'], 
													$_POST['fichaGestante'], 
													$_POST['fichaQuantoTempoGestante'], 
													$_POST['fichaFilhos'], 
													$_POST['fichaQuantosFilhosIdade'],
													$_POST['fichaFezAlgumaCirurgiaPlastica'], 
													$_POST['fichaQualFezAlgumaCirurgiaPlastica'], 
													$_POST['fichaRealizouProcedimentosEsteticosAnteriores'], 
													$_POST['fichaQualRealizouProcedimentosEsteticosAnteriores'], 
													$_POST['fichaFazUsoDeMaquiagem'], 
													$_POST['fichaQualFazUsoDeMaquiagem'], 
													$_POST['fichaFazAtividadeFisica'], 
													$_POST['fichaQuantasVezesPorSemanaAtividadeFisica'], 
													$_POST['fichaConsumoAguaIngereQuantosCoposDia'],
													$_POST['fichaIngereBebidaAlcoolica'], 
													$_POST['fichaQuantasDosesPorSemanaBebidaAlcoolica'], 
													$_POST['fichaIngereBebidaGaseificada'], 
													$_POST['fichaQuantasDosesPorSemanaBebidaGaseificada'], 
													$_POST['fichaJaFumouOuFuma'], 
													$_POST['fichaQuantosCigarrosPorDia'], 
													$_POST['fichaEstresse'], 
													$_POST['fichaSono'], 
													$_POST['fichaQuantasHorasPorNoiteSono'],
													$_POST['fichaPosicaoQuePermanecePorMaisTempo'], 
													$_POST['fichaAlimentacao'], 
													$_POST['fichaFazDietaQualQuantoTempo'], 
													$_POST['fichaOqueTeIncomoda'], 
													$_POST['fichaQualSeuTime'], 
													$_POST['fichaOqueVcMudariaEmVc'], 
													$_POST['fichaQualSeuEstiloMusica'],
													$DataAtual, 
													$HoraAtual, 
													0);
        echo json_encode($inserirAvaliacaoGeral);
    }

    public function atualizarAvaliacaoGeral($id)
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"d/m/Y");
        $HoraAtual = date_format($Data,"H:i:s");
        
        
        $Ficha 					= new Ficha();
        $atualizarAvaliacaoGeral 	= $Ficha->atualizarFichaGeral(												 
													$_POST['fichaHistoricoDeDermatiteOuCancerDePele'],
													$_POST['fichaEmQualLocalDoCorpo'],
													$_POST['fichaFazQuantoTempo'],
													$_POST['fichaFoiLiberadoPeloMedicoParaTratamentoEstetico'],
													$_POST['fichaHipertireoidismo'],
													$_POST['fichaTratamentoHipertireoidismo'], 
													$_POST['fichaHipotireoidismo'],
													$_POST['fichaTratamentoHipotireoidismo'], 
													$_POST['fichaDiabetes'], 
													$_POST['fichaTratamentoDiabetes'], 
													$_POST['fichaHipertensao'], 
													$_POST['fichaTratamentoHipertensao'],
													$_POST['fichaEplepsia'], 
													$_POST['fichaTratamentoEplepsia'], 
													$_POST['fichaPsoriase'], 
													$_POST['fichaTratamentoPsoriase'], 
													$_POST['fichaOutrasDoencas'],
													$_POST['fichaTemImplantesMetalicos'], 
													$_POST['fichaLocalTemImplantesMetalicos'], 
													$_POST['fichaPortadorDeMarcapasso'], 
													$_POST['fichaQuantoTempoPortadorDeMarcapasso'], 
													$_POST['fichaPortadorDeHIV'], 
													$_POST['fichaTratamentoPortadorDeHIV'], 
													$_POST['fichaPortadorDeHepatite'], 
													$_POST['fichaQualPortadorDeHepatite'], 
													$_POST['fichaAlteracaoHormonal'], 
													$_POST['fichaQualAlteracaoHormonal'], 
													$_POST['fichaAlergiaAlgumAlimento'], 
													$_POST['fichaQualAlergiaAlgumAlimento'],
													$_POST['fichaAlergiaAlgumMedicamento'], 
													$_POST['fichaQualAlergiaAlgumMedicamento'], 
													$_POST['fichaFazUsoDeMedicamento'], 
													$_POST['fichaQualFazUsoDeMedicamento'], 
													$_POST['fichaPresencaDeQueloides'], 
													$_POST['fichaQueLocalPresencaDeQueloides'], 
													$_POST['fichaGestante'], 
													$_POST['fichaQuantoTempoGestante'], 
													$_POST['fichaFilhos'], 
													$_POST['fichaQuantosFilhosIdade'],
													$_POST['fichaFezAlgumaCirurgiaPlastica'], 
													$_POST['fichaQualFezAlgumaCirurgiaPlastica'], 
													$_POST['fichaRealizouProcedimentosEsteticosAnteriores'], 
													$_POST['fichaQualRealizouProcedimentosEsteticosAnteriores'], 
													$_POST['fichaFazUsoDeMaquiagem'], 
													$_POST['fichaQualFazUsoDeMaquiagem'], 
													$_POST['fichaFazAtividadeFisica'], 
													$_POST['fichaQuantasVezesPorSemanaAtividadeFisica'], 
													$_POST['fichaConsumoAguaIngereQuantosCoposDia'],
													$_POST['fichaIngereBebidaAlcoolica'], 
													$_POST['fichaQuantasDosesPorSemanaBebidaAlcoolica'], 
													$_POST['fichaIngereBebidaGaseificada'], 
													$_POST['fichaQuantasDosesPorSemanaBebidaGaseificada'], 
													$_POST['fichaJaFumouOuFuma'], 
													$_POST['fichaQuantosCigarrosPorDia'], 
													$_POST['fichaEstresse'], 
													$_POST['fichaSono'], 
													$_POST['fichaQuantasHorasPorNoiteSono'],
													$_POST['fichaPosicaoQuePermanecePorMaisTempo'], 
													$_POST['fichaAlimentacao'], 
													$_POST['fichaOqueTeIncomoda'], 
													$_POST['fichaQualSeuTime'], 
													$_POST['fichaOqueVcMudariaEmVc'], 
													$_POST['fichaQualSeuEstiloMusica'], 
													$DataAtual, 
                                                    $HoraAtual,
                                                    1,
                                                    $id);
        echo json_encode($atualizarAvaliacaoGeral);
    }

    /* ############ INSERIR FICHA GERAL LISTANDO TODOS OS DADOS DO BANCO E A QUANTIDADE DE ITENS ################*/
    public function inserirAvaliacaoFacial()
    {
        
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"d/m/Y");
        $HoraAtual = date_format($Data,"H:i:s");
        
        if(isset($_POST['fichaFacilFrequencia'])){
            $fichaFacilFrequencia = $_POST['fichaFacilFrequencia'];
        } else {
            $fichaFacilFrequencia = 0;
        }

        if(isset($_POST['fichaFacilNaoUsa'])){
            $fichaFacilNaoUsa = $_POST['fichaFacilNaoUsa'];
        } else {
            $fichaFacilNaoUsa = 0;
        }

        if(isset($_POST['fichaFacilUsoSomenteQuandoExpostoAoSol'])){
            $fichaFacilUsoSomenteQuandoExpostoAoSol = $_POST['fichaFacilUsoSomenteQuandoExpostoAoSol'];
        } else {
            $fichaFacilUsoSomenteQuandoExpostoAoSol = 0;
        }

        if(isset($_POST['fichaFacilFrequenciaProtecaoSolar'])){
            $fichaFacilFrequenciaProtecaoSolar = $_POST['fichaFacilFrequenciaProtecaoSolar'];
        } else {
            $fichaFacilFrequenciaProtecaoSolar = 0;
        }

        if(isset($_POST['fichaFacilProtecaoSolarUsoEsporadico'])){
            $fichaFacilProtecaoSolarUsoEsporadico = $_POST['fichaFacilProtecaoSolarUsoEsporadico'];
        } else {
            $fichaFacilProtecaoSolarUsoEsporadico = 0;
        }

        if(isset($_POST['fichaFacilProtecaoSolarUsoDiario'])){
            $fichaFacilProtecaoSolarUsoDiario = $_POST['fichaFacilProtecaoSolarUsoDiario'];
        } else {
            $fichaFacilProtecaoSolarUsoDiario = 0;
        }

        $Ficha 					= new Ficha();
        $inserirAvaliacaoGeral 	= $Ficha->addAvaliacaoFacial(	
                                                    $_POST['idFichaGeral'], 
													$_SESSION['idUsuario'],
                                                    $_POST['fichaFacilLinhasDeExpressao'], 
                                                    $_POST['fichaFacilCicatrizesDeAcneCicatrizAtrofica'], 
                                                    $_POST['fichaFacilEnvelhecimentoPrecoce'], 
                                                    $_POST['fichaFacilFlacidez'], 
                                                    $_POST['fichaFacilDesidratacao'], 
                                                    $_POST['fichaFacilManchas'], 
                                                    $_POST['fichaFacilOutra'], 
                                                    $_POST['fichaFacilFototipoDePele'], 
                                                    $_POST['fichaFacilBiotipoCutaneo'], 
                                                    $_POST['fichaFacilHidratadaCutanea'], 
                                                    $_POST['fichaFacilComedao'], 
                                                    $_POST['fichaFacilMilium'], 
                                                    $_POST['fichaFacilPapula'], 
                                                    $_POST['fichaFacilPustula'], 
                                                    $_POST['fichaFacilMicrocisto'], 
                                                    $_POST['fichaFacilNodulo'], 
                                                    $_POST['fichaFacilQueratoseActinica'], 
                                                    $_POST['fichaFacilDermatosePapulosaNigra'], 
                                                    $_POST['fichaFacilObsDesiquilibrioDaPele'], 
                                                    $_POST['fichaFacilSequelaDeAcne'], 
                                                    $_POST['fichaFacilRegiaoSequelaDeAcne'], 
                                                    $_POST['fichaFacilExposiccaoSolar'], 
                                                    $fichaFacilFrequencia, 
                                                    $fichaFacilNaoUsa, 
                                                    $fichaFacilUsoSomenteQuandoExpostoAoSol, 
                                                    $fichaFacilFrequenciaProtecaoSolar, 
                                                    $fichaFacilProtecaoSolarUsoEsporadico, 
                                                    $fichaFacilProtecaoSolarUsoDiario, 
                                                    $_POST['fichaFacilProtecaoSolarFrequencia'], 
                                                    $_POST['fichaFacilHistorico'], 
                                                    $_POST['fichaFacilHipercromia'],  
                                                    $_POST['fichaFacilRegiaoHipercromia'], 
                                                    $_POST['fichaFacilPeleSensivel'],  
                                                    $_POST['fichaFacilObsTratamentoIndicado'],
                                                    $_POST['fichaFacilQtdSessoes'],
                                                    $DataAtual,
                                                    $HoraAtual,
                                                    1);
        echo json_encode($inserirAvaliacaoGeral);
    }

    public function inserirAvaliacaoCorporal()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"d/m/Y");
        $HoraAtual = date_format($Data,"H:i:s");
        
        $Ficha 					= new Ficha();
        $inserirAvaliacaoGeral 	= $Ficha->addFichaCorporal(	$_POST['idFichaGeral'],	
															$_SESSION['idUsuario'],
															$_POST['fichaCorporalHipolipodistrofia'], 
															$_POST['fichaCorporalGrau'], 
															$_POST['fichaCorporalTemperatura'], 
															$_POST['fichaCorporalPalpacao'], 
															$_POST['fichaCorporalTemperaturaLocalizacao'], 
															$_POST['fichaCorporalTemperaturaColoracaoDoTecido'], 
															$_POST['fichaCorporalEdemaTesteDeCacifo'], 
															$_POST['fichaCorporalEdemaTesteDeDigitoPressao'], 
															$_POST['fichaCorporalEdemaSensacaoDePesoCansacoEmMMII'], 
															$_POST['fichaCorporalEdemaObservacoes'], 
															$_POST['fichaCorporalLipodistrofiaGorduraCompactaFlacida'], 
															$_POST['fichaCorporalLipodistrofiaDistribuicaoGordura'], 
															$_POST['fichaCorporalLipodistrofiaLocalizacao'], 
															$_POST['fichaCorporalBiotipo'], 
															$_POST['fichaCorporalBiotipoPeso'], 
															$_POST['fichaCorporalBiotipoAltura'], 
															$_POST['fichaCorporalBiotipoIMC'], 
															$_POST['fichaCorporalBiotipoPesoMin'], 
															$_POST['fichaCorporalBiotipoPesoMax'], 
															$_POST['fichaCorporalBiotipoIMCSituacao'], 
															$_POST['fichaCorporalBiotipoObservacoes'], 
															$_POST['fichaCorporalFlacidezTissular'], 
															$_POST['fichaCorporalFlacidezLocalizacaoDaFlacidezTissular'], 
															$_POST['fichaCorporalFlacidezMuscular'], 
															$_POST['fichaCorporalFlacidezLocalizacaoDaFlacidezMuscular'], 
															$_POST['fichaCorporalEstriasCor'], 
															$_POST['fichaCorporalEstriasLargura'], 
															$_POST['fichaCorporalEstriasQuantidade'], 
															$_POST['fichaCorporalEstriasRegiao'], 
															$_POST['fichaCorporalAlteracoesPosturaisOmbrosAnteriorizacao'], 
															$_POST['fichaCorporalAlteracoesPosturaisOmbrosHiperextensao'], 
															$_POST['fichaCorporalAlteracoesPosturaisColunaEscolioseEmC'], 
															$_POST['ColunaEscolioseEmS'], 
															$_POST['ColunaHipercifose'], 
															$_POST['ColunaHiperlordose'], 
															$_POST['ColunaRetificacaoDaCifose'], 
															$_POST['ColunaRetificacaoDaLordose'], 
															$_POST['QuadrilAntiversao'], 
															$_POST['QuadrilRetroversao'], 
															$_POST['fichaCorporalAlteracoesPosturaisJoelhosGenovalgo'], 
															$_POST['JoelhosGenovaro'], 
															$_POST['JoelhosHiperextensao'], 
                                                            $_POST['fichaCorporalAlteracoesPosturaisOutros'],
                                                            $_POST['fichaCorporalObsTratamentoIndicado'],
                                                            $_POST['fichaCorporalQtdSessoes'],
															$DataAtual, 
															$HoraAtual, 
															1);
        echo json_encode($inserirAvaliacaoGeral);
    }

    public function inserirPerimetria()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data 				= date_create();
        $DataAtual 			= date_format($Data,"Y-m-d");
        
        $HoraAtual = date_format($Data,"H:i:s");
        
        $Ficha 					= new Ficha();
        $inserirAvaliacaoGeral 	= $Ficha->addFichainserirPerimetria(	$_POST['idFichaCorporal'],	
                                                                        $_POST['fichaCorporalPerimetriaBracoD'], 
                                                                        $_POST['fichaCorporalPerimetriaBracoE'], 
                                                                        $_POST['fichaCorporalPerimetriaAbdSupCintura'], 
                                                                        $_POST['fichaCorporalPerimetriaAbdInfQuadril'], 
                                                                        $_POST['fichaCorporalPerimetriaCoxaSupD'], 
                                                                        $_POST['fichaCorporalPerimetriaCoxaSupE'], 
                                                                        $_POST['fichaCorporalPerimetriaCoxaInfD'], 
                                                                        $_POST['fichaCorporalPerimetriaCoxaInfE'], 
                                                                        $_POST['fichaCorporalPerimetriaJoelhoD'], 
                                                                        $_POST['fichaCorporalPerimetriaJoelhoE'],
                                                                        strval($DataAtual));
        echo json_encode($inserirAvaliacaoGeral);
    }

    public function inserirAdipometria()
    {
        $Ficha 					= new Ficha();

        date_default_timezone_set('America/Sao_paulo');
        $Data 				= date_create();
        $DataAtual 			= date_format($Data,"Y-m-d");
        
        $HoraAtual = date_format($Data,"H:i:s");

        //var_dump($_POST['idFichaGeral']);
        //var_dump($_POST['fichaCorporalAdipometriaRegiaoCorpo']);
        //var_dump($_POST['fichaCorporalAdipometriaPregaCM']);

        $regiao = $_POST['fichaCorporalAdipometriaRegiaoCorpo'];
        $prega = $_POST['fichaCorporalAdipometriaPregaCM'];

        foreach ($regiao as $key =>$linhaRegiao) { 
                $inserirAvaliacaoGeral 	= $Ficha->addFichainserirAdipometria(   $_POST['idFichaCorporal'],
                                                                                $linhaRegiao,
                                                                                $prega[$key],
                                                                                $DataAtual);
        }


        echo json_encode($inserirAvaliacaoGeral);
        /*$Ficha 					= new Ficha();
        $inserirAvaliacaoGeral 	= $Ficha->addFichainserirAdipometria(   6,
                                                                        $_POST['fichaCorporalAdipometriaRegiaoCorpo'],
												                        $_POST['fichaCorporalAdipometriaPregaCM'],
                                                                        strval($DataAtual));
        echo json_encode($inserirAvaliacaoGeral);*/
    }

    public function inseririmg()
    {        
        $imagem = $_FILES['foto'];

        if (isset($imagem)) {

            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/corporal');
            $handle = new \Verot\Upload\Upload($imagem);


            if ($handle->uploaded) {
                $handle->image_resize             	= true;
                $handle->image_x                  	= 100;
                $handle->image_ratio_y            	= true;
                $handle->file_safe_name         	= false;
                $handle->file_name_body_add     	= '_mini';
                $handle->process($diretorio_destino);
            }

            if ($handle->uploaded) {
                $handle->image_resize         		= true;
                $handle->image_x              		= 680;
                $handle->image_ratio_y        		= true;
                $handle->process($diretorio_destino);
            }

            $img = $handle->file_src_name;
        } else {
            $img = "sem-foto.jpg";
        }
		
		$Servicos 			= new Ficha();
        $inserirServicos 	= $Servicos->inseririmg($_POST['idFichaCorporal'],$img);
        echo json_encode($inserirServicos);
    }

    public function inserirMarcacaoCorpotal()
    {
        $Ficha 	= new Ficha();
        $marcacao = $_POST['marcacao'];
           
        $data = json_decode($marcacao, true);

        for ($i=0; $i < count($data); $i++) { 
            $inserirAvaliacaoGeral 	= $Ficha->inserirMarcacaoCorporal($_POST['idFichaCorporal'], $data[$i]['Descricao'],$data[$i]['x'], $data[$i]['y']);
        }

        echo json_encode($inserirAvaliacaoGeral);
           
    }

    public function inserirMarcacaoFacial()
    {
        $Ficha 	= new Ficha();
        $marcacao = $_POST['marcacao'];
           
        $data = json_decode($marcacao, true);

        for ($i=0; $i < count($data); $i++) { 
            $inserir 	= $Ficha->inserirMarcacaoFacial($_POST['idFichaFacial'], $data[$i]['Descricao'],$data[$i]['x'], $data[$i]['y']);
        }
        echo json_encode($inserir);
    }
/* 
#####################################################################
#############											#############
#############			FICHA ANAMNESE COVID-19			#############
#############											#############
#####################################################################
*/
	
/* ############ CARREGAR PÁGINA FICHA ANAMNESE COVID-19 LISTANDO TODOS OS DADOS DO BANCO E A QUANTIDADE DE ITENS ################*/	
public function fichaAnamneseCovid19()
    {
		$FichaAnamneseCovid19 			= new Ficha();
        $fichaAnamneseCovid19 			= $FichaAnamneseCovid19->todosFichaAnamneseCovid19();
        $qtdFichaAnamneseCovid19 		= $FichaAnamneseCovid19->qtdFichaAnamneseCovid19();
		
        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/ficha/fichaAnamneseCovid19.php';
        require APP . 'view/templates/footer.php';
    }
/* ############ CARREGAR PÁGINA CADASTRO FICHA ANAMNESE COVID-19 ################*/
    public function novoFichaAnamneseCovid19()
    {
        //$nivelAcesso = new NivelController();
        //$nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
        //$teste = $nivelAcesso;

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/ficha/novoFichaAnamneseCovid19.php';
        require APP . 'view/templates/footer.php';
        
    }
	
/* ############ CARREGAR PÁGINA EDITAR FICHA ANAMNESE COVID-19 - CARREGANDO TODOS OS DADOS DO BANCO ################*/
    public function editarFichaAnamneseCovid19($id)
    {
        $FichaAnamneseCovid19 		= new Ficha();
        $FichaAnamneseCovid19Lista 	= $FichaAnamneseCovid19->listaFichaAnamneseCovid19($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/ficha/editarFichaAnamneseCovid19.php';
        require APP . 'view/templates/footer.php';
    }
	
/* ############ DESATIVAR PÁGINA FICHA ANAMNESE COVID-19 COM BASE NO ID ################*/
    public function desativarAnamneseCovid19($id)
    {
        $FichaAnamneseCovid19 			= new Ficha();
        $desativarFichaAnamneseCovid19 	= $FichaAnamneseCovid19->desativarAnamneseCovid19($id);
        echo json_decode($desativarFichaAnamneseCovid19);
    }
	
	public function inserirFichaAnamneseCovid19()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"d/m/Y");
        $HoraAtual = date_format($Data,"H:i:s");
		
		$Ficha 							= new Ficha();
        $inserirFichaAnamneseCovid19 	= $Ficha->addAnamneseCovid19(	$_POST['idCliente'], 
																		$_POST['fichaAnamneseCovid19JaFezTesteCovid19'], 
																		$_POST['fichaAnamneseCovid19QualTesteCovid19'], 
																		$_POST['fichaAnamneseCovid19Resultado'], 
																		$_POST['fichaAnamneseCovid19ObsTesteCovid19'], 
																		$_POST['fichaAnamneseCovid19QuantoTempoFazQueFezTeste'], 
																		$_POST['fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo'], 
																		$_POST['fichaAnamneseCovid19QuantoTempoFazContatoComPessoas'], 
																		$_POST['fichaAnamneseCovid19FebreQuantoTempoFaz'], 
																		$_POST['fichaAnamneseCovid19TosseQuantoTempoFaz'], 
																		$_POST['fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz'], 
																		$_POST['fichaAnamneseCovid19FaltaDeArQuantoTempoFaz'], 
																		$_POST['fichaAnamneseCovid19DiarreiaQuantoTempoFaz'], 
																		$_POST['fichaAnamneseCovid19CorizaQuantoTempoFaz'], 
																		$_POST['fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz'], 
																		$_POST['fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz'], 
																		$_POST['fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz'], 
																		$_POST['fichaAnamneseCovid19CansacoQuantoTempoFaz'], 
																		$_POST['fichaAnamneseCovid19eFumante'], 
																		$_POST['fichaAnamneseCovid19QuantoTempoEFumante'], 
																		$_POST['fichaAnamneseCovid19TemAlgumVicio'], 
																		$_POST['fichaAnamneseCovid19QualAlgumVicio'], 
																		$_POST['fichaAnamneseCovid19TemAlgumProblemaDeSaude'], 
																		$_POST['fichaAnamneseCovid19QualProblemaDeSaude'], 
																		$_POST['fichaAnamneseCovid19Diabete'], 
																		$_POST['fichaAnamneseCovid19DoencaCardiaca'], 
																		$_POST['fichaAnamneseCovid19ProblemaRenal'], 
																		$_POST['fichaAnamneseCovid19DoencaRespiratotiaCronica'], 
																		$_POST['fichaAnamneseCovid19RiniteSinusite'], 
																		$_POST['fichaAnamneseCovid19PressaoAlta'], 
																		$_POST['fichaAnamneseCovid19Asma'], 
																		$_POST['fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament'], 
																		$_POST['fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais'], 
																		$_POST['fichaAnamneseCovid19MoraComFamiliares'], 
																		$_POST['fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude'], 
																		$_POST['fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude'], 
																		$DataAtual, 
																		$HoraAtual,  
																		1);
        echo json_encode($inserirFichaAnamneseCovid19);
    }

    public function inserirMarcacaoCorporalAnamnese()
    {
        $Ficha 	= new Ficha();
        $marcacao = $_POST['marcacao'];
           
        $data = json_decode($marcacao, true);

        for ($i=0; $i < count($data); $i++) { 
            $inserircorpo 	= $Ficha->inserirMarcacaoCorporalAnamnese($_POST['idFichaGeral'], $data[$i]['Descricao'],$data[$i]['x'], $data[$i]['y']);
        }  

        echo json_encode($inserircorpo);
    }
    
    public function atualizarFichaAnamneseCovid19($idCliente)
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"d/m/Y");
        $HoraAtual = date_format($Data,"H:i:s");
		
		$Ficha 							= new Ficha();
        $inserirFichaAnamneseCovid19 	= $Ficha->atualizarFichaAnamneseCovid19($idCliente,
																				$_POST['fichaAnamneseCovid19JaFezTesteCovid19'], 
																				$_POST['fichaAnamneseCovid19QualTesteCovid19'], 
																				$_POST['fichaAnamneseCovid19Resultado'], 
																				$_POST['fichaAnamneseCovid19QuantoTempoFazQueFezTeste'], 
																				$_POST['fichaAnamneseCovid19ObsTesteCovid19'], 
																				$_POST['fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo'], 
																				$_POST['fichaAnamneseCovid19QuantoTempoFazContatoComPessoas'], 
																				$_POST['fichaAnamneseCovid19FebreQuantoTempoFaz'], 
																				$_POST['fichaAnamneseCovid19TosseQuantoTempoFaz'], 
																				$_POST['fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz'], 
																				$_POST['fichaAnamneseCovid19FaltaDeArQuantoTempoFaz'], 
																				$_POST['fichaAnamneseCovid19DiarreiaQuantoTempoFaz'], 
																				$_POST['fichaAnamneseCovid19CorizaQuantoTempoFaz'], 
																				$_POST['fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz'],  
																				$_POST['fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz'],  
																				$_POST['fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz'],  
																				$_POST['fichaAnamneseCovid19CansacoQuantoTempoFaz'], 
																				$_POST['fichaAnamneseCovid19eFumante'], 
																				$_POST['fichaAnamneseCovid19QuantoTempoEFumante'], 
																				$_POST['fichaAnamneseCovid19TemAlgumVicio'], 
																				$_POST['fichaAnamneseCovid19QualAlgumVicio'], 
																				$_POST['fichaAnamneseCovid19TemAlgumProblemaDeSaude'], 
																				$_POST['fichaAnamneseCovid19QualProblemaDeSaude'], 
																				$_POST['fichaAnamneseCovid19Diabete'], 
																				$_POST['fichaAnamneseCovid19DoencaCardiaca'], 
																				$_POST['fichaAnamneseCovid19ProblemaRenal'], 
																				$_POST['fichaAnamneseCovid19DoencaRespiratotiaCronica'], 
																				$_POST['fichaAnamneseCovid19RiniteSinusite'], 
																				$_POST['fichaAnamneseCovid19PressaoAlta'], 
																				$_POST['fichaAnamneseCovid19Asma'], 
																				$_POST['fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament'], 
																				$_POST['fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais'], 
																				$_POST['fichaAnamneseCovid19MoraComFamiliares'], 
																				$_POST['fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude'], 
																				$_POST['fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude'],        
																				$DataAtual, 
																				$HoraAtual, 
																				1);
        echo json_encode($inserirFichaAnamneseCovid19);
    }
	
	
	
	
/* 
#####################################################################
#############											#############
#############			FICHA ANAMNESE Criolipolise		#############
#############											#############
#####################################################################
*/
	
/* ############ CARREGAR PÁGINA FICHA ANAMNESE Criolipolise LISTANDO TODOS OS DADOS DO BANCO E A QUANTIDADE DE ITENS ################*/	
    public function fichaAnamneseCriolipolise()
    {
		
		
		
		
		$FichaAnamneseCriolipolise 			= new Ficha();
        //$fichaGeralAnamneseCriolipolise 	= $FichaAnamneseCriolipolise->todosFichaAnamneseCriolipolise();
        $qtdFichaAnamneseCriolipolise 		= $FichaAnamneseCriolipolise->qtdFichaAnamneseCriolipolise();
		
        
		
		date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y/m/d");

		$FichaGeral 		= new Ficha();    
        $qtdFichaGeral 		= $FichaGeral->dadosQtdFichaGeral();

        if ($_SESSION['funcNivel'] == "Profissional") {

            $fichaGeral 		= $FichaGeral->agendaProfissional($_SESSION['idUsuario'], "$DataAtual");

        } elseif($_SESSION['funcNivel'] == "Atendimento"){

            $fichaGeral 		= $FichaGeral->todosFichaGeral();

        } elseif($_SESSION['funcNivel'] == "Admin"){
            $fichaGeral 		= $FichaGeral->todos();
        }
		
		
		
		
		
		require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/ficha/fichaAnamneseCriolipolise.php';
        require APP . 'view/templates/footer.php';
    }

    public function verfichaAnamnese($id)
    {
		$FichaAnamneseCriolipolise 			= new Ficha();
        $listaAnamnese 	= $FichaAnamneseCriolipolise->FichaAnamneseCriolipolise($id);

        $listaMarcacao 	= $FichaAnamneseCriolipolise->marcacaoFichaAnamneseCriolipolise($id);
		
        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/ficha/verFichaAnamneseCriolipolise.php';
        require APP . 'view/templates/footer.php';
    }
	
/* ############ CARREGAR PÁGINA CADASTRO FICHA ANAMNESE COVID-19 ################*/
    public function novoFichaAnamneseCriolipolise($cpf)
    {
        $CPF = $cpf;
        //$nivelAcesso = new NivelController();
        //$nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
        //$teste = $nivelAcesso;

        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/ficha/novoFichaAnamneseCriolipolise.php';
        require APP . 'view/templates/footer.php';
        echo "<script>$(document).ready(function () {
            ajax();});</script>";
    }
	
/* ############ CARREGAR PÁGINA EDITAR FICHA ANAMNESE COVID-19 - CARREGANDO TODOS OS DADOS DO BANCO ################*/
    public function editarFichaAnamneseCriolipolise($id)
    {
        $FichaAnamneseCriolipolise 			= new Ficha();
        $FichaAnamneseCriolipoliseLista 	= $FichaAnamneseCriolipolise->listaFichaAnamneseCriolipolise($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/ficha/editarFichaAnamneseCriolipolise.php';
        require APP . 'view/templates/footer.php';
    }
	
/* ############ DESATIVAR PÁGINA FICHA ANAMNESE COVID-19 COM BASE NO ID ################*/
    public function desativarAnamneseCriolipolise($id)
    {
        $FichaAnamneseCriolipolise 				= new Ficha();
        $desativarFichaAnamneseCriolipolise 	= $FichaAnamneseCriolipolise->desativarAnamneseCovid19($id);
        echo json_decode($desativarFichaAnamneseCriolipolise);
    }
	
	public function inserirFichaAnamneseCriolipolise()
    {
        
		date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"d/m/Y");
        $HoraAtual = date_format($Data,"H:i:s");
		
		
		$Ficha 								= new Ficha();
        $inserirFichaAnamneseCriolipolise 	= $Ficha->AddFichaAnamneseCriolipolise( $_POST['idFichaGeral'],
                                                                                    $_POST['idFunc'],
                                                                                    $_POST['fichaAnamnesePraticaAtividadeFisica'], 
                                                                                    $_POST['fichaAnamneseQualPraticaAtividadeFisica'], 
                                                                                    $_POST['fichaAnamneseTemAlgumTipoDeAlergia'], 
                                                                                    $_POST['fichaAnamneseQualAlgumTipoDeAlergia'], 
                                                                                    $_POST['fichaAnamneseTemAlgumtipodehormonal'], 
                                                                                    $_POST['fichaAnamneseQualAlgumtipodehormonal'], 
                                                                                    $_POST['fichaAnamnesePeso'], 
                                                                                    $_POST['fichaAnamneseDoencasRelacionadasAoFrio'], 
                                                                                    $_POST['fichaAnamneseHerniasUmbilicalOuInguinal'], 
                                                                                    $_POST['fichaAnamneseDiastaseAbdominal'], 
                                                                                    $_POST['fichaAnamneseLesaoAbertaNaPele'], 
                                                                                    $_POST['fichaAnamneseLupusEritematosoSistemico'], 
                                                                                    $_POST['fichaAnamneseCancerDePele'], 
                                                                                    $_POST['fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves'], 
                                                                                    $_POST['fichaAnamneseUsoDeAnticoagulante'], 
                                                                                    $_POST['fichaAnamneseCirurgiaRecenteNaAreaTratada'], 
                                                                                    $_POST['fichaAnamneseGestanteeOuAmamentando'], 
                                                                                    $_POST['fichaAnamneseObservacoes'],  
                                                                                    $_POST['fichaAnamneseObsTratamentoIndicado'],  
                                                                                    $_POST['fichaAnamneseQtdSessoes'],  
                                                                                    $DataAtual, 
                                                                                    $HoraAtual, 
                                                                                    1);
		
        echo json_encode($inserirFichaAnamneseCriolipolise);
    }

    public function inserirTratamentoIndicado()
    {
        $Ficha 								= new Ficha();
        $inserir 	= $Ficha->inserirTratamentoIndicado();
        echo json_encode($inserir);
    }	
/* 
#####################################################################
#############											#############
#############			FICHA ANAMNESE COVID-19			#############
#############											#############
#####################################################################
*/
    

/* ############ CARREGAR PÁGINA FICHA ANAMNESE COVID-19 LISTANDO TODOS OS DADOS DO BANCO E A QUANTIDADE DE ITENS ################*/
public function fichaContratoCliente()
    {
		$FichaContratoCliente	= new Ficha();
        $ContratoCliente 		= $FichaContratoCliente->todosFichaContratoCliente();
        $qtdContratoCliente 	= $FichaContratoCliente->qtdContratoCliente();

        $Servicos				= new Servicos();
		$listaServicos			= $Servicos->listaTodos(1);
		
        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/ficha/fichaContratoCliente.php';
        require APP . 'view/templates/footer.php';
    }

/* ############ CARREGAR PÁGINA CADASTRO FICHA ANAMNESE COVID-19 ################*/
    public function novoFichaContratoCliente()
    {
        $Funcionario = new Funcionario();
        $funcionario = $Funcionario->idNomeFuncionario(1);

        $Servicos				= new Servicos();
		$listaServicos			= $Servicos->listaTodos(1);

        $Pacote					= new Pacote();
		$listaPacote			= $Pacote->listaTodos(1);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/ficha/novoFichaContratoCliente.php';
        require APP . 'view/templates/footer.php'; 
    }

    public function verFichaContratoCliente($id)
    {
        $FichaContratoCliente	= new Ficha();
        $fichaContratoCliente 		= $FichaContratoCliente->listaFichaContratoCliente($id);

        $Servicos				= new Servicos();
		$listaServicos			= $Servicos->listarServicoContrato($id);

		$listaPacotes			= $Servicos->listarPacoteContrato($id);

        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/ficha/verFichaContratoCliente.php';
        require APP . 'view/templates/footer.php';
    }

    public function formFichaContratoCliente($id)
    {
        $FichaContratoCliente	= new Ficha();
        $fichaContratoCliente 		= $FichaContratoCliente->listaFichaContratoCliente($id);

        $Servicos				= new Servicos();
		$listaServicos			= $Servicos->listarServicoContrato($id);

        $listaPacote			= $Servicos->listarPacoteContrato($id);

        require APP . 'view/ficha/formFichaContratoCliente.php'; 
    }
	
/* ############ CARREGAR PÁGINA EDITAR FICHA ANAMNESE COVID-19 - CARREGANDO TODOS OS DADOS DO BANCO ################*/
    public function editarFichaContratoCliente($id)
    {
        $FichaContratoCliente 		= new Ficha();
        $FichaContratoClienteLista 	= $FichaContratoCliente->listaContratoCliente($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/ficha/editarFichaContratoCliente.php';
        require APP . 'view/templates/footer.php';
    }
	
/* ############ DESATIVAR PÁGINA FICHA ANAMNESE COVID-19 COM BASE NO ID ################*/
    public function desativarContratoCliente($id)
    {
        $FichaContratoCliente 			= new Ficha();
        $desativarFichaContratoCliente 	= $FichaGeral->desativarFichaContratoCliente($id);
        echo json_decode($desativarFichaContratoCliente);
    }
	
	public function inserirFichaContratoCliente()
    {

        $Ficha 							= new Ficha();
        $Servico 						= new Servicos(); 

        $inserirFichaContratoCliente 	= $Ficha->addContratoCliente(	$_POST['idCliente'], 
                                                                        $_POST['idFunc'],
                                                                        $_POST['qtdSessoes'],
                                                                        $_POST['contratoData']);

        if ($_POST['servicos'] == 'servicos') {
            
            $idsservicos = array();
            $idServico = $_POST['itemDoContrato'];
            foreach ($idServico as $linha) {
                array_push($idsservicos, $Servico->retornaIdServico($linha));
            }
    
            foreach ($idsservicos as $linhaa) {
                $agendamentos = $Ficha->inserirServico($inserirFichaContratoCliente,$linhaa[0]->idServico);
            }
            echo json_encode($agendamentos);

        } else {

            $idsservicos = array();
            $idServico = $_POST['itemDoContrato'];
            foreach ($idServico as $linha) {
                array_push($idsservicos, $Servico->retornaIdPacote($linha));
            }
    
            foreach ($idsservicos as $linhaa) {
                $agendamentos = $Ficha->inserirPacote($inserirFichaContratoCliente,$linhaa[0]->idPacoteServico);
            }
            echo json_encode($agendamentos);

        }  

        /*$idsservicos = array();
		$idServico = $_POST['itemDoContrato'];
		foreach ($idServico as $linha) {
			array_push($idsservicos, $Servico->retornaIdServico($linha));
		}

		foreach ($idsservicos as $linhaa) {
			$agendamentos = $Ficha->inserirServico($inserirFichaContratoCliente,$linhaa[0]->idServico);
        }*/   
    }
	
} /* ################## FIM DA CLASS FICHA CONTROLLER */ 

