<?php

namespace App\Controller;

use App\Controller\NivelController;
use App\Model\Funcionario;
use App\Model\Agenda;
use App\Model\Usuario;
use Respect\Validation\Rules\Length;
use Verot\Upload;

class FuncionarioController
{
    private $controller;

    public function __construct()
    {
        (new LoginController)->usuarioLongado();
        $this->controller =  get_class($this);
    }

    public function header()
    {
        if ($_SESSION['funcNivel'] == "Profissional") {
            require APP . 'view/templates/header-profissional.php';
        } elseif($_SESSION['funcNivel'] == "Atendimento"){
            require APP . 'view/templates/header-atendente.php';
        } elseif($_SESSION['funcNivel'] == "Admin"){
            require APP . 'view/templates/header.php';
        }   
    }

    public function index()
    {
        $controller =  $this->controller;
        $Funcionario = new Funcionario();
        $funcionarios = $Funcionario->getAllFuncionario(1);
        $amount_of_funcionario = $Funcionario->getAmountOfFuncionario();

        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/funcionario/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function desativado()
    {
        $Funcionario = new Funcionario();
        $funcionarios = $Funcionario->getAllFuncionario(0);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/funcionario/desativados.php';
        require APP . 'view/templates/footer.php';
    }

    public function ativarDesativar($boleano)
    {
        $id = $_POST['id'];
        $cliente = new Funcionario();
        $cliente = $cliente->ativarDesativar($id,$boleano);
        echo json_decode($cliente);
    }

    public function novo()
    {
        $controller =  $this->controller;
        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/funcionario/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        $Funcionario = new Funcionario();
        $FuncionarioLista = $Funcionario->lista($id);
        $EspecializcoesLista = $Funcionario->listaEspecializcoes($id);

        $especialidadeLista = $Funcionario->getPerfilEspecialidade($id);
        $profissionaisLista = $Funcionario->getPerfilProfissional($id);
        
        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/funcionario/editar.php';
        require APP . 'view/templates/footer.php';
    }
	
	public function desativar()
    {
        $id = $_POST['id'];
        $Funcionario = new Funcionario();
        $funcionario = $Funcionario->desativar($id);
        echo json_decode($funcionario);
    }

	public function atualizar($id)
    {
            $imagem = $_FILES['funcFoto'];

            $Funcionario = new Funcionario();
            $funcionario = $Funcionario->lista($id);

            $imgBanco = $funcionario[0]->funcFoto;
            $webBanco = $funcionario[0]->funcWebcam;
            $handle = new \Verot\Upload\Upload($imagem);
            $imgInput = $handle->file_src_name;

            if (!empty($_POST['funcWebcam'])) { //Se não estiver vazia

                $img = ""; // img
                $webcam = $_POST['funcWebcam'];

            } elseif(!empty($imgInput)) {//Se não estiver vazia
                
                $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/func');
                $handle = new \Verot\Upload\Upload($imagem);

                if ($handle->uploaded) {
                    $handle->image_resize         	= true;
                    $handle->image_x              	= 100;
                    $handle->image_ratio_y        	= true;
                    $handle->file_safe_name 		= false;
                    $handle->file_name_body_add 	= '_mini';
                    $handle->process($diretorio_destino);
                }

                if ($handle->uploaded) {
                    $handle->image_resize         	= true;
                    $handle->image_x              	= 680;
                    $handle->image_ratio_y        	= true;
                    $handle->process($diretorio_destino);
                }

                $img = $handle->file_src_name;
                $webcam = "";
            }else{
				$img    = $imgBanco;
				$webcam = $webBanco;
            }
            
            if(!empty($_POST['funcSenha'])){
                $senhadHash = password_hash($_POST['funcSenha'], PASSWORD_DEFAULT);
            } else {
                $senhadHash = $funcionario[0]->funcSenha;
            }

            
            /*if($_SESSION['funcNivel'] == "Admin"){
                $nivel = $_SESSION['funcNivel'];
            } else if() {
                $nivel = "Atendimento";
            } else {
                $nivel = "Atendimento";
            }*/

            $atualizarFuncionario 	= $Funcionario->atualizar(	$id,
                                                                $_POST['funcNome'], 
                                                                $_POST['funcCpf'], 
                                                                $_POST['funcRg'], 
                                                                $_POST['funcEmail'], 
                                                                $senhadHash, 
                                                                $_POST['funcFone'], 
                                                                $_POST['funcCel'], 
                                                                $_POST['funcCedulaProfissional'], 
                                                                $_POST['funcComissao'], 
                                                                $_POST['funcNasci'], 
                                                                $_POST['funcSexo'], 
                                                                $_POST['funcNivel'], 
                                                                $_POST['funcEnd'], 
                                                                $_POST['funcNumero'], 
                                                                $_POST['funcEndComp'], 
                                                                $_POST['funcCidade'], 
                                                                $_POST['funcCEP'], 
																$img,
																$webcam,
																$_POST['funcAssinatura'], 
																$_POST['funcBiografia'], 
																1);						          
            echo json_encode($atualizarFuncionario); 
    }
	
	
    public function inserir()
    {
        $imagem = $_FILES['funcFoto'];

        if (isset($imagem)) {

            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/func');
            $handle = new \Verot\Upload\Upload($imagem);


            if ($handle->uploaded) {
                $handle->image_resize             	= true;
                $handle->image_x                  	= 100;
                $handle->image_ratio_y            	= true;
                $handle->file_safe_name         	= false;
                $handle->file_name_body_add     	= '_mini';
                $handle->process($diretorio_destino);
            }

            if ($handle->uploaded) {
                $handle->image_resize         		= true;
                $handle->image_x              		= 680;
                $handle->image_ratio_y        		= true;
                $handle->process($diretorio_destino);
            }

            $img = $handle->file_src_name;
        } else {
            $img = "";
        }

        $senhadHash = password_hash($_POST['funcSenha'], PASSWORD_DEFAULT);

        $Funcionario = new Funcionario();
        $funcionarios = $Funcionario->inserir(
            $_POST['funcNome'],
            $_POST['funcCpf'],
            $_POST['funcRg'],
            $_POST['funcEmail'],
            $senhadHash, 
            $_POST['funcFone'],
            $_POST['funcCel'],
            $_POST['funcCedulaProfissional'],
            $_POST['funcComissao'],
            $_POST['funcNasci'],
            $_POST['funcSexo'],
            $_POST['funcEnd'],
            $_POST['funcNumero'],
            $_POST['funcEndComp'],
            $_POST['funcCidade'],
            $_POST['funcCEP'],
            $_POST['funcNivel'],
            $img,
            $_POST['funcWebcam'],
            $_POST['funcAssinatura'],
            $_POST['funcBiografia'],
            "1"
        );
        echo json_encode($funcionarios);
    }

    public function inserircurso()
    {
        $Funcionario = new Funcionario();
        $funcionarios = $Funcionario->inserircurso(
            
            $_POST['idfunc'],
            $_POST['cursonome'],
            $_POST['cursodescricao']
           );
        echo json_encode($funcionarios);
    }

    /* ############################ PROFISSIONAL ########################### */
    public function profissional()
    {
        $controller =  "Profissional";
        $Funcionario = new Funcionario();
        $profissionais = $Funcionario->getProfissional();
        $amount_of_funcionario = $Funcionario->getAmountOfFuncionario();

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/funcionario/profissional.php';
        require APP . 'view/templates/footer.php';
    }

    public function perfil($id)
    {
        $Funcionario 			= new Funcionario();
        $profissionaisLista 	= $Funcionario->getPerfilProfissional($id);
        $especialidadeLista 	= $Funcionario->getPerfilEspecialidade($id);
        $comissaoProfissional 	= $Funcionario->comissaoProfissional($id);

        $Agendamento 	= new Agenda();
		$agendaLista 	= $Agendamento->listaAgendamentoFunc($id);

        require APP . 'view/templates/head.php';

        if ($_SESSION['funcNivel'] == "Profissional") {
            require APP . 'view/templates/header-profissional.php';
        } else{
            require APP . 'view/templates/header.php';
        }
        require APP . 'view/funcionario/perfil.php';
        require APP . 'view/templates/footer.php';
    } 

    public function deleteEspecialidade()
    {
        $Funcionario = new Funcionario();
        $funcionarios = $Funcionario->deleteEspecialidade($_POST['id']);
        echo json_encode($funcionarios);
    }
}
