<?php

namespace App\Controller;

use App\Model\Paginas;
use App\Model\Slider;
use App\Model\ServicoHomem;
use App\Model\CategoriaServico;
use App\Model\Equipe;
use App\Model\Depoimento;
use App\Model\Servicos;

class HomeController
{
    public function index()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");  
        
        $ServicosLista 	= new Servicos();
        $servicosLista 	= $ServicosLista->listaTodos(1);
		
		$DepoLista 		= new Depoimento();
        $depoLista 		= $DepoLista->listaStatus(1);

        require APP . 'view/site/head.php';
        require APP . 'view/templates/header-front.php';
        require APP . 'view/site/index.php';
		require APP . 'view/templates/footer-front.php';
    }
}
