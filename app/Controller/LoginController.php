<?php

namespace App\Controller;

use App\Controller\NivelController;
use App\Controller\EmailController;
use App\Model\Usuario;
use App\Model\Cliente;
use App\Model\Funcionario;
use App\Model\Reserva;

class LoginController
{
    private $email;
    private $senha;
    private $idCliente;
    private $variavel;

    public function index()
    {
        $msgErro =  $this->variavel;

        require APP . 'view/login/head-admin.php';
        //require APP . 'view/login/index.php';
        require APP . 'view/login/login-admin.php';
    }

    public function newuser()
    {
        require APP . 'view/templates/head.php';
        require APP . 'view/login/novo.php';
        require APP . 'view/templates/footer-login.php';
    }

    public function loginCliente()
    {
        $msgErro =  $this->variavel;

        require APP . 'view/login/head.php';
        require APP . 'view/login/index-cliente.php';
    }

    public function acessoClientee()
    {
        $this->email = $_POST['email'];
        $this->senha = $_POST['senha'];
        $array = array();

        $cliente = new Cliente();
        $cliente = $cliente->acessoCliente($this->email);

        if (!empty($cliente)) {
            if (password_verify($this->senha, $cliente[0]->senha)) {
                $_SESSION['idCliente'] = $cliente[0]->id;
                $_SESSION['nomeCliente'] = $cliente[0]->nome_cliente;
                $_SESSION['imgCliente'] = $cliente[0]->img;
                $array += ["boleano" => 1];
                $array += ["msg" => "Acessando..."];
                echo json_encode($array);
            } else {
                $array += ["boleano" => 0];
                $array += ["msg" => "Senha incorreta"];
                echo json_encode($array);
            }
        } else {
            $array += ["boleano" => 0];
            $array += ["msg" => "Email não cadastrado"];
            echo json_encode($array);
        }
    }

    public function verificarEmailCliente($codigo)
    {
        $verificar = explode("X", $codigo);

        $Cliente = new Cliente();
        $cliente = $Cliente->idRecuperar($verificar[0]);

        if ($cliente[0]->CliRecuperar == $codigo) {
            header('location: ' . URL . 'login/novasenhacliente/' . $cliente[0]->idCliente);
        } else {
            echo "Erro";
        }
    }

    public function novaSenhaCliente($idCliente)
    {
        $Cliente = new Cliente();
        $clientes = $Cliente->idNomeImg($idCliente);

        if (!empty($clientes[0]->CliRecuperar)) {

            require APP . 'view/login/recuperar/head.php';
            require APP . 'view/login/recuperar/nova-senha-cliente.php';
            require APP . 'view/templates/footer-cliente.php';
        } else {
            header('location: ' . URL);
        }
    }

    public function salvarSenhaCliente()
    {
        $this->senha = $_POST['nova-senha'];
        $this->idCliente = $_POST['id'];

        $senhaHasg = password_hash($this->senha, PASSWORD_DEFAULT);

        $cliente = new Cliente();
        $atualizadoSenha = $cliente->atualizarSenha($this->idCliente, $senhaHasg);
        $limpadoRecuperar = $cliente->limparaRecuperar($this->idCliente);

        if ($atualizadoSenha && $limpadoRecuperar) {
            $_SESSION['idCliente'] = $this->idCliente;
            echo json_encode(1);
        } else {
            echo json_encode(0);
        }
    }

    public function carregarSession()
    {
        require APP . 'view/login/carregamento.php';
    }

    public function acessoAdmin()
    {
        // Recaptch
        if (isset($_POST['submit'])) {

            $secret = "6LeLq64ZAAAAABQEwntJV8-gfGxbDJkTIoNiYLJt";
            $response = $_POST['g-recaptcha-response'];
            $verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
            $captcha_success = json_decode($verify);

            if ($captcha_success->success == false) {
                $msgErro = 'reCaptcha não selecionado';
                $this->variavel = $msgErro;
                $this->index();
            } else if ($captcha_success->success == true) {
                $nomeEmail = $_POST['nome_email'];
                $senha = $_POST['senha'];

                $Usuario = new Funcionario();
                $usuario = $Usuario->acessoFunc($nomeEmail); 

                if (!empty($usuario) && $usuario[0]->funcStatus == 1) {
                    if (password_verify($senha, $usuario[0]->funcSenha)) {

                        $_SESSION['idUsuario'] = $usuario[0]->idFunc;
                        $_SESSION['userNome'] = $usuario[0]->funcNome;
                        $_SESSION['funcNivel'] = $usuario[0]->funcNivel;
                        $_SESSION['funcFoto'] = $usuario[0]->funcFoto;
                        $_SESSION['funcWebcam'] = $usuario[0]->funcWebcam;

                        if ($usuario[0]->funcNivel == "Admin") {
                            header('location: ' . URL . 'admin');
                        } elseif($usuario[0]->funcNivel == "Profissional"){
                            header('location: ' . URL . 'funcionario/perfil/' . $usuario[0]->idFunc);
                        } elseif($usuario[0]->funcNivel == "Atendimento"){
                            header('location: ' . URL . 'admin');
                        }

                    } else {
                        $msgErro = 'Senha incorreta';
                        $this->variavel = $msgErro;
                        $this->index();
                    }
                } else {
                    $msgErro = 'Email incorreto';
                    $this->variavel = $msgErro;
                    $this->index();
                }
            }
        }
    }

    public function acessoCliente()
    {
        // Recaptch
        if (isset($_POST['submit'])) {

            $secret = "6LeLq64ZAAAAABQEwntJV8-gfGxbDJkTIoNiYLJt";
            $response = $_POST['g-recaptcha-response'];
            $verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
            $captcha_success = json_decode($verify);

            if ($captcha_success->success == false) {
                $msgErro = 'reCaptcha não selecionado';
                $this->variavel = $msgErro;
                $this->loginCliente();
            } else if ($captcha_success->success == true) {
                $Email = $_POST['nome_email'];
                $senha = $_POST['senha'];

                $Usuario = new cliente();
                $usuario = $Usuario->acessoCliente($Email);

                if (!empty($usuario)) {
                    if (password_verify($senha, $usuario[0]->cliSenha)) {
                        $_SESSION['idCliente'] = $usuario[0]->idCliente;
                        $_SESSION['cliNome'] = $usuario[0]->cliNome;
                        $_SESSION['cliFoto'] = $usuario[0]->cliFoto;
                        $_SESSION['cliWebcam'] = $usuario[0]->cliWebcam;
                        $_SESSION['funcNivel'] = "cliente";
                        header('location: ' . URL . 'cliente/perfil/' . $usuario[0]->idCliente);   
                    } else {
                        $msgErro = 'Senha incorreta';
                        $this->variavel = $msgErro;
                        $this->loginCliente();
                    }
                } else {
                    $msgErro = 'Email incorreto';
                    $this->variavel = $msgErro;
                    $this->loginCliente();
                }
            }
        }
    }

    public function recuperarSenha()
    {

        if (isset($_POST["recuperar"])) {

            $nomeEmail = $_POST['nome_email'];
            $Cliente = new Cliente();
            $verificarUsuario = $Cliente->recuperarSenha($nomeEmail);

            if (!empty($verificarUsuario)) {

                //Gerar Código  
                $gerarCodigo = substr(str_shuffle('Az23456789'), 1, 10);
                $novoCodigo = $verificarUsuario[0]->idCliente . 'X' . $gerarCodigo;

                $usuario = $Cliente->recuperar($verificarUsuario[0]->idCliente, $novoCodigo);
                $emailClass = new EmailController("$nome","","$assunto",$this->email,URL.'login/verificaremailcliente/'.$novoCodigo,'templateRecuperarSenha()');

                $boleano = $emailClass->recuperarSenha();
            
                echo json_encode($boleano);

            } else {
                $msgErro = 'Não existe usuário ou Email em nossos registro';
                $this->recuperarSenha();
            }

        }

        require APP . 'view/login/recuperar/head.php';
        require APP . 'view/login/recuperar/index.php';
    }

    public function recuperarSenhaCliente()
    {
            $nomeEmail = $_POST['email'];
            $Cliente = new Cliente();
            $verificarUsuario = $Cliente->recuperarSenha($nomeEmail);

            if (!empty($verificarUsuario)) {
                
                //Gerar Código  
                $gerarCodigo = substr(str_shuffle('Az23456789'), 1, 10);
                $novoCodigo = $verificarUsuario[0]->idCliente . 'X' . $gerarCodigo;

                $usuario = $Cliente->recuperar($verificarUsuario[0]->idCliente, $novoCodigo);
                $emailClass = new EmailController($verificarUsuario[0]->cliNome,$_POST['email'],"Nova Senha",URL.'login/verificaremailcliente/'.$novoCodigo,'www.belizestime.com.br/login/verificaremailcliente/'.$novoCodigo);

                $boleano = $emailClass->recuperarSenha();
            
                echo json_encode($boleano);

            } else {
                $msgErro = 'Não existe usuário ou Email em nossos registro';
                echo json_encode(0);
            }
        

        //require APP . 'view/login/recuperar/recuperar-cliente.php';
    }

    public function verificarEmail($codigo)
    {
        $verificar = explode("X", $codigo);

        $usuario = new Usuario();
        $usuario = $usuario->usuario($verificar[0]);

        if ($usuario[0]->recuperar == $codigo) {
            header('location: ' . URL . 'login/novasenha/' . $usuario->id);
        } else {
            echo "Erro";
        }
    }

    public function novasenha($idUsuario)
    {
        $_SESSION['idUsuario'] = $idUsuario;

        $this->usuarioLongado();

        require APP . 'view/login/recuperar/head.php';
        require APP . 'view/login/recuperar/nova-senha.php';
        require APP . 'view/templates/footer.php';
    }

    public function usuarioLongado()
    {
        if (!isset($_SESSION['idUsuario'])) {
            header('location: ' . URL . 'login');
        }
    }

    public function clienteLongado()
    {
        if (!isset($_SESSION['idCliente'])) {
            header('location: ' . URL);
        }
    }

    public function sairUsuario()
    {
        session_destroy();
        header('location: ' . URL . 'login');
    }

    public function sairCliente()
    {
        session_destroy();
        header('location: ' . URL . 'login/logincliente');
    }

}
