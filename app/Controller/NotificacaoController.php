<?php
namespace App\Controller;

use App\Model\Notificacao;
use App\Model\Usuario;
use App\Controller\LoginController;

class NotificacaoController
{

    public function notificacao()
    {
        $notificacaoLista = new Notificacao();
        $notificacaoLista = $notificacaoLista->listaTodos();
        return $notificacaoLista;
    }

    public function verificar()
    {
        $quantidadeAtual = $_POST['qtd'];

        $notificacaoLista = new Notificacao();
        $quantidadeAgora = $notificacaoLista->quantidade();

        if ($quantidadeAgora[0]->notificacao > $quantidadeAtual) {
            echo json_decode(true);
        } else {
            echo json_decode(false);
        }
        
            
        
    }
    public function certo()
    {
        $quantidadeAtual = $_POST['qtd'];

        $notificacaoLista = new Notificacao();
        $notificacaoLista = $notificacaoLista->listaTodos();

            foreach ($notificacaoLista as $linha) {

                echo
                "<a class='dropdown-item'>
                  <div class='item-thumbnail'>
                    <div class='item-icon bg-success'>
                      <i class='mdi mdi-information mx-0'></i>
                    </div>
                  </div>
                  <div class='item-content'>
                    <h6 class='font-weight-normal'>$linha->categoria</h6>
                    <p class='font-weight-light small-text mb-0 text-muted'>
                      $linha->descricao
                    </p>
                  </div>
                </a>";            
                  
            }
        
    }

    public function novo()
    {
        (new LoginController)->usuarioLongado();

        require APP . 'view/Notificacao/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/Notificacao/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        (new LoginController)->usuarioLongado();

        $NotificacaoLista = new Notificacao();
        $NotificacaoLista = $NotificacaoLista->lista($id);

        require APP . 'view/Notificacao/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/Notificacao/editar.php';
        require APP . 'view/templates/footer.php';

    }

    public function deletar($id)
    {
        $Notificacao = new Notificacao();
        $Notificacao = $Notificacao->deletar($id);
        echo json_decode($Notificacao);

    }

    public function inserir()
    {

        $Notificacao = new Notificacao();
        $msgModal = $Notificacao->inserir(
            "categoria",
            "cliente"
        );
 
        echo json_encode($msgModal);
    }

    public function lixeira()
    {
        (new LoginController)->usuarioLongado();

        $NotificacaoLista = new Notificacao();
        $NotificacaoLista = $NotificacaoLista->listaTodos();

        require APP . 'view/Notificacao/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/Notificacao/lixeira.php';
        require APP . 'view/templates/footer.php';
    }

}

