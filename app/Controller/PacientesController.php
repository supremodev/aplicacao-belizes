<?php

namespace App\Controller;

use App\Controller\NivelController;
use App\Model\Pacientes;
use App\Model\Usuario;
use App\Model\ServicoHomem;
use App\Model\CategoriaServico;
use Respect\Validation\Rules\Length;
use Verot\Upload;

class PacientesController
{
    private $categorias;

    public function __construct()
    {
        (new LoginController)->usuarioLongado();
    }

    public function index()
    {

        require APP . 'view/Pacientes/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/Pacientes/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function diasTrabalho()
    {
        $Pacientes = new Pacientes();
        $diasLista = $Pacientes->listaDias($_SESSION['idUsuario']);

        require APP . 'view/Pacientes/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/Pacientes/dias-trabalho.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function servico()
    {
        $Pacientes = new Pacientes();
        $PacientesLista = $Pacientes->Pacientes($_SESSION['idUsuario']);

        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();

        $servico = new ServicoHomem();
        $servicoLista = $servico->TodosIdNomeCategoria();

        $categorias = array();
        foreach ($categoriaLista as $linha) {

            $categoria = $servico->consultaCategoria($linha->categoria);

            if (!empty($categoria)) {
                array_push($categorias, $linha->categoria); 
            }
        }

        require APP . 'view/Pacientes/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/Pacientes/servico.php';
        require APP . 'view/templates/footer.php';
    }

    public function novo()
    {
        $nivelAcesso = new NivelController();
        $nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
        $teste = $nivelAcesso;

        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();

        $servico = new ServicoHomem();
        $servicoLista = $servico->TodosIdNomeCategoria();

        $categorias = array();
        foreach ($categoriaLista as $linha) {

            $categoria = $servico->consultaCategoria($linha->categoria);

            if (!empty($categoria)) {
                array_push($categorias, $linha->categoria); 
            }
        }

        require APP . 'view/Pacientes/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/Pacientes/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        $Pacientes = new Pacientes();
        $PacientesLista = $Pacientes->Pacientes($id);

        $servicoLista = new ServicoHomem();
        $servicoLista = $servicoLista->listaTodos();

        require APP . 'view/Pacientes/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/Pacientes/editar.php';
        require APP . 'view/templates/footer.php';
    }

    public function deletar($id)
    {
        $Pacientes = new Pacientes();
        $Pacientes = $Pacientes->deletar($id);
        echo json_decode($Pacientes);
    }

    public function atualizar($id)
    {
        $imagem = $_FILES['imagem'];

        $destaque = new Pacientes();
        $destaque = $destaque->Pacientes($id);

        $imgBanco = $destaque[0]->img;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;
    
        if (!empty($imgInput)) { //Se tiver imagem input
            
            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/Pacientes');    
            $handle = new \Verot\Upload\Upload($imagem);
    
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 100;
                $handle->image_ratio_y        = true;
                $handle->file_safe_name = false;
                $handle->file_name_body_add = '_mini';
                $handle->process($diretorio_destino);
        
            }
        
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 300;
                $handle->image_ratio_y        = true;
                $handle->process($diretorio_destino);
        
            }

            $img = $handle->file_src_name;
            
        } else {
            $img = $imgBanco;
        } 

        $servicos = serialize($_POST['servico']);
        
        $destaque = new Pacientes();
        $msgModal = $destaque->atualizar($id, $_POST["nome"], $_POST["email"], $img, $servicos);

        echo json_encode($msgModal);    
    }

    public function atualizarDias()
    {

        if (isset($_POST["monday"])) {
            $monday = $_POST["monday"];
        } else {
            $monday = 0;
        }

        if (isset($_POST["tuesday"])) {
            $tuesday = $_POST["tuesday"];
        } else {
            $tuesday = 0;
        }

        if (isset($_POST["wednesday"])) {
            $wednesday = $_POST["wednesday"];
        } else {
            $wednesday = 0;
        }

        if (isset($_POST["thursday"])) {
            $thursday = $_POST["thursday"];
        } else {
            $thursday = 0;
        }

        if (isset($_POST["thursday"])) {
            $thursday = $_POST["thursday"];
        } else {
            $thursday = 0;
        }

        if (isset($_POST["friday"])) {
            $friday = $_POST["friday"];
        } else {
            $friday = 0;
        }

        if (isset($_POST["friday"])) {
            $friday = $_POST["friday"];
        } else {
            $friday = 0;
        }
  
        if (isset($_POST["saturday"])) {
            $saturday = $_POST["saturday"];
        } else {
            $saturday = 0;
        }

        if (isset($_POST["sunday"])) {
            $sunday = $_POST["sunday"];
        } else {
            $sunday = 0;
        }

        $Pacientes = new Pacientes();
        $msgModal = $Pacientes->atualizarDias( $_SESSION['idUsuario'], $monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $sunday);

        echo json_encode($msgModal);    

    }

    public function inserir()
    {
        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();

        $servico = new ServicoHomem();

        $categorias = array();
        foreach ($categoriaLista as $linha) {

            $categoria = $servico->consultaCategoria($linha->categoria);

            if (!empty($categoria)) {
                array_push($categorias, $linha->categoria); 
            }
        }

        foreach ($categorias as $key => $linha) {
            if (!empty($_POST['servico'."$key"])) {
                $array[] = [$linha=> $_POST['servico'."$key"]];
            } else {
                $array[] = [$linha=> false];
            }  
        }

        $img = "icaro.jpg";
        $servicos = serialize($array);
        $usuario = new Pacientes();
        $msgModal = $usuario->inserir(
            $_SESSION['idUsuario'],
            $_POST["email"],
            $img,
            $servicos    
        );
 
        echo json_encode($msgModal);
    }

    public function nomeDia()
    {
        $diasemana = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        $hoje = date('Y-m-d');
        $diasemana_numero = date('w', strtotime($hoje));
        return $diasemana[$diasemana_numero];
    }

    public function consultaProfissional()
    {
        $Pacientes = new Pacientes();
        $PacientesTrabalhaHoje = $Pacientes->consultaDia($this->nomeDia());

        //Array de id de serviços Checked
        $servicoCheked = json_decode($_POST['servico']);

       $totalServico = count($servicoCheked);
        $PacientesDisponivel = array();
        $t = 0;
        foreach ($PacientesTrabalhaHoje as  $profissional) {
            $PacientesServico = $Pacientes->consultaServico($profissional->id);
            $PacientesServico = unserialize($PacientesServico[0]->servicos);

            for ($i=0; $i < $totalServico;  $i++) { 
                //Se não tiver o serviço
                if (in_array($servicoCheked[$i], $PacientesServico)) {
                    $t = ++$i;  
                    if($totalServico == $t) {
                        array_push($PacientesDisponivel,$profissional->id);
                    } 
              } else{
            break;
              }
            }      
        }

        if (!empty($PacientesDisponivel)) {
            $ids = '';
            $len = count($PacientesDisponivel);
            foreach ($PacientesDisponivel as $index => $item) {
                if ($index == 0) {
                    $ids .= $item;
                } else if ($index == $len - 1) {
                    $ids .= ','.$item;
                }
            }
        }

        if (isset($ids)) {
            $PacientesLista = $Pacientes->listaCabeleireiroFiltro($ids);
            echo '<option value="ambos" imagem="'.URL.'images/usuario/usuario.jpg">Ambos</option>';
            foreach ($PacientesLista as $profissional) {
                echo '<option value="' . $profissional->id . '" imagem="'.URL.'images/usuario/'.$profissional->img.'">'. $profissional->nome .'</option>';
            }
        } else {
            echo json_encode(0);
        }

    }

}

