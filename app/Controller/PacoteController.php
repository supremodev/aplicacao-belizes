<?php
namespace App\Controller;

use App\Model\Pacote;
use App\Controller\NivelController;
use App\Model\Usuario;
use App\Controller\LoginController;
use Verot\Upload;

class PacoteController
{

    public function __construct()
    {
        (new LoginController)->usuarioLongado();
    }

    public function index()
    {
        $PacoteLista = new Pacote();
        $PacoteLista = $PacoteLista->listaTodos(1);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/pacote/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function desativado()
    {
        $PacoteLista = new Pacote();
        $PacoteLista = $PacoteLista->listaTodos(0);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/pacote/desativados.php';
        require APP . 'view/templates/footer.php';
    }

    public function novo()
    {
        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/pacote/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        $Pacote 		= new Pacote();
        $PacoteEditar = $Pacote->lista($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/pacote/editar.php';
        require APP . 'view/templates/footer.php';
    }

    public function deletar($id)
    {
        $pacote = new Pacote();
        $pacote = $pacote->deletar($id);
        echo json_decode($pacote);
    }

    public function atualizar($id)
    {
        $imagem = $_FILES['pacoteFoto'];

        $Pacote       = new Pacote();
        $pacote       = $Pacote->lista($id);

        $imgBanco = $pacote[0]->pacoteFoto;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;

        if (!empty($imgInput)) {//Se não estiver vazia

            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/pacote');

            if ($handle->uploaded) {
                $handle->image_resize         	= true;
                $handle->image_x              	= 100;
                $handle->image_ratio_y        	= true;
                $handle->file_safe_name 		= false;
                $handle->file_name_body_add 	= '_mini';
                $handle->process($diretorio_destino);
            }

            if ($handle->uploaded) {
                $handle->image_resize         	= true;
                $handle->image_x              	= 680;
                $handle->image_ratio_y        	= true;
                $handle->process($diretorio_destino);
            }

            $img = $handle->file_src_name;
            
        }else{
            $img    = $imgBanco;
        }
        
        $pacote             = new Pacote();
        $pacoteAtualizar    = $pacote->atualizar(   $id, 
                                                    $_POST['pacoteNome'], 
                                                    $_POST['pacoteDescricao'], 
                                                    $img, 
                                                    $_POST['pacoteValor'], 
                                                    $_POST['pacoteValorPromo'], 
                                                    $_POST['pacoteComissao'], 
                                                    $_POST['pacoteTempo'], 
                                                    $_POST['pacoteSessoes'],
                                                    $_POST['pacotePublicar']);
        echo json_encode($pacoteAtualizar);    
    }
    

    public function inserir()
    {
        $imagem = $_FILES['pacoteFoto'];

        if (isset($imagem)) {

            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/pacote');
            $handle = new \Verot\Upload\Upload($imagem);


            if ($handle->uploaded) {
                $handle->image_resize             	= true;
                $handle->image_x                  	= 100;
                $handle->image_ratio_y            	= true;
                $handle->file_safe_name         	= false;
                $handle->file_name_body_add     	= '_mini';
                $handle->process($diretorio_destino);
            }

            if ($handle->uploaded) {
                $handle->image_resize         		= true;
                $handle->image_x              		= 680;
                $handle->image_ratio_y        		= true;
                $handle->process($diretorio_destino);
            }

            $img = $handle->file_src_name;
        } else {
            $img = "sem-foto.jpg";
        }        
            
        
        $Pacote 		= new Pacote();
        $inserirPacote 	= $Pacote->inserir(	$_POST['pacoteNome'], 
											$_POST['pacoteDescricao'],
										   	$img,
											$_POST['pacoteValor'], 
											$_POST['pacoteValorPromo'], 
											$_POST['pacoteComissao'], 
											$_POST['pacoteTempo'], 	
                                            $_POST['pacoteSessoes'],										 
											$_POST['pacotePublicar']);
        echo json_encode($inserirPacote);
    }
	
	/* ############################ DETALHE DO SERVIÇO ########################### */
	public function detalhe($id)
    {
        $DetalhePacote 		= new Pacote();
        $detalheLista 		= $DetalhePacote->getDetalhePacote($id);
		$detalheServico 	= $DetalhePacote->getServicoPacote($id);
		$listarServico 		= $DetalhePacote->listarServico();

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/pacote/detalhe.php';
        require APP . 'view/templates/footer.php';
    }

    public function ativarDesativar($boleano)
    {
        $id         = $_POST['id'];
        $Servicos   = new Pacote();
        $servicos   = $Servicos->ativarDesativar($id,$boleano);
        echo json_decode($servicos);
    }

    public function inserirServico()
    {
       //$_POST['idPacote'];	 
       $idServico = $_POST['idsServicos'];
        //echo $_POST['idPacote']; 
        //print_r ($_POST['idsServicos']);
        $Servicos   = new Pacote();

        foreach ($idServico as $linha) {
           $servicos   = $Servicos->inserirServico($_POST['idPacote'], $linha);
        }

        echo json_decode($servicos);
    }

    public function removerServico()
    {
	 
        $idServico = $_POST['idsServicos'];
        $Servicos   = new Pacote();

        foreach ($idServico as $linha) {
           $servicos   = $Servicos->removerServico($linha);
        }

        echo json_decode($servicos);
    }

}

