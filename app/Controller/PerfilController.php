<?php

namespace App\Controller;

use App\Controller\NivelController;
use App\Model\Usuario;
use Verot\Upload;

class PerfilController
{
    public function __construct()
    {
        (new LoginController)->usuarioLongado();
    }

    public function index()
    {
        $usuarioLista = new Usuario();
        $usuarioLista = $usuarioLista->lista($_SESSION['idUsuario']);

        require APP . 'view/perfil/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/perfil/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function atualizar($id)
    {
        $usuario = new Usuario();
        $usuario = $usuario->lista($id);

        $senhadHash = $usuario[0]->senha;
        $senhaAntiga = $_POST['senha_antiga'];

        $imagem = $_FILES['imagem'];

        $imgBanco = $usuario[0]->img;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;

        if (!empty($imgInput)) { //Se tiver imagem input

            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/usuario');
            $handle = new \Verot\Upload\Upload($imagem);

            if ($handle->uploaded) {
                $handle->image_resize         = true;
                $handle->image_x              = 100;
                $handle->image_ratio_y        = true;
                $handle->file_safe_name = false;
                $handle->file_name_body_add = '_mini';
                $handle->process($diretorio_destino);
            }

            if ($handle->uploaded) {
                $handle->image_resize         = true;
                $handle->image_x              = 300;
                $handle->image_ratio_y        = true;
                $handle->process($diretorio_destino);
            }

            $img = $handle->file_src_name;

        } else {
            $img = $imgBanco;
        }

        $boleano = array();

        if (!empty($senhaAntiga)) {
            if (password_verify($senhaAntiga, $senhadHash)) {
                $senha = $_POST['nova_senha'];
                $senhadHash = password_hash($senha, PASSWORD_DEFAULT);
                $boleano['senha'] = 1;
            } else {
                $boleano['senha'] = 0;
            }
        }

        $usuarios = new Usuario();
        $usuarios = $usuarios->atualizar
        (
            $id,
            $_POST['nome'],
            $_POST['email'],
            $_POST['celular'],
            $_POST['telefone'],
            $_POST['aniversario'],
            $usuario[0]->id_nivel,
            $senhadHash,
            $img
        );

        $boleano['dados'] = $usuarios;

        echo json_encode($boleano);
    }
}
