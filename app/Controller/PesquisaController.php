<?php

namespace App\Controller;

use App\Model\Pesquisa;

class PesquisaController
{
    public function index($id)
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");       

        require APP . 'view/site/pesquisa.php';
        
    }

    public function header()
    {
        if ($_SESSION['funcNivel'] == "Profissional") {
            require APP . 'view/templates/header-profissional.php';
        } elseif($_SESSION['funcNivel'] == "Atendimento"){
            require APP . 'view/templates/header-atendente.php';
        } elseif($_SESSION['funcNivel'] == "Admin"){
            require APP . 'view/templates/header.php';
        }   
    }

    public function cliente($id)
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");       
        require APP . 'view/site/pesquisa.php';
        
    }

    public function listacliente($id)
    {
		$MediaPesquisa  = new Pesquisa();
        $listaPesquisa	= $MediaPesquisa->listacliente($id);
        
        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/pesquisa/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function pesquisacliente($id)
    {
		$MediaPesquisa  = new Pesquisa();
        $listaPesquisa	= $MediaPesquisa->pesquisacliente($id);
        
        require APP . 'view/templates/head.php';
        $this->header();
        require APP . 'view/pesquisa/pesquisacliente.php';
        require APP . 'view/templates/footer.php';
    }
	
	public function inserir()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");  
        
        $Pesquisa 			= new Pesquisa();
        $inserirPesquisa 	= $Pesquisa->inserirPesquisa(	$_POST['idCliente'],
                                                            $_POST['idAgenda'],
                                                            $_POST['pesquisaQualidadeComoServicoFoiPrestado'], 
                                                            $_POST['pesquisaQualidadeObsComoServicoFoiPrestado'], 
                                                            $_POST['pesquisaQualidadeAtendimentoRecepcao'], 
                                                            $_POST['pesquisaQualidadeObsAtendimentoRecepcao'], 
                                                            $_POST['pesquisaQualidadeAtendimentoBiomedico'], 
                                                            $_POST['pesquisaQualidadeObsAtendimentoBiomedico'], 
                                                            $_POST['pesquisaQualidadeAtendimentoEsteticista'], 
                                                            $_POST['pesquisaQualidadeObsAtendimentoEsteticista'], 
                                                            $_POST['pesquisaQualidadeAtendimentoGeral'], 
                                                            $_POST['pesquisaQualidadeObsAtendimentoGeral'], 
                                                            $_POST['pesquisaQualidadeLimpezaDosBanheiros'], 
                                                            $_POST['pesquisaQualidadeObsLimpezaDosBanheiros'], 
                                                            $_POST['pesquisaQualidadeLimpezaDasSalas'], 
                                                            $_POST['pesquisaQualidadeObsLimpezaDasSalas'], 
                                                            $_POST['pesquisaQualidadeConfortoDoEspaco'], 
                                                            $_POST['pesquisaQualidadeObsConfortoDoEspaco'], 
                                                            $_POST['pesquisaQualidadeSeuTratamento'], 
                                                            $_POST['pesquisaQualidadeObsSeuTratamento'], 
                                                            $_POST['pesquisaQualidadeFicaramEsclarecidasTodasAsDuvidas'], 
                                                            $_POST['pesquisaQualidadeFoiAferidaSuaPressaoArterial'],
                                                            $_POST['pesquisaQualidadeDesejaFazerUmaObservacao'],
                                                            1,
                                                            $DataAtual);
        echo json_encode($inserirPesquisa);
    }
	

	
	
}
