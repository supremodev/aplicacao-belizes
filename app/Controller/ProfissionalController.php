<?php

namespace App\Controller;

use App\Controller\NivelController;
use App\Model\Equipe;
use App\Model\Reserva;
use App\Model\Cliente;
use App\Model\Usuario;
use App\Model\ServicoHomem;
use App\Model\CategoriaServico;
use Verot\Upload;

class ProfissionalController
{
    private $controller;

    public function __construct()
    {
        (new LoginController)->usuarioLongado();
        $this->controller =  get_class($this);
    }

    public function index()
    {
        $controller =  $this->controller;
        //$equipe = new Equipe();
        //$equipeLista = $equipe->lista();

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/profissional/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function cliente()
    {
        $clienteLista = new Cliente();
        $clienteLista = $clienteLista->listaClienteProfissional($_SESSION['idUsuario']);

        require APP . 'view/profissional/head.php';
        require APP . 'view/templates/header-cabele.php';
        require APP . 'view/profissional/cliente.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function atendimento()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");

        $reservaLista = new Reserva();
        $reservaLista = $reservaLista->listaAtendimentoProfissional($DataAtual, $_SESSION['idUsuario']);

        $servicoLista = new ServicoHomem();
        $servicoLista = $servicoLista->TodosIdNomeCategoria();

        require APP . 'view/profissional/head.php';
        require APP . 'view/templates/header-cabele.php';
        require APP . 'view/reserva/atendimento.php';
        require APP . 'view/reserva/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function servico()
    {
        $equipe = new Equipe();
        $equipeLista = $equipe->equipe($_SESSION['idUsuario']);

        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();

        $servico = new ServicoHomem();
        $servicoLista = $servico->TodosIdNomeCategorias();

        $categorias = array();
        foreach ($categoriaLista as $linha) {

            $categoria = $servico->consultaCategoria($linha->id);

            if (!empty($categoria)) {
                array_push($categorias, $linha->categoria); 
            }
        }

        require APP . 'view/profissional/head.php';
        require APP . 'view/templates/header-cabele.php';
        require APP . 'view/profissional/servico.php';
        require APP . 'view/templates/footer.php';
    }

    

    public function inserirFichaContratoCliente()
    {
        $Funcionario = new Funcionario();
        $funcionarios = $Funcionario->inserir(	$_POST['funcNome'], 
												$_POST['funcCpf'], 
												$_POST['funcRg'], 
												$_POST['funcEmail'], 
												$_POST['funcSenha'], 
												$_POST['funcFone'], 
												$_POST['funcCel'], 
												$_POST['funcNasci'], 
												$_POST['funcSexo'], 
												$_POST['funcCargo'], 
												$_POST['funcEnd'], 
												$_POST['funcNumero'], 
												$_POST['funcEndComp'], 
												$_POST['funcCidade'], 
												$_POST['funcCEP'], 
												$_POST['funcNivel'], 
												"user.jpg", 
												"assinatura.png", 
												$_POST['funcBiografia'], 
												true);
        echo json_encode($funcionarios);
    }

    public function perfil()
    {
        $usuarioLista = new Usuario();
        $usuarioLista = $usuarioLista->lista($_SESSION['idUsuario']);

        require APP . 'view/profissional/head.php';
        require APP . 'view/templates/header-cabele.php';
        require APP . 'view/profissional/perfil.php';
        require APP . 'view/templates/footer.php';
    }
}

