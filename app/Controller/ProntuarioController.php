<?php
namespace App\Controller;

use App\Model\Prontuario;

class ProntuarioController
{
	
    public function __construct()
    {
    	//(new LoginController)->usuarioLongado();
	}

	public function header()
    {
        if ($_SESSION['funcNivel'] == "Profissional") {
            require APP . 'view/templates/header-profissional.php';
        } elseif($_SESSION['funcNivel'] == "Atendimento"){
            require APP . 'view/templates/header-atendente.php';
        } elseif($_SESSION['funcNivel'] == "Admin"){
            require APP . 'view/templates/header.php';
        } else {
            require APP . 'view/templates/header-cliente.php';
        }
    }

	public function show($id)
    {
        $Prontu 	= new Prontuario();
		$prontuLista 	= $Prontu->lista($id);
        
        require APP . 'view/cliente/prontuario-show.php';
	}

    public function editar($id)
    {
        $Prontu 	= new Prontuario();
		$prontuLista 	= $Prontu->lista($id);
        
        require APP . 'view/cliente/prontuario.php';
	}

    public function atualizar()
    {
        $Prontuario = new Prontuario();
        $msgModal = $Prontuario->atualizar($_POST["idProntuario"], $_POST["descricao"]);

        echo json_encode($msgModal);
    }
	
    public function insert()
    {
        $Prontuario 		= new Prontuario();
        $prontuario	= $Prontuario->insert($_POST['idCliente'],$_POST['idFunc'],$_POST['descricao']);
        echo json_encode($prontuario);
	}
	
	
}

