<?php

namespace App\Controller;

class SemAcessoController
{
    public function index()
    {

        require APP . 'view/semacesso/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/semacesso/index.php';
        require APP . 'view/templates/footer.php';
    }
}
