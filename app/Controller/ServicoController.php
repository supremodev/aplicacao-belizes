<?php

namespace App\Controller;


use App\Model\Servicos;

class ServicoController
{
    public function index()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");  
		
		$ServicosLista = new Servicos();
        $servicosLista = $ServicosLista->listaTodos(1);

        require APP . 'view/site/head.php';
        require APP . 'view/templates/header-front.php';
        require APP . 'view/site/servico.php';
		require APP . 'view/templates/footer-front.php';
    }
}













