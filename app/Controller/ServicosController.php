<?php
namespace App\Controller;

use App\Model\Servicos;
use App\Model\CategoriaServico;
use App\Controller\NivelController;
use App\Model\Usuario;
use App\Controller\LoginController;
use Verot\Upload;

class ServicosController
{
	private $alerta;
    private $controller;

    public function __construct()
    {
        (new LoginController)->usuarioLongado();
        $this->controller =  get_class($this);
    }

    public function index()
    {
        $ServicosLista = new Servicos();
        $ServicosLista = $ServicosLista->listaTodos(1);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/servicos/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function novo()
    {
        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/servicos/novo.php';
        require APP . 'view/templates/footer.php';
    }
    
    public function desativado()
    {
        $Servicos   = new Servicos();
        $servicos   = $Servicos->getAllServicos(0);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/servicos/desativados.php';
        require APP . 'view/templates/footer.php';
    }
    
    public function ativarDesativar($boleano)
    {
        $id         = $_POST['id'];
        $Servicos   = new Servicos();
        $servicos   = $Servicos->ativarDesativar($id,$boleano);
        echo json_decode($servicos);
    }

    public function editar($id)
    {
        $Servico 		= new Servicos();
        $ServicoEditar = $Servico->lista($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/servicos/editar.php';
        require APP . 'view/templates/footer.php';
    }

    public function desativar()
    {
		$id 				= $_POST['id'];
        $Servicos 			= new Servicos();
        $desativarServicos 	= $Servicos->desativar($id);
        echo json_decode($desativarServicos);
    }
    

    public function atualizar($id)
    {
        $imagem = $_FILES['servicoFoto'];

        $Servicos       = new Servicos();
        $servicos       = $Servicos->lista($id);

        $imgBanco = $servicos[0]->servicoFoto;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;

        if (!empty($imgInput)) {//Se não estiver vazia

            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/servico');

            if ($handle->uploaded) {
                $handle->image_resize         	= true;
                $handle->image_x              	= 100;
                $handle->image_ratio_y        	= true;
                $handle->file_safe_name 		= false;
                $handle->file_name_body_add 	= '_mini';
                $handle->process($diretorio_destino);
            }

            if ($handle->uploaded) {
                $handle->image_resize         	= true;
                $handle->image_x              	= 680;
                $handle->image_ratio_y        	= true;
                $handle->process($diretorio_destino);
            }

            $img = $handle->file_src_name;
            
        }else{
            $img    = $imgBanco;
        }
		
        $atualizarServicos 	= $Servicos->atualizar(	$id,
                                                    $_POST['servicoNome'], 
                                                    $_POST['servicoDescricao'], 
                                                    $_POST['servicoValor'], 
                                                    $_POST['servicoValorPromo'], 
                                                    $_POST['servicoComissao'], 
                                                    $_POST['servicoTempo'], 
                                                    $img, 
                                                    $_POST['servicoSessoes'],
													1,
                                                    1);
        echo json_encode($atualizarServicos);
    }

    public function inserir()
    {        
        $imagem = $_FILES['servicoFoto'];

        if (isset($imagem)) {

            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/servico');
            $handle = new \Verot\Upload\Upload($imagem);


            if ($handle->uploaded) {
                $handle->image_resize             	= true;
                $handle->image_x                  	= 100;
                $handle->image_ratio_y            	= true;
                $handle->file_safe_name         	= false;
                $handle->file_name_body_add     	= '_mini';
                $handle->process($diretorio_destino);
            }

            if ($handle->uploaded) {
                $handle->image_resize         		= true;
                $handle->image_x              		= 680;
                $handle->image_ratio_y        		= true;
                $handle->process($diretorio_destino);
            }

            $img = $handle->file_src_name;
        } else {
            $img = "sem-foto.jpg";
        }
		
		$Servicos 			= new Servicos();
        $inserirServicos 	= $Servicos->inserir(	$img,
													$_POST['servicoNome'], 
													$_POST['servicoDescricao'], 
													$_POST['servicoValor'], 
													$_POST['servicoValorPromo'], 
													$_POST['servicoComissao'], 
													$_POST['servicoTempo'], 
                                                    $_POST['servicoSessoes'],
													1,
													1);
        echo json_encode($inserirServicos);
    }

    public function inseririmg()
    {        
        $imagem = $_FILES['foto'];

        if (isset($imagem)) {

            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/servico');
            $handle = new \Verot\Upload\Upload($imagem);


            if ($handle->uploaded) {
                $handle->image_resize             	= true;
                $handle->image_x                  	= 100;
                $handle->image_ratio_y            	= true;
                $handle->file_safe_name         	= false;
                $handle->file_name_body_add     	= '_mini';
                $handle->process($diretorio_destino);
            }

            if ($handle->uploaded) {
                $handle->image_resize         		= true;
                $handle->image_x              		= 680;
                $handle->image_ratio_y        		= true;
                $handle->process($diretorio_destino);
            }

            $img = "servico/".$handle->file_src_name;
        } else {
            $img = "sem-foto.jpg";
        }
		
		$Servicos 			= new Servicos();
        $inserirServicos 	= $Servicos->inseririmg(
                                                    $_POST['idServico'], 
                                                    $_POST['fotoTitulo'],
													$_POST['fotoDescricao'],
                                                	$img);
        echo json_encode($inserirServicos);
    }

	
	/* ############################ DETALHE DO SERVIÇO ########################### */
	public function detalhe($id)
    {
        $Detalhe 		= new Servicos();
        $detalheLista 	= $Detalhe->getDetalheServico($id);
		$fotoLista 		= $Detalhe->getfotoServico($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/servicos/detalhe.php';
        require APP . 'view/templates/footer.php';
    }

    public function deleteFoto()
    {
        $Detalhe 		= new Servicos();
        $detalhe 	= $Detalhe->deleteFoto($_POST['id']);
        echo json_encode($detalhe);
    }
}

