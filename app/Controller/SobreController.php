<?php

namespace App\Controller;

use App\Model\Unidade;

class SobreController
{
    public function index()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");       

		$UnidadeLista = new Unidade();
        $unidadeLista = $UnidadeLista->listaTodosEquipamento(1);

        require APP . 'view/site/head.php';
        require APP . 'view/templates/header-front.php';
        require APP . 'view/site/sobre.php';
		require APP . 'view/templates/footer-front.php';
    }
}
