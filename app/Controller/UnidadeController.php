<?php
namespace App\Controller;

use App\Controller\NivelController;
use App\Controller\EmailController;
use App\Model\Unidade;
use App\Model\Cliente;
use App\Model\Funcionario;
use App\Model\Agenda;
use Respect\Validation\Rules\Length;
use Verot\Upload;



class UnidadeController
{
	private $tipo;

/*	
#####################################################################
#############											#############
#############					UNIDADE					#############
#############											#############
#####################################################################
*/
    public function __construct()
    {
        (new LoginController)->usuarioLongado();
    }

/* ######## LISTAR TODAS AS UNIDADES ########## */
    public function index()
    {
        $UnidadeLista 			= new Unidade();
		$unidadeLista 			= $UnidadeLista->listaTodosUnidade(1);
		
		$Cliente 				= new Cliente();
        $unidadeCliente 		= $Cliente->getAmountOfCliente();
		
		$Funcionario 			= new Funcionario();
        $unidadeFunc 			= $Funcionario->getAmountOfFuncionario();
		

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/unidade/index.php';
        require APP . 'view/templates/footer.php';
	}
	
	public function desativados()
    {
        $UnidadeLista 			= new Unidade();
		$unidadeLista 			= $UnidadeLista->listaTodosUnidade(0);
		
		$Agenda 				= new Agenda();
        $unidadeAgenda 			= $Agenda->qtdAgendamento();
		
		$Cliente 				= new Cliente();
        $unidadeCliente 		= $Cliente->getAmountOfCliente();
		
		$Funcionario 			= new Funcionario();
        $unidadeFunc 			= $Funcionario->getAmountOfFuncionario();
		

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/unidade/desativados.php';
        require APP . 'view/templates/footer.php';
    }

/* ######## CARREGAR PÁGINA NOVA UNIDADE PELO ID ########## */	
    public function novo()
    {
        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/unidade/novo.php';
        require APP . 'view/templates/footer.php';
    }
	
/* ######## CARREGAR PÁGINA EDITAR E LISTAR DADOS DA UNIDADE PELO ID ########## */
    public function editar($id)
    {
        $unidadeLista 			= new Unidade();
        $unidadeLista 			= $unidadeLista->lista($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/unidade/editar.php';
        require APP . 'view/templates/footer.php';

    }

/* ######## ATUALIZAR DADOS DA UNIDADE PELO ID ########## */
    public function atualizar($id)
    {
        
        $imagemFoto 		= $_FILES['unidadeFoto'];
        $novoNomeImg 	    = $_POST['unidadeCNPJ'];
        
        $UnidadeLista 			= new Unidade();
        $unidadeLista 			= $UnidadeLista->lista($id);
        $fotoBanco = $unidadeLista[0]->unidadeFoto;

		$handleFoto              = new \Verot\Upload\Upload($imagemFoto);
        $imgInput = $handleFoto->file_src_name;

        if (!empty($imgInput)) {

			$diretorio_destinoFoto   = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/unidade');
			

			if ($handleFoto->uploaded) {
				$handleFoto->image_resize         	= true;
				$handleFoto->image_x              	= 100;
				$handleFoto->image_ratio_y        	= true;
				$handleFoto->file_new_name_body 	= '_mini';
				$handleFoto->process($diretorio_destinoFoto);
			}

			if ($handleFoto->uploaded) {
				$handleFoto->image_resize         	= true;
				$handleFoto->image_x              	= 400;
				$handleFoto->image_ratio_y        	= true;			
				$handleFoto->process($diretorio_destinoFoto);
			}

			$imgFoto = $handleFoto->file_src_name;
			//$imgFoto = preg_replace('/[[:space:]]+/', '-', $imgFoto);
		
		} else {
			
			$imgFoto = $fotoBanco;
        }
        //var_dump($unidadeLista);
        //echo $imgFoto;
        
        $UnidadeAtualizar 		= new Unidade();
        $atualizarUnidade   	= $UnidadeAtualizar->atualizar(	$id,
																$_POST['unidadeNomeEmpresa'], 
                                                                $_POST['unidadeRazaoSocial'], 
                                                                $_POST['unidadeCNPJ'], 
                                                                $_POST['unidadeFoneUm'], 
                                                                $_POST['unidadeFoneDois'], 
                                                                $_POST['unidadeFoneTres'], 
                                                                $_POST['unidadeEnd'], 
                                                                $_POST['unidadeEndRua'], 
                                                                $_POST['unidadeEndComp'], 
                                                                $_POST['unidadeEndBairro'], 
                                                                $_POST['unidadeEndCidade'], 
                                                                $_POST['unidadeCEP'], 
                                                                $_POST['unidadePessoaContato'], 
                                                                $_POST['unidadeEmail'],  
                                                                $imgFoto, 
                                                                $_POST['unidadeSite'], 
                                                                $_POST['unidadeFace'], 
                                                                $_POST['unidadeInsta'], 
                                                                $_POST['unidadeTwitter'], 
                                                                $_POST['unidadeYouTUBE'], 
                                                                1);
        echo json_encode($atualizarUnidade);    
    }
		
/* ######## INSERIR DADOS DA UNIDADE PELO ID ########## */
    public function inserirUnidade()
    {
        
        $imagemFoto 		= $_FILES['unidadeFoto'];
		$novoNomeImg 	    = $_POST['unidadeCNPJ'];
		

        if (isset($imagemFoto)) {

			$diretorio_destinoFoto   = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/unidade');
			$handleFoto              = new \Verot\Upload\Upload($imagemFoto);

			if ($handleFoto->uploaded) {
				$handleFoto->image_resize         	= true;
				$handleFoto->image_x              	= 100;
				$handleFoto->image_ratio_y        	= true;
				$handleFoto->image_convert       	= 'jpg';
				$handleFoto->file_new_name_body 	= $novoNomeImg.'_foto_mini';
				$handleFoto->process($diretorio_destinoFoto);
			}

			if ($handleFoto->uploaded) {
				$handleFoto->image_resize         	= true;
				$handleFoto->image_x              	= 400;
				$handleFoto->image_ratio_y        	= true;
				$handleFoto->image_convert       	= 'jpg';
				$handleFoto->file_new_name_body 	= $novoNomeImg.'_foto';				
				$handleFoto->process($diretorio_destinoFoto);
			}

			$imgFoto = $handleFoto->file_new_name_body 	= $novoNomeImg.'_foto';
			$imgFoto = preg_replace('/[[:space:]]+/', '-', $imgFoto);
			
			
		} else {
			
			$imgFoto = "local";
		}        
        
        $Unidade 			= new Unidade();
        $inserirUnidade 	= $Unidade->inserirUnidade(	$_POST['unidadeNomeEmpresa'], 
														$_POST['unidadeRazaoSocial'], 
														$_POST['unidadeCNPJ'], 
														$_POST['unidadeFoneUm'], 
														$_POST['unidadeFoneDois'], 
														$_POST['unidadeFoneTres'], 
														$_POST['unidadeEnd'], 
														$_POST['unidadeEndRua'], 
														$_POST['unidadeEndComp'], 
														$_POST['unidadeEndBairro'], 
														$_POST['unidadeEndCidade'], 
														$_POST['unidadeCEP'], 
														$_POST['unidadePessoaContato'], 
														$_POST['unidadeEmail'],  
                                                        $imgFoto,
														$_POST['unidadeSite'], 
														$_POST['unidadeFace'], 
														$_POST['unidadeInsta'], 
														$_POST['unidadeTwitter'], 
														$_POST['unidadeYouTUBE'], 
														"1");
        echo json_encode($inserirUnidade);
    }
	
/*	
#####################################################################
#############											#############
#############				SALA UNIDADE				#############
#############											#############
#####################################################################
*/

/* ######## LISTAR TODAS AS SALA DA UNIDADES ########## */
    public function sala()
    {
        $UnidadeLista 			= new Unidade();
		$salaUnidadeLista 		= $UnidadeLista->listaTodosSala(1);		

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/unidade/sala.php';
        require APP . 'view/templates/footer.php';
	}
	
	public function saladesativados()
    {
        $UnidadeLista 			= new Unidade();
		$salaUnidadeLista 		= $UnidadeLista->listaTodosSala(0);		

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/unidade/sala-desativados.php';
        require APP . 'view/templates/footer.php';
    }

/* ######## CARREGAR PÁGINA NOVA UNIDADE PELO ID ########## */	
    public function novoSala()
    {
        $UnidadeLista 			= new Unidade();
        $unidadeLista 			= $UnidadeLista->listaTodosUnidade(1);
        
        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/unidade/novoSala.php';
        require APP . 'view/templates/footer.php';
    }
	
/* ######## CARREGAR PÁGINA EDITAR E LISTAR DADOS DA UNIDADE PELO ID ########## */
    public function editarSala($id)
    {
        $UnidadeLista 			= new Unidade();
        $unidadeLista 			= $UnidadeLista->listaTodosUnidade(1);
        $salaUnidadeLista 		= $UnidadeLista->listaSalaUnidade($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/unidade/editarSala.php';
        require APP . 'view/templates/footer.php';

	}
	
	public function ativarDesativar($boleano)
    {
        $id         = $_POST['id'];
        $Servicos   = new Unidade();
        $servicos   = $Servicos->ativarDesativar($id,$boleano);
        echo json_decode($servicos);
    }

/* ######## ATUALIZAR DADOS DA SALA PELO ID ########## */
    public function atualizarSala($id)
    {
        
        $imagemFoto 		= $_FILES['salaFotoUnidade'];
		$novoNomeImg 	    = $_POST['salaNomeUnidade'];

		$UnidadeAtualizar 		= new Unidade();
		$salaUnidadeLista 		= $UnidadeAtualizar->listaSalaUnidade($id);

		$handleFoto              = new \Verot\Upload\Upload($imagemFoto);
		$imgInput = $handleFoto->file_src_name;

        if (!empty($imgInput)) {

			$diretorio_destinoFoto   = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/sala');
			

			if ($handleFoto->uploaded) {
				$handleFoto->image_resize         	= true;
				$handleFoto->image_x              	= 100;
				$handleFoto->image_ratio_y        	= true;
				$handleFoto->file_new_name_body 	= '_mini';
				$handleFoto->process($diretorio_destinoFoto);
			}

			if ($handleFoto->uploaded) {
				$handleFoto->image_resize         	= true;
				$handleFoto->image_x              	= 400;
				$handleFoto->image_ratio_y        	= true;			
				$handleFoto->process($diretorio_destinoFoto);
			}

			$imgFoto = $handleFoto->file_src_name;
			//$imgFoto = $handleFoto->file_new_name_body 	= $novoNomeImg;
			//$imgFoto = preg_replace('/[[:space:]]+/', '-', $imgFoto);
			
		
		} else {
			
			$imgFoto = $salaUnidadeLista[0]->salaFotoUnidade;
		}
        
        
        $atualizarSala 			= $UnidadeAtualizar->atualizarSala(	$id,
                                                                    $_POST['idUnidade'], 
                                                                    $_POST['salaNomeUnidade'], 
                                                                    $_POST['salaDescricaoUnidade'], 
                                                                    $imgFoto);
        echo json_encode($atualizarSala);    
    }
	
/* ######## INSERIR DADOS DA SALA PELO ID ########## */
    public function inserirSala()
    {
        $imagemFoto 		= $_FILES['salaFotoUnidade'];
		$novoNomeImg 	    = $_POST['salaNomeUnidade'];
		

        if (isset($imagemFoto)) {

			$diretorio_destinoFoto   = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/sala');
			$handleFoto              = new \Verot\Upload\Upload($imagemFoto);

			if ($handleFoto->uploaded) {
				$handleFoto->image_resize         	= true;
				$handleFoto->image_x              	= 100;
				$handleFoto->image_ratio_y        	= true;
				$handleFoto->file_new_name_body 	= '_mini';
				$handleFoto->process($diretorio_destinoFoto);
			}

			if ($handleFoto->uploaded) {
				$handleFoto->image_resize         	= true;
				$handleFoto->image_x              	= 400;
				$handleFoto->image_ratio_y        	= true;			
				$handleFoto->process($diretorio_destinoFoto);
            }
            
            $imgFoto = $handleFoto->file_src_name;
			//$imgFoto = $handleFoto->file_new_name_body 	= $novoNomeImg;
			//$imgFoto = preg_replace('/[[:space:]]+/', '-', $imgFoto);
			
			
		} else {
			
			$imgFoto = "local";
		}
        
        $Unidade 			= new Unidade();
        $inserirUnidadeSala = $Unidade->inserirSala($_POST['idUnidade'], 
													$_POST['salaNomeUnidade'], 
													$_POST['salaDescricaoUnidade'], 
                                                    $imgFoto,
                                                    1);
        echo json_encode($inserirUnidadeSala);
	} 

	public function ativarDesativarSala($boleano)
    {
        $id         = $_POST['id'];
        $Servicos   = new Unidade();
        $servicos   = $Servicos->ativarDesativarSala($id,$boleano);
        echo json_decode($servicos);
    }
	
    
/*	
#####################################################################
#############											#############
#############			EQUIPAMENTO UNIDADE				#############
#############											#############
#####################################################################
*/
	
/* ######## LISTAR TODOS OS EQUIPAMENTO ########## */
    public function equipamento()
    {
        $unidadeLista 			= new Unidade();
		$equipamentoLista 		= $unidadeLista->listaTodosEquipamento(1);
		

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/unidade/equipamento.php';
        require APP . 'view/templates/footer.php';
	}
	
	public function equipamentoDesativados()
    {
        $unidadeLista 			= new Unidade();
		$equipamentoLista 		= $unidadeLista->listaTodosEquipamento(0);
		

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/unidade/equipamento-desativados.php';
        require APP . 'view/templates/footer.php';
    }

/* ######## CARREGAR PÁGINA NOVO EQUIPAMENTO PELO ID ########## */	
    public function novoEquipamento()
    {
        $UnidadeLista 			= new Unidade();
        $unidadeLista 			= $UnidadeLista->listaTodosUnidade(1);
        
        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/unidade/novoEquipamento.php';
        require APP . 'view/templates/footer.php';
    }
	
/* ######## CARREGAR PÁGINA EDITAR E LISTAR EQUIPAMENTO PELO ID ########## */
    public function editarEquipamento($id)
    {
        $UnidadeLista 			= new Unidade($id);
        $unidadeLista 			= $UnidadeLista->lista($id);
        $equipamentoLista 		= $UnidadeLista->listaEquipamentoUnidade($id);


        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/unidade/editarEquipamento.php';
        require APP . 'view/templates/footer.php';

    }

/* ######## ATUALIZAR DADOS DO EQUIPAMENTO PELO ID ########## */
    public function atualizarEquipamento($id)
    {
		$UnidadeAtualizar 		= new Unidade();
		$unidadeLista 			= $UnidadeAtualizar->listaEquipamentoUnidade($id);
		
        $imagemFoto 		= $_FILES['equipamentoFoto'];
		$novoNomeImg 	    = $_POST['equipamentoNome'];

		$handleFoto              = new \Verot\Upload\Upload($imagemFoto);
		$imgInput = $handleFoto->file_src_name;

        if (!empty($imgInput)) {

			$diretorio_destinoFoto   = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/equipamento');

			if ($handleFoto->uploaded) {
				$handleFoto->image_resize         	= true;
				$handleFoto->image_x              	= 100;
				$handleFoto->image_ratio_y        	= true;
				$handleFoto->file_new_name_body 	= '_mini';
				$handleFoto->process($diretorio_destinoFoto);
			}

			if ($handleFoto->uploaded) {
				$handleFoto->image_resize         	= true;
				$handleFoto->image_x              	= 400;
				$handleFoto->image_ratio_y        	= true;		
				$handleFoto->process($diretorio_destinoFoto);
			}
			$imgFoto = $handleFoto->file_src_name;
			//$imgFoto = $handleFoto->file_new_name_body 	= $novoNomeImg;
			//$imgFoto = preg_replace('/[[:space:]]+/', '-', $imgFoto);
			
		} else {
			$imgFoto = $unidadeLista[0]->equipamentoFoto;
		}

        
        $UnidadeAtualizar 		= new Unidade();
        $atualizarEquipamento 	= $UnidadeAtualizar->atualizarEquipamento(	$id,
                                                                            $_POST['idUnidade'], 
                                                                            $_POST['equipamentoNome'], 
                                                                            $_POST['equipamentoDescricao'], 
                                                                            $imgFoto,
																			1);
																		
		echo json_encode($atualizarEquipamento);  
		  
    }
	
/* ######## CADASTRO DADOS DA UNIDADE PELO ID ########## */
    public function inserirEquipamento()
    {
        $imagemFoto 		= $_FILES['equipamentoFoto'];
		$novoNomeImg 	    = $_POST['equipamentoNome'];
		

        if (isset($imagemFoto)) {

			$diretorio_destinoFoto   = (isset($_GET['dir']) ? $_GET['dir'] : 'assets/img/equipamento');
			$handleFoto              = new \Verot\Upload\Upload($imagemFoto);

			if ($handleFoto->uploaded) {
				$handleFoto->image_resize         	= true;
				$handleFoto->image_x              	= 100;
				$handleFoto->image_ratio_y        	= true;
				$handleFoto->file_new_name_body 	= '_mini';
				$handleFoto->process($diretorio_destinoFoto);
			}

			if ($handleFoto->uploaded) {
				$handleFoto->image_resize         	= true;
				$handleFoto->image_x              	= 400;
				$handleFoto->image_ratio_y        	= true;			
				$handleFoto->process($diretorio_destinoFoto);
			}

			$imgFoto = $handleFoto->file_src_name;
			//$imgFoto = preg_replace('/[[:space:]]+/', '-', $imgFoto);
			
			
		} else {
			
			$imgFoto = "equipamento";
		}
        
        $Unidade 				= new Unidade();
        $inserirEquipamento 	= $Unidade->inserirEquipamento(	$_POST['idUnidade'],
                                                                $_POST['equipamentoNome'], 
																$_POST['equipamentoDescricao'], 
																$imgFoto, 
                                                                1,
                                                                1);
        echo json_encode($inserirEquipamento);
	}	
	
	public function ativarDesativarEquipamento($boleano)
    {
        $id         = $_POST['id'];
        $Servicos   = new Unidade();
        $servicos   = $Servicos->ativarDesativarEquipamento($id,$boleano);
        echo json_decode($servicos);
    }

	
/*	
#####################################################################
#############											#############
#############				UNIDADE DETALHE				#############
#############											#############
#####################################################################
*/
	

/* ################# DETALHE DA UNIDADE - SALAS ###################### */
	public function detalhe($id)
    {
        $DetalheUnidade 			= new Unidade();
        $detalheLista 				= $DetalheUnidade->getDetalheUnidade($id);
		$detalheSala	 			= $DetalheUnidade->getSalasUnidade($id);
		$listarEquipamento 			= $DetalheUnidade->geteEquipamentoUnidade($id);

        require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/unidade/detalhe.php';
        require APP . 'view/templates/footer.php';
    }

}






