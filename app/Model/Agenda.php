<?php

namespace App\Model;

use App\Core\Model;

class Agenda extends Model
{
	public $idAgendamento = "TESTE";
	
/* ######## LISTAR TODOS OS AGENDAMENTO ########## */
    public function listaTodosAgendamento()
    {
        $sql = "SELECT agendamentos.*, cliente.cliNome, cliente.cliCel, funcionario.idFunc, funcionario.funcNome, funcionario.funcComissao  FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE agendamentos.agendaData >= CURDATE() ORDER BY agendamentos.agendaData ASC";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
    
    public function listaServicoAgendamento($idAgenda)
    {
        $sql = "SELECT servicoValor FROM agendamentos INNER JOIN itensDaAgenda ON itensDaAgenda.idAgenda = agendamentos.idAgenda INNER JOIN servicos ON servicos.idServico = itensDaAgenda.idServico WHERE agendamentos.idAgenda = $idAgenda";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaPacoteAgendamento($idAgenda)
    {
        $sql = "SELECT pacotevalor FROM agendamentos INNER JOIN itensDaAgenda ON itensDaAgenda.idAgenda = agendamentos.idAgenda INNER JOIN pacoteservico ON pacoteservico.idPacoteServico = itensDaAgenda.idPacoteServico WHERE agendamentos.idAgenda = $idAgenda";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

/* ######## CONTAGEM DE AGENDAMENTO ########## */
    public function qtdAgendamento()
    {
        $sql = "SELECT COUNT(idAgenda) AS amount_of_agendamentos FROM agendamentos WHERE agendaData = CURDATE() ";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetch()->amount_of_agendamentos;
    }

    public function qtdAgendamentoMes()
    {
        $sql = "SELECT COUNT(idAgenda) AS amount_of_agendamentos FROM agendamentos WHERE Month(agendaData) = Month(Now())";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetch()->amount_of_agendamentos;
    }
/* ######## CONTAGEM DE AGENDAMENTO ########## */
	public function agendamentoDia()
    {
        $sql = "SELECT agendamentos.*, cliente.cliNome, cliente.cliCel, funcionario.idFunc, funcionario.funcNome, funcionario.funcComissao FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE agendaData BETWEEN CURRENT_DATE() AND CURRENT_DATE()";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        return $query->fetchAll();
    }
	
	
	public function agendamentoSemana()
    {
        $sql = "SELECT agendamentos.*, cliente.cliNome, cliente.cliCel, funcionario.idFunc, funcionario.funcNome, funcionario.funcComissao FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE YEARWEEK(agendaData) = YEARWEEK(NOW() - INTERVAL 0 WEEK) ORDER BY agendamentos.agendaData ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        return $query->fetchAll();
    }

    public function agendamentoMes()
    {
        $sql = "SELECT agendamentos.*, cliente.cliNome, cliente.cliCel, funcionario.idFunc, funcionario.funcNome, funcionario.funcComissao FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE Month(agendaData) = Month(Now()) ORDER BY agendaData ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        return $query->fetchAll();
    }

    public function compromissoDia($id)
    {          //"SELECT cliente.cliNome, agendamentos.* FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE agendamentos.idFunc = $id AND Month(agendaData) = Month(Now()) ORDER BY agendamentos.agendaData ASC"
        $sql = "SELECT cliente.cliNome, agendamentos.* FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE agendamentos.idFunc = $id AND agendaData = CURRENT_DATE() ORDER BY agendamentos.agendaData ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        return $query->fetchAll();
    }

    public function compromissoSemana($id)
    {          //"SELECT cliente.cliNome, agendamentos.* FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE agendamentos.idFunc = $id AND Month(agendaData) = Month(Now()) ORDER BY agendamentos.agendaData ASC"
        $sql = "SELECT cliente.cliNome, agendamentos.* FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE agendamentos.idFunc = $id AND YEARWEEK(agendaData) = YEARWEEK(NOW() - INTERVAL 0 WEEK) ORDER BY agendamentos.agendaData ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        return $query->fetchAll();
    }

    public function compromissoMes($id)
    {          //"SELECT cliente.cliNome, agendamentos.* FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE agendamentos.idFunc = $id AND Month(agendaData) = Month(Now()) ORDER BY agendamentos.agendaData ASC"
        $sql = "SELECT cliente.cliNome, agendamentos.* FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE agendamentos.idFunc = $id AND Month(agendaData) = Month(Now()) ORDER BY agendamentos.agendaData ASC, agendamentos.agendaHora ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        return $query->fetchAll();
    }
    
/* ######## CONSULTAR AGENDAMENTO POR NOME CLIENTE ########## */
	public function consultaNome($idAgenda)
    {
        $sql = "SELECT * FROM agendamentos WHERE idAgenda LIKE '%$idAgenda%'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
/* ######## CONSULTAR AGENDAMENTO POR CPF CLIENTE ########## */
	public function consultaCPF($idAgenda)
    {
        $sql = "SELECT * FROM agendamentos WHERE idAgenda LIKE '%$idAgenda%'";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

/* ######## VERIFICAR SE EXITE FICHA ########## */
    public function ExiteFichaFacial($idCliente, $idFunc)
    {
        $sql = "SELECT idTratamento, fichaFacilData, fichaFacilQtdSessoes, tratamentoQtd FROM `tratamento` INNER JOIN fichaFacil on fichaFacil.idFichaFacil = tratamento.idFichaFacil WHERE fichaFacil.idFichaGeral = (SELECT idFichaGeral FROM `fichageral` WHERE idCliente = $idCliente ORDER BY idFichaGeral DESC LIMIT 1) && fichaFacil.idFunc = $idFunc";
        //$sql = "SELECT idFichaFacil, fichaFacilData FROM `fichaFacil` WHERE idFichaGeral = (SELECT idFichaGeral FROM `fichageral` WHERE idCliente = $idCliente ORDER BY idFichaGeral DESC LIMIT 1) && idFunc = $idFunc ORDER BY idFichaFacil DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function ExiteFichaCorporal($idCliente, $idFunc)
    {
        $sql = "SELECT idTratamento, fichaCorporalData, fichaCorporalQtdSessoes, tratamentoQtd FROM `tratamento` INNER JOIN fichaCorporal on fichaCorporal.idFichaCorporal = tratamento.idFichaCorporal WHERE fichaCorporal.idFichaGeral = (SELECT idFichaGeral FROM `fichageral` WHERE idCliente = $idCliente ORDER BY idFichaGeral DESC LIMIT 1) && fichaCorporal.idFunc = $idFunc";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
    
    public function ExiteFichaCriolipolise($idCliente, $idFunc)
    {
        $sql = "SELECT idTratamento, fichaAnamneseData, fichaAnamneseQtdSessoes, tratamentoQtd FROM `tratamento` INNER JOIN fichaAnamneseCriolipolise on fichaAnamneseCriolipolise.idfichaAnamneseCriolipolise = tratamento.idfichaAnamneseCriolipolise WHERE fichaAnamneseCriolipolise.idFichaGeral = (SELECT idFichaGeral FROM `fichageral` WHERE idCliente = $idCliente ORDER BY idFichaGeral DESC LIMIT 1) && fichaAnamneseCriolipolise.idFunc = $idFunc";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function qtdTratamento($idTratamento)
    {
        $sql = "SELECT tratamentoQtd FROM `tratamento` WHERE idTratamento = $idTratamento";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function atualizarTratamento($idTratamento,$qtdSessoes)
    {
        $sql = "UPDATE `tratamento` SET `tratamentoQtd`= $qtdSessoes WHERE idTratamento = $idTratamento";
        $query = $this->db->prepare($sql);
        $query->execute();
        
    }
	
/* ######## LISTAR AGENDAMENTO PELO ID DA AGENDA ########## */
	public function lista($id)
    {
        $sql = "SELECT agendamentos.*, cliente.idCliente, cliente.cliNome, cliente.cliCPF, funcionario.funcNome, funcionario.idFunc FROM agendamentos
        INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente
        INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE agendamentos.idAgenda = $id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaIdCliente($id)
    {
        $sql = "SELECT agendamentos.*, funcionario.*, cliente.cliNome FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE agendamentos.idCliente = $id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
    
    public function visualizardata($id, $data)
    {
        $sql = "SELECT * FROM `agendamentos` WHERE idFunc = $id AND agendaData = '$data'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaAgendamentoFunc($id)
    {
        $sql = "SELECT cliente.cliNome, agendamentos.* FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE agendamentos.idFunc = $id AND agendaData >= CURDATE() ORDER BY agendamentos.agendaData ASC, agendamentos.agendaHora ASC";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaIdFunc($id)
    {
        $sql = "SELECT 	agendamentos.*, 
        cliente.*, 
        FROM agendamentos 
        INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente  WHERE agendamentos.idFunc = $id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
/* ######## ATUALIZAR DADOS DO AGENDAMENTO ########## */
    public function atualizarRegistro(	$id,                            
                                        $agendaData, 
										$agendaHora, 
										$idCliente, 
										$agendaFormaPG, 
										$idFunc, 
                                        $agendaStatus,
                                        $agendaMensagem)
    {
        $sql = "UPDATE agendamentos set  	agendaData 		= '".$agendaData."', 
											agendaHora 		= '".$agendaHora."',
											idCliente 		= '".$idCliente."', 
											agendaFormaPG 	= '".$agendaFormaPG."', 
											idFunc 			= '".$idFunc."', 
                                            agendaStatus 	= '".$agendaStatus."',
                                            agendaMensagem 	= '".$agendaMensagem."' WHERE idAgenda = ".$id;
		
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        } 
    }

    public function retornaIdServicoEIdPacote($idCliente, $idFunc, $idTratamento)
    {
        $sql = "SELECT idServico, idPacoteServico FROM `itensDaAgenda` WHERE idAgenda = (SELECT idAgenda FROM `itensDaAgenda` WHERE idAgenda = (SELECT idAgenda FROM agendamentos WHERE agendaData = (SELECT fichaFacilData from fichaFacil WHERE idFichaFacil = (SELECT idFichaFacil FROM `tratamento` WHERE idTratamento = $idTratamento)) && idFunc = $idFunc && idCliente = $idCliente))";
		
        $query = $this->db->prepare($sql);    

        $query->execute();

        return $query->fetchAll();
    }

    public function atualizarTratamentoo($id, $idTratamento)
    {
        $sql = "UPDATE agendamentos set idTratamento = '".$idTratamento."' WHERE idAgenda = ".$id;
		
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        } 
    }
	
	/* ######## CADASTRO AGENDA ########## */
	public function cadastro(	$agendaData, 
								$agendaHora, 
								$idCliente, 
								$idUnidade,  
								$agendaFormaPG, 
								$idFunc, 
								$idPacoteServico, 
								$agendaStatus)
    {
        $sql = "INSERT INTO agendamentos (	agendaData, 
											agendaHora, 
											idCliente, 
											idUnidade,  
											agendaFormaPG, 
											idFunc, 
											idPacoteServico, 
											agendaStatus) VALUES (	:agendaData, 
																	:agendaHora, 
																	:idCliente, 
																	:idUnidade, 
																	:agendaFormaPG, 
																	:idFunc, 
																	:idPacoteServico, 
																	:agendaStatus)";
        $query = $this->db->prepare($sql);
        $parameters = array(	':agendaData' 		=> $agendaData, 
								':agendaHora' 		=> $agendaHora, 
								':idCliente' 		=> $idCliente, 
								':idUnidade' 		=> $idUnidade, 
								':agendaFormaPG' 	=> $agendaFormaPG, 
								':idFunc' 			=> $idFunc, 
								':idPacoteServico' 	=> $idPacoteServico, 
								':agendaStatus' 	=> $agendaStatus);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return array('id'=> $this->db->lastInsertId(),'boleano'=> 1);
        }else{
            return false;
        }
    }
	
	
	/* ######## DESATIVAR CLIENTE ########## */
	public function desativar($id)
    {
        $sql = "UPDATE cliente SET cliStatus = 'Desativado' WHERE cliente.idCliente = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
	
	
	/* ######## DIFERENÇA DO METODO CADASTRO PARA INSERIR ????? ########## */
	public function inserir($agendaData, 
							$agendaHora, 
							$idCliente, 
							$agendaFormaPG, 
							$idFunc, 
							$agendaMensagem, 
							$agendaStatus)
    {
        $sql = "INSERT INTO agendamentos (	agendaData, 
										agendaHora, 
										idCliente, 
										agendaFormaPG, 
										idFunc, 
										agendaMensagem,
										agendaStatus) VALUES (	:agendaData, 
																:agendaHora, 
																:idCliente, 
																:agendaFormaPG, 
																:idFunc, 
																:agendaMensagem,
																:agendaStatus)";
        $query = $this->db->prepare($sql);
        $parameters = array(':agendaData' => $agendaData, 
							':agendaHora' => $agendaHora, 
							':idCliente' => $idCliente, 
							':agendaFormaPG' => $agendaFormaPG, 
							':idFunc' => $idFunc, 
							':agendaMensagem' => $agendaMensagem,
							':agendaStatus' => $agendaStatus);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return array('id'=> $this->db->lastInsertId(),'boleano'=> 1);
        }else{
            return false;
        }
    }

    public function inserirServico($idAgenda,$idServico)
    {
        $sql = "INSERT INTO itensDaAgenda (	idAgenda, 
										    idServico) VALUES (	:idAgenda, 
																:idServico)";
        $query = $this->db->prepare($sql);
        $parameters = array(':idAgenda' => $idAgenda, 
							':idServico' => $idServico);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function inserirPacote($idAgenda,$idPacote)
    {
        $sql = "INSERT INTO itensDaAgenda (	idAgenda, 
										    idPacoteServico) VALUES (	:idAgenda, 
																:idPacoteServico)";
        $query = $this->db->prepare($sql);
        $parameters = array(':idAgenda' => $idAgenda, 
							':idPacoteServico' => $idPacote);

        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function retornaIdServico($nome)
    {
        $sql = "SELECT idServico FROM servicos WHERE servicoNome = '$nome'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function retornaIdPacote($nome)
    {
        $sql = "SELECT idPacoteServico FROM pacoteservico WHERE pacoteNome = '$nome'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	
	
	/* ######## ?????? ########## */
	public function img($id)
    {
        $sql = "SELECT cliFoto FROM cliente WHERE idCliente = $id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	/* ######## ?????? ########## */
	public function id($cliEmail)
    {
        $sql = "SELECT idCliente FROM cliente WHERE cliEmail = '$cliEmail'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	/* ######## ?????? ########## */
    public function listaClienteProfissional($id)
    {
        $sql = "SELECT cliente.nome_cliente,cliente.sobrenome,cliente.tipo_cadastro,cliente.img FROM cliente INNER JOIN reserva ON reserva.id_cliente=cliente.id WHERE reserva.id_usuario=$id ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

	/* ######## ?????? ########## */
    public function idRecuperar($id)
    {
        $sql = "SELECT id,recuperar FROM `cliente` WHERE id='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

	/* ######## ?????? ########## */
    public function recuperar($id, $recuperar)
    {
        $sql = "UPDATE cliente SET recuperar = '$recuperar' WHERE id = $id";
        $query = $this->db->prepare($sql);   
        $query->execute(); 
    }
	
	/* ######## ?????? ########## */
    public function idNomeImg($id)
    {
        $sql = "SELECT id,nome_cliente,img,recuperar FROM `cliente` WHERE id='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
    
	/* ######## ?????? ########## */
    public function limparaRecuperar($id)
    {
        $sql = "UPDATE cliente SET recuperar = '' WHERE id = $id";
        $query = $this->db->prepare($sql);   
        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    
	/* ######## ?????? ########## */
    public function nome($id)
    {
        $sql = "SELECT id,nome_cliente FROM `cliente` WHERE id='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

	/* ######## ?????? ########## */
    public function acessocliente($email)
    {
        $sql = "SELECT id,nome_cliente,senha, img FROM `cliente` WHERE email='$email'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
    

	/* ######## ?????? ########## */
    public function cadastroReserva($nome_cliente,$email, $whatsapp, $senha, $img, $tipo_cadastro, $data_cadastro, $verificacao)
    {
        $sql = "INSERT INTO cliente (nome_cliente,email, whatsapp, tipo_cadastro, data_cadastro, senha, img, verificacao) VALUES (:nome_cliente, :email, :whatsapp, :tipo_cadastro, :data_cadastro, :senha, :img, :verificacao)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome_cliente' => $nome_cliente, ':email' => $email, ':whatsapp' => $whatsapp, ':tipo_cadastro' => $tipo_cadastro, ':data_cadastro' => $data_cadastro, ':senha' => $senha, ':img' => $img, ':verificacao' => $verificacao);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return $this->db->lastInsertId();
        }else{
            return false;
        }
    }

	/* ######## ?????? ########## */
    public function existeEmail($email)
    {
        $sql = "SELECT id FROM `cliente` WHERE email='$email'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }	
	
}
