<?php

namespace App\Model;

use App\Core\Model;

class Caixa extends Model
{

	/* ######## LISTAR TODOS OS Caixa ########## */
    public function listaTodosCaixa()
    {
        $sql = "SELECT * FROM caixa WHERE caixaStatus = 1 ORDER BY idCaixa DESC";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	/* ######## CONTAGEM DOS REGISTRO NO CAIXA ########## */
	public function qtdOfCaixa()
    {
        $sql = "SELECT COUNT(idCaixa) AS amount_of_caixa FROM caixa";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetch()->amount_of_caixa;
    }

    /* ######## FILTRO DE CAIXA ########## */
	public function caixaDia()
    {
        $sql = "SELECT * FROM caixa WHERE caixa.caixaDataPagamento = CURRENT_DATE()";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        return $query->fetchAll();
    }
	
	
	public function caixaSemana()
    {
        $sql = "SELECT * FROM caixa WHERE YEARWEEK(caixaDataPagamento) = YEARWEEK(NOW() - INTERVAL 0 WEEK) ORDER BY caixa.caixaDataPagamento ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        return $query->fetchAll();
    }

    public function caixaMes()
    {
        $sql = "SELECT * FROM caixa WHERE Month(caixaDataPagamento) = Month(Now()) ORDER BY caixaDataPagamento ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        return $query->fetchAll();
    }

    public function caixaSemestre()
    { 
        $sql = "SELECT * FROM caixa WHERE caixaDataPagamento BETWEEN CURDATE() - INTERVAL 6 MONTH AND CURDATE() ORDER BY caixa.caixaDataPagamento ASC";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        return $query->fetchAll();
    }
	
	
	/* ######## TOTAL DE ENTRADAS NO CAIXA ########## */
	public function totalEntradaCaixa()
    {
        $sql = "SELECT SUM(caixaValor) AS totalEntrada FROM caixa WHERE caixaTipo = 'entrada' AND caixaStatus = 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetch()->totalEntrada;
    }
	
	
	/* ######## TOTAL DE SAÍDA NO CAIXA ########## */
	public function totalSaidaCaixa()
    {
        $sql = "SELECT SUM(caixaValor) AS totalSaida FROM caixa WHERE caixaTipo = 'saida' AND caixaStatus = 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetch()->totalSaida;
    }
	
	/* ######## CONSULTAR POR ENTRADA CAIXA ########## */
	public function consultaTipoEntrada($tipo)
    {
        $sql = "SELECT * FROM caixa WHERE cliNome = 'entrada'";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	/* ######## CONSULTAR POR SAÍDA CAIXA ########## */
	public function consultaTipoSaida($tipo)
    {
        $sql = "SELECT * FROM caixa WHERE caixaTipo = 'saida'";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	
	/* ######## LISTAR CAIXA PELO ID ########## */
	public function lista($id)
    {
        $sql = "SELECT * FROM caixa WHERE idCaixa = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	/* ######## ATUALIZAR DADOS DO CIXA ########## */
	public function atualizar(	$id,
								$caixaCategoria, 
								$caixaTipo, 
								$caixaVencimento, 
								$caixaValor, 
								$caixaObs, 
								$caixaDataPagamento)
    {
        $sql = "UPDATE caixa set  	caixaCategoria 		= '".$caixaCategoria."', 
									caixaTipo 			= '".$caixaTipo."', 
									caixaVencimento 	= '".$caixaVencimento."', 
									caixaValor 			= '".$caixaValor."', 
									caixaObs 			= '".$caixaObs."', 
									caixaDataPagamento 	= '".$caixaDataPagamento."' WHERE idCaixa = ".$id;
		
        $query = $this->db->prepare($sql);    
        if($query->execute()){
            return true;
        }else{
            return false;
        } 
    }
	
	/* ######## INSERIR CAIXA ########## */
	public function inserir(	$caixaCategoria, 
								$caixaTipo, 
								$caixaData, 
								$caixaVencimento, 
								$caixaValor, 
								$caixaObs, 
								$caixaStatus, 
								$caixaDataPagamento)
    {
        $sql = "INSERT INTO caixa (	caixaCategoria, 
									caixaTipo, 
									caixaData, 
									caixaVencimento, 
									caixaValor, 
									caixaObs, 
									caixaStatus, 
									caixaDataPagamento) VALUES (:caixaCategoria, 
																:caixaTipo, 
																:caixaData, 
																:caixaVencimento, 
																:caixaValor, 
																:caixaObs, 
																:caixaStatus, 
																:caixaDataPagamento)";
        $query = $this->db->prepare($sql);
        $parameters = array(':caixaCategoria' 		=> $caixaCategoria, 
							':caixaTipo' 			=> $caixaTipo, 
							':caixaData' 			=> $caixaData, 
							':caixaVencimento' 		=> $caixaVencimento, 
							':caixaValor' 			=> $caixaValor, 
							':caixaObs' 			=> $caixaObs, 
							':caixaStatus' 			=> $caixaStatus, 
							':caixaDataPagamento' 	=> $caixaDataPagamento);
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function comissaoAgendamento($idAgenda, 
                                        $caixaCategoria, 
                                        $caixaTipo, 
                                        $caixaData, 
                                        $caixaVencimento, 
                                        $caixaValor, 
                                        $caixaObs, 
                                        $caixaStatus, 
                                        $caixaDataPagamento)
    {
        $sql = "UPDATE agendamentos set agendaPG = 1 WHERE idAgenda = $idAgenda";
		
        $query = $this->db->prepare($sql);    

        if($query->execute())
        {
        
            $sql = "INSERT INTO caixa (	caixaCategoria, 
                                        caixaTipo, 
                                        caixaData, 
                                        caixaVencimento, 
                                        caixaValor, 
                                        caixaObs, 
                                        caixaStatus, 
                                        caixaDataPagamento) VALUES (:caixaCategoria, 
                                                                    :caixaTipo, 
                                                                    :caixaData, 
                                                                    :caixaVencimento, 
                                                                    :caixaValor, 
                                                                    :caixaObs, 
                                                                    :caixaStatus, 
                                                                    :caixaDataPagamento)";
            $query = $this->db->prepare($sql);
            $parameters = array(':caixaCategoria' 		=> $caixaCategoria, 
                                ':caixaTipo' 			=> $caixaTipo, 
                                ':caixaData' 			=> $caixaData, 
                                ':caixaVencimento' 		=> $caixaVencimento, 
                                ':caixaValor' 			=> $caixaValor, 
                                ':caixaObs' 			=> $caixaObs, 
                                ':caixaStatus' 			=> $caixaStatus, 
                                ':caixaDataPagamento' 	=> $caixaDataPagamento);
            if($query->execute($parameters)){
                return true;
            }else{
                return false;
            }
        } else {
            return false;
        }
    }
	
	/* ######## DESATIVAR ITEM DO CAIXA ########## */
	public function desativar($id)
    {
        $sql = "UPDATE caixa SET caixaStatus = 'desativado' WHERE caixa.idCaixa = $id";
        $query = $this->db->prepare($sql);
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
	
	
} /*################################ FIM CLASS ##############################*/
