<?php

namespace App\Model;

use App\Core\Model;

class Cliente extends Model
{

	/* ######## LISTAR TODOS OS CLIENTE ATIVOS ########## */
    public function listaTodos()
    {
        $sql = "SELECT * FROM cliente WHERE cliStatus = 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
	}

	public function listafiltrocpf($cpf)
    {
        $sql = "SELECT * FROM cliente WHERE cliCPF = $cpf";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
	}
	
	public function listaDesativados()
    {
        $sql = "SELECT * FROM cliente WHERE cliStatus = 0";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	/* ######## CONTAGEM DE CLIENTES ATIVOS  ########## */
	public function getAmountOfCliente()
    {
        $sql = "SELECT COUNT(idCliente) AS amount_of_cliente FROM cliente WHERE cliStatus = 1";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        return $query->fetch()->amount_of_cliente;
    }
	
	/* ######## CONSULTAR POR NOME CLIENTE ########## */
	public function consultaNome($cliNome)
    {
        $sql = "SELECT * FROM cliente WHERE cliNome LIKE '%$cliNome%'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	/* ######## CONSULTAR POR CPF CLIENTE ########## */
	public function consultaCPF($cliCPF)
    {
        $sql = "SELECT * FROM cliente WHERE cliCPF LIKE '%$cliCPF%'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	/* ######## RECUPERAR SENHA CLIENTE ########## */
	public function recuperarSenha($cliEmail)
    {
        $sql = "SELECT idCliente, cliNome, cliSenha FROM cliente WHERE cliEmail='$cliEmail'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
	}

	public function idNomeImg($id)
    {
        $sql = "SELECT idCliente,cliNome,cliFoto,CliRecuperar FROM `cliente` WHERE idCliente='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

	public function idRecuperar($id)
    {
        $sql = "SELECT idCliente,CliRecuperar FROM `cliente` WHERE idCliente='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	public function recuperar($id, $recuperar)
    {
        $sql = "UPDATE cliente SET cliRecuperar = '$recuperar' WHERE idCliente = $id";
        $query = $this->db->prepare($sql);   
        $query->execute(); 
    }
	
	/* ######## LISTAR CLIENTE PELO ID ########## */
	public function lista($id)
    {
        $sql = "SELECT * FROM cliente WHERE idCliente = $id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	/*################ RECEBER PERFIL DO CLIENTE PELO ID ##########################*/
    public function getPerfilCliente($cliente_id)
    {
        $sql = "SELECT * FROM cliente WHERE idCliente = :cliente_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array('cliente_id' => $cliente_id);
		$query->execute($parameters);
        return ($query->rowcount() ? $query->fetch() : false);
    }
	
	/* ######## ATUALIZAR SENHA CLIENTE ########## */
	public function atualizarSenha($id, $cliSenha)
    {
        $sql = "UPDATE cliente SET cliSenha = '$cliSenha' WHERE idCliente = $id";
        $query = $this->db->prepare($sql);   
        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
	}
	
	/* ######## ATUALIZAR DADOS DO CLIENTE ########## */
	public function atualizar(	$idCliente, 
								$cliNome, 
								$cliCPF, 
								$cliRG, 
								$cliEstadoCivil, 
								$cliEmail, 
								$cliSenha, 
								$cliFone, 
								$cliCel, 
								$cliNasci, 
								$cliSexo, 
								$cliEnd, 
								$cliEndNumero, 
								$cliEndComp, 
								$cliCEP,
								$cliEstado,
								$cliCidade, 
								$cliFoto, 
								$cliWebcam, 
								$cliComoConheceu, 
								$cliTratamento, 
								$cliAssinatura, 
								$cliStatus)
    {
        $sql = "UPDATE cliente set  cliNome 			= '".$cliNome."', 
									cliCPF 				= '".$cliCPF."', 
									cliRG 				= '".$cliRG."', 
									cliEstadoCivil 		= '".$cliEstadoCivil."', 
									cliEmail 			= '".$cliEmail."', 
									cliSenha 			= '".$cliSenha."', 
									cliFone 			= '".$cliFone."', 
									cliCel 				= '".$cliCel."', 
									cliNasci 			= '".$cliNasci."', 
									cliSexo 			= '".$cliSexo."', 
									cliEnd 				= '".$cliEnd."', 
									cliEndNumero 		= '".$cliEndNumero."', 
									cliEndComp 			= '".$cliEndComp."', 
									cliCEP 				= '".$cliCEP."', 
									cliEstado 			= '".$cliEstado."', 
									cliCidade 			= '".$cliCidade."', 
									cliFoto 			= '".$cliFoto."', 
									cliWebcam 			= '".$cliWebcam."', 
									cliComoConheceu 	= '".$cliComoConheceu."', 
									cliTratamento 		= '".$cliTratamento."', 
									cliAssinatura 		= '".$cliAssinatura."', 
									cliStatus 			= '".$cliStatus."' WHERE idCliente = ".$idCliente;
		
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
		} 
    }
	
	/* ######## CADASTRO (INSERIR) CLIENTE ########## */
	public function inserir(	$cliNome, 
								$cliCPF, 
								$cliRG, 
								$cliEstadoCivil, 
								$cliEmail, 
								$cliSenha, 
								$cliFone, 
								$cliCel, 
								$cliNasci, 
								$cliSexo, 
								$cliProfissao, 
								$cliEnd, 
								$cliEndNumero, 
								$cliEndComp, 
								$cliCEP, 
								$cliEstado, 
								$cliCidade, 
								$cliFoto, 
								$cliWebcam, 
								$cliComoConheceu, 
								$cliTratamento, 
								$cliAssinatura, 
								$cliStatus)
    {
        $sql = "INSERT INTO cliente (	cliNome, 
										cliCPF, 
										cliRG, 
										cliEstadoCivil, 
										cliEmail, 
										cliSenha, 
										cliFone, 
										cliCel, 
										cliNasci, 
										cliSexo, 
										cliProfissao, 
										cliEnd, 
										cliEndNumero, 
										cliEndComp, 
										cliCEP, 
										cliEstado, 
										cliCidade, 
										cliFoto, 
										cliWebcam, 
										cliComoConheceu, 
										cliTratamento, 
										cliAssinatura, 
										cliStatus) VALUES (	:cliNome, 
															:cliCPF, 
															:cliRG, 
															:cliEstadoCivil, 
															:cliEmail, 
															:cliSenha, 
															:cliFone, 
															:cliCel, 
															:cliNasci, 
															:cliSexo, 
															:cliProfissao, 
															:cliEnd, 
															:cliEndNumero, 
															:cliEndComp, 
															:cliCEP, 
															:cliEstado, 
															:cliCidade, 
															:cliFoto, 
															:cliWebcam, 
															:cliComoConheceu, 
															:cliTratamento, 
															:cliAssinatura, 
															:cliStatus)";
        $query = $this->db->prepare($sql);
        $parameters = array(':cliNome' => $cliNome, 
							':cliCPF' => $cliCPF, 
							':cliRG' => $cliRG, 
							':cliEstadoCivil' => $cliEstadoCivil, 
							':cliEmail' => $cliEmail, 
							':cliSenha' => $cliSenha, 
							':cliFone' => $cliFone, 
							':cliCel' => $cliCel, 
							':cliNasci' => $cliNasci, 
							':cliSexo' => $cliSexo, 
							':cliProfissao' => $cliProfissao, 
							':cliEnd' => $cliEnd, 
							':cliEndNumero' => $cliEndNumero, 
							':cliEndComp' => $cliEndComp, 
							':cliCEP' => $cliCEP, 
							':cliEstado' => $cliEstado, 
							':cliCidade' => $cliCidade, 
							':cliFoto' => $cliFoto, 
							':cliWebcam' => $cliWebcam, 
							':cliComoConheceu' => $cliComoConheceu, 
							':cliTratamento' => $cliTratamento, 
							':cliAssinatura' => $cliAssinatura, 
							':cliStatus' => $cliStatus);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }
	
	public function ativarDesativar($id,$boleano)
    {
        $sql = "UPDATE cliente SET cliStatus = $boleano WHERE cliente.idCliente = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
	
	public function permissaoAtualizarGeral($id,$boleano)
    {
        $sql = "UPDATE cliente SET cliPermitirAtualizacaoGeral = $boleano WHERE cliente.idCliente = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
	}
	
	public function permissaoAtualizarCovid($id,$boleano)
    {
        $sql = "UPDATE cliente SET cliPermitirAtualizacaoCovid = $boleano WHERE cliente.idCliente = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
	
	/* ############# PERFIL - INSERIR DEPOIMENTO ##################*/
	public function inserirDepo($idCliente, 
								$depoimentoTexto, 
								$depoimentoStatus)
    {
        $sql = "INSERT INTO depoimento (idCliente, 
										depoimentoTexto, 
										depoimentoStatus) VALUES (	:idCliente, 
																	:depoimentoTexto, 
																	:depoimentoStatus)";
        $query = $this->db->prepare($sql);
        $parameters = array(':idCliente' 		=> $idCliente, 
							':depoimentoTexto' 	=> $depoimentoTexto, 
							':depoimentoStatus' => $depoimentoStatus);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
	}
	
    public function acessocliente($email)
    {
        $sql = "SELECT idCliente, cliNome, cliSenha, cliFoto, cliWebcam FROM cliente WHERE cliEmail = '$email'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
	}

	public function existeCPF($cpf)
    {
        $sql = "SELECT idCliente FROM cliente WHERE cliCPF = $cpf";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
	}

	public function idNome($cpf)
    {
        $sql = "SELECT idCliente,cliNome FROM cliente WHERE cliCPF = $cpf";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
	}
	
}
