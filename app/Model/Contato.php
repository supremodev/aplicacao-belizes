<?php

namespace App\Model;

use App\Core\Model;

class Contato extends Model
{

    public function listaTodos()
    {
        $sql = "SELECT * FROM contato";
        $query = $this->db->prepare($sql);
        $resultado = $query->execute();

        return $query->fetchAll();
    }

    public function listaContatoJson($id)
    {

        $sql = "SELECT * FROM contato WHERE id=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        $encode = array();

        while ($row = $query->fetchAll()) {
            $encode[] = $row;
        }
        return $encode;

    }

    public function lista($id)
    {
        $sql = "SELECT * FROM contato WHERE id=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizar($id, $titulo, $descricao, $img, $alt )
    {
        $sql = "update contato set titulo = '".$titulo."', descricao = '".$descricao."',img = '".$img."',alt = '".$alt."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    
    }

    public function inserir($titulo, $descricao, $img, $alt)
    {
        $sql = "INSERT INTO contato (titulo, descricao, img, alt) VALUES (:titulo, :descricao,  :img, :alt)";
        $query = $this->db->prepare($sql);
        $parameters = array(':titulo' => $titulo, ':descricao' => $descricao, ':img' => $img, 'alt' => $alt);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function deletar($id)
    {
        $sql = "DELETE FROM contato WHERE id = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

}
