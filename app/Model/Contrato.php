<?php

namespace App\Model;

use App\Core\Model;

class Contrato extends Model
{

    public function listaContratoCliente($id)
    {
        $sql = "SELECT * FROM `contratoServicosEsteticos` WHERE idCliente = $id ORDER BY contratoData DESC";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

}
