<?php

namespace App\Model;

use App\Core\Model;

class Depoimento extends Model
{

    public function lista($id)
    {
        $sql = "SELECT depoimento.*,cliente.cliNome FROM `depoimento` INNER JOIN cliente ON cliente.idCliente=depoimento.idCliente WHERE depoimento.idDepo='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaIdUsuario($id)
    {
        $sql = "SELECT depoimentoTexto FROM `depoimento` WHERE idCliente='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaTodos()
    {
        $sql = "SELECT depoimento.*,cliente.cliNome FROM `depoimento` INNER JOIN cliente ON cliente.idCliente=depoimento.idCliente";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	public function listaStatus($id)
    {
		$sql = "SELECT depoimento.*, cliente.* FROM depoimento INNER JOIN cliente ON cliente.idCliente = depoimento.idCliente WHERE depoimentoStatus = '$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function novoDepoimento()
    {
        $sql = "SELECT cliente.nome_cliente,cliente.img,depoimento.id,depoimento.profissao, depoimento.depoimento, depoimento.avaliacao FROM `depoimento` inner join cliente ON cliente.id=depoimento.id_cliente WHERE depoimento.estatus='analise'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizarEstatus($id, $estatus)
    {
        $sql = "UPDATE depoimento SET depoimentoStatus = :estatus WHERE idDepo = :id";
        $query = $this->db->prepare($sql);
        $parameters = array('estatus' => $estatus, ':id' => $id);

        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function atualizar($id, $nome, $profissao, $depoimento, $avaliacao, $img, $alt)
    {
        $sql = "UPDATE depoimento SET nome = :nome, profissao = :profissao, depoimento = :depoimento, avaliacao = :avaliacao, img = :img, alt = :alt WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':profissao' => $profissao, ':depoimento' => $depoimento, ':avaliacao' => $avaliacao, 'img' => $img, 'alt' => $alt, ':id' => $id);

        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function inserir($nome, $profissao, $depoimento, $avaliacao, $img, $alt)
    {
        $sql = "INSERT INTO depoimento (nome, profissao, depoimento, avaliacao, img) VALUES (:nome, :profissao, :depoimento, :avaliacao, :img)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':profissao' => $profissao, ':depoimento' => $depoimento, ':avaliacao' => $avaliacao,':img' => $img);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function inserirCliente($id, $profissao, $depoimento, $avaliacao, $img, $estatus)
    {
        $sql = "INSERT INTO depoimento (id_cliente, profissao, depoimento, avaliacao, img, estatus) VALUES (:id_cliente, :profissao, :depoimento, :avaliacao, :img, :estatus)";
        $query = $this->db->prepare($sql);
        $parameters = array(':id_cliente' => $id, ':profissao' => $profissao, ':depoimento' => $depoimento, ':avaliacao' => $avaliacao,':img' => $img,':estatus' => $estatus);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }
    
    public function atualizarCliente($id, $profissao, $depoimento, $avaliacao, $img, $estatus)
    {
        $sql = "UPDATE depoimento SET profissao = :profissao, depoimento = :depoimento, avaliacao = :avaliacao, img = :img,  estatus = :estatus WHERE id_cliente = :id_cliente";
        $query = $this->db->prepare($sql);
        $parameters = array(':profissao' => $profissao, ':depoimento' => $depoimento, ':avaliacao' => $avaliacao, ':img' => $img, ':estatus' => $estatus,':id_cliente' => $id);

        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function deletar($id)
    {
        $sql = "DELETE FROM depoimento WHERE id = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
}
