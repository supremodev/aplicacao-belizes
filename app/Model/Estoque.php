<?php

namespace App\Model;

use App\Core\Model;

class Estoque extends Model
{

	/* ######## LISTAR TODOS OS Estoque ########## */
    public function listaTodosEstoque()
    {
        $sql = "SELECT * FROM estoque WHERE estoqueStatus = 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	/* ######## CONTAGEM DOS REGISTRO NO Estoque ########## */
	public function qtdOfEstoque()
    {
        $sql = "SELECT COUNT(idEstoque) AS amount_of_Estoque FROM estoque WHERE estoqueStatus = 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetch()->amount_of_Estoque;
    }

    /* ######## FILTRO PRODUTO ########## */

    public function produtoNome($nome)
    {          
        $sql = "SELECT * FROM estoque WHERE estoque.estoqueProduto like '%$nome%'";
        $query = $this->db->prepare($sql);
        $query->execute();
		
        return $query->fetchAll();
    }
	
	
	/* ######## TOTAL DE ENTRADAS MILILITRO (ML) NO Estoque ########## */
	public function totalEstoqueML()
    {
        $sql = "SELECT SUM(estoqueQtd) AS totalML FROM estoque WHERE estoqueMedida = 'ML' AND estoqueStatus = 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetch()->totalML;
    }
	
	
	/* ######## TOTAL DE GRAMA ( G ) NO Estoque ########## */
	public function totalEstoqueGrama()
    {
        $sql = "SELECT SUM(estoqueQtd) AS totalG FROM estoque WHERE estoqueMedida = 'G' AND estoqueStatus = 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetch()->totalG;
    }
		
	
	/* ######## LISTAR Estoque PELO ID ########## */
	public function lista($id)
    {
        $sql = "SELECT * FROM estoque WHERE idEstoque = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	/* ######## ATUALIZAR DADOS DO ESTOQUE ########## */
	public function atualizar(	$id,
								$estoqueProduto, 
								$estoqueTipo, 
								$estoqueQtd, 
								$estoqueUsar15Dias, 
								$estoqueMedida, 
								$estoqueQtdUnidade, 
								$estoqueVencimento,
								$estoqueStatus)
    {
        $sql = "UPDATE estoque set  estoqueProduto 		= '".$estoqueProduto."', 
									estoqueTipo 		= '".$estoqueTipo."', 
									estoqueQtd 			= '".$estoqueQtd."', 
									estoqueUsar15Dias 	= '".$estoqueUsar15Dias."', 
									estoqueMedida 		= '".$estoqueMedida."', 
									estoqueQtdUnidade 	= '".$estoqueQtdUnidade."', 
									estoqueVencimento 	= '".$estoqueVencimento."',
									estoqueStatus 		= '".$estoqueStatus."' WHERE idEstoque = ".$id;
		
        $query = $this->db->prepare($sql);    
        if($query->execute()){
            return true;
        }else{
            return false;
        }  
    }
	
	/* ######## INSERIR Estoque ########## */
	public function inserir($estoqueProduto, 
							$estoqueTipo, 
							$estoqueQtd, 
							$estoqueUsar15Dias, 
							$estoqueMedida, 
							$estoqueQtdUnidade, 
							$estoqueVencimento,
							$estoqueStatus)
    {
        $sql = "INSERT INTO estoque (	estoqueProduto, 
										estoqueTipo, 
										estoqueQtd, 
										estoqueUsar15Dias, 
										estoqueMedida, 
										estoqueQtdUnidade, 
										estoqueVencimento, 
										estoqueStatus) VALUES (	:estoqueProduto, 
																:estoqueTipo, 
																:estoqueQtd, 
																:estoqueUsar15Dias, 
																:estoqueMedida, 
																:estoqueQtdUnidade, 
																:estoqueVencimento,
																:estoqueStatus)";
        $query = $this->db->prepare($sql);
        $parameters = array(':estoqueProduto' 		=> $estoqueProduto, 
							':estoqueTipo' 			=> $estoqueTipo, 
							':estoqueQtd' 			=> $estoqueQtd, 
							':estoqueUsar15Dias' 	=> $estoqueUsar15Dias, 
							':estoqueMedida' 		=> $estoqueMedida, 
							':estoqueQtdUnidade' 	=> $estoqueQtdUnidade, 
							':estoqueVencimento' 	=> $estoqueVencimento,
							':estoqueStatus' 		=> $estoqueStatus);
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }
	
	/* ######## DESATIVAR ITEM DO Estoque ########## */
	public function desativar($id)
    {
        $sql = "UPDATE estoque SET estoqueStatus = '0' WHERE estoque.idEstoque = $id";
        $query = $this->db->prepare($sql);
        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
	

	
	
} /*################################ FIM CLASS ##############################*/
