<?php

/**
 * Class Ficha
 */

namespace App\Model;

use App\Core\Model;

class Ficha extends Model
{
	/*	
#####################################################################
#############											#############
#############				FICHA GERAL					#############
#############											#############
#####################################################################
*/

	/* ########## OBTER TODAS OS DADOS DA FICHAS GERAIS, CLIENTE E FUNCIONARIO DO BANCO DE DADOS ################     */
	public function todosFichaGeral()
	{
		$sql = "SELECT 	fichageral.*,
						cliente.*,
						funcionario.funcNome 
						FROM fichageral 
						INNER JOIN cliente ON cliente.idCliente = fichageral.idCliente 
						INNER JOIN funcionario ON funcionario.idFunc = fichageral.idFunc";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function todos()
	{
		$sql = "SELECT agendaData,agendaHora,funcNome, cliCPF, cliNome FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE agendamentos.agendaStatus = 1 AND agendamentos.agendaData >= CURDATE()";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function agendaProfissional($id,$data)
	{
		$sql = "SELECT agendaData,agendaHora,funcNome, cliCPF, cliNome FROM agendamentos INNER JOIN cliente ON cliente.idCliente = agendamentos.idCliente INNER JOIN funcionario ON funcionario.idFunc = agendamentos.idFunc WHERE agendamentos.idFunc = $id && agendamentos.agendaData >= '$data'";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}
	
	public function listaMarcacaoCorpo($idCliente)
	{
		$sql = "SELECT 	* FROM marcacaoCorporal WHERE idCliente = $idCliente";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function ConsultaCpfFichaGeral($cpf)
	{
		$sql = "SELECT fichageral.*, cliente.idCliente, cliNome, cliEnd, cliRg,  cliFoto, cliWebcam, cliProfissao, cliNasci, cliSexo, cliAssinatura FROM cliente INNER JOIN fichageral ON fichageral.idCliente = cliente.idCliente WHERE REPLACE(REPLACE(REPLACE(cliCPF,'-',''),'.',''),' ','') = $cpf ORDER BY fichageral.fichaData DESC LIMIT 1";
		$query = $this->db->prepare($sql);
		$query->execute();

		$encode = array();

		while ($row = $query->fetchAll()) {
			$encode[] = $row;
		}
		return $encode;
	}

	public function ConsultaCpfNecessario($cpf)
	{
		$sql = "SELECT idCliente, cliNome, cliFoto, cliWebcam, cliProfissao, cliNasci, cliSexo FROM cliente WHERE REPLACE(REPLACE(REPLACE(cliCPF,'-',''),'.',''),' ','') = $cpf";
		$query = $this->db->prepare($sql);
		$query->execute();

		$encode = array();

		while ($row = $query->fetchAll()) {
			$encode[] = $row;
		}
		return $encode;
	}

	/* ############ ADICIONAR UMA FICHA NO BANCO ################ */
	public function addFichaGeral(	$idCliente,
									$fichaHistoricoDeDermatiteOuCancerDePele,
									$fichaEmQualLocalDoCorpo,
									$fichaFazQuantoTempo,
									$fichaFoiLiberadoPeloMedicoParaTratamentoEstetico,
									$fichaHipertireoidismo,
									$fichaTratamentoHipertireoidismo,
									$fichaHipotireoidismo,
									$fichaTratamentoHipotireoidismo,
									$fichaDiabetes,
									$fichaTratamentoDiabetes,
									$fichaHipertensao,
									$fichaTratamentoHipertensao,
									$fichaEplepsia,
									$fichaTratamentoEplepsia,
									$fichaPsoriase,
									$fichaTratamentoPsoriase,
									$fichaOutrasDoencas,
									$fichaTemImplantesMetalicos,
									$fichaLocalTemImplantesMetalicos,
									$fichaPortadorDeMarcapasso,
									$fichaQuantoTempoPortadorDeMarcapasso,
									$fichaPortadorDeHIV,
									$fichaTratamentoPortadorDeHIV,
									$fichaPortadorDeHepatite,
									$fichaQualPortadorDeHepatite,
									$fichaAlteracaoHormonal,
									$fichaQualAlteracaoHormonal,
									$fichaAlergiaAlgumAlimento,
									$fichaQualAlergiaAlgumAlimento,
									$fichaAlergiaAlgumMedicamento,
									$fichaQualAlergiaAlgumMedicamento,
									$fichaFazUsoDeMedicamento,
									$fichaQualFazUsoDeMedicamento,
									$fichaPresencaDeQueloides,
									$fichaQueLocalPresencaDeQueloides,
									$fichaGestante,
									$fichaQuantoTempoGestante,
									$fichaFilhos,
									$fichaQuantosFilhosIdade,
									$fichaFezAlgumaCirurgiaPlastica,
									$fichaQualFezAlgumaCirurgiaPlastica,
									$fichaRealizouProcedimentosEsteticosAnteriores,
									$fichaQualRealizouProcedimentosEsteticosAnteriores,
									$fichaFazUsoDeMaquiagem,
									$fichaQualFazUsoDeMaquiagem,
									$fichaFazAtividadeFisica,
									$fichaQuantasVezesPorSemanaAtividadeFisica,
									$fichaConsumoAguaIngereQuantosCoposDia,
									$fichaIngereBebidaAlcoolica,
									$fichaQuantasDosesPorSemanaBebidaAlcoolica,
									$fichaIngereBebidaGaseificada,
									$fichaQuantasDosesPorSemanaBebidaGaseificada,
									$fichaJaFumouOuFuma,
									$fichaQuantosCigarrosPorDia,
									$fichaEstresse,
									$fichaSono,
									$fichaQuantasHorasPorNoiteSono,
									$fichaPosicaoQuePermanecePorMaisTempo,
									$fichaAlimentacao,
									$fichaFazDietaQualQuantoTempo,
									$fichaOqueTeIncomoda, 
									$fichaQualSeuTime, 
									$fichaOqueVcMudariaEmVc, 
									$fichaQualSeuEstiloMusica, 
									$fichaData,
									$fichaHora,
									$fichaStatus) {

		$sql = "UPDATE cliente SET cliPermitirAtualizacaoGeral = 0 WHERE idCliente = $idCliente";
		$query = $this->db->prepare($sql);   
		
		if($query->execute()) {

			$sql = "INSERT INTO fichageral(	idCliente, 
											fichaHistoricoDeDermatiteOuCancerDePele, 
											fichaEmQualLocalDoCorpo, 
											fichaFazQuantoTempo, 
											fichaFoiLiberadoPeloMedicoParaTratamentoEstetico, 
											fichaHipertireoidismo, 
											fichaTratamentoHipertireoidismo, 
											fichaHipotireoidismo, 
											fichaTratamentoHipotireoidismo, 
											fichaDiabetes, 
											fichaTratamentoDiabetes, 
											fichaHipertensao, 
											fichaTratamentoHipertensao, 
											fichaEplepsia, 
											fichaTratamentoEplepsia, 
											fichaPsoriase, 
											fichaTratamentoPsoriase, 
											fichaOutrasDoencas, 
											fichaTemImplantesMetalicos, 
											fichaLocalTemImplantesMetalicos, 
											fichaPortadorDeMarcapasso, 
											fichaQuantoTempoPortadorDeMarcapasso, 
											fichaPortadorDeHIV, 
											fichaTratamentoPortadorDeHIV, 
											fichaPortadorDeHepatite, 
											fichaQualPortadorDeHepatite, 
											fichaAlteracaoHormonal, 
											fichaQualAlteracaoHormonal, 
											fichaAlergiaAlgumAlimento, 
											fichaQualAlergiaAlgumAlimento, 
											fichaAlergiaAlgumMedicamento, 
											fichaQualAlergiaAlgumMedicamento, 
											fichaFazUsoDeMedicamento, 
											fichaQualFazUsoDeMedicamento, 
											fichaPresencaDeQueloides, 
											fichaQueLocalPresencaDeQueloides, 
											fichaGestante, 
											fichaQuantoTempoGestante, 
											fichaFilhos, 
											fichaQuantosFilhosIdade, 
											fichaFezAlgumaCirurgiaPlastica, 
											fichaQualFezAlgumaCirurgiaPlastica, 
											fichaRealizouProcedimentosEsteticosAnteriores, 
											fichaQualRealizouProcedimentosEsteticosAnteriores, 
											fichaFazUsoDeMaquiagem, 
											fichaQualFazUsoDeMaquiagem, 
											fichaFazAtividadeFisica, 
											fichaQuantasVezesPorSemanaAtividadeFisica, 
											fichaConsumoAguaIngereQuantosCoposDia, 
											fichaIngereBebidaAlcoolica, 
											fichaQuantasDosesPorSemanaBebidaAlcoolica, 
											fichaIngereBebidaGaseificada, 
											fichaQuantasDosesPorSemanaBebidaGaseificada, 
											fichaJaFumouOuFuma, 
											fichaQuantosCigarrosPorDia, 
											fichaEstresse, 
											fichaSono, 
											fichaQuantasHorasPorNoiteSono, 
											fichaPosicaoQuePermanecePorMaisTempo, 
											fichaAlimentacao, 
											fichaFazDietaQualQuantoTempo,
											fichaOqueTeIncomoda, 
											fichaQualSeuTime, 
											fichaOqueVcMudariaEmVc, 
											fichaQualSeuEstiloMusica, 
											fichaData, 
											fichaHora, 
											fichaStatus) VALUES (	:idCliente, 
																	:fichaHistoricoDeDermatiteOuCancerDePele, 
																	:fichaEmQualLocalDoCorpo, 
																	:fichaFazQuantoTempo, 
																	:fichaFoiLiberadoPeloMedicoParaTratamentoEstetico, 
																	:fichaHipertireoidismo, 
																	:fichaTratamentoHipertireoidismo, 
																	:fichaHipotireoidismo, 
																	:fichaTratamentoHipotireoidismo, 
																	:fichaDiabetes, 
																	:fichaTratamentoDiabetes, 
																	:fichaHipertensao, 
																	:fichaTratamentoHipertensao, 
																	:fichaEplepsia, 
																	:fichaTratamentoEplepsia, 
																	:fichaPsoriase, 
																	:fichaTratamentoPsoriase, 
																	:fichaOutrasDoencas, 
																	:fichaTemImplantesMetalicos, 
																	:fichaLocalTemImplantesMetalicos, 
																	:fichaPortadorDeMarcapasso, 
																	:fichaQuantoTempoPortadorDeMarcapasso, 
																	:fichaPortadorDeHIV, 
																	:fichaTratamentoPortadorDeHIV, 
																	:fichaPortadorDeHepatite, 
																	:fichaQualPortadorDeHepatite, 
																	:fichaAlteracaoHormonal, 
																	:fichaQualAlteracaoHormonal, 
																	:fichaAlergiaAlgumAlimento, 
																	:fichaQualAlergiaAlgumAlimento, 
																	:fichaAlergiaAlgumMedicamento, 
																	:fichaQualAlergiaAlgumMedicamento, 
																	:fichaFazUsoDeMedicamento, 
																	:fichaQualFazUsoDeMedicamento, 
																	:fichaPresencaDeQueloides, 
																	:fichaQueLocalPresencaDeQueloides, 
																	:fichaGestante, 
																	:fichaQuantoTempoGestante, 
																	:fichaFilhos, 
																	:fichaQuantosFilhosIdade, 
																	:fichaFezAlgumaCirurgiaPlastica, 
																	:fichaQualFezAlgumaCirurgiaPlastica, 
																	:fichaRealizouProcedimentosEsteticosAnteriores, 
																	:fichaQualRealizouProcedimentosEsteticosAnteriores, 
																	:fichaFazUsoDeMaquiagem, 
																	:fichaQualFazUsoDeMaquiagem, 
																	:fichaFazAtividadeFisica, 
																	:fichaQuantasVezesPorSemanaAtividadeFisica, 
																	:fichaConsumoAguaIngereQuantosCoposDia, 
																	:fichaIngereBebidaAlcoolica, 
																	:fichaQuantasDosesPorSemanaBebidaAlcoolica, 
																	:fichaIngereBebidaGaseificada, 
																	:fichaQuantasDosesPorSemanaBebidaGaseificada, 
																	:fichaJaFumouOuFuma, 
																	:fichaQuantosCigarrosPorDia, 
																	:fichaEstresse, 
																	:fichaSono, 
																	:fichaQuantasHorasPorNoiteSono, 
																	:fichaPosicaoQuePermanecePorMaisTempo, 
																	:fichaAlimentacao, 
																	:fichaFazDietaQualQuantoTempo, 
																	:fichaOqueTeIncomoda, 
																	:fichaQualSeuTime, 
																	:fichaOqueVcMudariaEmVc, 
																	:fichaQualSeuEstiloMusica, 
																	:fichaData, 
																	:fichaHora, 
																	:fichaStatus)";
			$query = $this->db->prepare($sql);
			$parameters = array(':idCliente' => $idCliente,
								':fichaHistoricoDeDermatiteOuCancerDePele' => $fichaHistoricoDeDermatiteOuCancerDePele,
								':fichaEmQualLocalDoCorpo' => $fichaEmQualLocalDoCorpo,
								':fichaFazQuantoTempo' => $fichaFazQuantoTempo,
								':fichaFoiLiberadoPeloMedicoParaTratamentoEstetico' => $fichaFoiLiberadoPeloMedicoParaTratamentoEstetico,
								':fichaHipertireoidismo' => $fichaHipertireoidismo,
								':fichaTratamentoHipertireoidismo' => $fichaTratamentoHipertireoidismo,
								':fichaHipotireoidismo' => $fichaHipotireoidismo,
								':fichaTratamentoHipotireoidismo' => $fichaTratamentoHipotireoidismo,
								':fichaDiabetes' => $fichaDiabetes,
								':fichaTratamentoDiabetes' => $fichaTratamentoDiabetes,
								':fichaHipertensao' => $fichaHipertensao,
								':fichaTratamentoHipertensao' => $fichaTratamentoHipertensao,
								':fichaEplepsia' => $fichaEplepsia,
								':fichaTratamentoEplepsia' => $fichaTratamentoEplepsia,
								':fichaPsoriase' => $fichaPsoriase,
								':fichaTratamentoPsoriase' => $fichaTratamentoPsoriase,
								':fichaOutrasDoencas' => $fichaOutrasDoencas,
								':fichaTemImplantesMetalicos' => $fichaTemImplantesMetalicos,
								':fichaLocalTemImplantesMetalicos' => $fichaLocalTemImplantesMetalicos,
								':fichaPortadorDeMarcapasso' => $fichaPortadorDeMarcapasso,
								':fichaQuantoTempoPortadorDeMarcapasso' => $fichaQuantoTempoPortadorDeMarcapasso,
								':fichaPortadorDeHIV' => $fichaPortadorDeHIV,
								':fichaTratamentoPortadorDeHIV' => $fichaTratamentoPortadorDeHIV,
								':fichaPortadorDeHepatite' => $fichaPortadorDeHepatite,
								':fichaQualPortadorDeHepatite' => $fichaQualPortadorDeHepatite,
								':fichaAlteracaoHormonal' => $fichaAlteracaoHormonal,
								':fichaQualAlteracaoHormonal' => $fichaQualAlteracaoHormonal,
								':fichaAlergiaAlgumAlimento' => $fichaAlergiaAlgumAlimento,
								':fichaQualAlergiaAlgumAlimento' => $fichaQualAlergiaAlgumAlimento,
								':fichaAlergiaAlgumMedicamento' => $fichaAlergiaAlgumMedicamento,
								':fichaQualAlergiaAlgumMedicamento' => $fichaQualAlergiaAlgumMedicamento,
								':fichaFazUsoDeMedicamento' => $fichaFazUsoDeMedicamento,
								':fichaQualFazUsoDeMedicamento' => $fichaQualFazUsoDeMedicamento,
								':fichaPresencaDeQueloides' => $fichaPresencaDeQueloides,
								':fichaQueLocalPresencaDeQueloides' => $fichaQueLocalPresencaDeQueloides,
								':fichaGestante' => $fichaGestante,
								':fichaQuantoTempoGestante' => $fichaQuantoTempoGestante,
								':fichaFilhos' => $fichaFilhos,
								':fichaQuantosFilhosIdade' => $fichaQuantosFilhosIdade,
								':fichaFezAlgumaCirurgiaPlastica' => $fichaFezAlgumaCirurgiaPlastica,
								':fichaQualFezAlgumaCirurgiaPlastica' => $fichaQualFezAlgumaCirurgiaPlastica,
								':fichaRealizouProcedimentosEsteticosAnteriores' => $fichaRealizouProcedimentosEsteticosAnteriores,
								':fichaQualRealizouProcedimentosEsteticosAnteriores' => $fichaQualRealizouProcedimentosEsteticosAnteriores,
								':fichaFazUsoDeMaquiagem' => $fichaFazUsoDeMaquiagem,
								':fichaQualFazUsoDeMaquiagem' => $fichaQualFazUsoDeMaquiagem,
								':fichaFazAtividadeFisica' => $fichaFazAtividadeFisica,
								':fichaQuantasVezesPorSemanaAtividadeFisica' => $fichaQuantasVezesPorSemanaAtividadeFisica,
								':fichaConsumoAguaIngereQuantosCoposDia' => $fichaConsumoAguaIngereQuantosCoposDia,
								':fichaIngereBebidaAlcoolica' => $fichaIngereBebidaAlcoolica,
								':fichaQuantasDosesPorSemanaBebidaAlcoolica' => $fichaQuantasDosesPorSemanaBebidaAlcoolica,
								':fichaIngereBebidaGaseificada' => $fichaIngereBebidaGaseificada,
								':fichaQuantasDosesPorSemanaBebidaGaseificada' => $fichaQuantasDosesPorSemanaBebidaGaseificada,
								':fichaJaFumouOuFuma' => $fichaJaFumouOuFuma,
								':fichaQuantosCigarrosPorDia' => $fichaQuantosCigarrosPorDia,
								':fichaEstresse' => $fichaEstresse,
								':fichaSono' => $fichaSono,
								':fichaQuantasHorasPorNoiteSono' => $fichaQuantasHorasPorNoiteSono,
								':fichaPosicaoQuePermanecePorMaisTempo' => $fichaPosicaoQuePermanecePorMaisTempo,
								':fichaAlimentacao' => $fichaAlimentacao,
								':fichaFazDietaQualQuantoTempo' => $fichaFazDietaQualQuantoTempo, 
								':fichaOqueTeIncomoda' => $fichaOqueTeIncomoda, 
								':fichaQualSeuTime' => $fichaQualSeuTime, 
								':fichaOqueVcMudariaEmVc' => $fichaOqueVcMudariaEmVc, 
								':fichaQualSeuEstiloMusica' => $fichaQualSeuEstiloMusica, 
								':fichaData' => $fichaData,
								':fichaHora' => $fichaHora,
								':fichaStatus' => $fichaStatus);
			if ($query->execute($parameters)) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function addFichaCorporal($idFichaGeral,
									$idFunc, 
									$fichaCorporalHipolipodistrofia, 
									$fichaCorporalGrau, 
									$fichaCorporalTemperatura, 
									$fichaCorporalPalpacao, 
									$fichaCorporalTemperaturaLocalizacao, 
									$fichaCorporalTemperaturaColoracaoDoTecido, 
									$fichaCorporalEdemaTesteDeCacifo, 
									$fichaCorporalEdemaTesteDeDigitoPressao, 
									$fichaCorporalEdemaSensacaoDePesoCansacoEmMMII, 
									$fichaCorporalEdemaObservacoes, 
									$fichaCorporalLipodistrofiaGorduraCompactaFlacida, 
									$fichaCorporalLipodistrofiaDistribuicaoGordura, 
									$fichaCorporalLipodistrofiaLocalizacao, 
									$fichaCorporalBiotipo, 
									$fichaCorporalBiotipoPeso, 
									$fichaCorporalBiotipoAltura, 
									$fichaCorporalBiotipoIMC, 
									$fichaCorporalBiotipoPesoMin, 
									$fichaCorporalBiotipoPesoMax, 
									$fichaCorporalBiotipoIMCSituacao, 
									$fichaCorporalBiotipoObservacoes, 
									$fichaCorporalFlacidezTissular, 
									$fichaCorporalFlacidezLocalizacaoDaFlacidezTissular, 
									$fichaCorporalFlacidezMuscular, 
									$fichaCorporalFlacidezLocalizacaoDaFlacidezMuscular, 
									$fichaCorporalEstriasCor, 
									$fichaCorporalEstriasLargura, 
									$fichaCorporalEstriasQuantidade, 
									$fichaCorporalEstriasRegiao, 
									$fichaCorporalAlteracoesPosturaisOmbrosAnteriorizacao, 
									$fichaCorporalAlteracoesPosturaisOmbrosHiperextensao, 
									$fichaCorporalAlteracoesPosturaisColunaEscolioseEmC, 
									$fichaCorporalAlteracoesPosturaisColunaEscolioseEmS, 
									$fichaCorporalAlteracoesPosturaisColunaHipercifose, 
									$fichaCorporalAlteracoesPosturaisColunaHiperlordose, 
									$fichaCorporalAlteracoesPosturaisColunaRetificacaoDaCifose, 
									$fichaCorporalAlteracoesPosturaisColunaRetificacaoDaLordose, 
									$fichaCorporalAlteracoesPosturaisQuadrilAntiversao, 
									$fichaCorporalAlteracoesPosturaisQuadrilRetroversao, 
									$fichaCorporalAlteracoesPosturaisJoelhosGenovalgo, 
									$fichaCorporalAlteracoesPosturaisJoelhosGenovaro, 
									$fichaCorporalAlteracoesPosturaisJoelhosHiperextensao, 
									$fichaCorporalAlteracoesPosturaisOutros,
									$fichaCorporalObsTratamentoIndicado,
                                    $fichaCorporalQtdSessoes,
									$fichaCorporalData,
									$fichaCorporalHora,
									$fichaCorporalStatus
	) {
		$sql = "INSERT INTO fichaCorporal( 	idFichaGeral,
											idFunc,
											fichaCorporalHipolipodistrofia,
											fichaCorporalGrau,
											fichaCorporalTemperatura,
											fichaCorporalPalpacao,
											fichaCorporalTemperaturaLocalizacao,
											fichaCorporalTemperaturaColoracaoDoTecido,
											fichaCorporalEdemaTesteDeCacifo,
											fichaCorporalEdemaTesteDeDigitoPressao,
											fichaCorporalEdemaSensacaoDePesoCansacoEmMMII,
											fichaCorporalEdemaObservacoes,
											fichaCorporalLipodistrofiaGorduraCompactaFlacida,
											fichaCorporalLipodistrofiaDistribuicaoGordura,
											fichaCorporalLipodistrofiaLocalizacao,
											fichaCorporalBiotipo,
											fichaCorporalBiotipoPeso,
											fichaCorporalBiotipoAltura,
											fichaCorporalBiotipoIMC,
											fichaCorporalBiotipoPesoMin,
											fichaCorporalBiotipoPesoMax,
											fichaCorporalBiotipoIMCSituacao,
											fichaCorporalBiotipoObservacoes,
											fichaCorporalFlacidezTissular,
											fichaCorporalFlacidezLocalizacaoDaFlacidezTissular,
											fichaCorporalFlacidezMuscular,
											fichaCorporalFlacidezLocalizacaoDaFlacidezMuscular,
											fichaCorporalEstriasCor,
											fichaCorporalEstriasLargura,
											fichaCorporalEstriasQuantidade,
											fichaCorporalEstriasRegiao,
											fichaCorporalAlteracoesPosturaisOmbrosAnteriorizacao,
											fichaCorporalAlteracoesPosturaisOmbrosHiperextensao,
											fichaCorporalAlteracoesPosturaisColunaEscolioseEmC,
											fichaCorporalAlteracoesPosturaisColunaEscolioseEmS,
											fichaCorporalAlteracoesPosturaisColunaHipercifose,
											fichaCorporalAlteracoesPosturaisColunaHiperlordose,
											fichaCorporalAlteracoesPosturaisColunaRetificacaoDaCifose,
											fichaCorporalAlteracoesPosturaisColunaRetificacaoDaLordose,
											fichaCorporalAlteracoesPosturaisQuadrilAntiversao,
											fichaCorporalAlteracoesPosturaisQuadrilRetroversao,
											fichaCorporalAlteracoesPosturaisJoelhosGenovalgo,
											fichaCorporalAlteracoesPosturaisJoelhosGenovaro,
											fichaCorporalAlteracoesPosturaisJoelhosHiperextensao,
											fichaCorporalAlteracoesPosturaisOutros,
											fichaCorporalObsTratamentoIndicado,
                                    		fichaCorporalQtdSessoes,
											fichaCorporalData, 
											fichaCorporalHora, 
											fichaCorporalStatus) VALUES (	:idFichaGeral, 
																			:idFunc, 
																			:fichaCorporalHipolipodistrofia, 
																			:fichaCorporalGrau, 
																			:fichaCorporalTemperatura, 
																			:fichaCorporalPalpacao, 
																			:fichaCorporalTemperaturaLocalizacao, 
																			:fichaCorporalTemperaturaColoracaoDoTecido, 
																			:fichaCorporalEdemaTesteDeCacifo, 
																			:fichaCorporalEdemaTesteDeDigitoPressao, 
																			:fichaCorporalEdemaSensacaoDePesoCansacoEmMMII, 
																			:fichaCorporalEdemaObservacoes, 
																			:fichaCorporalLipodistrofiaGorduraCompactaFlacida, 
																			:fichaCorporalLipodistrofiaDistribuicaoGordura, 
																			:fichaCorporalLipodistrofiaLocalizacao, 
																			:fichaCorporalBiotipo, 
																			:fichaCorporalBiotipoPeso, 
																			:fichaCorporalBiotipoAltura, 
																			:fichaCorporalBiotipoIMC, 
																			:fichaCorporalBiotipoPesoMin, 
																			:fichaCorporalBiotipoPesoMax, 
																			:fichaCorporalBiotipoIMCSituacao, 
																			:fichaCorporalBiotipoObservacoes, 
																			:fichaCorporalFlacidezTissular, 
																			:fichaCorporalFlacidezLocalizacaoDaFlacidezTissular, 
																			:fichaCorporalFlacidezMuscular, 
																			:fichaCorporalFlacidezLocalizacaoDaFlacidezMuscular, 
																			:fichaCorporalEstriasCor, 
																			:fichaCorporalEstriasLargura, 
																			:fichaCorporalEstriasQuantidade, 
																			:fichaCorporalEstriasRegiao, 
																			:fichaCorporalAlteracoesPosturaisOmbrosAnteriorizacao, 
																			:fichaCorporalAlteracoesPosturaisOmbrosHiperextensao, 
																			:fichaCorporalAlteracoesPosturaisColunaEscolioseEmC, 
																			:fichaCorporalAlteracoesPosturaisColunaEscolioseEmS, 
																			:fichaCorporalAlteracoesPosturaisColunaHipercifose, 
																			:fichaCorporalAlteracoesPosturaisColunaHiperlordose, 
																			:fichaCorporalAlteracoesPosturaisColunaRetificacaoDaCifose, 
																			:fichaCorporalAlteracoesPosturaisColunaRetificacaoDaLordose, 
																			:fichaCorporalAlteracoesPosturaisQuadrilAntiversao, 
																			:fichaCorporalAlteracoesPosturaisQuadrilRetroversao, 
																			:fichaCorporalAlteracoesPosturaisJoelhosGenovalgo, 
																			:fichaCorporalAlteracoesPosturaisJoelhosGenovaro, 
																			:fichaCorporalAlteracoesPosturaisJoelhosHiperextensao, 
																			:fichaCorporalAlteracoesPosturaisOutros, 
																			:fichaCorporalObsTratamentoIndicado,
                                    										:fichaCorporalQtdSessoes,
																			CURDATE(), 
																			:fichaCorporalHora, 
																			:fichaCorporalStatus)";

		$query = $this->db->prepare($sql);
		$parameters = array(':idFichaGeral' => $idFichaGeral, 
							':idFunc' => $idFunc, 
							':fichaCorporalHipolipodistrofia' => $fichaCorporalHipolipodistrofia, 
							':fichaCorporalGrau' => $fichaCorporalGrau, 
							':fichaCorporalTemperatura' => $fichaCorporalTemperatura, 
							':fichaCorporalPalpacao' => $fichaCorporalPalpacao, 
							':fichaCorporalTemperaturaLocalizacao' => $fichaCorporalTemperaturaLocalizacao, 
							':fichaCorporalTemperaturaColoracaoDoTecido' => $fichaCorporalTemperaturaColoracaoDoTecido, 
							':fichaCorporalEdemaTesteDeCacifo' => $fichaCorporalEdemaTesteDeCacifo, 
							':fichaCorporalEdemaTesteDeDigitoPressao' => $fichaCorporalEdemaTesteDeDigitoPressao, 
							':fichaCorporalEdemaSensacaoDePesoCansacoEmMMII' => $fichaCorporalEdemaSensacaoDePesoCansacoEmMMII, 
							':fichaCorporalEdemaObservacoes' => $fichaCorporalEdemaObservacoes, 
							':fichaCorporalLipodistrofiaGorduraCompactaFlacida' => $fichaCorporalLipodistrofiaGorduraCompactaFlacida, 
							':fichaCorporalLipodistrofiaDistribuicaoGordura' => $fichaCorporalLipodistrofiaDistribuicaoGordura, 
							':fichaCorporalLipodistrofiaLocalizacao' => $fichaCorporalLipodistrofiaLocalizacao, 
							':fichaCorporalBiotipo' => $fichaCorporalBiotipo, 
							':fichaCorporalBiotipoPeso' => $fichaCorporalBiotipoPeso, 
							':fichaCorporalBiotipoAltura' => $fichaCorporalBiotipoAltura, 
							':fichaCorporalBiotipoIMC' => $fichaCorporalBiotipoIMC, 
							':fichaCorporalBiotipoPesoMin' => $fichaCorporalBiotipoPesoMin, 
							':fichaCorporalBiotipoPesoMax' => $fichaCorporalBiotipoPesoMax, 
							':fichaCorporalBiotipoIMCSituacao' => $fichaCorporalBiotipoIMCSituacao, 
							':fichaCorporalBiotipoObservacoes' => $fichaCorporalBiotipoObservacoes, 
							':fichaCorporalFlacidezTissular' => $fichaCorporalFlacidezTissular, 
							':fichaCorporalFlacidezLocalizacaoDaFlacidezTissular' => $fichaCorporalFlacidezLocalizacaoDaFlacidezTissular, 
							':fichaCorporalFlacidezMuscular' => $fichaCorporalFlacidezMuscular, 
							':fichaCorporalFlacidezLocalizacaoDaFlacidezMuscular' => $fichaCorporalFlacidezLocalizacaoDaFlacidezMuscular,
							':fichaCorporalEstriasCor' => $fichaCorporalEstriasCor, 
							':fichaCorporalEstriasLargura' => $fichaCorporalEstriasLargura, 
							':fichaCorporalEstriasQuantidade' => $fichaCorporalEstriasQuantidade, 
							':fichaCorporalEstriasRegiao' => $fichaCorporalEstriasRegiao, 
							':fichaCorporalAlteracoesPosturaisOmbrosAnteriorizacao' => $fichaCorporalAlteracoesPosturaisOmbrosAnteriorizacao, 
							':fichaCorporalAlteracoesPosturaisOmbrosHiperextensao' => $fichaCorporalAlteracoesPosturaisOmbrosHiperextensao, 
							':fichaCorporalAlteracoesPosturaisColunaEscolioseEmC' => $fichaCorporalAlteracoesPosturaisColunaEscolioseEmC, 
							':fichaCorporalAlteracoesPosturaisColunaEscolioseEmS' => $fichaCorporalAlteracoesPosturaisColunaEscolioseEmS, 
							':fichaCorporalAlteracoesPosturaisColunaHipercifose' => $fichaCorporalAlteracoesPosturaisColunaHipercifose, 
							':fichaCorporalAlteracoesPosturaisColunaHiperlordose' => $fichaCorporalAlteracoesPosturaisColunaHiperlordose, 
							':fichaCorporalAlteracoesPosturaisColunaRetificacaoDaCifose' => $fichaCorporalAlteracoesPosturaisColunaRetificacaoDaCifose, 
							':fichaCorporalAlteracoesPosturaisColunaRetificacaoDaLordose' => $fichaCorporalAlteracoesPosturaisColunaRetificacaoDaLordose, 
							':fichaCorporalAlteracoesPosturaisQuadrilAntiversao' => $fichaCorporalAlteracoesPosturaisQuadrilAntiversao, 
							':fichaCorporalAlteracoesPosturaisQuadrilRetroversao' => $fichaCorporalAlteracoesPosturaisQuadrilRetroversao, 
							':fichaCorporalAlteracoesPosturaisJoelhosGenovalgo' => $fichaCorporalAlteracoesPosturaisJoelhosGenovalgo, 
							':fichaCorporalAlteracoesPosturaisJoelhosGenovaro' => $fichaCorporalAlteracoesPosturaisJoelhosGenovaro, 
							':fichaCorporalAlteracoesPosturaisJoelhosHiperextensao' => $fichaCorporalAlteracoesPosturaisJoelhosHiperextensao, 
							':fichaCorporalAlteracoesPosturaisOutros' => $fichaCorporalAlteracoesPosturaisOutros,
							':fichaCorporalObsTratamentoIndicado' => $fichaCorporalObsTratamentoIndicado,
                            ':fichaCorporalQtdSessoes' => $fichaCorporalQtdSessoes,
							':fichaCorporalHora' => $fichaCorporalHora,
							':fichaCorporalStatus' => $fichaCorporalStatus);

		if ($query->execute($parameters)) {
			$idFicha = $this->db->lastInsertId();
			$sql = "INSERT INTO `tratamento` (`idFichaCorporal`) VALUES ($idFicha)";
			$query = $this->db->prepare($sql);
        	$query->execute();
			return array('id'=> $idFicha,'boleano'=> 1);	
		} else {
			return false;
		}
	}

	public function addFichainserirPerimetria(	$idFichaCorporal,
															$fichaCorporalPerimetriaBracoD, 
															$fichaCorporalPerimetriaBracoE, 
															$fichaCorporalPerimetriaAbdSupCintura, 
															$fichaCorporalPerimetriaAbdInfQuadril, 
															$fichaCorporalPerimetriaCoxaSupD, 
															$fichaCorporalPerimetriaCoxaSupE, 
															$fichaCorporalPerimetriaCoxaInfD, 
															$fichaCorporalPerimetriaCoxaInfE, 
															$fichaCorporalPerimetriaJoelhoD, 
															$fichaCorporalPerimetriaJoelhoE,
															$dataAtual) 
	{
		$sql = "INSERT INTO fichaCorporalPerimetria( 	idFichaCorporal,
														fichaCorporalPerimetriaBracoD, 
														fichaCorporalPerimetriaBracoE, 
														fichaCorporalPerimetriaAbdSupCintura, 
														fichaCorporalPerimetriaAbdInfQuadril, 
														fichaCorporalPerimetriaCoxaSupD, 
														fichaCorporalPerimetriaCoxaSupE, 
														fichaCorporalPerimetriaCoxaInfD, 
														fichaCorporalPerimetriaCoxaInfE, 
														fichaCorporalPerimetriaJoelhoD, 
														fichaCorporalPerimetriaJoelhoE,
														fichaCorporalPerimetriaData) VALUES (	:idFichaCorporal,
																									:fichaCorporalPerimetriaBracoD, 
																									:fichaCorporalPerimetriaBracoE, 
																									:fichaCorporalPerimetriaAbdSupCintura, 
																									:fichaCorporalPerimetriaAbdInfQuadril, 
																									:fichaCorporalPerimetriaCoxaSupD, 
																									:fichaCorporalPerimetriaCoxaSupE, 
																									:fichaCorporalPerimetriaCoxaInfD, 
																									:fichaCorporalPerimetriaCoxaInfE, 
																									:fichaCorporalPerimetriaJoelhoD, 
																									:fichaCorporalPerimetriaJoelhoE,
																									CURDATE())";
		$query = $this->db->prepare($sql);
		$parameters = array(':idFichaCorporal' => $idFichaCorporal,
							':fichaCorporalPerimetriaBracoD' => $fichaCorporalPerimetriaBracoD, 
							':fichaCorporalPerimetriaBracoE' => $fichaCorporalPerimetriaBracoE, 
							':fichaCorporalPerimetriaAbdSupCintura' => $fichaCorporalPerimetriaAbdSupCintura, 
							':fichaCorporalPerimetriaAbdInfQuadril' => $fichaCorporalPerimetriaAbdInfQuadril, 
							':fichaCorporalPerimetriaCoxaSupD' => $fichaCorporalPerimetriaCoxaSupD, 
							':fichaCorporalPerimetriaCoxaSupE' => $fichaCorporalPerimetriaCoxaSupE, 
							':fichaCorporalPerimetriaCoxaInfD' => $fichaCorporalPerimetriaCoxaInfD, 
							':fichaCorporalPerimetriaCoxaInfE' => $fichaCorporalPerimetriaCoxaInfE, 
							':fichaCorporalPerimetriaJoelhoD' => $fichaCorporalPerimetriaJoelhoD, 
							':fichaCorporalPerimetriaJoelhoE' => $fichaCorporalPerimetriaJoelhoE);

		if ($query->execute($parameters)) {
			return true;
		} else {
			return false;
		}
	}

	public function addFichainserirAdipometria(	$idFichaGeral,
												$fichaCorporalAdipometriaRegiaoCorpo,
												$fichaCorporalAdipometriaPregaCM) 
	{
		$sql = "INSERT INTO fichaCorporalAdipometria( 	idFichaCorporal,
														fichaCorporalAdipometriaRegiaoCorpo,
														fichaCorporalAdipometriaPregaCM,
														fichaCorporalAdipometriaData) VALUES (		:idFichaCorporal,
																									:fichaCorporalAdipometriaRegiaoCorpo,
																									:fichaCorporalAdipometriaPregaCM,
																									CURDATE())";
		$query = $this->db->prepare($sql);
		$parameters = array(':idFichaCorporal' => $idFichaGeral,
							':fichaCorporalAdipometriaRegiaoCorpo' => $fichaCorporalAdipometriaRegiaoCorpo,
							':fichaCorporalAdipometriaPregaCM' => $fichaCorporalAdipometriaPregaCM);

		if ($query->execute($parameters)) {
			return true;
		} else {
			return false;
		}
	}

	public function inseririmg($idFichaCorporal,$foto)
    {
        $sql = "INSERT INTO fichaCorporalFoto (	idFichaCorporal, 
												fichaCorporalFotoNome) VALUES (	:idFichaCorporal, 
																				:fichaCorporalFotoNome)";
        $query = $this->db->prepare($sql);
        $parameters = array(':idFichaCorporal' 			=> $idFichaCorporal, 
							':fichaCorporalFotoNome' 	=> $foto);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
	}
	
	public function inserirMarcacaoCorporal($idFichaGeral,$descricao, $x, $y)
    {
        $sql = "INSERT INTO marcacaoCorporal (	idFichaCorporal, 
												obsCorporal,
												xCorporal,
												yCorporal) VALUES (	:idFichaCorporal, 
																	:obsCorporal,
																	:xCorporal,
																	:yCorporal)";
        $query = $this->db->prepare($sql);
		$parameters = array(':idFichaCorporal' 		=> $idFichaGeral,
							':obsCorporal' 			=> $descricao,
							':xCorporal' 			=> $x, 
							':yCorporal' 			=> $y);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
	}

	public function inserirMarcacaoCorporalAnamnese($idFichaAnamneseCriolipolise,$descricao, $x, $y)
    {
        $sql = "INSERT INTO marcacaoAnamneseCriolipolise (	idFichaAnamneseCriolipolise, 
												obsAnamneseCriolipolise,
												xAnamneseCriolipolise,
												yAnamneseCriolipolise) VALUES (	:idFichaAnamneseCriolipolise, 
																	:obsAnamneseCriolipolise,
																	:xAnamneseCriolipolise,
																	:yAnamneseCriolipolise)";
        $query = $this->db->prepare($sql);
		$parameters = array(':idFichaAnamneseCriolipolise' 		=> $idFichaAnamneseCriolipolise,
							':obsAnamneseCriolipolise' 			=> $descricao,
							':xAnamneseCriolipolise' 			=> $x, 
							':yAnamneseCriolipolise' 			=> $y);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
	}
	
	public function inserirMarcacaoFacial($idFichaFacil,$descricao, $x, $y)
    {
        $sql = "INSERT INTO marcacaoFacil 	(	idFichaFacil, 
												obsFacil,
												xFacil,
												yFacil) VALUES (	:idFichaFacil, 
																	:obsFacil,
																	:xFacil,
																	:yFacil)";
        $query = $this->db->prepare($sql);
		$parameters = array(':idFichaFacil' 		=> $idFichaFacil,
							':obsFacil' 			=> $descricao,
							':xFacil' 				=> $x, 
							':yFacil' 				=> $y);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

	public function addAvaliacaoFacial(	$idFichaGeral, 
										$idFunc, 
										$fichaFacilLinhasDeExpressao, 
										$fichaFacilCicatrizesDeAcneCicatrizAtrofica,
										$fichaFacilEnvelhecimentoPrecoce, 
										$fichaFacilFlacidez, 
										$fichaFacilDesidratacao, 
										$fichaFacilManchas, 
										$fichaFacilOutra, 
										$fichaFacilFototipoDePele, 
										$fichaFacilBiotipoCutaneo, 
										$fichaFacilHidratadaCutanea, 
										$fichaFacilComedao, 
										$fichaFacilMilium, 
										$fichaFacilPapula, 
										$fichaFacilPustula, 
										$fichaFacilMicrocisto, 
										$fichaFacilNodulo, 
										$fichaFacilQueratoseActinica, 
										$fichaFacilDermatosePapulosaNigra, 
										$fichaFacilObsDesiquilibrioDaPele, 
										$fichaFacilSequelaDeAcne, 
										$fichaFacilRegiaoSequelaDeAcne, 
										$fichaFacilExposiccaoSolar, 
										$fichaFacilFrequencia, 
										$fichaFacilNaoUsa, 
										$fichaFacilUsoSomenteQuandoExpostoAoSol, 
										$fichaFacilFrequenciaProtecaoSolar, 
										$fichaFacilProtecaoSolarUsoEsporadico, 
										$fichaFacilProtecaoSolarUsoDiario, 
										$fichaFacilProtecaoSolarFrequencia, 
										$fichaFacilHistorico, 
										$fichaFacilHipercromia,
										$fichaFacilRegiaoHipercromia, 
										$fichaFacilPeleSensivel,
									    $fichaFacilObsTratamentoIndicado,
									    $fichaFacilQtdSessoes,
										$fichaFacilData, 
										$fichaFacilHora, 
										$fichaFacilStatus) {		
		$sql = "INSERT INTO fichaFacil(	idFichaGeral, 
										idFunc, 
										fichaFacilLinhasDeExpressao, 
										fichaFacilCicatrizesDeAcneCicatrizAtrofica, 
										fichaFacilEnvelhecimentoPrecoce, 
										fichaFacilFlacidez, 
										fichaFacilDesidratacao, 
										fichaFacilManchas, 
										fichaFacilOutra, 
										fichaFacilFototipoDePele, 
										fichaFacilBiotipoCutaneo, 
										fichaFacilHidratadaCutanea, 
										fichaFacilComedao, 
										fichaFacilMilium, 
										fichaFacilPapula, 
										fichaFacilPustula, 
										fichaFacilMicrocisto, 
										fichaFacilNodulo, 
										fichaFacilQueratoseActinica, 
										fichaFacilDermatosePapulosaNigra, 
										fichaFacilObsDesiquilibrioDaPele, 
										fichaFacilSequelaDeAcne, 
										fichaFacilRegiaoSequelaDeAcne, 
										fichaFacilExposiccaoSolar, 
										fichaFacilFrequencia, 
										fichaFacilNaoUsa, 
										fichaFacilUsoSomenteQuandoExpostoAoSol, 
										fichaFacilFrequenciaProtecaoSolar, 
										fichaFacilProtecaoSolarUsoEsporadico, 
										fichaFacilProtecaoSolarUsoDiario, 
										fichaFacilProtecaoSolarFrequencia, 
										fichaFacilHistorico, 
										fichaFacilHipercromia,
										fichaFacilRegiaoHipercromia, 
										fichaFacilPeleSensivel,
										fichaFacilObsTratamentoIndicado,
                                        fichaFacilQtdSessoes,
										fichaFacilData, 
										fichaFacilHora, 
										fichaFacilStatus) VALUES (	:idFichaGeral,
																	:idFunc, 
																	:fichaFacilLinhasDeExpressao, 
																	:fichaFacilCicatrizesDeAcneCicatrizAtrofica, 
																	:fichaFacilEnvelhecimentoPrecoce, 
																	:fichaFacilFlacidez, 
																	:fichaFacilDesidratacao, 
																	:fichaFacilManchas, 
																	:fichaFacilOutra, 
																	:fichaFacilFototipoDePele, 
																	:fichaFacilBiotipoCutaneo, 
																	:fichaFacilHidratadaCutanea, 
																	:fichaFacilComedao, 
																	:fichaFacilMilium, 
																	:fichaFacilPapula, 
																	:fichaFacilPustula, 
																	:fichaFacilMicrocisto, 
																	:fichaFacilNodulo, 
																	:fichaFacilQueratoseActinica, 
																	:fichaFacilDermatosePapulosaNigra, 
																	:fichaFacilObsDesiquilibrioDaPele, 
																	:fichaFacilSequelaDeAcne, 
																	:fichaFacilRegiaoSequelaDeAcne, 
																	:fichaFacilExposiccaoSolar, 
																	:fichaFacilFrequencia, 
																	:fichaFacilNaoUsa, 
																	:fichaFacilUsoSomenteQuandoExpostoAoSol, 
																	:fichaFacilFrequenciaProtecaoSolar, 
																	:fichaFacilProtecaoSolarUsoEsporadico, 
																	:fichaFacilProtecaoSolarUsoDiario, 
																	:fichaFacilProtecaoSolarFrequencia, 
																	:fichaFacilHistorico, 
																	:fichaFacilHipercromia,
																	:fichaFacilRegiaoHipercromia, 
																	:fichaFacilPeleSensivel,
																	:fichaFacilObsTratamentoIndicado,
                                        							:fichaFacilQtdSessoes, 
																	CURDATE(), 
																	:fichaFacilHora, 
																	:fichaFacilStatus)";
		$query = $this->db->prepare($sql);
		$parameters = array(':idFichaGeral' 								=> $idFichaGeral,  
							':idFunc' 										=> $idFunc,   
							':fichaFacilLinhasDeExpressao' 					=> $fichaFacilLinhasDeExpressao, 
							':fichaFacilCicatrizesDeAcneCicatrizAtrofica' 	=> $fichaFacilCicatrizesDeAcneCicatrizAtrofica, 
							':fichaFacilEnvelhecimentoPrecoce' 				=> $fichaFacilEnvelhecimentoPrecoce, 
							':fichaFacilFlacidez' 							=> $fichaFacilFlacidez, 
							':fichaFacilDesidratacao'						=> $fichaFacilDesidratacao, 
							':fichaFacilManchas' 							=> $fichaFacilManchas, 
							':fichaFacilOutra' 								=> $fichaFacilOutra, 
							':fichaFacilFototipoDePele' 					=> $fichaFacilFototipoDePele, 
							':fichaFacilBiotipoCutaneo' 					=> $fichaFacilBiotipoCutaneo, 
							':fichaFacilHidratadaCutanea' 					=> $fichaFacilHidratadaCutanea, 
							':fichaFacilComedao' 							=> $fichaFacilComedao, 
							':fichaFacilMilium' 							=> $fichaFacilMilium, 
							':fichaFacilPapula' 							=> $fichaFacilPapula, 
							':fichaFacilPustula' 							=> $fichaFacilPustula, 
							':fichaFacilMicrocisto' 						=> $fichaFacilMicrocisto, 
							':fichaFacilNodulo' 							=> $fichaFacilNodulo, 
							':fichaFacilQueratoseActinica' 					=> $fichaFacilQueratoseActinica, 
							':fichaFacilDermatosePapulosaNigra' 			=> $fichaFacilDermatosePapulosaNigra, 
							':fichaFacilObsDesiquilibrioDaPele' 			=> $fichaFacilObsDesiquilibrioDaPele, 
							':fichaFacilSequelaDeAcne' 						=> $fichaFacilSequelaDeAcne, 
							':fichaFacilRegiaoSequelaDeAcne' 				=> $fichaFacilRegiaoSequelaDeAcne, 
							':fichaFacilExposiccaoSolar' 					=> $fichaFacilExposiccaoSolar, 
							':fichaFacilFrequencia' 						=> $fichaFacilFrequencia, 
							':fichaFacilNaoUsa' 							=> $fichaFacilNaoUsa, 
							':fichaFacilUsoSomenteQuandoExpostoAoSol' 		=> $fichaFacilUsoSomenteQuandoExpostoAoSol, 
							':fichaFacilFrequenciaProtecaoSolar' 			=> $fichaFacilFrequenciaProtecaoSolar, 
							':fichaFacilProtecaoSolarUsoEsporadico' 		=> $fichaFacilProtecaoSolarUsoEsporadico, 
							':fichaFacilProtecaoSolarUsoDiario' 			=> $fichaFacilProtecaoSolarUsoDiario, 
							':fichaFacilProtecaoSolarFrequencia' 			=> $fichaFacilProtecaoSolarFrequencia, 
							':fichaFacilHistorico' 							=> $fichaFacilHistorico, 
							':fichaFacilHipercromia' 						=> $fichaFacilHipercromia,
							':fichaFacilRegiaoHipercromia' 					=> $fichaFacilRegiaoHipercromia, 
							':fichaFacilPeleSensivel' 						=> $fichaFacilPeleSensivel, 
							':fichaFacilObsTratamentoIndicado' 				=> $fichaFacilObsTratamentoIndicado,
                            ':fichaFacilQtdSessoes' 						=> $fichaFacilQtdSessoes,
							':fichaFacilHora' 								=> $fichaFacilHora,
							':fichaFacilStatus' 							=> $fichaFacilStatus
		);
		if ($query->execute($parameters)) {
			$idFicha = $this->db->lastInsertId();
			$sql = "INSERT INTO `tratamento` (`idFichaFacil`) VALUES ($idFicha)";
			$query = $this->db->prepare($sql);
        	$query->execute();
			return array('id'=> $idFicha,'boleano'=> 1);
		} else {
			return false;
		}
	}

	public function AddFichaAnamneseCriolipolise(   $idFichaGeral, 
                                                    $idFunc, 
                                                    $fichaAnamnesePraticaAtividadeFisica, 
                                                    $fichaAnamneseQualPraticaAtividadeFisica, 
                                                    $fichaAnamneseTemAlgumTipoDeAlergia, 
                                                    $fichaAnamneseQualAlgumTipoDeAlergia, 
                                                    $fichaAnamneseTemAlgumtipodehormonal, 
                                                    $fichaAnamneseQualAlgumtipodehormonal, 
                                                    $fichaAnamnesePeso, 
                                                    $fichaAnamneseDoencasRelacionadasAoFrio, 
                                                    $fichaAnamneseHerniasUmbilicalOuInguinal, 
                                                    $fichaAnamneseDiastaseAbdominal, 
                                                    $fichaAnamneseLesaoAbertaNaPele, 
                                                    $fichaAnamneseLupusEritematosoSistemico, 
                                                    $fichaAnamneseCancerDePele, 
                                                    $fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves, 
                                                    $fichaAnamneseUsoDeAnticoagulante, 
                                                    $fichaAnamneseCirurgiaRecenteNaAreaTratada, 
                                                    $fichaAnamneseGestanteeOuAmamentando, 
                                                    $fichaAnamneseObservacoes, 
                                                    $fichaAnamneseObsTratamentoIndicado, 
                                                    $fichaAnamneseQtdSessoes, 
                                                    $fichaAnamneseData, 
                                                    $fichaAnamneseHora, 
                                                    $fichaAnamneseStatus) {
		$sql = "INSERT INTO fichaAnamneseCriolipolise(  idFichaGeral,
                                                        idFunc, 
                                                        fichaAnamnesePraticaAtividadeFisica, 
                                                        fichaAnamneseQualPraticaAtividadeFisica, 
                                                        fichaAnamneseTemAlgumTipoDeAlergia, 
                                                        fichaAnamneseQualAlgumTipoDeAlergia, 
                                                        fichaAnamneseTemAlgumtipodehormonal, 
                                                        fichaAnamneseQualAlgumtipodehormonal, 
                                                        fichaAnamnesePeso, 
                                                        fichaAnamneseDoencasRelacionadasAoFrio, 
                                                        fichaAnamneseHerniasUmbilicalOuInguinal, 
                                                        fichaAnamneseDiastaseAbdominal, 
                                                        fichaAnamneseLesaoAbertaNaPele, 
                                                        fichaAnamneseLupusEritematosoSistemico, 
                                                        fichaAnamneseCancerDePele, 
                                                        fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves, 
                                                        fichaAnamneseUsoDeAnticoagulante, 
                                                        fichaAnamneseCirurgiaRecenteNaAreaTratada, 
                                                        fichaAnamneseGestanteeOuAmamentando, 
                                                        fichaAnamneseObservacoes, 
                                                        fichaAnamneseObsTratamentoIndicado, 
                                                        fichaAnamneseQtdSessoes, 
                                                        fichaAnamneseData,  
                                                        fichaAnamneseHora, 
                                                        fichaAnamneseStatus) VALUES (   :idFichaGeral,
                                                                                        :idFunc, 
                                                                                        :fichaAnamnesePraticaAtividadeFisica, 
                                                                                        :fichaAnamneseQualPraticaAtividadeFisica, 
                                                                                        :fichaAnamneseTemAlgumTipoDeAlergia, 
                                                                                        :fichaAnamneseQualAlgumTipoDeAlergia, 
                                                                                        :fichaAnamneseTemAlgumtipodehormonal, 
                                                                                        :fichaAnamneseQualAlgumtipodehormonal, 
                                                                                        :fichaAnamnesePeso, 
                                                                                        :fichaAnamneseDoencasRelacionadasAoFrio, 
                                                                                        :fichaAnamneseHerniasUmbilicalOuInguinal, 
                                                                                        :fichaAnamneseDiastaseAbdominal, 
                                                                                        :fichaAnamneseLesaoAbertaNaPele, 
                                                                                        :fichaAnamneseLupusEritematosoSistemico, 
                                                                                        :fichaAnamneseCancerDePele, 
                                                                                        :fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves, 
                                                                                        :fichaAnamneseUsoDeAnticoagulante, 
                                                                                        :fichaAnamneseCirurgiaRecenteNaAreaTratada, 
                                                                                        :fichaAnamneseGestanteeOuAmamentando, 
                                                                                        :fichaAnamneseObservacoes, 
                                                                                        :fichaAnamneseObsTratamentoIndicado, 
                                                                                        :fichaAnamneseQtdSessoes, 
                                                                                        CURRENT_DATE(), 
                                                                                        :fichaAnamneseHora, 
                                                                                        :fichaAnamneseStatus)";
		$query = $this->db->prepare($sql);
		$parameters = array(':idFichaGeral'                                                     => $idFichaGeral, 
                            ':idFunc'                                                           => $idFunc, 
                            ':fichaAnamnesePraticaAtividadeFisica'                              => $fichaAnamnesePraticaAtividadeFisica, 
                            ':fichaAnamneseQualPraticaAtividadeFisica'                          => $fichaAnamneseQualPraticaAtividadeFisica, 
                            ':fichaAnamneseTemAlgumTipoDeAlergia'                               => $fichaAnamneseTemAlgumTipoDeAlergia, 
                            ':fichaAnamneseQualAlgumTipoDeAlergia'                              => $fichaAnamneseQualAlgumTipoDeAlergia, 
                            ':fichaAnamneseTemAlgumtipodehormonal'                              => $fichaAnamneseTemAlgumtipodehormonal, 
                            ':fichaAnamneseQualAlgumtipodehormonal'                             => $fichaAnamneseQualAlgumtipodehormonal, 
                            ':fichaAnamnesePeso'                                                => $fichaAnamnesePeso, 
                            ':fichaAnamneseDoencasRelacionadasAoFrio'                           => $fichaAnamneseDoencasRelacionadasAoFrio, 
                            ':fichaAnamneseHerniasUmbilicalOuInguinal'                          => $fichaAnamneseHerniasUmbilicalOuInguinal, 
                            ':fichaAnamneseDiastaseAbdominal'                                   => $fichaAnamneseDiastaseAbdominal, 
                            ':fichaAnamneseLesaoAbertaNaPele'                                   => $fichaAnamneseLesaoAbertaNaPele, 
                            ':fichaAnamneseLupusEritematosoSistemico'                           => $fichaAnamneseLupusEritematosoSistemico, 
                            ':fichaAnamneseCancerDePele'                                        => $fichaAnamneseCancerDePele, 
                            ':fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves'    => $fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves, 
                            ':fichaAnamneseUsoDeAnticoagulante'                                 => $fichaAnamneseUsoDeAnticoagulante, 
                            ':fichaAnamneseCirurgiaRecenteNaAreaTratada'                        => $fichaAnamneseCirurgiaRecenteNaAreaTratada, 
                            ':fichaAnamneseGestanteeOuAmamentando'                              => $fichaAnamneseGestanteeOuAmamentando, 
                            ':fichaAnamneseObservacoes'                                         => $fichaAnamneseObservacoes, 
                            ':fichaAnamneseObsTratamentoIndicado'                               => $fichaAnamneseObsTratamentoIndicado, 
                            ':fichaAnamneseQtdSessoes'                                          => $fichaAnamneseQtdSessoes, 
                            ':fichaAnamneseHora'                                                => $fichaAnamneseHora, 
                            ':fichaAnamneseStatus'                                              => $fichaAnamneseStatus);
		if ($query->execute($parameters)) {
			$idFicha = $this->db->lastInsertId();
			$sql = "INSERT INTO `tratamento` (`idFichaAnamneseCriolipolise`) VALUES ($idFicha)";
			$query = $this->db->prepare($sql);
        	$query->execute();
			return array('id'=> $idFicha,'boleano'=> 1);
		} else {
			return false;
		}
	}

	/*#################  DESATIVAR UM Ficha DO BANCO DE DADOS ####################*/
	public function desativarFichaGeral($idFichaGeral)
	{
		$sql = "UPDATE fichageral SET fichaStatus = '0' WHERE fichageral.idFichaGeral = :idFichaGeral";
		$query = $this->db->prepare($sql);
		$parameters = array(':idFichaGeral' => $idFichaGeral);
		$query->execute($parameters);
	}

	/* ########################## LISTAR FICHA PARA EDIÇÃO PELO ID ################ */
	public function listaFichaGeral($id)
	{
		$sql = "SELECT * FROM fichageral WHERE fichageral.idFichaGeral = $id";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function listaFichaGeralCliente($id, $DataAtual)
	{
		$sql = "SELECT * FROM fichageral WHERE idCliente = $id ORDER BY idFichaGeral DESC LIMIT 1";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	public function listaFichaGeralClienteHistorico($idFicha)
	{
		$sql = "SELECT * FROM fichageral WHERE idFichaGeral = $idFicha";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}
	
	
    /** ############ AVALIAÇÃO GERAL ( ANAMSESE ) ###########################################*/
    public function listaFichaGeralClienteData($id)
	{
		$sql = "SELECT * FROM fichageral WHERE idCliente = $id ORDER BY fichaData DESC";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}
	
	/** ############ AVALIAÇÃO CORPORAL ###########################################*/
    public function listaFichaCorporalClienteData($id)
	{
		$sql = "SELECT fichaCorporal.*, fichageral.idCliente 
				FROM fichaCorporal 
				INNER JOIN fichageral ON fichageral.idFichaGeral = fichaCorporal.idFichaGeral 
				WHERE fichageral.idCliente = $id ORDER BY fichaCorporal.fichaCorporalData DESC";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	public function FichaCorporalCliente($idFicha)
	{
		$sql = "SELECT cliSexo FROM `cliente` WHERE idCliente = (SELECT idCliente FROM `fichageral` WHERE idFichaGeral = (SELECT idFichaGeral FROM `fichaCorporal` WHERE idFichaCorporal = $idFicha))";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}
	
	public function FichaCorporalMarcacao($idFicha)
	{
		//$sql = "SELECT * FROM `marcacaoCorporal` WHERE idFichaCorporal = (SELECT fichaCorporal.idFichaCorporal FROM `fichageral` INNER JOIN fichaCorporal ON fichaCorporal.idFichaGeral = fichageral.idFichaGeral WHERE fichageral.idFichaGeral = $idFicha ORDER BY fichaCorporal.idFichaCorporal DESC LIMIT 1)";
		$sql = "SELECT * FROM `marcacaoCorporal` WHERE idFichaCorporal = $idFicha";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	public function FichaCorporal($idFicha)
	{
		//$ sql = "SELECT fichaCorporal.*, fichaCorporalPerimetria.* FROM `fichageral` INNER JOIN fichaCorporal ON fichaCorporal.idFichaGeral = fichageral.idFichaGeral INNER JOIN fichaCorporalPerimetria ON fichaCorporalPerimetria.idFichaCorporal = fichaCorporal.idFichaCorporal WHERE fichageral.idFichaGeral = $idFicha ORDER BY fichaCorporal.idFichaCorporal DESC limit 1";
		$sql = "SELECT fichaCorporal.* FROM fichaCorporal WHERE fichaCorporal.idfichaCorporal = $idFicha";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	public function FichaCorporalFoto($idFicha)
	{
		$sql = "SELECT * FROM `fichaCorporalFoto` WHERE idFichaCorporal = (SELECT fichaCorporal.idFichaCorporal FROM `fichageral` INNER JOIN fichaCorporal ON fichaCorporal.idFichaGeral = fichageral.idFichaGeral WHERE fichageral.idFichaGeral = $idFicha ORDER by fichaCorporal.idFichaCorporal DESC LIMIT 1)";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	public function FichaCorporalPerimetria($idFicha)
	{
		//$sql = "SELECT * FROM `fichaCorporalAdipometria` WHERE idFichaCorporal = (SELECT fichaCorporal.idFichaCorporal FROM `fichageral` INNER JOIN fichaCorporal ON fichaCorporal.idFichaGeral = fichageral.idFichaGeral WHERE fichageral.idFichaGeral = $idFicha ORDER BY fichaCorporal.idFichaCorporal DESC LIMIT 1)";
		$sql = "SELECT fichaCorporalPerimetria.* FROM `fichaCorporalPerimetria` WHERE idFichaCorporal = $idFicha";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	public function FichaCorporalAdipometria($idFicha)
	{
		//$sql = "SELECT * FROM `fichaCorporalAdipometria` WHERE idFichaCorporal = (SELECT fichaCorporal.idFichaCorporal FROM `fichageral` INNER JOIN fichaCorporal ON fichaCorporal.idFichaGeral = fichageral.idFichaGeral WHERE fichageral.idFichaGeral = $idFicha ORDER BY fichaCorporal.idFichaCorporal DESC LIMIT 1)";
		$sql = "SELECT * FROM `fichaCorporalAdipometria` WHERE idFichaCorporal = $idFicha";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}


	
	/** ############ AVALIAÇÃO FACIAL ############################################*/
    public function listaFichaFacialClienteData($id)
	{
		$sql = "SELECT fichaFacil.*, fichageral.idCliente 
				FROM fichaFacil 
				INNER JOIN fichageral ON fichageral.idFichaGeral = fichaFacil.idFichaGeral 
				WHERE fichageral.idCliente = $id ORDER BY fichaFacil.fichaFacilData DESC";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}
	

	public function FichaFacial($idFicha)
	{
		$sql = "SELECT * FROM `fichaFacil` WHERE idfichaFacil = $idFicha ORDER BY idFichaFacil DESC limit 1 ";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	public function FichaFaciall($idFicha)
	{
		$sql = "SELECT cliSexo FROM `cliente` WHERE idCliente = (SELECT idCliente FROM `fichageral` WHERE idFichaGeral = (SELECT idFichaGeral FROM `fichaFacil` WHERE idFichaFacil = $idFicha))";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	public function FichaFacialMarcacao($idFicha)
	{
		$sql = "SELECT * FROM `marcacaoFacil` WHERE idFichaFacil =  $idFicha ";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	public function FichaFacialCliente($idFicha, $idcliente)
	{
		//$sql = "SELECT fichaCorporal.*, cliente.cliSexo FROM `fichaCorporal` INNER JOIN cliente ON cliente.idCliente = (SELECT idCliente FROM cliente WHERE idcliente = $id ) ORDER BY fichaCorporal.idFichaCorporal LIMIT 1";
		$sql = "SELECT fichaCorporal.*, cliente.cliSexo FROM `fichaCorporal` INNER JOIN cliente ON cliente.idCliente = $idcliente WHERE fichaCorporal.idFichaCorporal = $idFicha";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	
	/** ############ ANAMNESE CRIOLIPOLISE ############################################*/
    public function listaFichaAnamneseClienteData($id)
	{
		$sql = "SELECT fichaAnamneseCriolipolise.*, fichageral.idCliente 
				FROM fichaAnamneseCriolipolise 
				INNER JOIN fichageral ON fichageral.idFichaGeral = fichaAnamneseCriolipolise.idFichaGeral 
				WHERE fichageral.idCliente = $id ORDER BY fichaAnamneseCriolipolise.fichaAnamneseData DESC";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	public function sexoFichaAnamnese($idFicha)
	{
		$sql = "SELECT cliSexo FROM `cliente` WHERE idCliente = (SELECT idCliente FROM `fichageral` WHERE idFichaGeral = (SELECT idFichaGeral FROM `fichaAnamneseCriolipolise` WHERE idFichaAnamneseCriolipolise = $idFicha))";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	public function listaIdFichaAnamnese($idFicha)
	{
		$sql = "SELECT * FROM `fichaAnamneseCriolipolise` WHERE idFichaAnamneseCriolipolise = $idFicha";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	public function FichaAnamneseMarcacao($idFicha)
	{
		$sql = "SELECT * FROM `marcacaoAnamneseCriolipolise` WHERE idFichaAnamneseCriolipolise =  $idFicha ";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll(); 
	}

	/*####################### ATUALIZAR UM FICHA NO BANCO ######################## */
	public function atualizarFichaGeral($fichaHistoricoDeDermatiteOuCancerDePele,
										$fichaEmQualLocalDoCorpo,
										$fichaFazQuantoTempo,
										$fichaFoiLiberadoPeloMedicoParaTratamentoEstetico,
										$fichaHipertireoidismo,
										$fichaTratamentoHipertireoidismo,
										$fichaHipotireoidismo,
										$fichaTratamentoHipotireoidismo,
										$fichaDiabetes,
										$fichaTratamentoDiabetes,
										$fichaHipertensao,
										$fichaTratamentoHipertensao,
										$fichaEplepsia,
										$fichaTratamentoEplepsia,
										$fichaPsoriase,
										$fichaTratamentoPsoriase,
										$fichaOutrasDoencas,
										$fichaTemImplantesMetalicos,
										$fichaLocalTemImplantesMetalicos,
										$fichaPortadorDeMarcapasso,
										$fichaQuantoTempoPortadorDeMarcapasso,
										$fichaPortadorDeHIV,
										$fichaTratamentoPortadorDeHIV,
										$fichaPortadorDeHepatite,
										$fichaQualPortadorDeHepatite,
										$fichaAlteracaoHormonal,
										$fichaQualAlteracaoHormonal,
										$fichaAlergiaAlgumAlimento,
										$fichaQualAlergiaAlgumAlimento,
										$fichaAlergiaAlgumMedicamento,
										$fichaQualAlergiaAlgumMedicamento,
										$fichaFazUsoDeMedicamento,
										$fichaQualFazUsoDeMedicamento,
										$fichaPresencaDeQueloides,
										$fichaQueLocalPresencaDeQueloides,
										$fichaGestante,
										$fichaQuantoTempoGestante,
										$fichaFilhos,
										$fichaQuantosFilhosIdade,
										$fichaFezAlgumaCirurgiaPlastica,
										$fichaQualFezAlgumaCirurgiaPlastica,
										$fichaRealizouProcedimentosEsteticosAnteriores,
										$fichaQualRealizouProcedimentosEsteticosAnteriores,
										$fichaFazUsoDeMaquiagem,
										$fichaQualFazUsoDeMaquiagem,
										$fichaFazAtividadeFisica,
										$fichaQuantasVezesPorSemanaAtividadeFisica,
										$fichaConsumoAguaIngereQuantosCoposDia,
										$fichaIngereBebidaAlcoolica,
										$fichaQuantasDosesPorSemanaBebidaAlcoolica,
										$fichaIngereBebidaGaseificada,
										$fichaQuantasDosesPorSemanaBebidaGaseificada,
										$fichaJaFumouOuFuma,
										$fichaQuantosCigarrosPorDia,
										$fichaEstresse,
										$fichaSono,
										$fichaQuantasHorasPorNoiteSono,
										$fichaPosicaoQuePermanecePorMaisTempo,
										$fichaAlimentacao,
										$fichaFazDietaQualQuantoTempo,
										$fichaTermo,
										$fichaTratamentoEsteticoIndicado,
										$fichaNumeroSessoesTratamentoEsteticoIndicado,
										$fichaAssinaturaCliente,
										$fichaAssinaturaFuncionario,
										$fichaCidade,
										$fichaOqueTeIncomoda, 
										$fichaQualSeuTime, 
										$fichaOqueVcMudariaEmVc, 
										$fichaQualSeuEstiloMusica, 
										$fichaData,
										$fichaHora,
										$fichaStatus,
										$idFichaGeral
	) {
		$sql = "UPDATE fichageral, cliente SET 	 
										fichaHistoricoDeDermatiteOuCancerDePele = :fichaHistoricoDeDermatiteOuCancerDePele, 
										fichaEmQualLocalDoCorpo = :fichaEmQualLocalDoCorpo, 
										fichaFazQuantoTempo = :fichaFazQuantoTempo, 
										fichaFoiLiberadoPeloMedicoParaTratamentoEstetico = :fichaFoiLiberadoPeloMedicoParaTratamentoEstetico, 
										fichaHipertireoidismo = :fichaHipertireoidismo, 
										fichaTratamentoHipertireoidismo = :fichaTratamentoHipertireoidismo, 
										fichaHipotireoidismo = :fichaHipotireoidismo, 
										fichaTratamentoHipotireoidismo = :fichaTratamentoHipotireoidismo, 
										fichaDiabetes = :fichaDiabetes, 
										fichaTratamentoDiabetes = :fichaTratamentoDiabetes, 
										fichaHipertensao = :fichaHipertensao, 
										fichaTratamentoHipertensao = :fichaTratamentoHipertensao, 
										fichaEplepsia = :fichaEplepsia, 
										fichaTratamentoEplepsia = :fichaTratamentoEplepsia, 
										fichaPsoriase = :fichaPsoriase, 
										fichaTratamentoPsoriase = :fichaTratamentoPsoriase, 
										fichaOutrasDoencas = :fichaOutrasDoencas, 
										fichaTemImplantesMetalicos = :fichaTemImplantesMetalicos, 
										fichaLocalTemImplantesMetalicos = :fichaLocalTemImplantesMetalicos, 
										fichaPortadorDeMarcapasso = :fichaPortadorDeMarcapasso, 
										fichaQuantoTempoPortadorDeMarcapasso = :fichaQuantoTempoPortadorDeMarcapasso, 
										fichaPortadorDeHIV = :fichaPortadorDeHIV, 
										fichaTratamentoPortadorDeHIV = :fichaTratamentoPortadorDeHIV, 
										fichaPortadorDeHepatite = :fichaPortadorDeHepatite, 
										fichaQualPortadorDeHepatite = :fichaQualPortadorDeHepatite, 
										fichaAlteracaoHormonal = :fichaAlteracaoHormonal, 
										fichaQualAlteracaoHormonal = :fichaQualAlteracaoHormonal, 
										fichaAlergiaAlgumAlimento = :fichaAlergiaAlgumAlimento, 
										fichaQualAlergiaAlgumAlimento = :fichaQualAlergiaAlgumAlimento, 
										fichaAlergiaAlgumMedicamento = :fichaAlergiaAlgumMedicamento, 
										fichaQualAlergiaAlgumMedicamento = :fichaQualAlergiaAlgumMedicamento, 
										fichaFazUsoDeMedicamento = :fichaFazUsoDeMedicamento, 
										fichaQualFazUsoDeMedicamento = :fichaQualFazUsoDeMedicamento, 
										fichaPresencaDeQueloides = :fichaPresencaDeQueloides, 
										fichaQueLocalPresencaDeQueloides = :fichaQueLocalPresencaDeQueloides, 
										fichaGestante = :fichaGestante, 
										fichaQuantoTempoGestante = :fichaQuantoTempoGestante, 
										fichaFilhos = :fichaFilhos, 
										fichaQuantosFilhosIdade = :fichaQuantosFilhosIdade, 
										fichaFezAlgumaCirurgiaPlastica = :fichaFezAlgumaCirurgiaPlastica, 
										fichaQualFezAlgumaCirurgiaPlastica = :fichaQualFezAlgumaCirurgiaPlastica, 
										fichaRealizouProcedimentosEsteticosAnteriores = :fichaRealizouProcedimentosEsteticosAnteriores, 
										fichaQualRealizouProcedimentosEsteticosAnteriores = :fichaQualRealizouProcedimentosEsteticosAnteriores, 
										fichaFazUsoDeMaquiagem = :fichaFazUsoDeMaquiagem, 
										fichaQualFazUsoDeMaquiagem = :fichaQualFazUsoDeMaquiagem, 
										fichaFazAtividadeFisica = :fichaFazAtividadeFisica, 
										fichaQuantasVezesPorSemanaAtividadeFisica = :fichaQuantasVezesPorSemanaAtividadeFisica, 
										fichaConsumoAguaIngereQuantosCoposDia = :fichaConsumoAguaIngereQuantosCoposDia, 
										fichaIngereBebidaAlcoolica = :fichaIngereBebidaAlcoolica, 
										fichaQuantasDosesPorSemanaBebidaAlcoolica = :fichaQuantasDosesPorSemanaBebidaAlcoolica, 
										fichaIngereBebidaGaseificada = :fichaIngereBebidaGaseificada, 
										fichaQuantasDosesPorSemanaBebidaGaseificada = :fichaQuantasDosesPorSemanaBebidaGaseificada, 
										fichaJaFumouOuFuma = :fichaJaFumouOuFuma, 
										fichaQuantosCigarrosPorDia = :fichaQuantosCigarrosPorDia, 
										fichaEstresse = :fichaEstresse, 
										fichaSono = :fichaSono, 
										fichaQuantasHorasPorNoiteSono = :fichaQuantasHorasPorNoiteSono, 
										fichaPosicaoQuePermanecePorMaisTempo = :fichaPosicaoQuePermanecePorMaisTempo, 
										fichaAlimentacao = :fichaAlimentacao, 
										fichaFazDietaQualQuantoTempo = :fichaFazDietaQualQuantoTempo, 
										fichaTermo = :fichaTermo, 
										fichaTratamentoEsteticoIndicado = :fichaTratamentoEsteticoIndicado, 
										fichaNumeroSessoesTratamentoEsteticoIndicado = :fichaNumeroSessoesTratamentoEsteticoIndicado, 
										fichaAssinaturaCliente = :fichaAssinaturaCliente, 
										fichaAssinaturaFuncionario = :fichaAssinaturaFuncionario, 
										fichaCidade = :fichaCidade,
										:fichaOqueTeIncomoda, 
										:fichaQualSeuTime, 
										:fichaOqueVcMudariaEmVc, 
										:fichaQualSeuEstiloMusica, 
										fichaData = :fichaData, 
										fichaHora = :fichaHora, 
										fichaStatus = :fichaStatus,
										cliente.cliPermitirAtualizacaoGeral = :cliPermitirAtualizacaoGeral WHERE idFichaGeral = :idFichaGeral";
		$query = $this->db->prepare($sql);
		$parameters = array(':fichaHistoricoDeDermatiteOuCancerDePele' => $fichaHistoricoDeDermatiteOuCancerDePele,
							':fichaEmQualLocalDoCorpo' => $fichaEmQualLocalDoCorpo,
							':fichaFazQuantoTempo' => $fichaFazQuantoTempo,
							':fichaFoiLiberadoPeloMedicoParaTratamentoEstetico' => $fichaFoiLiberadoPeloMedicoParaTratamentoEstetico,
							':fichaHipertireoidismo' => $fichaHipertireoidismo,
							':fichaTratamentoHipertireoidismo' => $fichaTratamentoHipertireoidismo,
							':fichaHipotireoidismo' => $fichaHipotireoidismo,
							':fichaTratamentoHipotireoidismo' => $fichaTratamentoHipotireoidismo,
							':fichaDiabetes' => $fichaDiabetes,
							':fichaTratamentoDiabetes' => $fichaTratamentoDiabetes,
							':fichaHipertensao' => $fichaHipertensao,
							':fichaTratamentoHipertensao' => $fichaTratamentoHipertensao,
							':fichaEplepsia' => $fichaEplepsia,
							':fichaTratamentoEplepsia' => $fichaTratamentoEplepsia,
							':fichaPsoriase' => $fichaPsoriase,
							':fichaTratamentoPsoriase' => $fichaTratamentoPsoriase,
							':fichaOutrasDoencas' => $fichaOutrasDoencas,
							':fichaTemImplantesMetalicos' => $fichaTemImplantesMetalicos,
							':fichaLocalTemImplantesMetalicos' => $fichaLocalTemImplantesMetalicos,
							':fichaPortadorDeMarcapasso' => $fichaPortadorDeMarcapasso,
							':fichaQuantoTempoPortadorDeMarcapasso' => $fichaQuantoTempoPortadorDeMarcapasso,
							':fichaPortadorDeHIV' => $fichaPortadorDeHIV,
							':fichaTratamentoPortadorDeHIV' => $fichaTratamentoPortadorDeHIV,
							':fichaPortadorDeHepatite' => $fichaPortadorDeHepatite,
							':fichaQualPortadorDeHepatite' => $fichaQualPortadorDeHepatite,
							':fichaAlteracaoHormonal' => $fichaAlteracaoHormonal,
							':fichaQualAlteracaoHormonal' => $fichaQualAlteracaoHormonal,
							':fichaAlergiaAlgumAlimento' => $fichaAlergiaAlgumAlimento,
							':fichaQualAlergiaAlgumAlimento' => $fichaQualAlergiaAlgumAlimento,
							':fichaAlergiaAlgumMedicamento' => $fichaAlergiaAlgumMedicamento,
							':fichaQualAlergiaAlgumMedicamento' => $fichaQualAlergiaAlgumMedicamento,
							':fichaFazUsoDeMedicamento' => $fichaFazUsoDeMedicamento,
							':fichaQualFazUsoDeMedicamento' => $fichaQualFazUsoDeMedicamento,
							':fichaPresencaDeQueloides' => $fichaPresencaDeQueloides,
							':fichaQueLocalPresencaDeQueloides' => $fichaQueLocalPresencaDeQueloides,
							':fichaGestante' => $fichaGestante,
							':fichaQuantoTempoGestante' => $fichaQuantoTempoGestante,
							':fichaFilhos' => $fichaFilhos,
							':fichaQuantosFilhosIdade' => $fichaQuantosFilhosIdade,
							':fichaFezAlgumaCirurgiaPlastica' => $fichaFezAlgumaCirurgiaPlastica,
							':fichaQualFezAlgumaCirurgiaPlastica' => $fichaQualFezAlgumaCirurgiaPlastica,
							':fichaRealizouProcedimentosEsteticosAnteriores' => $fichaRealizouProcedimentosEsteticosAnteriores,
							':fichaQualRealizouProcedimentosEsteticosAnteriores' => $fichaQualRealizouProcedimentosEsteticosAnteriores,
							':fichaFazUsoDeMaquiagem' => $fichaFazUsoDeMaquiagem,
							':fichaQualFazUsoDeMaquiagem' => $fichaQualFazUsoDeMaquiagem,
							':fichaFazAtividadeFisica' => $fichaFazAtividadeFisica,
							':fichaQuantasVezesPorSemanaAtividadeFisica' => $fichaQuantasVezesPorSemanaAtividadeFisica,
							':fichaConsumoAguaIngereQuantosCoposDia' => $fichaConsumoAguaIngereQuantosCoposDia,
							':fichaIngereBebidaAlcoolica' => $fichaIngereBebidaAlcoolica,
							':fichaQuantasDosesPorSemanaBebidaAlcoolica' => $fichaQuantasDosesPorSemanaBebidaAlcoolica,
							':fichaIngereBebidaGaseificada' => $fichaIngereBebidaGaseificada,
							':fichaQuantasDosesPorSemanaBebidaGaseificada' => $fichaQuantasDosesPorSemanaBebidaGaseificada,
							':fichaJaFumouOuFuma' => $fichaJaFumouOuFuma,
							':fichaQuantosCigarrosPorDia' => $fichaQuantosCigarrosPorDia,
							':fichaEstresse' => $fichaEstresse,
							':fichaSono' => $fichaSono,
							':fichaQuantasHorasPorNoiteSono' => $fichaQuantasHorasPorNoiteSono,
							':fichaPosicaoQuePermanecePorMaisTempo' => $fichaPosicaoQuePermanecePorMaisTempo,
							':fichaAlimentacao' => $fichaAlimentacao,
							':fichaFazDietaQualQuantoTempo' => $fichaFazDietaQualQuantoTempo,
							':fichaTermo' => $fichaTermo,
							':fichaTratamentoEsteticoIndicado' => $fichaTratamentoEsteticoIndicado,
							':fichaNumeroSessoesTratamentoEsteticoIndicado' => $fichaNumeroSessoesTratamentoEsteticoIndicado,
							':fichaAssinaturaCliente' => $fichaAssinaturaCliente,
							':fichaAssinaturaFuncionario' => $fichaAssinaturaFuncionario,
							':fichaCidade' => $fichaCidade,
							':fichaOqueTeIncomoda' => $fichaOqueTeIncomoda, 
							':fichaQualSeuTime' => $fichaQualSeuTime, 
							':fichaOqueVcMudariaEmVc' => $fichaOqueVcMudariaEmVc, 
							':fichaQualSeuEstiloMusica' => $fichaQualSeuEstiloMusica, 
							':fichaData' => $fichaData,
							':fichaHora' => $fichaHora,
							':fichaStatus' => $fichaStatus,
							':idFichaGeral' => $idFichaGeral,
							':cliPermitirAtualizacaoGeral' => 0
		);
		if ($query->execute($parameters)) {
			return true;
		} else {
			return false;
		}
	}


	/*################### "ESTATÍSTICAS" QTD DE FICHA GERAL ################# */
	public function dadosQtdFichaGeral()
	{
		$sql = "SELECT COUNT(idFichaGeral) AS amount_of_fichaGeral FROM fichageral";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetch()->amount_of_fichaGeral;
	}

	/*################ RECEBER DETALHE DA FICHA GERAL DO BANCO E CRIAR O PERFIL ##########################*/
	public function getDetalheFichaGeral($idFichaGeral)
	{
		$sql = "SELECT fichageral.*, cliente.*, funcionario.funcNome FROM fichageral 
				INNER JOIN cliente ON cliente.idCliente = fichageral.idCliente 
				INNER JOIN funcionario ON funcionario.idFunc = fichageral.idFunc
				WHERE fichageral.idFichaGeral = :idFichaGeral";
		$query = $this->db->prepare($sql);
		$parameters = array('idFichaGeral' => $idFichaGeral);
		$query->execute($parameters);
		return ($query->rowcount() ? $query->fetch() : false);
	}

	/* ICARO... ESSE METODO... QUAL O OBJETIVO??? */
	/*############# RECEBER UM FICHA COM OS DADOS DO CLIENTE E ESPECIALISTA DO BANCO ######################*/
	public function getFichaGeral($idFichaGeral)
	{
		$sql = "SELECT 	fichageral.*,
						cliente.*,
						funcionario.funcNome 
						FROM fichageral 
						INNER JOIN cliente ON cliente.idCliente = fichageral.idCliente 
						INNER JOIN funcionario ON funcionario.idFunc = fichageral.idFunc WHERE idFichaGeral = :idFichaGeral";
		$query = $this->db->prepare($sql);
		$parameters = array('idFichaGeral' => $idFichaGeral);
		$query->execute($parameters);
		return ($query->rowcount() ? $query->fetch() : false);
	}

	/* 
#####################################################################
#############											#############
#############			FICHA ANAMNESE COVID-19			#############
#############											#############
#####################################################################
*/

	/* ########## OBTER TODAS OS DADOS DA FICHA ANAMNESE COVID-19, CLIENTE E FUNCIONARIO DO BANCO DE DADOS ################     */
	public function todosFichaAnamneseCovid19()
	{
		$sql = "SELECT 	fichaAnamneseCovid19.*,
						cliente.*,
						funcionario.funcNome 
						FROM fichaAnamneseCovid19 
						INNER JOIN cliente ON cliente.idCliente = fichaAnamneseCovid19.idCliente 
						INNER JOIN funcionario ON funcionario.idFunc = fichaAnamneseCovid19.idFunc";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	/* ############ ADICIONAR UMA FICHA ANAMNESE COVID-19 NO BANCO ################*/
	public function addAnamneseCovid19(	$idCliente,
										$fichaAnamneseCovid19JaFezTesteCovid19,
										$fichaAnamneseCovid19QualTesteCovid19,
										$fichaAnamneseCovid19Resultado,
										$fichaAnamneseCovid19ObsTesteCovid19,
										$fichaAnamneseCovid19QuantoTempoFazQueFezTeste,
										$fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo,
										$fichaAnamneseCovid19QuantoTempoFazContatoComPessoas,
										$fichaAnamneseCovid19FebreQuantoTempoFaz,
										$fichaAnamneseCovid19TosseQuantoTempoFaz,
										$fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz,
										$fichaAnamneseCovid19FaltaDeArQuantoTempoFaz,
										$fichaAnamneseCovid19DiarreiaQuantoTempoFaz,
										$fichaAnamneseCovid19CorizaQuantoTempoFaz,
										$fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz,
										$fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz,
										$fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz,
										$fichaAnamneseCovid19CansacoQuantoTempoFaz,
										$fichaAnamneseCovid19eFumante,
										$fichaAnamneseCovid19QuantoTempoEFumante,
										$fichaAnamneseCovid19TemAlgumVicio,
										$fichaAnamneseCovid19QualAlgumVicio,
										$fichaAnamneseCovid19TemAlgumProblemaDeSaude,
										$fichaAnamneseCovid19QualProblemaDeSaude,
										$fichaAnamneseCovid19Diabete,
										$fichaAnamneseCovid19DoencaCardiaca,
										$fichaAnamneseCovid19ProblemaRenal,
										$fichaAnamneseCovid19DoencaRespiratotiaCronica,
										$fichaAnamneseCovid19RiniteSinusite,
										$fichaAnamneseCovid19PressaoAlta,
										$fichaAnamneseCovid19Asma,
										$fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament,
										$fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais,
										$fichaAnamneseCovid19MoraComFamiliares,
										$fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude,
										$fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude,
										$fichaAnamneseCovid19Data,
										$fichaAnamneseCovid19Hora,
										$fichaAnamneseCovid19Status
	) {

		$sql = "UPDATE cliente SET cliPermitirAtualizacaoCovid = 0 WHERE idCliente = $idCliente";
		$query = $this->db->prepare($sql);   
		
		if($query->execute()) {
			$sql = "INSERT INTO fichaAnamneseCovid19(	idCliente,
														fichaAnamneseCovid19JaFezTesteCovid19, 
														fichaAnamneseCovid19QualTesteCovid19, 
														fichaAnamneseCovid19Resultado, 
														fichaAnamneseCovid19ObsTesteCovid19, 
														fichaAnamneseCovid19QuantoTempoFazQueFezTeste, 
														fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo, 
														fichaAnamneseCovid19QuantoTempoFazContatoComPessoas, 
														fichaAnamneseCovid19FebreQuantoTempoFaz, 
														fichaAnamneseCovid19TosseQuantoTempoFaz, 
														fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz, 
														fichaAnamneseCovid19FaltaDeArQuantoTempoFaz, 
														fichaAnamneseCovid19DiarreiaQuantoTempoFaz, 
														fichaAnamneseCovid19CorizaQuantoTempoFaz, 
														fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz, 
														fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz, 
														fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz, 
														fichaAnamneseCovid19CansacoQuantoTempoFaz, 
														fichaAnamneseCovid19eFumante, 
														fichaAnamneseCovid19QuantoTempoEFumante, 
														fichaAnamneseCovid19TemAlgumVicio, 
														fichaAnamneseCovid19QualAlgumVicio, 
														fichaAnamneseCovid19TemAlgumProblemaDeSaude, 
														fichaAnamneseCovid19QualProblemaDeSaude, 
														fichaAnamneseCovid19Diabete, 
														fichaAnamneseCovid19DoencaCardiaca, 
														fichaAnamneseCovid19ProblemaRenal, 
														fichaAnamneseCovid19DoencaRespiratotiaCronica, 
														fichaAnamneseCovid19RiniteSinusite, 
														fichaAnamneseCovid19PressaoAlta, 
														fichaAnamneseCovid19Asma, 
														fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament, 
														fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais, 
														fichaAnamneseCovid19MoraComFamiliares,
														fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude, 
														fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude, 
														fichaAnamneseCovid19Data, 
														fichaAnamneseCovid19Hora,
														fichaAnamneseCovid19Status) VALUES (:idCliente, 
																							:fichaAnamneseCovid19JaFezTesteCovid19, 
																							:fichaAnamneseCovid19QualTesteCovid19, 
																							:fichaAnamneseCovid19Resultado, 
																							:fichaAnamneseCovid19ObsTesteCovid19, 
																							:fichaAnamneseCovid19QuantoTempoFazQueFezTeste, 
																							:fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo, 
																							:fichaAnamneseCovid19QuantoTempoFazContatoComPessoas, 
																							:fichaAnamneseCovid19FebreQuantoTempoFaz, 
																							:fichaAnamneseCovid19TosseQuantoTempoFaz, 
																							:fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz, 
																							:fichaAnamneseCovid19FaltaDeArQuantoTempoFaz, 
																							:fichaAnamneseCovid19DiarreiaQuantoTempoFaz, 
																							:fichaAnamneseCovid19CorizaQuantoTempoFaz, 
																							:fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz, 
																							:fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz, 
																							:fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz, 
																							:fichaAnamneseCovid19CansacoQuantoTempoFaz, 
																							:fichaAnamneseCovid19eFumante, 
																							:fichaAnamneseCovid19QuantoTempoEFumante, 
																							:fichaAnamneseCovid19TemAlgumVicio, 
																							:fichaAnamneseCovid19QualAlgumVicio, 
																							:fichaAnamneseCovid19TemAlgumProblemaDeSaude, 
																							:fichaAnamneseCovid19QualProblemaDeSaude, 
																							:fichaAnamneseCovid19Diabete, 
																							:fichaAnamneseCovid19DoencaCardiaca, 
																							:fichaAnamneseCovid19ProblemaRenal, 
																							:fichaAnamneseCovid19DoencaRespiratotiaCronica, 
																							:fichaAnamneseCovid19RiniteSinusite, 
																							:fichaAnamneseCovid19PressaoAlta, 
																							:fichaAnamneseCovid19Asma, 
																							:fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament, 
																							:fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais, 
																							:fichaAnamneseCovid19MoraComFamiliares, 
																							:fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude, 
																							:fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude, 
																							CURRENT_DATE(), 
																							:fichaAnamneseCovid19Hora,
																							:fichaAnamneseCovid19Status)";
			$query = $this->db->prepare($sql);
			$parameters = array(':idCliente' => $idCliente,
								':fichaAnamneseCovid19JaFezTesteCovid19' => $fichaAnamneseCovid19JaFezTesteCovid19,
								':fichaAnamneseCovid19QualTesteCovid19' => $fichaAnamneseCovid19QualTesteCovid19,
								':fichaAnamneseCovid19Resultado' => $fichaAnamneseCovid19Resultado,
								':fichaAnamneseCovid19ObsTesteCovid19' => $fichaAnamneseCovid19ObsTesteCovid19,
								':fichaAnamneseCovid19QuantoTempoFazQueFezTeste' => $fichaAnamneseCovid19QuantoTempoFazQueFezTeste,
								':fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo' => $fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo,
								':fichaAnamneseCovid19QuantoTempoFazContatoComPessoas' => $fichaAnamneseCovid19QuantoTempoFazContatoComPessoas,
								':fichaAnamneseCovid19FebreQuantoTempoFaz' => $fichaAnamneseCovid19FebreQuantoTempoFaz,
								':fichaAnamneseCovid19TosseQuantoTempoFaz' => $fichaAnamneseCovid19TosseQuantoTempoFaz,
								':fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz' => $fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz,
								':fichaAnamneseCovid19FaltaDeArQuantoTempoFaz' => $fichaAnamneseCovid19FaltaDeArQuantoTempoFaz,
								':fichaAnamneseCovid19DiarreiaQuantoTempoFaz' => $fichaAnamneseCovid19DiarreiaQuantoTempoFaz,
								':fichaAnamneseCovid19CorizaQuantoTempoFaz' => $fichaAnamneseCovid19CorizaQuantoTempoFaz,
								':fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz' => $fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz,
								':fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz' => $fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz,
								':fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz' => $fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz,
								':fichaAnamneseCovid19CansacoQuantoTempoFaz' => $fichaAnamneseCovid19CansacoQuantoTempoFaz,
								':fichaAnamneseCovid19eFumante' => $fichaAnamneseCovid19eFumante,
								':fichaAnamneseCovid19QuantoTempoEFumante' => $fichaAnamneseCovid19QuantoTempoEFumante,
								':fichaAnamneseCovid19TemAlgumVicio' => $fichaAnamneseCovid19TemAlgumVicio,
								':fichaAnamneseCovid19QualAlgumVicio' => $fichaAnamneseCovid19QualAlgumVicio,
								':fichaAnamneseCovid19TemAlgumProblemaDeSaude' => $fichaAnamneseCovid19TemAlgumProblemaDeSaude,
								':fichaAnamneseCovid19QualProblemaDeSaude' => $fichaAnamneseCovid19QualProblemaDeSaude,
								':fichaAnamneseCovid19Diabete' => $fichaAnamneseCovid19Diabete,
								':fichaAnamneseCovid19DoencaCardiaca' => $fichaAnamneseCovid19DoencaCardiaca,
								':fichaAnamneseCovid19ProblemaRenal' => $fichaAnamneseCovid19ProblemaRenal,
								':fichaAnamneseCovid19DoencaRespiratotiaCronica' => $fichaAnamneseCovid19DoencaRespiratotiaCronica,
								':fichaAnamneseCovid19RiniteSinusite' => $fichaAnamneseCovid19RiniteSinusite,
								':fichaAnamneseCovid19PressaoAlta' => $fichaAnamneseCovid19PressaoAlta,
								':fichaAnamneseCovid19Asma' => $fichaAnamneseCovid19Asma,
								':fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament' => $fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament,
								':fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais' => $fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais,
								':fichaAnamneseCovid19MoraComFamiliares' => $fichaAnamneseCovid19MoraComFamiliares,
								':fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude' => $fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude,
								':fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude' => $fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude,
								
								':fichaAnamneseCovid19Hora' => $fichaAnamneseCovid19Hora,
								':fichaAnamneseCovid19Status' => $fichaAnamneseCovid19Status
			);
			if ($query->execute($parameters)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/*#################  DESATIVAR FICHA ANAMNESE COVID-19 DO BANCO DE DADOS ####################*/
	public function desativarAnamneseCovid19($idFichaAnamneseCovid19)
	{
		$sql = "UPDATE fichaAnamneseCovid19 SET fichaAnamneseCovid19Status = '0' WHERE fichaAnamneseCovid19.idFichaAnamneseCovid19 = :idFichaAnamneseCovid19";
		$query = $this->db->prepare($sql);
		$parameters = array(':idFichaAnamneseCovid19' => $idFichaAnamneseCovid19);
		$query->execute($parameters);
	}


	/*############# RECEBER UM FICHA ANAMNESE COVID-19 COM OS DADOS DO CLIENTE E FUNCIONÁRIO DO BANCO ######################*/
	public function getFichaAnamneseCovid19($idFichaAnamneseCovid19)
	{
		$sql = "SELECT 	fichaAnamneseCovid19.*,
						cliente.*,
						funcionario.funcNome 
						FROM fichaAnamneseCovid19 
						INNER JOIN cliente ON cliente.idCliente = fichaAnamneseCovid19.idCliente 
						INNER JOIN funcionario ON funcionario.idFunc = fichaAnamneseCovid19.idFunc WHERE idFichaAnamneseCovid19 = :idFichaAnamneseCovid19";
		$query = $this->db->prepare($sql);
		$parameters = array('idFichaAnamneseCovid19' => $idFichaAnamneseCovid19);
		$query->execute($parameters);
		return ($query->rowcount() ? $query->fetch() : false);
	}
    
	public function getFichaAnamneseCovid19Cliente($idCliente)
	{
		$sql = "SELECT fichaAnamneseCovid19.*, cliente.cliNome, cliente.cliRG, cliente.cliCPF, cliente.cliAssinatura FROM `fichaAnamneseCovid19` INNER JOIN cliente ON cliente.idCliente = fichaAnamneseCovid19.idCliente WHERE fichaAnamneseCovid19.idCliente = $idCliente ORDER BY fichaAnamneseCovid19.idFichaAnamneseCovid19 DESC LIMIT 1";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function getFichaAnamneseCovid19ClienteHistorico($idFicha)
	{
		$sql = "SELECT fichaAnamneseCovid19.*, cliente.cliNome, cliente.cliCPF, cliente.cliRG, cliente.cliAssinatura FROM fichaAnamneseCovid19 INNER JOIN cliente ON cliente.idCliente = (SELECT idCliente FROM `fichaAnamneseCovid19` WHERE idFichaAnamneseCovid19 = $idFicha) WHERE idFichaAnamneseCovid19 = $idFicha";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}
    
    public function getFichaAnamneseCovid19ClienteData($idCliente)
	{
		$sql = "SELECT * FROM fichaAnamneseCovid19 WHERE idCliente = $idCliente ";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	/* ########################## LISTAR FICHA ANAMNESE COVID-19 PARA EDIÇÃO PELO ID ################ */
	public function listaFichaAnamneseCovid19($id)
	{
		$sql = "SELECT * FROM fichaAnamneseCovid19 WHERE fichaAnamneseCovid19.idFichaAnamneseCovid19 = $id";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	/*####################### ATUALIZAR UM FICHA ANAMNESE COVID-19 NO BANCO ######################## */
	public function atualizarFichaAnamneseCovid19(	$idCliente,
													$fichaAnamneseCovid19JaFezTesteCovid19, 
													$fichaAnamneseCovid19QualTesteCovid19, 
													$fichaAnamneseCovid19Resultado, 
													$fichaAnamneseCovid19QuantoTempoFazQueFezTeste, 
													$fichaAnamneseCovid19ObsTesteCovid19, 
													$fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo, 
													$fichaAnamneseCovid19QuantoTempoFazContatoComPessoas, 
													$fichaAnamneseCovid19FebreQuantoTempoFaz,  
													$fichaAnamneseCovid19TosseQuantoTempoFaz, 
													$fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz,  
													$fichaAnamneseCovid19FaltaDeArQuantoTempoFaz, 
													$fichaAnamneseCovid19DiarreiaQuantoTempoFaz, 
													$fichaAnamneseCovid19CorizaQuantoTempoFaz,  
													$fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz, 
													$fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz, 
													$fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz, 
													$fichaAnamneseCovid19CansacoQuantoTempoFaz, 
													$fichaAnamneseCovid19eFumante, 
													$fichaAnamneseCovid19QuantoTempoEFumante, 
													$fichaAnamneseCovid19TemAlgumVicio, 
													$fichaAnamneseCovid19QualAlgumVicio, 
													$fichaAnamneseCovid19TemAlgumProblemaDeSaude, 
													$fichaAnamneseCovid19QualProblemaDeSaude, 
													$fichaAnamneseCovid19Diabete, 
													$fichaAnamneseCovid19DoencaCardiaca, 
													$fichaAnamneseCovid19ProblemaRenal, 
													$fichaAnamneseCovid19DoencaRespiratotiaCronica, 
													$fichaAnamneseCovid19RiniteSinusite, 
													$fichaAnamneseCovid19PressaoAlta, 
													$fichaAnamneseCovid19Asma, 
													$fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament, 
													$fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais, 
													$fichaAnamneseCovid19MoraComFamiliares, 
													$fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude, 
													$fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude, 
													$fichaAnamneseCovid19Data, 
													$fichaAnamneseCovid19Hora, 
													$fichaAnamneseCovid19Status) {
		$sql = "UPDATE fichaAnamneseCovid19 SET idCliente = :idCliente, 
															fichaAnamneseCovid19JaFezTesteCovid19 = :fichaAnamneseCovid19JaFezTesteCovid19, 
															fichaAnamneseCovid19QualTesteCovid19 = :fichaAnamneseCovid19QualTesteCovid19, 
															fichaAnamneseCovid19Resultado = :fichaAnamneseCovid19Resultado, 
															fichaAnamneseCovid19QuantoTempoFazQueFezTeste = :fichaAnamneseCovid19QuantoTempoFazQueFezTeste, 
															fichaAnamneseCovid19ObsTesteCovid19 = :fichaAnamneseCovid19ObsTesteCovid19, 
															fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo = :fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo, 
															fichaAnamneseCovid19QuantoTempoFazContatoComPessoas = :fichaAnamneseCovid19QuantoTempoFazContatoComPessoas, 
															fichaAnamneseCovid19FebreQuantoTempoFaz = :fichaAnamneseCovid19FebreQuantoTempoFaz, 
															fichaAnamneseCovid19TosseQuantoTempoFaz = :fichaAnamneseCovid19TosseQuantoTempoFaz, 
															fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz = :fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz, 
															fichaAnamneseCovid19FaltaDeArQuantoTempoFaz = :fichaAnamneseCovid19FaltaDeArQuantoTempoFaz, 
															fichaAnamneseCovid19DiarreiaQuantoTempoFaz = :fichaAnamneseCovid19DiarreiaQuantoTempoFaz, 
															fichaAnamneseCovid19CorizaQuantoTempoFaz = :fichaAnamneseCovid19CorizaQuantoTempoFaz, 
															fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz = :fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz, 
															fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz = :fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz, 
															fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz = :fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz, 
															fichaAnamneseCovid19CansacoQuantoTempoFaz = :fichaAnamneseCovid19CansacoQuantoTempoFaz, 
															fichaAnamneseCovid19eFumante = :fichaAnamneseCovid19eFumante, 
															fichaAnamneseCovid19QuantoTempoEFumante = :fichaAnamneseCovid19QuantoTempoEFumante, 
															fichaAnamneseCovid19TemAlgumVicio = :fichaAnamneseCovid19TemAlgumVicio, 
															fichaAnamneseCovid19QualAlgumVicio = :fichaAnamneseCovid19QualAlgumVicio, 
															fichaAnamneseCovid19TemAlgumProblemaDeSaude = :fichaAnamneseCovid19TemAlgumProblemaDeSaude, 
															fichaAnamneseCovid19QualProblemaDeSaude = :fichaAnamneseCovid19QualProblemaDeSaude, 
															fichaAnamneseCovid19Diabete = :fichaAnamneseCovid19Diabete, 
															fichaAnamneseCovid19DoencaCardiaca = :fichaAnamneseCovid19DoencaCardiaca, 
															fichaAnamneseCovid19ProblemaRenal = :fichaAnamneseCovid19ProblemaRenal, 
															fichaAnamneseCovid19DoencaRespiratotiaCronica = :fichaAnamneseCovid19DoencaRespiratotiaCronica, 
															fichaAnamneseCovid19RiniteSinusite = :fichaAnamneseCovid19RiniteSinusite, 
															fichaAnamneseCovid19PressaoAlta = :fichaAnamneseCovid19PressaoAlta, 
															fichaAnamneseCovid19Asma = :fichaAnamneseCovid19Asma, 
															fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament = :fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament, 
															fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais = :fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais, 
															fichaAnamneseCovid19MoraComFamiliares = :fichaAnamneseCovid19MoraComFamiliares,  
															fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude = :fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude, 
															fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude = :fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude, 
															fichaAnamneseCovid19Data = :fichaAnamneseCovid19Data, 
															fichaAnamneseCovid19Hora = :fichaAnamneseCovid19Hora, 
															fichaAnamneseCovid19Status = :fichaAnamneseCovid19Status WHERE idCliente = :idCliente";
		$query = $this->db->prepare($sql);
		$parameters = array(':idCliente' => $idCliente, 
							':fichaAnamneseCovid19JaFezTesteCovid19' => $fichaAnamneseCovid19JaFezTesteCovid19, 
							':fichaAnamneseCovid19QualTesteCovid19' => $fichaAnamneseCovid19QualTesteCovid19, 
							':fichaAnamneseCovid19Resultado' => $fichaAnamneseCovid19Resultado, 
							':fichaAnamneseCovid19QuantoTempoFazQueFezTeste' => $fichaAnamneseCovid19QuantoTempoFazQueFezTeste, 
							':fichaAnamneseCovid19ObsTesteCovid19' => $fichaAnamneseCovid19ObsTesteCovid19, 
							':fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo' => $fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo, 
							':fichaAnamneseCovid19QuantoTempoFazContatoComPessoas' => $fichaAnamneseCovid19QuantoTempoFazContatoComPessoas, 
							':fichaAnamneseCovid19FebreQuantoTempoFaz' => $fichaAnamneseCovid19FebreQuantoTempoFaz, 
							':fichaAnamneseCovid19TosseQuantoTempoFaz' => $fichaAnamneseCovid19TosseQuantoTempoFaz, 
							':fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz' => $fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz, 
							':fichaAnamneseCovid19FaltaDeArQuantoTempoFaz' => $fichaAnamneseCovid19FaltaDeArQuantoTempoFaz, 
							':fichaAnamneseCovid19DiarreiaQuantoTempoFaz' => $fichaAnamneseCovid19DiarreiaQuantoTempoFaz, 
							':fichaAnamneseCovid19CorizaQuantoTempoFaz' => $fichaAnamneseCovid19CorizaQuantoTempoFaz, 
							':fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz' => $fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz,  
							':fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz' => $fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz, 
							':fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz' => $fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz, 
							':fichaAnamneseCovid19CansacoQuantoTempoFaz' => $fichaAnamneseCovid19CansacoQuantoTempoFaz, 
							':fichaAnamneseCovid19eFumante' => $fichaAnamneseCovid19eFumante, 
							':fichaAnamneseCovid19QuantoTempoEFumante' => $fichaAnamneseCovid19QuantoTempoEFumante, 
							':fichaAnamneseCovid19TemAlgumVicio' => $fichaAnamneseCovid19TemAlgumVicio, 
							':fichaAnamneseCovid19QualAlgumVicio' => $fichaAnamneseCovid19QualAlgumVicio, 
							':fichaAnamneseCovid19TemAlgumProblemaDeSaude' => $fichaAnamneseCovid19TemAlgumProblemaDeSaude, 
							':fichaAnamneseCovid19QualProblemaDeSaude' => $fichaAnamneseCovid19QualProblemaDeSaude, 
							':fichaAnamneseCovid19Diabete' => $fichaAnamneseCovid19Diabete, 
							':fichaAnamneseCovid19DoencaCardiaca' => $fichaAnamneseCovid19DoencaCardiaca, 
							':fichaAnamneseCovid19ProblemaRenal' => $fichaAnamneseCovid19ProblemaRenal, 
							':fichaAnamneseCovid19DoencaRespiratotiaCronica' => $fichaAnamneseCovid19DoencaRespiratotiaCronica, 
							':fichaAnamneseCovid19RiniteSinusite' => $fichaAnamneseCovid19RiniteSinusite, 
							':fichaAnamneseCovid19PressaoAlta' => $fichaAnamneseCovid19PressaoAlta, 
							':fichaAnamneseCovid19Asma' => $fichaAnamneseCovid19Asma, 
							':fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament' => $fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament, 
							':fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais' => $fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais, 
							':fichaAnamneseCovid19MoraComFamiliares' => $fichaAnamneseCovid19MoraComFamiliares, 
							':fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude' => $fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude, 
							':fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude' => $fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude, 
							':fichaAnamneseCovid19Data' => $fichaAnamneseCovid19Data, 
							':fichaAnamneseCovid19Hora' => $fichaAnamneseCovid19Hora, 
							':fichaAnamneseCovid19Status' => $fichaAnamneseCovid19Status
		);
		if ($query->execute($parameters)) {
			return true;
		} else {
			return false;
		}
	}


	/*################### "ESTATÍSTICAS" QTD DE FICHA ANAMNESE COVID-19 ################# */
	public function qtdFichaAnamneseCovid19()
	{
		$sql = "SELECT COUNT(idFichaAnamneseCovid19) AS amount_of_fichaAnamneseCovid19 FROM fichaAnamneseCovid19";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetch()->amount_of_fichaAnamneseCovid19;
	}

	/*################ RECEBER DETALHE DA FICHA ANAMNESE COVID-19 DO BANCO E CRIAR O PERFIL ##########################*/
	public function detalheFichaAnamneseCovid19($idFichaAnamneseCovid19)
	{
		$sql = "SELECT fichaAnamneseCovid19.*, cliente.*, funcionario.funcNome FROM fichaAnamneseCovid19 
				INNER JOIN cliente ON cliente.idCliente = fichaAnamneseCovid19.idCliente 
				INNER JOIN funcionario ON funcionario.idFunc = fichaAnamneseCovid19.idFunc
				WHERE fichaAnamneseCovid19.idFichaAnamneseCovid19 = :idFichaAnamneseCovid19";
		$query = $this->db->prepare($sql);
		$parameters = array('idFichaAnamneseCovid19' => $idFichaAnamneseCovid19);
		$query->execute($parameters);
		return ($query->rowcount() ? $query->fetch() : false);
	}



	/* 
#####################################################################
#############											#############
#############		FICHA ANAMNESE CRIOLIPÓLISE			#############
#############											#############
#####################################################################
*/


	/* ########## OBTER TODAS OS DADOS DA FICHA ANAMNESE CRIOLIPÓLISE, CLIENTE E FUNCIONARIO DO BANCO DE DADOS ################     */
	public function todosFichaAnamneseCriolipolise()
	{
		$sql = "SELECT 	fichaAnamneseCriolipolise.*,
						cliente.*,
						funcionario.funcNome 
						FROM fichaAnamneseCriolipolise 
						INNER JOIN cliente ON cliente.idCliente = fichaAnamneseCriolipolise.idCliente 
						INNER JOIN funcionario ON funcionario.idFunc = fichaAnamneseCriolipolise.idFunc";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function FichaAnamneseCriolipolise($id)
	{
		$sql = "SELECT cliente.cliCPF, cliente.cliSexo, cliente.cliNome, cliente.cliNasci, cliente.cliProfissao, cliente.cliFoto, cliente.cliWebcam, fichaAnamneseCriolipolise.* FROM fichaAnamneseCriolipolise 
		INNER JOIN cliente ON cliente.idCliente = fichaAnamneseCriolipolise.idCliente 
		WHERE fichaAnamneseCriolipolise.idFichaAnamneseCriolipolise = $id";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function marcacaoFichaAnamneseCriolipolise($id)
	{
		$sql = "SELECT * FROM `marcacaoAnamneseCriolipolise` WHERE idFichaAnamneseCriolipolise = $id";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	/* ############ ADICIONAR UMA FICHA ANAMNESE CRIOLIPÓLISE NO BANCO ################*/
	public function addAnamneseCriolipolise($idFichaGeral,
											$idFunc,
											$fichaAnamnesePraticaAtividadeFísica,
											$fichaAnamneseQualPraticaAtividadeFísica,
											$fichaAnamneseTemAlgumTipoDeAlergia,
											$fichaAnamneseQualAlgumTipoDeAlergia,
											$fichaAnamneseTemAlgumtipodehormonal,
											$fichaAnamneseQualAlgumtipodehormonal,
											$fichaAnamnesePeso,
											$fichaAnamneseDoencasRelacionadasAoFrio,
											$fichaAnamneseHerniasUmbilicalOuInguinal,
											$fichaAnamneseDiastaseAbdominal,
											$fichaAnamneseLesaoAbertaNaPele,
											$fichaAnamneseLupusEritematosoSistemico,
											$fichaAnamneseCancerDePele,
											$fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves,
											$fichaAnamneseUsoDeAnticoagulante,
											$fichaAnamneseCirurgiaRecenteNaAreaTratada,
											$fichaAnamneseGestanteeOuAmamentando,
											$fichaAnamneseObservacoes,
											$fichaAnamneseObsTratamentoIndicado,
											$fichaAnamneseQtdSessoes,
											$fichaAnamneseData,
											$fichaAnamneseHora,
											$fichaAnamneseStatus) {
		$sql = "INSERT INTO fichaAnamneseCriolipolise(	idFichaGeral, 
														idFunc, 
														fichaAnamnesePraticaAtividadeFísica, 
														fichaAnamneseQualPraticaAtividadeFísica, 
														fichaAnamneseTemAlgumTipoDeAlergia, 
														fichaAnamneseQualAlgumTipoDeAlergia, 
														fichaAnamneseTemAlgumtipodehormonal, 
														fichaAnamneseQualAlgumtipodehormonal, 
														fichaAnamnesePeso, 
														fichaAnamneseDoencasRelacionadasAoFrio, 
														fichaAnamneseHerniasUmbilicalOuInguinal, 
														fichaAnamneseDiastaseAbdominal, 
														fichaAnamneseLesaoAbertaNaPele, 
														fichaAnamneseLupusEritematosoSistemico, 
														fichaAnamneseCancerDePele, 
														fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves, 
														fichaAnamneseUsoDeAnticoagulante, 
														fichaAnamneseCirurgiaRecenteNaAreaTratada, 
														fichaAnamneseGestanteeOuAmamentando, 
														fichaAnamneseObservacoes, 
														fichaAnamneseObsTratamentoIndicado, 
														fichaAnamneseQtdSessoes, 
														fichaAnamneseData, 
														fichaAnamneseHora, 
														fichaAnamneseStatus) VALUES (	:idFichaGeral, 
																						:idFunc, 
																						:fichaAnamnesePraticaAtividadeFísica, 
																						:fichaAnamneseQualPraticaAtividadeFísica, 
																						:fichaAnamneseTemAlgumTipoDeAlergia, 
																						:fichaAnamneseQualAlgumTipoDeAlergia, 
																						:fichaAnamneseTemAlgumtipodehormonal, 
																						:fichaAnamneseQualAlgumtipodehormonal, 
																						:fichaAnamnesePeso, 
																						:fichaAnamneseDoencasRelacionadasAoFrio, 
																						:fichaAnamneseHerniasUmbilicalOuInguinal, 
																						:fichaAnamneseDiastaseAbdominal, 
																						:fichaAnamneseLesaoAbertaNaPele, 
																						:fichaAnamneseLupusEritematosoSistemico, 
																						:fichaAnamneseCancerDePele, 
																						:fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves, 
																						:fichaAnamneseUsoDeAnticoagulante, 
																						:fichaAnamneseCirurgiaRecenteNaAreaTratada, 
																						:fichaAnamneseGestanteeOuAmamentando, 
																						:fichaAnamneseObservacoes, 
																						:fichaAnamneseObsTratamentoIndicado, 
																						:fichaAnamneseQtdSessoes, 
																						:fichaAnamneseData, 
																						:fichaAnamneseHora, 
																						:fichaAnamneseStatus)";
		$query = $this->db->prepare($sql);
		$parameters = array(':idFichaGeral' => $idFichaGeral,
							':idFunc' => $idFunc,
							':fichaAnamnesePraticaAtividadeFísica' => $fichaAnamnesePraticaAtividadeFísica,
							':fichaAnamneseQualPraticaAtividadeFísica' => $fichaAnamneseQualPraticaAtividadeFísica,
							':fichaAnamneseTemAlgumTipoDeAlergia' => $fichaAnamneseTemAlgumTipoDeAlergia,
							':fichaAnamneseQualAlgumTipoDeAlergia' => $fichaAnamneseQualAlgumTipoDeAlergia,
							':fichaAnamneseTemAlgumtipodehormonal' => $fichaAnamneseTemAlgumtipodehormonal,
							':fichaAnamneseQualAlgumtipodehormonal' => $fichaAnamneseQualAlgumtipodehormonal,
							':fichaAnamnesePeso' => $fichaAnamnesePeso,
							':fichaAnamneseDoencasRelacionadasAoFrio' => $fichaAnamneseDoencasRelacionadasAoFrio,
							':fichaAnamneseHerniasUmbilicalOuInguinal' => $fichaAnamneseHerniasUmbilicalOuInguinal,
							':fichaAnamneseDiastaseAbdominal' => $fichaAnamneseDiastaseAbdominal,
							':fichaAnamneseLesaoAbertaNaPele' => $fichaAnamneseLesaoAbertaNaPele,
							':fichaAnamneseLupusEritematosoSistemico' => $fichaAnamneseLupusEritematosoSistemico,
							':fichaAnamneseCancerDePele' => $fichaAnamneseCancerDePele,
							':fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves' => $fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves,
							':fichaAnamneseUsoDeAnticoagulante' => $fichaAnamneseUsoDeAnticoagulante,
							':fichaAnamneseCirurgiaRecenteNaAreaTratada' => $fichaAnamneseCirurgiaRecenteNaAreaTratada,
							':fichaAnamneseGestanteeOuAmamentando' => $fichaAnamneseGestanteeOuAmamentando,
							':fichaAnamneseObservacoes' => $fichaAnamneseObservacoes,
							':fichaAnamneseObsTratamentoIndicado' => $fichaAnamneseObsTratamentoIndicado,
							':fichaAnamneseQtdSessoes' => $fichaAnamneseQtdSessoes,
							':fichaAnamneseData' => $fichaAnamneseData,
							':fichaAnamneseHora' => $fichaAnamneseHora,
							':fichaAnamneseStatus' => $fichaAnamneseStatus
		);
		$query->execute($parameters);
	}

	/*#################  DESATIVAR FICHA ANAMNESE CRIOLIPÓLISE DO BANCO DE DADOS ####################*/
	public function desativarAnamneseCriolipolise($idFichaAnamneseCriolipolise)
	{
		$sql = "UPDATE fichaAnamneseCriolipolise SET fichaAnamneseStatus = '0' WHERE fichaAnamneseCriolipolise.idFichaAnamneseCriolipolise = :	idFichaAnamneseCriolipolise";
		$query = $this->db->prepare($sql);
		$parameters = array(':idFichaAnamneseCriolipolise' => $idFichaAnamneseCriolipolise);
		$query->execute($parameters);
	}


	/*############# RECEBER UM FICHA ANAMNESE CRIOLIPÓLISE COM OS DADOS DO CLIENTE E FUNCIONÁRIO DO BANCO ######################*/
	public function getFichaAnamneseCriolipolise($idFichaAnamneseCriolipolise)
	{
		$sql = "SELECT 	fichaAnamneseCriolipolise.*,
						cliente.*,
						funcionario.funcNome 
						FROM fichaAnamneseCriolipolise 
						INNER JOIN cliente ON cliente.idCliente = fichaAnamneseCriolipolise.idCliente 
						INNER JOIN funcionario ON funcionario.idFunc = fichaAnamneseCriolipolise.idFunc WHERE idFichaAnamneseCriolipolise = :idFichaAnamneseCriolipolise";
		$query = $this->db->prepare($sql);
		$parameters = array('idFichaAnamneseCriolipolise' => $idFichaAnamneseCriolipolise);
		$query->execute($parameters);
		return ($query->rowcount() ? $query->fetch() : false);
	}

	/* ########################## LISTAR FICHA ANAMNESE CRIOLIPÓLISE PARA EDIÇÃO PELO ID ################ */
	public function listaFichaAnamneseCriolipolise($id)
	{
		$sql = "SELECT * FROM fichaAnamneseCriolipolise WHERE fichaAnamneseCriolipolise.idFichaAnamneseCriolipolise = $id";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	/*####################### ATUALIZAR UM FICHA ANAMNESE CRIOLIPÓLISE NO BANCO ######################## */
	public function atualizarFichaAnamneseCriolipolise(	$idCliente,
														$idFunc,
														$fichaAnamnesePraticaAtividadeFísica,
														$fichaAnamneseQualPraticaAtividadeFísica,
														$fichaAnamneseTemAlgumTipoDeAlergia,
														$fichaAnamneseQualAlgumTipoDeAlergia,
														$fichaAnamneseTemAlgumtipodehormonal,
														$fichaAnamneseQualAlgumtipodehormonal,
														$fichaAnamnesePeso,
														$fichaAnamneseDoencasRelacionadasAoFrio,
														$fichaAnamneseHerniasUmbilicalOuInguinal,
														$fichaAnamneseDiastaseAbdominal,
														$fichaAnamneseLesaoAbertaNaPele,
														$fichaAnamneseLupusEritematosoSistemico,
														$fichaAnamneseCancerDePele,
														$fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves,
														$fichaAnamneseUsoDeAnticoagulante,
														$fichaAnamneseCirurgiaRecenteNaAreaTratada,
														$fichaAnamneseGestanteeOuAmamentando,
														$fichaAnamneseObservacoes,
														$fichaAnamneseData,
														$fichaAnamneseHora,
														$fichaAnamneseStatus
	) {
		$sql = "UPDATE fichaAnamneseCriolipolise SET 	idCliente = :idCliente, 
														idFunc = :idFunc, 
														fichaAnamnesePraticaAtividadeFísica = :fichaAnamnesePraticaAtividadeFísica, 
														fichaAnamneseQualPraticaAtividadeFísica = :fichaAnamneseQualPraticaAtividadeFísica, 
														fichaAnamneseTemAlgumTipoDeAlergia = :fichaAnamneseTemAlgumTipoDeAlergia, 
														fichaAnamneseQualAlgumTipoDeAlergia = :fichaAnamneseQualAlgumTipoDeAlergia, 
														fichaAnamneseTemAlgumtipodehormonal = :fichaAnamneseTemAlgumtipodehormonal, 
														fichaAnamneseQualAlgumtipodehormonal = :fichaAnamneseQualAlgumtipodehormonal, 
														fichaAnamnesePeso = :fichaAnamnesePeso, 
														fichaAnamneseDoencasRelacionadasAoFrio = :fichaAnamneseDoencasRelacionadasAoFrio, 
														fichaAnamneseHerniasUmbilicalOuInguinal = :fichaAnamneseHerniasUmbilicalOuInguinal, 
														fichaAnamneseDiastaseAbdominal = :fichaAnamneseDiastaseAbdominal, 
														fichaAnamneseLesaoAbertaNaPele = :fichaAnamneseLesaoAbertaNaPele, 
														fichaAnamneseLupusEritematosoSistemico = :fichaAnamneseLupusEritematosoSistemico, 
														fichaAnamneseCancerDePele = :fichaAnamneseCancerDePele, 
														fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves = :fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves, 
														fichaAnamneseUsoDeAnticoagulante = :fichaAnamneseUsoDeAnticoagulante, 
														fichaAnamneseCirurgiaRecenteNaAreaTratada = :fichaAnamneseCirurgiaRecenteNaAreaTratada, 
														fichaAnamneseGestanteeOuAmamentando = :fichaAnamneseGestanteeOuAmamentando, 
														fichaAnamneseObservacoes = :fichaAnamneseObservacoes, 
														fichaAnamneseData = :fichaAnamneseData, 
														fichaAnamneseHora = :fichaAnamneseHora, 
														fichaAnamneseStatus = :fichaAnamneseStatus WHERE idFichaAnamneseCriolipolise = :idFichaAnamneseCriolipolise";
		$query = $this->db->prepare($sql);
		$parameters = array(':idCliente' => $idCliente,
							':idFunc' => $idFunc,
							':fichaAnamnesePraticaAtividadeFísica' => $fichaAnamnesePraticaAtividadeFísica,
							':fichaAnamneseQualPraticaAtividadeFísica' => $fichaAnamneseQualPraticaAtividadeFísica,
							':fichaAnamneseTemAlgumTipoDeAlergia' => $fichaAnamneseTemAlgumTipoDeAlergia,
							':fichaAnamneseQualAlgumTipoDeAlergia' => $fichaAnamneseQualAlgumTipoDeAlergia,
							':fichaAnamneseTemAlgumtipodehormonal' => $fichaAnamneseTemAlgumtipodehormonal,
							':fichaAnamneseQualAlgumtipodehormonal' => $fichaAnamneseQualAlgumtipodehormonal,
							':fichaAnamnesePeso' => $fichaAnamnesePeso,
							':fichaAnamneseDoencasRelacionadasAoFrio' => $fichaAnamneseDoencasRelacionadasAoFrio,
							':fichaAnamneseHerniasUmbilicalOuInguinal' => $fichaAnamneseHerniasUmbilicalOuInguinal,
							':fichaAnamneseDiastaseAbdominal' => $fichaAnamneseDiastaseAbdominal,
							':fichaAnamneseLesaoAbertaNaPele' => $fichaAnamneseLesaoAbertaNaPele,
							':fichaAnamneseLupusEritematosoSistemico' => $fichaAnamneseLupusEritematosoSistemico,
							':fichaAnamneseCancerDePele' => $fichaAnamneseCancerDePele,
							':fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves' => $fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves,
							':fichaAnamneseUsoDeAnticoagulante' => $fichaAnamneseUsoDeAnticoagulante,
							':fichaAnamneseCirurgiaRecenteNaAreaTratada' => $fichaAnamneseCirurgiaRecenteNaAreaTratada,
							':fichaAnamneseGestanteeOuAmamentando' => $fichaAnamneseGestanteeOuAmamentando,
							':fichaAnamneseObservacoes' => $fichaAnamneseObservacoes,
							':fichaAnamneseData' => $fichaAnamneseData,
							':fichaAnamneseHora' => $fichaAnamneseHora,
							':fichaAnamneseStatus' => $fichaAnamneseStatus,
							':idFichaAnamneseCriolipolise' => $idFichaAnamneseCriolipolise
		);
		$query->execute($parameters);
	}


	/*################### "ESTATÍSTICAS" QTD DE FICHA ANAMNESE CRIOLIPÓLISE ################# */
	public function qtdFichaAnamneseCriolipolise()
	{
		$sql = "SELECT COUNT(idFichaAnamneseCriolipolise) AS amount_of_fichaAnamneseCriolipolise FROM fichaAnamneseCriolipolise";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetch()->amount_of_fichaAnamneseCriolipolise;
	}

	/*################ RECEBER DETALHE DA FICHA ANAMNESE CRIOLIPÓLISE DO BANCO E CRIAR O PERFIL ##########################*/
	public function detalheFichaAnamneseCriolipolise($idFichaAnamneseCriolipolise)
	{
		$sql = "SELECT fichaAnamneseCriolipolise.*, cliente.*, funcionario.funcNome FROM fichaAnamneseCriolipolise 
				INNER JOIN cliente ON cliente.idCliente = fichaAnamneseCriolipolise.idCliente 
				INNER JOIN funcionario ON funcionario.idFunc = fichaAnamneseCriolipolise.idFunc
				WHERE fichaAnamneseCriolipolise.idFichaAnamneseCriolipolise = :idFichaAnamneseCriolipolise";
		$query = $this->db->prepare($sql);
		$parameters = array('idFichaAnamneseCriolipolise' => $idFichaAnamneseCriolipolise);
		$query->execute($parameters);
		return ($query->rowcount() ? $query->fetch() : false);
	}

	/* 
#####################################################################
#############											#############
#############				FICHA CONTRATO				#############
#############											#############
#####################################################################
*/

	/* ########## OBTER TODAS OS DADOS DO CONTRATO, CLIENTE E FUNCIONARIO DO BANCO DE DADOS ################     */
	public function todosFichaContratoCliente()
	{
		$sql = "SELECT 	contratoServicosEsteticos.*,
						cliente.*,
						funcionario.funcNome 
						FROM contratoServicosEsteticos 
						INNER JOIN cliente ON cliente.idCliente = contratoServicosEsteticos.idCliente 
						INNER JOIN funcionario ON funcionario.idFunc = contratoServicosEsteticos.idFunc";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function listaFichaContratoCliente($id)
	{
		$sql = "SELECT 	contratoServicosEsteticos.*,
						cliente.*,
						funcionario.funcNome,
						funcionario.funcAssinatura 
						FROM contratoServicosEsteticos 
						INNER JOIN cliente ON cliente.idCliente = contratoServicosEsteticos.idCliente 
						INNER JOIN funcionario ON funcionario.idFunc = contratoServicosEsteticos.idFunc
						WHERE  contratoServicosEsteticos.idContrato = $id";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	/* ############ ADICIONAR UM CONTRATO NO BANCO ################*/
	public function addContratoCliente(
		$idCliente,
		$idFunc,
		$qtdSessoes,
		$contratoData
	) {
		$sql = "INSERT INTO contratoServicosEsteticos(	idCliente, 
														idFunc, 
														qtdSessoes, 
														contratoData 
														) VALUES (:idCliente, 
																  :idFunc, 
																  :qtdSessoes, 
																  :contratoData)";
		$query = $this->db->prepare($sql);
		$parameters = array(
			':idCliente' => $idCliente,
			':idFunc' => $idFunc,
			':qtdSessoes' => $qtdSessoes,
			':contratoData' => $contratoData
		);

		if($query->execute($parameters)){
            return $this->db->lastInsertId();
        }else{
            return false;
        }
		
	}

	/*#################  DESATIVAR CONTRATO DO BANCO DE DADOS ####################*/
	public function desativarContratoCliente($idContrato)
	{
		$sql = "UPDATE contratoServicosEsteticos SET contratoStatu = '0' WHERE contratoServicosEsteticos.idContrato = :idContrato";
		$query = $this->db->prepare($sql);
		$parameters = array(':idContrato' => $idContrato);
		$query->execute($parameters);
	}


	/*############# RECEBER UM CONTRATO COM OS DADOS DO CLIENTE E FUNCIONÁRIO DO BANCO ######################*/
	public function getContratoCliente($idContrato)
	{
		$sql = "SELECT 	contratoServicosEsteticos.*,
						cliente.*,
						funcionario.funcNome 
						FROM contratoServicosEsteticos 
						INNER JOIN cliente ON cliente.idCliente = contratoServicosEsteticos.idCliente 
						INNER JOIN funcionario ON funcionario.idFunc = contratoServicosEsteticos.idFunc WHERE idContrato = :idContrato";
		$query = $this->db->prepare($sql);
		$parameters = array('idContrato' => $idContrato);
		$query->execute($parameters);
		return ($query->rowcount() ? $query->fetch() : false);
	}

	/* ########################## LISTAR CONTRATO PARA EDIÇÃO PELO ID ################ */
	public function listaContratoCliente($id)
	{
		$sql = "SELECT * FROM contratoServicosEsteticos WHERE contratoServicosEsteticos.idContrato = $id";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function inserirServico($idContrato,$idServico)
    {
        $sql = "INSERT INTO itensDoContrato (idContrato, 
										     idServico) VALUES (:idContrato, 
																:idServico)";
        $query = $this->db->prepare($sql);
        $parameters = array(':idContrato' => $idContrato, 
							':idServico' => $idServico);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

	public function inserirPacote($idContrato,$idPacote)
    {
        $sql = "INSERT INTO itensDoContrato (idContrato,idPacoteServico) VALUES (:idContrato,:idPacoteServico)";
        $query = $this->db->prepare($sql);
        $parameters = array(':idContrato' => $idContrato, 
							':idPacoteServico' => $idPacote);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

	/*####################### ATUALIZAR UM CONTRATO NO BANCO ######################## */
	public function atualizarContratoCliente(
		$idCliente,
		$idFunc,
		$idTratamento,
		$contratoData,
		$contratoHora,
		$contratoStatus
	) {
		$sql = "UPDATE contratoServicosEsteticos SET 	idCliente = :idCliente, 
														idFunc = :idFunc, 
														idTratamento = :idTratamento, 
														contratoData = :contratoData, 
														contratoHora = :contratoHora, 
														contratoStatus = :contratoStatus WHERE idContrato = :idContrato";
		$query = $this->db->prepare($sql);
		$parameters = array(
			':idCliente' => $idCliente,
			':idFunc' => $idFunc,
			':idTratamento' => $idTratamento,
			':contratoData' => $contratoData,
			':contratoHora' => $contratoHora,
			':contratoStatus' => $contratoStatus,
			':idContrato' => $idContrato
		);
		$query->execute($parameters);
	}


	/*################### "ESTATÍSTICAS" QTD DE CONTRATO ################# */
	public function qtdContratoCliente()
	{
		$sql = "SELECT COUNT(idContrato) AS amount_of_contratoServicosEsteticos FROM contratoServicosEsteticos";
		$query = $this->db->prepare($sql);
		$query->execute();
		return $query->fetch()->amount_of_contratoServicosEsteticos;
	}

	/*################ RECEBER DETALHE DO CONTRATO DO BANCO E CRIAR O PERFIL ##########################*/
	public function detalheContratoCliente($idContrato)
	{
		$sql = "SELECT contratoServicosEsteticos.*, cliente.*, funcionario.funcNome FROM contratoServicosEsteticos 
				INNER JOIN cliente ON cliente.idCliente = contratoServicosEsteticos.idCliente 
				INNER JOIN funcionario ON funcionario.idFunc = contratoServicosEsteticos.idFunc
				WHERE contratoServicosEsteticos.idContrato = :idContrato";
		$query = $this->db->prepare($sql);
		$parameters = array('idContrato' => $idContrato);
		$query->execute($parameters);
		return ($query->rowcount() ? $query->fetch() : false);
	}
}
