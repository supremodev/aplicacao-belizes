<?php

/**
 * Class Funcionarios
 */

namespace App\Model;

use App\Core\Model;

class Funcionario extends Model
{
    /* ########## OBTER TODOS OS FUNCIONIOS ATIVOS DO BANCO DE DADOS ################     */
    public function getAllFuncionario($boleano)
    {
        $sql = "SELECT * FROM funcionario WHERE funcStatus = $boleano";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
	}

	public function idNomeFuncionario($boleano)
    {
        $sql = "SELECT idFunc,funcNome, funcAssinatura FROM funcionario WHERE funcStatus = $boleano";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
	}
	
	public function ativarDesativar($id,$boleano)
    {
        $sql = "UPDATE funcionario SET funcStatus = $boleano WHERE funcionario.idFunc = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
	
/* ########## OBTER TODOS OS FUNCIONIOS DO TIPO PROFISSIONAL ################     */
	public function todosProfissionais()
    {
        $sql = "SELECT * FROM funcionario WHERE funcNivel = 'Profissional'";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	/*####################### ATUALIZAR UM FUNCIONARIO NO BANCO ######################## */
	public function atualizar(	$funcionario_id,
								$funcNome, 
								$funcCpf, 
								$funcRg, 
								$funcEmail, 
								$funcSenha, 
								$funcFone, 
								$funcCel, 
								$funcCedulaProfissional, 
								$funcComissao, 
								$funcNasci, 
								$funcSexo, 
								$funcNivel, 
								$funcEnd, 
								$funcNumero, 
								$funcEndComp, 
								$funcCidade, 
								$funcCEP, 
								$funcFoto, 
								$funcWebcam, 
								$funcAssinatura, 
								$funcBiografia, 
								$funcStatus)
    {
        $sql = "UPDATE funcionario SET  funcNome = '".$funcNome."', 
										funcCpf = '".$funcCpf."', 
										funcRg = '".$funcRg."', 
										funcEmail = '".$funcEmail."', 
										funcSenha = '".$funcSenha."', 
										funcFone = '".$funcFone."', 
										funcCel = '".$funcCel."', 
										funcCedulaProfissional = '".$funcCedulaProfissional."', 
										funcComissao = '".$funcComissao."', 
										funcNasci = '".$funcNasci."', 
										funcSexo = '".$funcSexo."', 
										funcNivel = '".$funcNivel."', 
										funcEnd = '".$funcEnd."', 
										funcNumero = '".$funcNumero."', 
										funcEndComp = '".$funcEndComp."', 
										funcCidade = '".$funcCidade."', 
										funcCEP = '".$funcCEP."', 
										funcFoto = '".$funcFoto."', 
										funcWebcam = '".$funcWebcam."', 
										funcAssinatura = '".$funcAssinatura."', 
										funcBiografia = '".$funcBiografia."', 
										funcStatus = '".$funcStatus."' WHERE idFunc = ".$funcionario_id;
		 
		 $query = $this->db->prepare($sql);    

		 //Retonar SQL com sucesso ou erro
		 if($query->execute()){
			 return true;
		 }else{
			 return false;
		 }  
    }

	
    /* ############ ADICIONAR UM FUNCIONARIO PARA O BANCO ################*/
    public function inserir($funcNome, 
							$funcCpf, 
							$funcRg, 
							$funcEmail, 
							$funcSenha, 
							$funcFone, 
							$funcCel, 
							$funcCedulaProfissional, 
							$funcComissao, 
							$funcNasci, 
							$funcSexo, 
							$funcEnd, 
							$funcNumero, 
							$funcEndComp, 
							$funcCidade, 
							$funcCEP, 
							$funcNivel, 
							$funcFoto, 
							$funcWebcam,
							$funcAssinatura, 
							$funcBiografia, 
							$funcStatus)
    {
        $sql = "INSERT INTO funcionario	(funcNome, 
										funcCpf, 
										funcRg, 
										funcEmail, 
										funcSenha, 
										funcFone, 
										funcCel, 
										funcCedulaProfissional, 
										funcComissao, 
										funcNasci, 
										funcSexo, 
										funcEnd, 
										funcNumero, 
										funcEndComp, 
										funcCidade, 
										funcCEP, 
										funcNivel, 
										funcFoto, 
										funcWebcam, 
										funcAssinatura, 
										funcBiografia, 
										funcStatus) VALUES (:funcNome, 
															:funcCpf, 
															:funcRg, 
															:funcEmail, 
															:funcSenha, 
															:funcFone, 
															:funcCel, 
															:funcCedulaProfissional, 
															:funcComissao, 
															:funcNasci, 
															:funcSexo, 
															:funcEnd, 
															:funcNumero, 
															:funcEndComp, 
															:funcCidade, 
															:funcCEP, 
															:funcNivel, 
															:funcFoto, 
															:funcWebcam, 
															:funcAssinatura, 
															:funcBiografia, 
															:funcStatus)";
        $query = $this->db->prepare($sql);		
		$parameters = array(':funcNome' 		      => $funcNome, 
							':funcCpf' 			      => $funcCpf, 
							':funcRg' 			      => $funcRg, 
							':funcEmail' 		      => $funcEmail, 
							':funcSenha' 		      => $funcSenha, 
							':funcFone' 		      => $funcFone, 
							':funcCel' 			      => $funcCel, 
							':funcCedulaProfissional' => $funcCedulaProfissional, 
							':funcComissao' 		  => $funcComissao, 
							':funcNasci' 		      => $funcNasci, 
							':funcSexo' 		      => $funcSexo, 
							':funcEnd' 			      => $funcEnd, 
							':funcNumero' 		      => $funcNumero, 
							':funcEndComp' 		      => $funcEndComp, 
							':funcCidade' 		      => $funcCidade, 
							':funcCEP' 			      => $funcCEP, 
							':funcNivel' 		      => $funcNivel, 
							':funcFoto' 		      => $funcFoto, 
							':funcWebcam' 		      => $funcWebcam, 
							':funcAssinatura' 	      => $funcAssinatura, 
							':funcBiografia' 	      => $funcBiografia, 
							':funcStatus' 		      => $funcStatus);

        // útil para debugar: você pode ver o SQL atrás da construção usando:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

		if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
	}
	
	public function inserircurso($idFunc, 
								$cursonome, 
								$cursodescricao)
    {
        $sql = "INSERT INTO especialidade (	idFunc, 
											espeNome, 
											espeDescricao) VALUES (:idFunc, 
																	:espeNome,
																	:espeDescricao)";
        $query = $this->db->prepare($sql);		
		$parameters = array(':idFunc'   => $idFunc, 
							':espeNome' 		  => $cursonome,
							':espeDescricao' 	  => $cursodescricao);

        // útil para debugar: você pode ver o SQL atrás da construção usando:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

		if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    /*#################  DESATIVAR UM FUNCIONARIO DO BANCO DE DADOS ####################*/
	public function desativar($id)
    {
        $sql = "UPDATE funcionario SET funcStatus = 0 WHERE funcionario.idFunc = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
	
	/* ########################## LISTAR TODOS OS FUNCIONARIOS PELO ID ################ */
    public function lista($id)
    {
        $sql = "SELECT * FROM funcionario WHERE funcionario.idFunc = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	public function listaEspecializcoes($id)
    {
        $sql = "SELECT * FROM especialidade WHERE especialidade.idEspecialidade = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

	
    /*############# RECEBER UM FUNCIONARIO DO BANCO ######################*/
    public function getFuncionario($funcionario_id)
    {
        $sql = "SELECT 	idFunc, 
						funcNome, 
						funcCpf, 
						funcRg, 
						funcEmail, 
						funcSenha, 
						funcFone, 
						funcCel, 
						funcComissao, 
						funcNasci, 
						funcSexo, 
						funcEnd, 
						funcNumero, 
						funcEndComp, 
						funcCidade, 
						funcCEP, 
						funcNivel, 
						funcFoto, 
						funcWebcam, 
						funcAssinatura, 
						funcBiografia, 
						funcStatus FROM funcionario WHERE idFunc = :funcionario_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array('funcionario_id' => $funcionario_id);

        // útil para debugar: você pode ver o SQL atrás da construção usando:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);

        // fetch() é o método do PDO que recebe exatamente um registro
        return ($query->rowcount() ? $query->fetch() : false);
    }

    
    /*################### "ESTATÍSTICAS" QTD DE FUNCIONÁRIO ATIVOS ################# */
    public function getAmountOfFuncionario()
    {
        $sql = "SELECT COUNT(idFunc) AS amount_of_funcionario FROM funcionario WHERE funcStatus = 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        // fetch() é o método do PDO que recebe exatamente um registro
        return $query->fetch()->amount_of_funcionario;
    }
	
	
	
	/* ################ LISTAR OS PROFISSIONAL ############################### */
    public function getProfissional()
    {
        $sql = "SELECT 	idFunc, 
						funcNome, 
						funcCpf, 
						funcRg, 
						funcEmail, 
						funcSenha, 
						funcFone, 
						funcCel, 
						funcNasci, 
						funcSexo, 
						funcComissao, 
						funcEnd, 
						funcNumero, 
						funcEndComp, 
						funcCidade, 
						funcCEP, 
						funcNivel, 
						funcFoto, 
						funcWebcam, 
						funcAssinatura, 
						funcBiografia, 
						funcStatus 
						FROM funcionario WHERE funcNivel = 'Profissional' AND funcStatus = 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	
	/*################ 	LISTAR A COMISSÃO DO PROFISSIONAL PELO ID ##########################*/
	public function comissaoProfissional($funcionario_id)
    {
        $sql = "SELECT servicos.servicoNome, servicos.servicoValor, funcionario.funcComissao, agendamentos.agendaData, agendamentos.agendaStatus, agendamentos.agendaPG FROM itensDaAgenda INNER JOIN servicos ON itensDaAgenda.idServico = servicos.idServico INNER JOIN agendamentos ON itensDaAgenda.idAgenda = agendamentos.idAgenda INNER JOIN funcionario ON agendamentos.idFunc = funcionario.idFunc WHERE agendamentos.idFunc = $funcionario_id ORDER BY agendamentos.agendaData DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }	
	
	
	/*################ RECEBER UM FUNCIONARIO DO BANCO PELO ID ##########################*/
    public function getPerfilProfissional($funcionario_id)
    {
        $sql = "SELECT * FROM funcionario WHERE idFunc = :funcionario_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array('funcionario_id' => $funcionario_id);
		$query->execute($parameters);
        return ($query->rowcount() ? $query->fetch() : false);
    }
	
	/*#################### RECEBER A ESPECIALIDADE DO FUNCIONARIO DO BANCO ####################*/
    public function getPerfilEspecialidade($funcionario_id)
    {
        $sql = "SELECT 	especialidade.idEspecialidade,especialidade.espeNome, 
						especialidade.espeDescricao 
						FROM especialidade INNER JOIN funcionario ON especialidade.idFunc = funcionario.idFunc 
						WHERE funcionario.idFunc = $funcionario_id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();		        
	}

	public function deleteEspecialidade($funcionario_id)
    {
        $sql = "DELETE FROM especialidade WHERE idEspecialidade = $funcionario_id";
        $query = $this->db->prepare($sql);
        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }		        
	}
	
	public function acessoFunc($email)
    {
        $sql = "SELECT idFunc, funcNome, funcSenha, funcNivel, funcFoto, funcWebcam, funcStatus  FROM funcionario WHERE funcEmail = '$email'";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
}
