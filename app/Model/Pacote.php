<?php

namespace App\Model;

use App\Core\Model;

class Pacote extends Model
{

    /* ########################## LISTAR TODOS OS SERVIÇOS COM FOTO ################ */
    public function listaTodos($publicar)
    {
        $sql = "SELECT * FROM pacoteservico WHERE pacoteservico.pacotePublicar=$publicar";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }


    /* ########################## LISTAR TODOS OS SERVIÇOS COM FOTO PELO ID ################ */
    public function lista($id)
    {
        $sql = "SELECT * FROM pacoteservico WHERE pacoteservico.idPacoteServico = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function listaAgendamento($id)
    {
        $sql = "SELECT * FROM itensDaAgenda INNER JOIN pacoteservico ON pacoteservico.idPacoteServico = itensDaAgenda.idPacoteServico WHERE itensDaAgenda.idAgenda = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
    

    /* ########################## INSERIR PACOTE DE SERVIÇO ################ */
    public function inserir(
        $pacoteNome,
        $pacoteDescricao,
        $pacoteFoto,
        $pacoteValor,
        $pacoteValorPromo,
        $pacoteComissao,
        $pacoteTempo,
        $pacoteSessoes,
        $pacotePublicar
    ) {
        $sql = "INSERT INTO pacoteservico (	pacoteNome, 
											pacoteDescricao,
                                            pacoteFoto,
											pacoteValor, 
											pacoteValorPromo, 
											pacoteComissao,
											pacoteTempo,
                                            pacoteSessoes,
											pacotePublicar) VALUES (	:pacoteNome, 
																		:pacoteDescricao,
                                                                        :pacoteFoto,
																		:pacoteValor, 
																		:pacoteValorPromo,
																		:pacoteComissao,
																		:pacoteTempo,
                                                                        :pacoteSessoes,	
																		:pacotePublicar)";
        $query = $this->db->prepare($sql);
        $parameters = array(
            ':pacoteNome'             => $pacoteNome,
            ':pacoteDescricao'         => $pacoteDescricao,
            ':pacoteFoto'             => $pacoteFoto,
            ':pacoteValor'             => $pacoteValor,
            ':pacoteValorPromo'     => $pacoteValorPromo,
            ':pacoteComissao'         => $pacoteComissao,
            ':pacoteTempo'             => $pacoteTempo,
            ':pacoteSessoes'          => $pacoteSessoes,
            ':pacotePublicar'         => $pacotePublicar);

        //Retonar SQL com sucesso ou erro
        if ($query->execute($parameters)) {
            return true;
        } else {
            return false;
        }
    }

    /* ########################## ATUALIZAR PACOTE SERVIÇO ################ */
    public function atualizar(  $idPacoteServico,
                                $pacoteNome,
                                $pacoteDescricao,
                                $pacoteFoto,
                                $pacoteValor,
                                $pacoteValorPromo,
                                $pacoteComissao,
                                $pacoteTempo,
                                $pacoteSessoes,
                                $pacotePublicar) {
        $sql = "UPDATE pacoteservico SET 	pacoteNome 			= '" . $pacoteNome . "', 
											pacoteDescricao 	= '" . $pacoteDescricao . "', 
                                            pacoteFoto 			= '" . $pacoteFoto . "',
											pacoteValor 		= '" . $pacoteValor . "', 
											pacoteValorPromo 	= '" . $pacoteValorPromo . "', 
											pacoteComissao 		= '" . $pacoteComissao . "', 
											pacoteTempo 		= '" . $pacoteTempo . "',
                                            pacoteSessoes 		= '" . $pacoteSessoes . "', 
											pacotePublicar 		= '" . $pacotePublicar . "' WHERE idPacoteServico = " . $idPacoteServico;
        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /* ########################## DESATIVAR PACOTE DE SERVIÇO ################ */
    public function desativar($id)
    {
        $sql = "UPDATE pacoteservico SET pacotePublicar = '0' WHERE pacoteservico.idPacoteServico = $id";
        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function ativarDesativar($id, $boleano)
    {
        $sql = "UPDATE pacoteservico SET pacotePublicar = $boleano WHERE pacoteservico.idPacoteServico = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }



    /* ########################## LISTAR TODAS OS SERVIÇOS DO PACOTE PELO ID ################ */
    public function listarFotosServico($id)
    {
        $sql = "SELECT pacoteservico.*, servicos.* FROM itempacoteservico 
				INNER JOIN pacoteservico ON pacoteservico.idPacoteServico = itempacoteservico.idPacoteServico 
				INNER JOIN servicos ON servicos.idServico = itempacoteservico.idServico 
				WHERE pacoteservico.idPacoteServico = $id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }



    /* ####################### PG DETALHE DO PACOTE DE SERVIÇOS ############################## */

    /*################ RECEBER DETALHE DO SERVIÇO PELO ID ##########################*/
    public function getDetalhePacote($idPacoteServico)
    {
        $sql = "SELECT * FROM pacoteservico WHERE idPacoteServico = :idPacoteServico";
        $query = $this->db->prepare($sql);
        $parameters = array('idPacoteServico' => $idPacoteServico);
        $query->execute($parameters);
        return ($query->rowcount() ? $query->fetch() : false);
    }

    /*#################### RECEBER AS FOTOS PARA O DETALHE DO SERVIÇO ####################*/
    public function getServicoPacote($idPacoteServico)
    {
        $sql = "SELECT pacoteservico.*, servicos.* FROM itempacoteservico INNER JOIN pacoteservico ON pacoteservico.idPacoteServico = itempacoteservico.idPacoteServico INNER JOIN servicos ON servicos.idServico = itempacoteservico.idServico WHERE pacoteservico.idPacoteServico = $idPacoteServico AND servicos.servicoStatus = 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }


    public function listarServico()
    {
        $sql = "SELECT * FROM servicos WHERE servicoStatus = 1";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function inserirServico($idPacote, $idServico)
    {
        $sql = "INSERT INTO itempacoteservico (
                        idPacoteServico, 
                        idServico) VALUES (
                            :idPacoteServico,
                            :idServico)";
        $query = $this->db->prepare($sql);
        $parameters = array(
            ':idServico'             => $idServico,
            ':idPacoteServico'         => $idPacote
        );

        //Retonar SQL com sucesso ou erro
        if ($query->execute($parameters)) {
            return true;
        } else {
            return false;
        }
    }

    public function removerServico($idServico)
    {
        $sql = "DELETE FROM itempacoteservico WHERE idServico=:idServico";
        $query = $this->db->prepare($sql);
        $parameters = array(
            ':idServico'             => $idServico,
        );

        //Retonar SQL com sucesso ou erro
        if ($query->execute($parameters)) {
            return true;
        } else {
            return false;
        }
    }
}
