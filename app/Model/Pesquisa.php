<?php

namespace App\Model;

use App\Core\Model;

class Pesquisa extends Model
{

    public function listaPesquisa()
    {
        $sql = "SELECT * FROM pesquisaQualidade";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function listacliente($id)
    {
        $sql = "SELECT * FROM pesquisaQualidade INNER JOIN cliente ON cliente.idCliente = pesquisaQualidade.idCliente WHERE pesquisaQualidade.idCliente = $id ORDER by pesquisaQualidade.pesquisaQualidadeData DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function pesquisacliente($id)
    {
        $sql = "SELECT pesquisaQualidade.*, cliente.cliNome, cliente.cliCPF, cliente.cliFoto, cliente.cliWebcam, cliente.cliNasci, cliente.cliSexo, cliente.cliProfissao  FROM pesquisaQualidade 
        INNER JOIN cliente ON cliente.idCliente = pesquisaQualidade.idCliente
        WHERE idPesquisaQualidade = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    } 

    public function listaPesquisa24H()
    {
        $sql = "SELECT agendamentos.idAgenda, cliente.idCliente, cliente.cliNome, cliente.cliCel, cliente.cliEmail, cliente.cliFoto, cliente.cliWebcam, agendamentos.agendaData FROM agendamentos inner JOIN cliente ON cliente.idCliente = agendamentos.idCliente WHERE agendaStatus = 3 AND agendaData < CURDATE() AND agendamentos.agendaPesquisa = 0 /*agendamentos.idCliente not in ( SELECT idCliente FROM pesquisaQualidade WHERE 1) AND agendaStatus = 3 AND agendaData < CURDATE() AND agendamentos.agendaPesquisa = 0*/";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	public function mediaPesquisa()
    {
        $sql = "SELECT * FROM pesquisaQualidade";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	
	public function qtdOfPesquisa()
    {
        $sql = "SELECT COUNT(idPesquisaQualidade) AS amount_of_pesquisa FROM pesquisaQualidade";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetch()->amount_of_pesquisa;
    }
	

    public function inserirPesquisa($idCliente, 
                                    $idAgenda,
                                    $pesquisaQualidadeComoServicoFoiPrestado, 
                                    $pesquisaQualidadeObsComoServicoFoiPrestado, 
                                    $pesquisaQualidadeAtendimentoRecepcao, 
                                    $pesquisaQualidadeObsAtendimentoRecepcao, 
                                    $pesquisaQualidadeAtendimentoBiomedico, 
                                    $pesquisaQualidadeObsAtendimentoBiomedico, 
                                    $pesquisaQualidadeAtendimentoEsteticista, 
                                    $pesquisaQualidadeObsAtendimentoEsteticista, 
                                    $pesquisaQualidadeAtendimentoGeral, 
                                    $pesquisaQualidadeObsAtendimentoGeral, 
                                    $pesquisaQualidadeLimpezaDosBanheiros, 
                                    $pesquisaQualidadeObsLimpezaDosBanheiros, 
                                    $pesquisaQualidadeLimpezaDasSalas, 
                                    $pesquisaQualidadeObsLimpezaDasSalas, 
                                    $pesquisaQualidadeConfortoDoEspaco, 
                                    $pesquisaQualidadeObsConfortoDoEspaco, 
                                    $pesquisaQualidadeSeuTratamento, 
                                    $pesquisaQualidadeObsSeuTratamento, 
                                    $pesquisaQualidadeFicaramEsclarecidasTodasAsDuvidas, 
                                    $pesquisaQualidadeFoiAferidaSuaPressaoArterial, 
                                    $pesquisaQualidadeDesejaFazerUmaObservacao,
                                    $status,
                                    $pesquisaQualidadeData)
    {
        $sql = "UPDATE agendamentos SET agendaPesquisa = 1 WHERE idAgenda = $idAgenda";
        $query = $this->db->prepare($sql);   

        if ($query->execute()) {

            $sql = "INSERT INTO pesquisaQualidade (	idCliente, 
                                                    pesquisaQualidadeComoServicoFoiPrestado, 
                                                    pesquisaQualidadeObsComoServicoFoiPrestado, 
                                                    pesquisaQualidadeAtendimentoRecepcao, 
                                                    pesquisaQualidadeObsAtendimentoRecepcao, 
                                                    pesquisaQualidadeAtendimentoBiomedico, 
                                                    pesquisaQualidadeObsAtendimentoBiomedico, 
                                                    pesquisaQualidadeAtendimentoEsteticista, 
                                                    pesquisaQualidadeObsAtendimentoEsteticista, 
                                                    pesquisaQualidadeAtendimentoGeral, 
                                                    pesquisaQualidadeObsAtendimentoGeral, 
                                                    pesquisaQualidadeLimpezaDosBanheiros, 
                                                    pesquisaQualidadeObsLimpezaDosBanheiros, 
                                                    pesquisaQualidadeLimpezaDasSalas, 
                                                    pesquisaQualidadeObsLimpezaDasSalas, 
                                                    pesquisaQualidadeConfortoDoEspaco, 
                                                    pesquisaQualidadeObsConfortoDoEspaco, 
                                                    pesquisaQualidadeSeuTratamento, 
                                                    pesquisaQualidadeObsSeuTratamento, 
                                                    pesquisaQualidadeFicaramEsclarecidasTodasAsDuvidas, 
                                                    pesquisaQualidadeFoiAferidaSuaPressaoArterial, 
                                                    pesquisaQualidadeDesejaFazerUmaObservacao, 
                                                    PesquisaQualidadeStatus,
                                                    pesquisaQualidadeData) VALUES ( :idCliente, 
                                                                                    :pesquisaQualidadeComoServicoFoiPrestado, 
                                                                                    :pesquisaQualidadeObsComoServicoFoiPrestado, 
                                                                                    :pesquisaQualidadeAtendimentoRecepcao, 
                                                                                    :pesquisaQualidadeObsAtendimentoRecepcao, 
                                                                                    :pesquisaQualidadeAtendimentoBiomedico, 
                                                                                    :pesquisaQualidadeObsAtendimentoBiomedico, 
                                                                                    :pesquisaQualidadeAtendimentoEsteticista, 
                                                                                    :pesquisaQualidadeObsAtendimentoEsteticista, 
                                                                                    :pesquisaQualidadeAtendimentoGeral, 
                                                                                    :pesquisaQualidadeObsAtendimentoGeral, 
                                                                                    :pesquisaQualidadeLimpezaDosBanheiros, 
                                                                                    :pesquisaQualidadeObsLimpezaDosBanheiros, 
                                                                                    :pesquisaQualidadeLimpezaDasSalas, 
                                                                                    :pesquisaQualidadeObsLimpezaDasSalas, 
                                                                                    :pesquisaQualidadeConfortoDoEspaco, 
                                                                                    :pesquisaQualidadeObsConfortoDoEspaco, 
                                                                                    :pesquisaQualidadeSeuTratamento, 
                                                                                    :pesquisaQualidadeObsSeuTratamento, 
                                                                                    :pesquisaQualidadeFicaramEsclarecidasTodasAsDuvidas, 
                                                                                    :pesquisaQualidadeFoiAferidaSuaPressaoArterial, 
                                                                                    :pesquisaQualidadeDesejaFazerUmaObservacao, 
                                                                                    :PesquisaQualidadeStatus,
                                                                                    :pesquisaQualidadeData)";
            $query = $this->db->prepare($sql);
            $parameters = array(':idCliente'                                            => $idCliente, 
                                ':pesquisaQualidadeComoServicoFoiPrestado'              => $pesquisaQualidadeComoServicoFoiPrestado, 
                                ':pesquisaQualidadeObsComoServicoFoiPrestado'           => $pesquisaQualidadeObsComoServicoFoiPrestado, 
                                ':pesquisaQualidadeAtendimentoRecepcao'                 => $pesquisaQualidadeAtendimentoRecepcao, 
                                ':pesquisaQualidadeObsAtendimentoRecepcao'              => $pesquisaQualidadeObsAtendimentoRecepcao, 
                                ':pesquisaQualidadeAtendimentoBiomedico'                => $pesquisaQualidadeAtendimentoBiomedico, 
                                ':pesquisaQualidadeObsAtendimentoBiomedico'             => $pesquisaQualidadeObsAtendimentoBiomedico, 
                                ':pesquisaQualidadeAtendimentoEsteticista'              => $pesquisaQualidadeAtendimentoEsteticista, 
                                ':pesquisaQualidadeObsAtendimentoEsteticista'           => $pesquisaQualidadeObsAtendimentoEsteticista, 
                                ':pesquisaQualidadeAtendimentoGeral'                    => $pesquisaQualidadeAtendimentoGeral, 
                                ':pesquisaQualidadeObsAtendimentoGeral'                 => $pesquisaQualidadeObsAtendimentoGeral, 
                                ':pesquisaQualidadeLimpezaDosBanheiros'                 => $pesquisaQualidadeLimpezaDosBanheiros, 
                                ':pesquisaQualidadeObsLimpezaDosBanheiros'              => $pesquisaQualidadeObsLimpezaDosBanheiros, 
                                ':pesquisaQualidadeLimpezaDasSalas'                     => $pesquisaQualidadeLimpezaDasSalas, 
                                ':pesquisaQualidadeObsLimpezaDasSalas'                  => $pesquisaQualidadeObsLimpezaDasSalas, 
                                ':pesquisaQualidadeConfortoDoEspaco'                    => $pesquisaQualidadeConfortoDoEspaco, 
                                ':pesquisaQualidadeObsConfortoDoEspaco'                 => $pesquisaQualidadeObsConfortoDoEspaco, 
                                ':pesquisaQualidadeSeuTratamento'                       => $pesquisaQualidadeSeuTratamento, 
                                ':pesquisaQualidadeObsSeuTratamento'                    => $pesquisaQualidadeObsSeuTratamento, 
                                ':pesquisaQualidadeFicaramEsclarecidasTodasAsDuvidas'   => $pesquisaQualidadeFicaramEsclarecidasTodasAsDuvidas, 
                                ':pesquisaQualidadeFoiAferidaSuaPressaoArterial'        => $pesquisaQualidadeFoiAferidaSuaPressaoArterial, 
                                ':pesquisaQualidadeDesejaFazerUmaObservacao'        	=> $pesquisaQualidadeDesejaFazerUmaObservacao, 
                                ':PesquisaQualidadeStatus'        	                    => $status, 
                                ':pesquisaQualidadeData'                                => $pesquisaQualidadeData);

            
            if($query->execute($parameters)){
                return true;
            }else{
                return false;
            }
        } else {
            return false; 
        }
    }
	
	
} /*################################ FIM CLASS ##############################*/
