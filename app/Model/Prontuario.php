<?php

namespace App\Model;

use App\Core\Model;

class Prontuario extends Model
{

    public function lista($id)
    {
        $sql = "SELECT funcionario.funcNome, cliente.cliNome, prontuario.* FROM `prontuario` INNER JOIN funcionario ON funcionario.idFunc = prontuario.idFunc INNER JOIN cliente ON cliente.idCliente = prontuario.idCliente WHERE prontuario.id = $id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaProntuarioIdCliente($id)
    {
        $sql = "SELECT funcionario.funcNome, cliente.cliNome, prontuario.* FROM `prontuario` INNER JOIN funcionario ON funcionario.idFunc = prontuario.idFunc INNER JOIN cliente ON cliente.idCliente = prontuario.idCliente WHERE prontuario.idCliente = $id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizar($id, $descricao)
    {
        $sql = "UPDATE prontuario set descricao = '".$descricao."' WHERE id = ".$id;

        $query = $this->db->prepare($sql);    
        return $query->execute();           
    }

    public function insert($idCliente, $idFunc, $descricao)
    {
        $sql = "INSERT INTO prontuario (idCliente, idFunc, descricao, prontuarioData) VALUES (:idCliente, :idFunc, :descricao, CURDATE())";
        $query = $this->db->prepare($sql);
        $parameters = array(':idCliente' => $idCliente, ':idFunc' => $idFunc, ':descricao' => $descricao);

        return $query->execute($parameters);
    }

    public function deletar($id_gatilho)
    {
        $sql = "DELETE FROM prontuario WHERE id = :id_gatilho";
        $query = $this->db->prepare($sql);
        $parameters = array(':cliente_id' => $id_gatilho);

        $query->execute($parameters);
    }

}
