<?php

namespace App\Model;

use App\Core\Model;

class Restaurar extends Model
{

    public function lista($id_pagina)
    {
        $sql = "SELECT * FROM `paginas_restauracao` WHERE id_pagina=$id_pagina";
        $query = $this->db->prepare($sql);    
        $query->execute();

        return $query->fetchAll();
    }

    public function deletar($id_gatilho)
    {
        $sql = "DELETE FROM gatilho WHERE id = :id_gatilho";
        $query = $this->db->prepare($sql);
        $parameters = array(':cliente_id' => $id_gatilho);

        $query->execute($parameters);
    }

    public function criarRestauracao($id_pagina)
    {
        $sql = "INSERT INTO paginas_restauracao (id_pagina, titulo, subtitulo, descricao, img, alt, meta_titulo, meta_descricao, usuario, data_alteracao) SELECT id, titulo, subtitulo, descricao, img, alt, meta_titulo, meta_descricao, usuario, data_alteracao FROM paginas Where id='$id_pagina' ";
        $query = $this->db->prepare($sql);    
        $query->execute();

    }

    public function restaurar($id_pagina)
    {
        $sql = "UPDATE `paginas` SET 
        `titulo`= (SELECT `titulo` FROM `paginas_restauracao` WHERE id = $id_pagina),
        `subtitulo`= (SELECT `subtitulo` FROM `paginas_restauracao` WHERE id = $id_pagina),
        `descricao` = (SELECT `descricao` FROM `paginas_restauracao` WHERE id = $id_pagina),
        `img`= (SELECT `img` FROM `paginas_restauracao` WHERE id = $id_pagina),
        `alt`= (SELECT `alt` FROM `paginas_restauracao` WHERE id = $id_pagina),
        `meta_titulo`= (SELECT `meta_titulo` FROM `paginas_restauracao` WHERE id = $id_pagina),
        `meta_descricao`= (SELECT `meta_titulo` FROM `paginas_restauracao` WHERE id = $id_pagina),
        `data_alteracao`= (SELECT `data_alteracao` FROM `paginas_restauracao` WHERE id = $id_pagina),
        `usuario`= (SELECT `usuario` FROM `paginas_restauracao` WHERE id = $id_pagina),
        `compartilhamento`= (SELECT `compartilhamento` FROM `paginas_restauracao` WHERE id = $id_pagina),
        `gatilho`= (SELECT `gatilho` FROM `paginas_restauracao` WHERE id = $id_pagina)
        WHERE id = 1";
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou errp
        if($query->execute()){
            return true;
        }else{
            return false;
        }

    }

    public function restaurarr($id_pagina, $titulo, $descricao, $alt, $img, $meta_titulo, $meta_descricao, $compartilhamento, $gatilho, $data, $usuario)
    {
        $sql = "update paginas set titulo = '".$titulo."', descricao = '".$descricao."',alt = '".$alt."',img = '".$img."',meta_titulo = '".$meta_titulo."',meta_descricao = '".$meta_descricao."',data_alteracao = '".$data."',compartilhamento = '".$compartilhamento."',gatilho = '".$gatilho."',usuario = '".$usuario."' where id = ".$id_pagina;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou errp
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }


}
