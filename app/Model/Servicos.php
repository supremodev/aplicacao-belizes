<?php

namespace App\Model;

use App\Core\Model;

class Servicos extends Model
{

	/* ########################## LISTAR TODOS OS SERVIÇOS COM FOTO ################ */
    public function listaTodos($status)
    {
        $sql = "SELECT * FROM servicos WHERE servicoStatus = $status";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function agendamentoServicos($id)
    {
        $sql = "SELECT * FROM itensDaAgenda 
        INNER JOIN servicos ON servicos.idServico = itensDaAgenda.idServico 
        WHERE itensDaAgenda.idAgenda = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function agendamentoPacotes($id)
    {
        $sql = "SELECT * FROM itensDaAgenda 
        INNER JOIN pacoteservico ON pacoteservico.idPacoteServico  = itensDaAgenda.idPacoteServico  
        WHERE itensDaAgenda.idAgenda = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function tratamentoQtd($id)
    {
        $sql = "SELECT tratamentoQtd, idFichaAnamneseCriolipolise, idFichaFacil, idFichaCorporal FROM `tratamento` WHERE idTratamento = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function tratamentoCriolipolise($id)
    {
        $sql = "SELECT fichaAnamneseQtdSessoes FROM `fichaAnamneseCriolipolise` WHERE idFichaAnamneseCriolipolise = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function tratamentoFacial($id)
    {
        $sql = "SELECT fichaFacilQtdSessoes FROM `fichaFacil` WHERE idFichaFacil = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function tratamentoCorporal($id)
    {
        $sql = "SELECT fichaCorporalQtdSessoes FROM `fichaCorporal` WHERE idFichaCorporal = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
    


	/* ########################## LISTAR TODOS OS SERVIÇOS COM FOTO PELO ID ################ */
    public function lista($id)
    {
        $sql = "SELECT * FROM servicos WHERE servicos.idServico = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function listarServicoContrato($id)
    {
        $sql = "SELECT servicoNome FROM servicos INNER JOIN itensDoContrato ON itensDoContrato.idServico = servicos.idServico WHERE idContrato = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function listarPacoteContrato($id)
    {
        $sql = "SELECT pacoteNome FROM pacoteservico INNER JOIN itensDoContrato ON itensDoContrato.idPacoteServico = pacoteservico.idPacoteServico WHERE idContrato = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
    
    /* ########## OBTER TODOS OS FUNCIONIOS ATIVOS DO BANCO DE DADOS ################     */
    public function getAllServicos($boleano)
    {
        $sql = "SELECT * FROM servicos WHERE servicoStatus = $boleano";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
	}
	
	public function ativarDesativar($id,$boleano)
    {
        $sql = "UPDATE servicos SET servicoStatus = $boleano WHERE servicos.idServico = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    
    /* ########################## DESATIVAR SERVIÇO ################ */
    public function desativar($id)
    {
        $sql = "UPDATE servicos SET servicoStatus = 0 WHERE servicos.idServico = $id";
        $query = $this->db->prepare($sql);
        
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

	
	/* ########################## INSERIR SERVIÇO ################ */
    public function inserir($servicoFoto, 
							$servicoNome, 
							$servicoDescricao, 
							$servicoValor, 
							$servicoValorPromo, 
							$servicoComissao, 
							$servicoTempo,
                            $servicoSessoes,  
							$servicoPublicar,
							$servicoStatus)
    {
        $sql = "INSERT INTO servicos (	servicoNome, 
										servicoDescricao, 
										servicoValor, 
										servicoValorPromo, 
										servicoComissao, 
										servicoTempo, 
										servicoFoto, 
                                        servicoSessoes,  
										servicoPublicar, 
										servicoStatus) VALUES (	:servicoNome, 
																	:servicoDescricao, 
																	:servicoValor, 
																	:servicoValorPromo, 
																	:servicoComissao, 
																	:servicoTempo, 
																	:servicoFoto,
                                                                    :servicoSessoes,  
																	:servicoPublicar,
																	:servicoStatus)";
        $query = $this->db->prepare($sql);
        $parameters = array(':servicoNome' 			=> $servicoNome, 
							':servicoDescricao' 	=> $servicoDescricao, 
							':servicoValor' 		=> $servicoValor, 
							':servicoValorPromo' 	=> $servicoValorPromo, 
							':servicoComissao' 		=> $servicoComissao, 
							':servicoTempo' 		=> $servicoTempo, 
							':servicoFoto' 			=> $servicoFoto, 
                            ':servicoSessoes' 		=> $servicoSessoes, 
							':servicoPublicar' 		=> $servicoPublicar,
							':servicoStatus' 		=> $servicoStatus);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function inseririmg($idservico, 
							$servicoNome, 
							$servicoDescricao, 
							$foto)
    {
        $sql = "INSERT INTO fotoservico (fotoNome, 
										fotoDescricao, 
										idServico, 
										fotoUrl) VALUES (	:fotoNome, 
															:fotoDescricao, 
															:idServico, 
															:fotoUrl)";
        $query = $this->db->prepare($sql);
        $parameters = array(':fotoNome' 		=> $servicoNome, 
							':fotoDescricao' 	=> $servicoDescricao, 
							':idServico' 		=> $idservico, 
							':fotoUrl' 	        => $foto);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }
	
	/* ########################## ATUALIZAR SERVIÇO ################ */
 	public function atualizar(	$idServico, 
     $servicoNome, 
     $servicoDescricao, 
     $servicoValor, 
     $servicoValorPromo, 
     $servicoComissao, 
     $servicoTempo, 
     $servicoFoto, 
     $servicoSessoes,
     $servicoPublicar, 
     $servicoStatus
     )
    {
        $sql = "UPDATE servicos SET        
                                    servicoNome = '".$servicoNome."', 
                                    servicoDescricao = '".$servicoDescricao."', 
                                    servicoValor = '".$servicoValor."', 
                                    servicoValorPromo = '".$servicoValorPromo."', 
                                    servicoComissao = '".$servicoComissao."', 
                                    servicoTempo = '".$servicoTempo."', 
                                    servicoFoto = '".$servicoFoto."', 
                                    servicoSessoes = '".$servicoSessoes."', 
                                    servicoPublicar = '".$servicoPublicar."', 
                                    servicoStatus = '".$servicoStatus."' WHERE idServico = ".$idServico;
                                    
        $query = $this->db->prepare($sql);  
        
        if($query->execute()){
            return true;
        }else{
            return false;
		}
    }
	
	/* ########################## LISTAR TODAS AS FOTOS DO SERVIÇOS PELO ID ################ */
	public function listarFotosServico($id)
    {
        $sql = "SELECT fotoservico.*, servicos.servicoNome FROM fotoservico INNER JOIN servicos ON servicos.idServico = fotoservico.idServico WHERE servicos.idServico = $id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }  
	
	
	
	/* ####################### PG DETALHE DO SERVIÇO ############################## */
	
	/*################ RECEBER DETALHE DO SERVIÇO PELO ID ##########################*/
    public function getDetalheServico($servico_id)
    {
        $sql = "SELECT * FROM servicos WHERE idServico = :servico_id";
        $query = $this->db->prepare($sql);
        $parameters = array('servico_id' => $servico_id);
		$query->execute($parameters);
        return ($query->rowcount() ? $query->fetch() : false);
    }
	
	/*#################### RECEBER AS FOTOS PARA O DETALHE DO SERVIÇO ####################*/
    public function getfotoServico($servico_id)
    {
        $sql = "SELECT * FROM fotoservico WHERE idServico = $servico_id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();		        
    }

    public function deleteFoto($id)
    {
        $sql = "DELETE FROM `fotoservico` WHERE idFoto = $id";
        $query = $this->db->prepare($sql);
        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }		        
    }
    
    public function retornaIdServico($nome)
    {
        $sql = "SELECT idServico FROM servicos WHERE servicoNome = '$nome'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function retornaIdPacote($nome)
    {
        $sql = "SELECT idPacoteServico FROM pacoteservico WHERE pacoteNome = '$nome'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

}
