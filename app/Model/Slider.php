<?php

namespace App\Model;

use App\Core\Model;

class Slider extends Model
{

    public function listaTodos()
    {
        $sql = "SELECT * FROM `slider` WHERE 1 ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function lista($id)
    {
        $sql = "SELECT * FROM `slider` WHERE id=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizar($id, $titulo, $descricao, $img, $alt )
    {
        $sql = "update slider set titulo = '".$titulo."', descricao = '".$descricao."',img = '".$img."',alt = '".$alt."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    
    }

    public function inserir($titulo, $descricao, $img, $alt)
    {
        $sql = "INSERT INTO slider (titulo, descricao, img, alt) VALUES (:titulo, :descricao,  :img, :alt)";
        $query = $this->db->prepare($sql);
        $parameters = array(':titulo' => $titulo, ':descricao' => $descricao, ':img' => $img, 'alt' => $alt);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function deletar($id)
    {
        $sql = "DELETE FROM slider WHERE id = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

}
