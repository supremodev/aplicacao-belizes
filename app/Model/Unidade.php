<?php

namespace App\Model;

use App\Core\Model;

class Unidade extends Model
{
	
/*	
#####################################################################
#############											#############
#############					UNIDADE					#############
#############											#############
#####################################################################
*/

/* ######## LISTAR TODAS AS UNIDADES ########## */
    public function listaTodosUnidade($status)
    {
        $sql = "SELECT * FROM unidade WHERE unidadeStatus=$status" ;
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	/* ######## LISTAR UNIDADE PELO ID ########## */
	public function lista($id)
    {
        $sql = "SELECT * FROM unidade WHERE idUnidade = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	/* ######## ATUALIZAR DADOS DA UNIDADE ########## */
	public function atualizar(	$id,
                                $unidadeNomeEmpresa, 
								$unidadeRazaoSocial, 
								$unidadeCNPJ, 
								$unidadeFoneUm, 
								$unidadeFoneDois, 
								$unidadeFoneTres, 
								$unidadeEnd, 
								$unidadeEndRua, 
								$unidadeEndComp, 
								$unidadeEndBairro, 
								$unidadeEndCidade, 
								$unidadeCEP, 
								$unidadePessoaContato, 
								$unidadeEmail, 
								$unidadeFoto, 
								$unidadeSite, 
								$unidadeFace, 
								$unidadeInsta, 
								$unidadeTwitter, 
								$unidadeYouTUBE,
								$unidadeStatus)
    {
        $sql = "UPDATE unidade set  unidadeNomeEmpresa 		= '".$unidadeNomeEmpresa."', 
									unidadeRazaoSocial 		= '".$unidadeRazaoSocial."', 
									unidadeCNPJ 			= '".$unidadeCNPJ."', 
									unidadeFoneUm 			= '".$unidadeFoneUm."', 
									unidadeFoneDois 		= '".$unidadeFoneDois."', 
									unidadeFoneTres 		= '".$unidadeFoneTres."', 
									unidadeEnd 				= '".$unidadeEnd."', 
									unidadeEndRua 			= '".$unidadeEndRua."', 
									unidadeEndComp 			= '".$unidadeEndComp."', 
									unidadeEndBairro 		= '".$unidadeEndBairro."', 
									unidadeEndCidade 		= '".$unidadeEndCidade."', 
									unidadeCEP 				= '".$unidadeCEP."', 
									unidadePessoaContato 	= '".$unidadePessoaContato."', 
									unidadeEmail 			= '".$unidadeEmail."', 
									unidadeFoto 			= '".$unidadeFoto."', 
									unidadeSite 			= '".$unidadeSite."', 
									unidadeFace 			= '".$unidadeFace."', 
									unidadeInsta 			= '".$unidadeInsta."', 
									unidadeTwitter 			= '".$unidadeTwitter."', 
									unidadeYouTUBE 			= '".$unidadeYouTUBE."', 
									unidadeStatus 			= '".$unidadeStatus."' WHERE idUnidade = ".$id;
		
        $query = $this->db->prepare($sql);    
        if($query->execute()){
            return true;
        }else{
            return false;
        } 
    }
	
	/* ######## INSERIR UNIDADE ########## */
	public function inserirUnidade(	$unidadeNomeEmpresa, 
									$unidadeRazaoSocial, 
									$unidadeCNPJ, 
									$unidadeFoneUm, 
									$unidadeFoneDois, 
									$unidadeFoneTres, 
									$unidadeEnd, 
									$unidadeEndRua, 
									$unidadeEndComp, 
									$unidadeEndBairro, 
									$unidadeEndCidade, 
									$unidadeCEP, 
									$unidadePessoaContato, 
									$unidadeEmail, 
									$unidadeFoto, 
									$unidadeSite, 
									$unidadeFace, 
									$unidadeInsta, 
									$unidadeTwitter, 
									$unidadeYouTUBE,
									$unidadeStatus)
    {
        $sql = "INSERT INTO unidade (	unidadeNomeEmpresa, 
										unidadeRazaoSocial, 
										unidadeCNPJ, 
										unidadeFoneUm, 
										unidadeFoneDois, 
										unidadeFoneTres, 
										unidadeEnd, 
										unidadeEndRua, 
										unidadeEndComp, 
										unidadeEndBairro, 
										unidadeEndCidade, 
										unidadeCEP, 
										unidadePessoaContato, 
										unidadeEmail, 
										unidadeFoto, 
										unidadeSite, 
										unidadeFace, 
										unidadeInsta, 
										unidadeTwitter, 
										unidadeYouTUBE, 
										unidadeStatus) VALUES (	:unidadeNomeEmpresa, 
																:unidadeRazaoSocial, 
																:unidadeCNPJ, 
																:unidadeFoneUm, 
																:unidadeFoneDois, 
																:unidadeFoneTres, 
																:unidadeEnd, 
																:unidadeEndRua, 
																:unidadeEndComp, 
																:unidadeEndBairro, 
																:unidadeEndCidade, 
																:unidadeCEP, 
																:unidadePessoaContato, 
																:unidadeEmail, 
																:unidadeFoto, 
																:unidadeSite, 
																:unidadeFace, 
																:unidadeInsta, 
																:unidadeTwitter, 
																:unidadeYouTUBE,
																:unidadeStatus)";
        $query = $this->db->prepare($sql);
        $parameters = array(':unidadeNomeEmpresa' 		=> $unidadeNomeEmpresa, 
							':unidadeRazaoSocial' 		=> $unidadeRazaoSocial, 
							':unidadeCNPJ' 				=> $unidadeCNPJ, 
							':unidadeFoneUm' 			=> $unidadeFoneUm, 
							':unidadeFoneDois'			=> $unidadeFoneDois, 
							':unidadeFoneTres' 			=> $unidadeFoneTres, 
							':unidadeEnd' 				=> $unidadeEnd, 
							':unidadeEndRua' 			=> $unidadeEndRua, 
							':unidadeEndComp' 			=> $unidadeEndComp, 
							':unidadeEndBairro' 		=> $unidadeEndBairro, 
							':unidadeEndCidade' 		=> $unidadeEndCidade, 
							':unidadeCEP' 				=> $unidadeCEP, 
							':unidadePessoaContato' 	=> $unidadePessoaContato, 
							':unidadeEmail' 			=> $unidadeEmail, 
							':unidadeFoto' 				=> $unidadeFoto, 
							':unidadeSite' 				=> $unidadeSite, 
							':unidadeFace' 				=> $unidadeFace, 
							':unidadeInsta' 			=> $unidadeInsta, 
							':unidadeTwitter' 			=> $unidadeTwitter, 							 
							':unidadeYouTUBE' 			=> $unidadeYouTUBE,
						   	':unidadeStatus' 			=> $unidadeStatus);
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
	}
	
	public function ativarDesativar($id,$boleano)
    {
        $sql = "UPDATE unidade SET unidadeStatus = $boleano WHERE idUnidade = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
	}
	
/*	
#####################################################################
#############											#############
#############				SALA UNIDADE				#############
#############											#############
#####################################################################
*/

/* ######## LISTAR TODAS AS SALAS DA UNIDADES ########## */
    public function listaTodosSala($boleano)
    {
        $sql = "SELECT * FROM salaUnidade WHERE salaStatus = $boleano";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	/* ######## LISTAR SALAS DA UNIDADE PELO ID ########## */
	public function listaSalaUnidade($id)
    {
        $sql = "SELECT * FROM salaUnidade WHERE idSalaUnidade = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	/* ######## ATUALIZAR DADOS DA SALA  ########## */
	public function atualizarSala(	$id,
                                    $idUnidade, 
									$salaNomeUnidade, 
									$salaDescricaoUnidade, 
									$salaFotoUnidade)
    {
        $sql = "UPDATE salaUnidade set  idUnidade 				= '".$idUnidade."', 
										salaNomeUnidade 		= '".$salaNomeUnidade."', 
										salaDescricaoUnidade 	= '".$salaDescricaoUnidade."', 
										salaFotoUnidade 		= '".$salaFotoUnidade."' WHERE idSalaUnidade = ".$id;
       $query = $this->db->prepare($sql);    
       if($query->execute()){
        return true;
       }else{
        return false;
       }  
    }
	
	/* ######## CADASTRO SALA UNIDADE ########## */
	public function inserirSala($idUnidade, 
								$salaNomeUnidade, 
								$salaDescricaoUnidade, 
								$salaFotoUnidade,
								$status)
    {
        $sql = "INSERT INTO salaUnidade (	idUnidade, 
											salaNomeUnidade, 
											salaDescricaoUnidade, 
											salaFotoUnidade,
											salaStatus) VALUES (		:idUnidade, 
																		:salaNomeUnidade, 
																		:salaDescricaoUnidade, 
																		:salaFotoUnidade,
																		:salaStatus)";
        $query = $this->db->prepare($sql);
        $parameters = array(':idUnidade' 			=> $idUnidade, 
							':salaNomeUnidade' 		=> $salaNomeUnidade, 
							':salaDescricaoUnidade' => $salaDescricaoUnidade, 
							':salaFotoUnidade' 		=> $salaFotoUnidade,
							':salaStatus' 			=> $status);
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
	}
	
	public function ativarDesativarSala($id,$boleano)
    {
        $sql = "UPDATE salaUnidade SET salaStatus = $boleano WHERE idSalaUnidade = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
	}
	
/*	
#####################################################################
#############											#############
#############			EQUIPAMENTO UNIDADE				#############
#############											#############
#####################################################################
*/

/* ######## LISTAR TODAS AS EQUIPAMENTO  ########## */
    public function listaTodosEquipamento($boleano)
    {
        $sql = "SELECT * FROM equipamentoUnidade WHERE equipamentoStatus = $boleano";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
	
	/* ######## LISTAR EQUIPAMENTO DA UNIDADE PELO ID ########## */
	public function listaEquipamentoUnidade($id)
    {
        $sql = "SELECT * FROM equipamentoUnidade WHERE idEquipamento = $id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	/* ######## ATUALIZAR DADOS DO EQUIPAMENTO ########## */
    public function atualizarEquipamento(	$id,
                                            $idUnidade, 
                                            $equipamentoNome, 
                                            $equipamentoDescricao, 
                                            $equipamentoFoto,
                                            $equipamentoPublicar)
    {
        $sql = "UPDATE equipamentoUnidade set idUnidade 		     = '".$idUnidade."', 
                                              equipamentoNome 	     = '".$equipamentoNome."', 
										      equipamentoDescricao 	 = '".$equipamentoDescricao."', 
										      equipamentoFoto 		 = '".$equipamentoFoto."',
                                              equipamentoPublicar	 = '".$equipamentoPublicar."' WHERE idEquipamento = ".$id;
       $query = $this->db->prepare($sql);    
       if($query->execute()){
        return true;
       }else{
        return false;
       }  
    }
	
	/* ######## INSERIR	EQUIPAMENTO ########## */
	public function inserirEquipamento(	$idUnidade, 
										$equipamentoNome, 
										$equipamentoDescricao, 
										$equipamentoFoto, 
										$equipamentoPublicar,
										$equipamentoStatus)
    {
        $sql = "INSERT INTO equipamentoUnidade (	idUnidade, 
													equipamentoNome, 
													equipamentoDescricao, 
													equipamentoFoto, 
													equipamentoPublicar,
													equipamentoStatus) VALUES (	:idUnidade, 
																				    :equipamentoNome, 
																				    :equipamentoDescricao, 
																				    :equipamentoFoto, 
																				    :equipamentoPublicar,
																					:equipamentoStatus)";
        $query = $this->db->prepare($sql);
        $parameters = array(':idUnidade' 			 => $idUnidade, 
							':equipamentoNome' 	     => $equipamentoNome, 
							':equipamentoDescricao'  => $equipamentoDescricao, 
							':equipamentoFoto' 	     => $equipamentoFoto, 
							':equipamentoPublicar' 	 => $equipamentoPublicar,
							':equipamentoStatus' 	 => $equipamentoStatus);
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
	}
	
	public function ativarDesativarEquipamento($id,$boleano)
    {
        $sql = "UPDATE equipamentoUnidade SET EquipamentoStatus = $boleano WHERE idEquipamento = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
	
/*	
#####################################################################
#############											#############
#############				DETALHE UNIDADE				#############
#############											#############
#####################################################################
*/

	
/*################ RECEBER DETALHE DA UNIDADE PELO ID ##########################*/
    public function getDetalheUnidade($idUnidade)
    {
        $sql = "SELECT * FROM unidade WHERE idUnidade = :idUnidade";
        $query = $this->db->prepare($sql);
        $parameters = array('idUnidade' => $idUnidade);
		$query->execute($parameters);
        return ($query->rowcount() ? $query->fetch() : false);
    }
	
/*#################### RECEBER AS INFOMAÇÕES DE SALAS DA UNIDADE ####################*/
    public function getSalasUnidade($idUnidade)
    {
        $sql = "SELECT unidade.*, salaUnidade.* FROM unidade INNER JOIN salaUnidade ON salaUnidade.idUnidade = unidade.idUnidade WHERE unidade.idUnidade = $idUnidade";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();		        
    }
	public function listarSalas()
    {
        $sql = "SELECT * FROM salaUnidade";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();		        
    }
	
	/*#################### RECEBER AS INFOMAÇÕES DOS EQUIPAMENTOS DA UNIDADE ####################*/
    public function geteEquipamentoUnidade($idUnidade)
    {
        $sql = "SELECT unidade.*, equipamentoUnidade.* FROM itemUnidadeEquipamento 
				INNER JOIN unidade ON unidade.idUnidade = itemUnidadeEquipamento.idUnidade 
				INNER JOIN equipamentoUnidade ON equipamentoUnidade.idEquipamento = itemUnidadeEquipamento.idEquipamento WHERE unidade.idUnidade = $idUnidade";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();		        
    }
	public function listarEquipamento()
    {
        $sql = "SELECT * FROM equipamentoUnidade";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();		        
    }	
	
} /*################################ FIM CLASS ##############################*/
