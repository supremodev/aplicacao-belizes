<?php

namespace App\Model;

use App\Core\Model;

class Usuario extends Model
{
    public function lista($id)
    {
        $sql = "SELECT usuario.*,  niveis.nivel FROM `usuario` INNER JOIN niveis ON usuario.id_nivel = niveis.id WHERE usuario.id=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaTodos()
    {
        $sql = "SELECT usuario.*,  niveis.nivel FROM `usuario` INNER JOIN niveis ON usuario.id_nivel = niveis.id WHERE 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaAdmin()
    {
        $sql = "SELECT usuario.email FROM `usuario` INNER JOIN niveis ON usuario.id_nivel = niveis.id WHERE niveis.nivel='admin'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaTodosProfissional()
    {
        $sql = "SELECT usuario.id,usuario.nome, usuario.img FROM `usuario` INNER JOIN niveis ON usuario.id_nivel = niveis.id WHERE usuario.id_nivel=3";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function usuario($Email)
    {
        $sql = "SELECT usuario.idUsuario, usuario.userNome, usuario.userSenha, nivel.nivel  FROM `usuario` INNER JOIN nivel ON usuario.idNivel = nivel.idNivel WHERE userEmail='$Email'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function ultimoRegistro($email)
    {
        $sql = "SELECT id FROM `usuario` WHERE email='$email'";
        $query = $this->db->prepare($sql);  
        
        return $query->fetchAll();

    }

    public function atualizar($id, $nome, $email, $celular, $telefone, $aniversario,$face,$insta,$twitter, $nivel, $senha, $img)
    {
        $sql = "update usuario set nome = '".$nome."', email = '".$email."',celular = '".$celular."',telefone = '".$telefone."',aniversario = '".$aniversario."',facebook = '".$face."',instagram = '".$insta."',twitter = '".$twitter."',id_nivel = '".$nivel."',senha = '".$senha."',img = '".$img."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

    public function atualizarUsuario($id, $nome, $email, $celular, $telefone, $aniversario, $nivel, $img)
    {
        $sql = "update usuario set nome = '".$nome."', email = '".$email."',celular = '".$celular."',telefone = '".$telefone."',aniversario = '".$aniversario."',id_nivel = '".$nivel."',img = '".$img."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

    public function criarUsuario($nome, $email, $senha, $nivel, $img)
    {
        $sql = "INSERT INTO usuario (nome, email, senha, id_nivel, img) VALUES (:nome, :email, :senha, :id_nivel, :img)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':email' => $email, ':senha' => $senha, 'id_nivel' => $nivel, 'img' => $img);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return array('id'=> $this->db->lastInsertId(),'boleano'=> 1);
        }else{
            return false;
        }
    }

    public function inserir($nome, $email, $img, $servicos)
    {
        $sql = "INSERT INTO equipe (titulo, descricao, img) VALUES (:titulo, :descricao,  :img)";
        $query = $this->db->prepare($sql);
        $parameters = array(':titulo' => $nome, ':descricao' => $descricao, ':img' => $img);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function recuperar($id, $recuperar)
    {
        $sql = "UPDATE usuario SET recuperar = '$recuperar' WHERE usuario . id = $id";
        $query = $this->db->prepare($sql);   
        $query->execute(); 
    }

    public function verificar($codigo)
    {
        $sql = "SELECT * FROM `usuario` WHERE recuperar='$codigo'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function deletar($id_usuario)
    {
        $sql = "DELETE FROM usuario WHERE id = :id_usuario";
        $query = $this->db->prepare($sql);
        $parameters = array(':cliente_id' => $id_usuario);

        $query->execute($parameters);
    }

}
