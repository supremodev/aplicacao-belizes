

<div class="page-wrapper">
            <div class="content">
				<!-- Resumo Clínica QTD: Profissionais / Pacientes /  Compromisso hoje /  Compromisso mês -->
                <div class="row">
					
                    <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                        <div class="dash-widget">
								<span class="dash-widget-bg1"><i class="fa fa-stethoscope" aria-hidden="true"></i></span>
								<div class="dash-widget-info text-right">
									<h3><?php echo $qtdfuncionario; ?></h3>
									<span class="widget-title1">Funcionários</span>
								</div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                        <div class="dash-widget">
                            <span class="dash-widget-bg2"><i class="fa fa-user-o"></i></span>
                            <div class="dash-widget-info text-right">
                                <h3><?php echo $qtdClientes; ?></h3>
                                <span class="widget-title2">Pacientes / Clientes </span>
								
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                        <div class="dash-widget">
                            <span class="dash-widget-bg3"><i class="fa fa-user-md" aria-hidden="true"></i></span>
                            <div class="dash-widget-info text-right">
                                <h3><?php echo $qtdCompromisso; ?></h3>
                                <span class="widget-title3">Compromisso hoje</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                        <div class="dash-widget">
                            <span class="dash-widget-bg4"><i class="fa fa-heartbeat" aria-hidden="true"></i></span>
                            <div class="dash-widget-info text-right">
                                <h3><?php echo $qtdCompromissoMes; ?></h3>
                                <span class="widget-title4">Compromisso mês</span>
                            </div>
                        </div>
                    </div>
					
                </div>
				<!-- FIM Resumo Clínica QTD: Profissionais / Pacientes /  Compromisso hoje /  Compromisso mês -->
				<!-- Grafico Pacientes e Tratamentos ->
				<div class="row">
					<div class="col-12 col-md-6 col-lg-6 col-xl-6">
						<div class="card">
							<div class="card-body">
								<div class="chart-title">
									<h4 class="title is-3">Pacientes</h4>
								</div>	
								<canvas id="canvas"></canvas>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-lg-6 col-xl-6">
						<div class="card">
							<div class="card-body">
								<div class="chart-title">
									<h4 class="title is-3">Tratamentos</h4>	
                                    <span class="float-right">
										<ul class="chat-user-total">
											<li><i class="fa fa-circle current-users" aria-hidden="true"></i>2020</li>
											<li><i class="fa fa-circle old-users" aria-hidden="true"></i>2021</li>
										</ul>
									</span>
								</div>	
								<canvas id="bargraph"></canvas>
							</div>
						</div>
					</div>
					
				</div><!- FIM Grafico Pacientes e Tratamentos -->
				<!-- Próximos compromisso -->
				<div class="row">
					<div class="col-12 col-md-12 col-lg-12 col-xl-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title d-inline-block">Próximos compromisso</h4>
							</div>
							<div class="card-body p-0">
								<div class="table-responsive">
									<table class="table mb-0">
										<thead class="d-none">
											<tr>
												<th>Paciente</th>
												<th>Compromisso com</th>
												<th>Timing</th>
												<th class="text-right">Status</th>
											</tr>
										</thead>
										<tbody>

										<?php foreach ($listaCompromisso as $compromisso) {?>
												
											<tr>
												<td>
													<a class="avatar" href="#">B</a>
													<h2><a href="#"><?php echo $compromisso->cliNome; ?> <span><?php echo $compromisso->cliCel; ?></span></a></h2>
												</td>                 
												<td>
													<h5 class="time-title p-0">Compromisso com</h5>
													<p><?php echo $compromisso->funcNome; ?></p>
												</td>
												<td>
													<h5 class="time-title p-0">Data/Hora</h5>
													<p><?php echo date("d/m/Y", strtotime($compromisso->agendaData)); ?> / <?php echo $compromisso->agendaHora; ?></p>
												</td>
												<td class="text-right">
													<a href="<?php echo URL; ?>agenda/visualizar/<?php echo $compromisso->idAgenda; ?>">
														<span class="btn btn-outline-primary">Visualizar</span>
													</a>
												</td>
											</tr>

										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>                    
				</div>
				<!-- FIM Próximos compromisso -->
				<!-- Pesquisa de Qualidade  -->
				<div class="row">
					<div class="col-12 col-md-12 col-lg-12 col-xl-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title d-inline-block">Pesquisa de Qualidade</h4>
							</div>
							<div class="card-block">
								<div class="table-responsive">
									<table class="table m-b-0 new-patient-table">
										<tbody>
											<?php foreach ($mediaPesquisa as $pesquisa) {?>
											
											<tr>
												<td>
												<img width="28" height="28" class="rounded-circle" src="<?php 
                                                    if (!empty($pesquisa->cliFoto)){
														echo URL."/assets/img/cliente/".$pesquisa->cliFoto;
                                                    }elseif(!empty($pesquisa->cliWebcam)){
                                                        echo $pesquisa->cliWebcam;
                                                    }else{
                                                        echo URL."/assets/img/user.jpg";
                                                    }
                                                    ?>" alt="">
													<h2><?php echo $pesquisa->cliNome;?></h2>
												</td>
												<td><?php echo $pesquisa->cliEmail;?></td>
												<td><?php echo $pesquisa->cliCel;?></td>
												<td><a target="_black" href="https://api.whatsapp.com/send?phone=55<?php echo str_replace('(', '',str_replace(')', '',str_replace(' ', '', str_replace('-', '', $pesquisa->cliCel))));?>&text=<?php echo URL;?>pesquisa/cliente/<?php echo $pesquisa->idCliente;?>?idagenda=<?php echo $pesquisa->idAgenda;?>" class="btn btn-primary btn-primary-two float-right"><i class="fa fa-whatsapp"></i> Enviar</a></td>
											</tr>

											<?php }?>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
        		</div>
				<!-- FIM Pesquisa de Qualidade -->
    		</div>
<div class="sidebar-overlay" data-reff=""></div>  