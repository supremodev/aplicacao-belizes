<div class="page-wrapper">
    <div class="content">
        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
            <span class="texto-ajax">Erro ao agendar.</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
            <span class="texto-ajax">Agendamento atualizado com sucesso.</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h4 class="page-title">Adicionar Compromisso na Agenda</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <?php foreach ($AgendaLista as $agenda) { ?>
                    <form id="formularios">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>CPF do Paciente (Pesquisa)</label>
                                    <input type="text" name="" class="form-control floating" max="11" maxlength="11" pattern="([0-9]{11})" value="<?php echo $agenda->cliCPF; ?>" required>
                                    <input type="text" name="idCliente" class="form-control floating" value="<?php echo $agenda->idCliente; ?>" hidden>
                                    <input type="text" name="idAgenda" value="<?php echo $agenda->idAgenda; ?>" hidden>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nome do Paciente</label>
                                    <input id="nome" type="text" name="cliNome" value="<?php echo $agenda->cliNome; ?>" class="form-control floating">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Data do Agendamento</label>
                                    <div>
                                        <input type="date" class="form-control" name="agendaData" value="<?php echo $agenda->agendaData; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Hora do Agendamento</label>
                                    <div>
                                        <input type="time" class="form-control" name="agendaHora" value="<?php echo $agenda->agendaHora; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Especialista</label>
                                    <select class="select" name="idFunc">
                                        <option>Selecionar</option>
                                        <?php foreach ($listaFuncionario as $func) { ?>
                                            <option value="<?php echo $func->idFunc; ?>" <?php if ($func->idFunc == $agenda->idFunc) {
                                                                                                echo "selected";
                                                                                            } ?>><?php echo $func->funcNome; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Forma de pagamento</label>
                                    <div>
                                        <select name="agendaFormaPG" class="form-control">
                                            <option value="Cartão" <?php if ($agenda->agendaFormaPG == "Cartão") {
                                                                        echo "checked";
                                                                    } ?>>Cartão</option>
                                            <option value="Dinheiro" <?php if ($agenda->agendaFormaPG == "Dinheiro") {
                                                                            echo "checked";
                                                                        } ?>>Dinheiro</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status da Agenda</label>
                                    <div>
                                        <select name="agendaStatus" class="form-control">
                                            <option value="1" <?php if ($agenda->agendaStatus == "1") {
                                                                    echo "checked";
                                                                } ?>>Confirmado</option>
                                            <option value="2" <?php if ($agenda->agendaStatus == "2") {
                                                                    echo "checked";
                                                                } ?>>Em Andamento</option>

                                            <option value="3" <?php if ($agenda->agendaStatus == "3") {
                                                                    echo "checked";
                                                                } ?>>Finalizado</option>
                                            <option value="4" <?php if ($agenda->agendaStatus == "4") {
                                                                    echo "checked";
                                                                } ?>>Cancelado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Mensagem:</label>
                                    <textarea cols="30" rows="4" class="form-control" name="agendaMensagem"><?php echo $agenda->agendaMensagem; ?></textarea>
                                </div>
                            </div>
                        </div>


                    </form>
                <?php } ?>
                <div class="row agenda-servico">
                    </form id="">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Serviços </label>

                            <span class="select2 select2-container select2-container--default select2-container--above select2-container--focus" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1">
                                        <ul class="select2-selection__rendered">
                                            <?php foreach ($listaServicos as $servico) { ?>
                                                <li class="select2-selection__choice" title="<?php echo $servico->servicoNome; ?>"><?php echo $servico->servicoNome; ?></li>
                                            <?php } ?>
                                        </ul>
                                    </span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                            <input id="idAgendamento" type="text" name="idAgendamento" hidden>
                        </div>
                    </div>
                    </form>

                </div>

                <div class="row " >
                    </form id="">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Pacote </label>
                            <span class="select2 select2-container select2-container--default select2-container--above select2-container--focus" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1">
                                        <ul class="select2-selection__rendered">
                                            <?php foreach ($listaPacote as $pacote) { ?>
                                                <li class="select2-selection__choice" title="<?php echo $pacote->pacoteNome; ?>"><?php echo $pacote->pacoteNome; ?></li>
                                            <?php } ?>
                                        </ul>
                                    </span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                            <input id="idAgendamento" type="text" name="idAgendamento" hidden>
                        </div>
                    </div>
                    </form>
                </div>

                <div class="m-t-20 text-center">
                    <button destino="agenda/atualizar/60" class="atualizar-ajax btn btn-primary submit-btn">Atualizar Compromisso</button>
                </div>
                <div class="add-servico-agenda m-t-20 text-center" style="display:none">
                    <button destino="agenda/atualizarServico" class="add-servico-agenda-ajax btn btn-primary submit-btn">Adicionar Serviço na Agenda</button>
                </div>
            </div>
        </div>
    </div>




    <div class="sidebar-overlay" data-reff=""></div>