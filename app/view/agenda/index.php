<div class="page-wrapper">
	<div class="content">
		<div class="row">
			<div class="col-sm-4 col-3">
				<h4 class="page-title">Agenda</h4>
				<div class="dataTables_length page-title" id="DataTables_Table_0_length"><label>Realizar filtro por:
					<select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" id="filtroAgenda" class="form-control form-control-sm">
						<option value=""></option>
						<option value="tudo">Tudo</option>
						<option value="dia">Dia</option>
						<option value="semana">Semana</option>
						<option value="mes">Mês</option>
					</select></label>
				</div>
				
			</div>
			<div class="col-sm-8 col-9 text-right m-b-20">
				<a href="<?php

use Http\Message\Formatter;

echo URL; ?>agenda/novo" class="btn btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i> Registrar Agendamento</a>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-border table-striped custom-table datatable m-b-0">
						<thead>
							<tr>
								<th>Nome</th>
								<th>Celular</th>
								<th>Especialista</th>
								<th>Data / Hora</th>
								<th>Status</th>
								<th>Pagamento</th>
								<th class="text-right">Editar</th>
							</tr>
						</thead>
						<tbody id="FiltroLista">
							<?php foreach ($agendaLista as $agenda) { ?>
								<tr>
									<td>
										<a href="<?php echo URL; ?>cliente/perfil/<?php echo $agenda->idCliente; ?>">
											<?php if (isset($agenda->cliNome)) echo htmlspecialchars($agenda->cliNome, ENT_QUOTES, 'UTF-8'); ?>
										</a>

									</td>

									<td><?php if (isset($agenda->cliCel)) echo htmlspecialchars($agenda->cliCel, ENT_QUOTES, 'UTF-8'); ?></td>
									<td><?php if (isset($agenda->funcNome)) echo htmlspecialchars($agenda->funcNome, ENT_QUOTES, 'UTF-8'); ?></td>
									<td>
										<?php if (isset($agenda->agendaData)) echo htmlspecialchars(date("d/m/Y", strtotime($agenda->agendaData)), ENT_QUOTES, 'UTF-8'); ?>
										-
										<?php if (isset($agenda->agendaHora)) echo htmlspecialchars($agenda->agendaHora, ENT_QUOTES, 'UTF-8'); ?>
									</td>

									<td>
										<?php
										if ($agenda->agendaStatus == 1) {
											echo "<span class='custom-badge status-green'>Confirmado</span>";
										} else if ($agenda->agendaStatus == 2) {
											echo "<span class='custom-badge status-orange'>Em Atendimento</span>";
										} else if ($agenda->agendaStatus == 3) {
											echo "<span class='custom-badge status-grey'>Finalizado</span>";
										} else {
											echo "<span class='custom-badge status-red'>Desmarcado</span>";
										}
										?>
									</td>
									<td class="text-right">
										<div>
								
										<?php 
										if (!empty($Agendamento->listaServicoAgendamento($agenda->idAgenda))) {

											$valorServico = $Agendamento->listaServicoAgendamento($agenda->idAgenda);
											$total = 0;
											foreach ($valorServico as $value) {
												$total = $total + $value->servicoValor;
											}

										} else {
											$valorServico = $Agendamento->listaPacoteAgendamento($agenda->idAgenda);

											$total = 0;
											foreach ($valorServico as $value) {
												$total = $total + $value->pacotevalor;
											}

										}

											
										?>
										<?php if($agenda->agendaStatus == 3 && $agenda->agendaPG == 0) {?>
											<a class="carregar-form-comissao dropdown-item" destino="caixa/pagarComissao" idagenda="<?php echo $agenda->idAgenda;?>" funcnome="<?php echo $agenda->funcNome;?>" comissao="<?php echo number_format($total * $agenda->funcComissao,2);?>" href="#" data-toggle="modal" data-target=".bd-example-modal-lg">
												<i class="fa fa-money m-r-5"></i>Pagar
											</a>
										<?php }; ?>
										</div>
									</td>
									<td class="text-right">
										<div>
											<a class="dropdown-item" href="<?php echo URL; ?>agenda/editar/<?php echo $agenda->idAgenda; ?>">
												<i class="fa fa-pencil m-r-5"></i>Editar
											</a>
										</div>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>



	</div>


	<div class="sidebar-overlay" data-reff=""></div>

	<!-- Modal -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="max-width: 1280px;">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Pagamento</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body modal-body-formgeral">
        ...
        </div>
    </div>
  </div>
</div>