<select name="idTratamento" class="form-control" id="seleTratameno" >
    <option value="0">Selecione o Tratamento</option>

    <?php foreach ($listafacial as $facial) { ?>
        <option value="<?php echo $facial->idTratamento; ?>" sessoesfeitas="<?php echo $facial->tratamentoQtd; ?>" sessao="<?php echo $facial->fichaFacilQtdSessoes; ?>">Facial - Início <?php echo $facial->fichaFacilData; ?></option>
    <?php } ?>

    <?php foreach ($listacorporal as $corporal) { ?>
        <option value="<?php echo $corporal->idTratamento; ?>" sessoesfeitas="<?php echo $corporal->tratamentoQtd; ?>" sessao="<?php echo $corporal->fichaCorporalQtdSessoes; ?>">Corporal - Início <?php echo $corporal->fichaCorporalData; ?></option>
    <?php } ?>

    <?php foreach ($listacriolipolise as $crioli) { ?>
        <option value="<?php echo $crioli->idTratamento; ?>" sessoesfeitas="<?php echo $crioli->tratamentoQtd; ?>" sessao="<?php echo $crioli->fichaAnamneseQtdSessoes; ?>">Criolipólise - Início <?php echo $crioli->fichaAnamneseData; ?></option>
    <?php } ?>

</select>