<?php foreach ($agendaLista as $agenda) { ?>
    <tr>
        <td>
            <a href="<?php echo URL; ?>cliente/perfil/<?php echo $agenda->idCliente; ?>">
                <?php if (isset($agenda->cliNome)) echo htmlspecialchars($agenda->cliNome, ENT_QUOTES, 'UTF-8'); ?>
            </a>

        </td>

        <td><?php if (isset($agenda->cliCel)) echo htmlspecialchars($agenda->cliCel, ENT_QUOTES, 'UTF-8'); ?></td>
        <td><?php if (isset($agenda->funcNome)) echo htmlspecialchars($agenda->funcNome, ENT_QUOTES, 'UTF-8'); ?></td>
        <td>
            <?php if (isset($agenda->agendaData)) echo htmlspecialchars(date("d/m/Y", strtotime($agenda->agendaData)), ENT_QUOTES, 'UTF-8'); ?>
            -
            <?php if (isset($agenda->agendaHora)) echo htmlspecialchars($agenda->agendaHora, ENT_QUOTES, 'UTF-8'); ?>
        </td>

        <td>
            <?php
            if ($agenda->agendaStatus == 1) {
                echo "<span class='custom-badge status-green'>Confirmado</span>";
            } else if ($agenda->agendaStatus == 2) {
                echo "<span class='custom-badge status-orange'>Em Atendimento</span>";
            } else if ($agenda->agendaStatus == 3) {
                echo "<span class='custom-badge status-grey'>Finalizado</span>";
            } else {
                echo "<span class='custom-badge status-red'>Desmarcado</span>";
            }
            ?>
        </td>
        <td class="text-right">
            <div>
                <a class="dropdown-item" href="<?php echo URL; ?>agenda/editar/<?php echo $agenda->idAgenda; ?>">
                    <i class="fa fa-pencil m-r-5"></i>Editar
                </a>
            </div>
        </td>
    </tr>
<?php } ?>