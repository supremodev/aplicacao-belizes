<div class="page-wrapper">
            <div class="content">
            <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
				<span class="texto-ajax">Erro ao agendar.</span>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
                <span class="texto-ajax">Agendamento feito com sucesso.</span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
                
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="page-title">Adicionar Compromisso na Agenda</h4>
                            </div>
                        </div>
                        <form id="formularios">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
										<label>CPF do Paciente (Pesquisa)</label>
                                        <input id="cpff" type="text" name="" class="ajax-consulta-cpf form-control floating" max="11" maxlength="11" pattern="([0-9]{11})" required>
                                        <input id="idcliente" type="text" name="idCliente" class="ajax-consulta-cpf form-control floating" hidden>
									</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group">
                                        <label>Nome do Paciente</label>
                                        <input id="nome" type="text" name="cliNome" class="form-control floating">
									</div>
                                </div>
                            </div>
                            <div class="row">
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label>Data do Agendamento</label>
                                        <div>
                                            <input type="date" class="filtro-data-agenda form-control" name="agendaData">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Hora do Agendamento</label>
                                        <div>
                                            <input type="time" class="form-control" name="agendaHora">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Especialista</label>
                                        <select class="select" name="idFunc" id="Selecione-profissional">
											<option>Selecionar</option>
											<?php foreach ($listaFuncionario as $func) { ?>
                                                <?php if ($func->funcStatus == 1) { ?>
											        <option value="<?php echo $func->idFunc; ?>"><?php echo $func->funcNome; ?></option>
                                                <?php } ?>
											<?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Forma de pagamento</label>
                                        <div>
                                            <select name="agendaFormaPG" class="form-control">
                                                <option value="Cartão">Cartão</option>
                                                <option value="Dinheiro">Dinheiro</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6">
                                <div class="form-group">
                                    <label>Status da Agenda</label>
                                    <div>
                                        <select name="agendaStatus" class="form-control">
                                            <option value="1">Confirmado</option>
                                            <option value="2">Em Andamento</option>
											<option value="3">Finalizado</option>
											<option value="4">Cancelado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
								<div class="col-md-12">
                                    <div class="form-group">
                                        <label>Mensagem:</label>
                                        <textarea cols="30" rows="4" class="form-control" name="agendaMensagem"></textarea>
                                    </div>
                                </div>
                            </div>
                            </form>
                                <div class="agenda-servico" style="display:none">
                                    <div class="row ficha">
                                        <label class="col-md-3 col-form-label form-group">Escolha o serviço:</label>
                                        <div class="col-md-9">
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for=""> 
                                                    <input class="form-check-input click-servico-pacote" type="radio" name="servicos" id="" value="servicos">
                                                Serviços </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="">
                                                    <input class="form-check-input click-servico-pacote" type="radio" name="servicos" id="" value="pacote">
                                                Pacote </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="">
                                                    <input class="form-check-input click-servico-pacote" type="radio" name="servicos" id="" value="tratamento">
                                                Tratamento </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row " id="servicos" style="display:none">
                                        </form id="">     
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Serviços </label>
                                                    <select class="pacote select" multiple name="idServico">
                                                        <?php foreach ($listaServicos as $servico) { ?>
                                                        <option name="itemDaAgenda[]" value="<?php echo $servico->idServico; ?>"><?php echo $servico->servicoNome; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <input id="idAgendamento" type="text" name="idAgendamento" hidden>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row " id="pacote" style="display:none">
                                        </form id="">     
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Pacote </label>
                                                    <select class="select" multiple name="idPacote">
                                                        <?php foreach ($listaPacote as $pacote) { ?>
                                                        <option name="itemDoPacote[]" value="<?php echo $pacote->idPacoteServico; ?>"><?php echo $pacote->pacoteNome; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <input id="idAgendamento" type="text" name="idAgendamento" hidden>
                                                </div>
                                            </div>
                                        </form>
                                    </div> 
                                    <div class="row " id="tratamento" style="display:none">
                                    <div id="lista-tratamento"></div>
                                    <!--<span class="select2 select2-container select2-container--default select2-container--above select2-container--focus" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1">
                                        <ul class="select2-selection__rendered">
                                            
                                                <li class="select2-selection__choice" title=""></li>
                                            
                                        </ul>
                                    </span></span><span class="dropdown-wrapper" aria-hidden="true"></span>
                                    </span>-->
                                    </div>
                                </div>
                        
                        <div class="m-t-20 text-center">
                            <button destino="agenda/inserir" class="agendamento btn btn-primary submit-btn">Criar Compromisso</button>
                        </div>
                        <div class="add-servico-agenda m-t-20 text-center" style="display:none">
                            <button destino="agenda/inserirServico" class="add-servico-agenda-ajax btn btn-primary submit-btn">Adicionar Serviço na Agenda</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="page-title">Compromisso dessa data</h4>
                            </div>
                        </div>
                        <div class="row" id="visualizarData">
                            <div class="table-responsive">
                                <table class="table table-border table-striped custom-table datatable m-b-0">
                                    <thead>
                                        <tr>
                                        <th>Data</th>
                                        <th>Hora</th>
                                        <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <tr>
                                            <td>
Sem nenhum resultado
                                            </td> 
                                            <td>
                                            </td>
                                            <td>
                                            </td>                                     
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>

	
	
	
<div class="sidebar-overlay" data-reff=""></div>   
