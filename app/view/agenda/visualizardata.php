<div class="table-responsive">
					<table class="table table-border table-striped custom-table datatable m-b-0">
						<thead>
							<tr>
								<th>Data</th>
                                <th>Hora</th>
                                <th>Status</th>
							</tr>
						</thead>
						<tbody id="FiltroLista">
							<?php foreach ($AgendaLista as $agenda) { ?>
								<tr>
	
									<td>
										<?php if (isset($agenda->agendaData)) echo htmlspecialchars(date("d/m/Y", strtotime($agenda->agendaData)), ENT_QUOTES, 'UTF-8'); ?>
									</td>

                                    <td>
                                        <?php if (isset($agenda->agendaHora)) echo htmlspecialchars($agenda->agendaHora, ENT_QUOTES, 'UTF-8'); ?>
									</td>

									<td>
										<?php
										if ($agenda->agendaStatus == 1) {
											echo "<span class='custom-badge status-green'>Confirmado</span>";
										} else if ($agenda->agendaStatus == 2) {
											echo "<span class='custom-badge status-orange'>Em Atendimento</span>";
										} else if ($agenda->agendaStatus == 3) {
											echo "<span class='custom-badge status-grey'>Finalizado</span>";
										} else {
											echo "<span class='custom-badge status-red'>Desmarcado</span>";
										}
										?>
									</td>
									
								</tr>
							<?php } ?>
						</tbody>
					</table>