<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-md-8 offset-md-1">
                        <h4 class="page-title">Editar Registro no Caixa</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
						<?php foreach ($caixaLista as $linha) { ?>
                        <form id="formularios">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Categoria (Informação do registro)</label>
                                        <input name="caixaCategoria" class="form-control" type="text" value="<?php echo $linha->caixaCategoria;?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Tipo</label>
                                        <select class="select form-control" name="caixaTipo">
                                            <option value="Entrada" <?php if($linha->caixaTipo == "Entrada") {echo "selected";};?>>Entrada</option>
                                            <option value="Saída" <?php if($linha->caixaTipo == "Saída") {echo "selected";};?>>Saída</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Data de Vencimento</label>
                                        <div>
                                            <input name="caixaVencimento" class="form-control" type="date" value="<?php echo $linha->caixaVencimento;?>">
											
                                        </div>
                                    </div>
                                </div>
								<div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Data de Pagamento</label>
                                        <div>
                                            <input name="caixaDataPagamento" class="form-control" type="date" value="<?php echo $linha->caixaDataPagamento; ?>">
											
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Valor</label>
                                        <input name="caixaValor" placeholder="R$...." class="form-control" type="text" value="<?php echo $linha->caixaValor; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
										<label class="focus-label">Observação sobre o registro</label>
										<textarea name="caixaObs" class="form-control floating" rows="3" cols="30"><?php echo $linha->caixaObs; ?></textarea>
									</div>	
                                </div>
							</div>
                            <div class="m-t-20 text-center">
								<button destino="caixa/atualizar/<?php echo $linha->idCaixa; ?>" class="atualizar-ajax btn btn-primary submit-btn" disabled>Atualizar Registro</button>
                            </div>
                        </form>
						<?php } ?>
                    </div>
                </div>
            </div>

<div class="sidebar-overlay" data-reff=""></div>