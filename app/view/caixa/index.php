<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-5 col-4">
                        <h4 class="page-title">Fluxo de Caixa</h4>
                    </div>
                    <div class="col-sm-7 col-8 text-right m-b-30">
                        <a href="<?php echo URL; ?>caixa/novo" class="btn btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i> Incluir Lançamento</a>
					</div>
					
                </div>
                <div class="row filter-row">                    
					<div class="col-sm-3 col-md-3">
						<div class="form-group  saida-icon">
							<label class="focus-label">Total Entrada:</label>
							<input type="text" class="form-control floating" value="R$ <?php echo $caixaEntrada; ?>" readonly>
						</div>
					</div>
					<div class="col-sm-3 col-md-3">
						<div class="form-group  saida-icon">
							<label class="focus-label">Total Saída:</label>
							<input type="text" class="form-control floating" value="R$ <?php echo $caixaSaida; ?>" readonly>
						</div>
					</div>
                    <div class="col-sm-6 col-md-6">
                        <div class="form-group  saida-icon">
							<label class="focus-label">Resumo Caixa:</label>
							<input type="text" class="form-control floating" value="R$ <?php echo $caixaEntrada - $caixaSaida; ?>" readonly>
						</div>
					</div>
					<div class="col-sm-3 col-3  m-b-30">
						<div class="dataTables_length" id="DataTables_Table_0_length"><label>Realizar filtro por:
							<select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" id="filtroCaixa" class="form-control form-control-sm">
								<option value=""></option>
								<option value="tudo">Tudo</option>
								<option value="dia">Dia</option>
								<option value="semana">Semana</option>
								<option value="mes">Mês</option>
								<option value="semeste">Semestre</option>
							</select></label>
						</div>
					</div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0"> 
								<thead>
									<tr>
										<th>Id Fatura</th>
                                        <th>Categoria</th>
                                        <th>Data de Vencimento</th>
                                        <th>Tipo</th>
                                        <th>Valor (R$)</th>
                                        <th>Status</th>
                                        <th>Data PG</th>
										<th class="text-right">Ação</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($caixaLista as $lista) { ?>
									<tr>
										<td><?php if (isset($lista->idCaixa)) echo htmlspecialchars($lista->idCaixa, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($lista->caixaCategoria)) echo htmlspecialchars($lista->caixaCategoria, ENT_QUOTES, 'UTF-8'); ?></td>
										<td>
                                            <?php if (isset($lista->caixaVencimento)) echo htmlspecialchars(date("d/m/Y", strtotime($lista->caixaVencimento)), ENT_QUOTES, 'UTF-8'); ?>
                                        </td>
										<td><?php if (isset($lista->caixaTipo)) echo htmlspecialchars($lista->caixaTipo, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($lista->caixaValor)) echo htmlspecialchars($lista->caixaValor, ENT_QUOTES, 'UTF-8'); ?></td>
										<td>											
											<?php 
												if (isset($lista->caixaDataPagamento)){
													if($lista->caixaDataPagamento == ""){
														echo "<span class='custom-badge status-red'> Pendente </span>";
													}else{
														echo "<span class='custom-badge status-green'> Pago </span>";														
													}														
												}													
											?>										
										</td>
										<td>
											<?php 
												if (isset($lista->caixaStatus)){
													if($lista->caixaStatus == "0"){
														echo " - ";
													}else{
														if (isset($lista->caixaDataPagamento)) echo htmlspecialchars(date("d/m/Y", strtotime($lista->caixaDataPagamento)), ENT_QUOTES, 'UTF-8');				
													}														
												}													
											?>								
										</td>
										<td class="text-right">											
											<div>
												<a class="dropdown-item" href="<?php echo URL; ?>caixa/editar/<?php echo $lista->idCaixa; ?>">
													<i class="fa fa-pencil m-r-5"></i>Editar
												</a>
												
											</div>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>

	
	
<div class="sidebar-overlay" data-reff=""></div>