<div class="page-wrapper">
            <div class="content">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
							Erro ao cadsatrar a Registro.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
							Registro cadastrada com sucesso.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
                <div class="row">
                    <div class="col-md-8 offset-md-1">
                        <h4 class="page-title">Novo Registro no Caixa</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <form id="formularios">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Categoria (Informação do registro)</label>
                                        <input class="form-control" type="text" name="caixaCategoria">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Tipo</label>
                                        <select class="select" name="caixaTipo">
                                            <option>Selecione o tipo</option>
                                            <option value="Entrada">Entrada</option>
                                            <option value="Saída">Saída</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Data de Vencimento</label>
                                        <div>
                                            <input class="form-control" type="date" name="caixaVencimento">
                                        </div>
                                    </div>
                                </div>
								<div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Data de Pagamento</label>
                                        <div>
                                            <input class="form-control" type="date" name="caixaDataPagamento">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Valor</label>
                                        <input placeholder="R$..." class="form-control" type="text" name="caixaValor">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
										<label class="focus-label">Observação sobre o registro</label>
										<textarea class="form-control floating" rows="3" cols="30" name="caixaObs"></textarea>
									</div>	
                                </div>
							</div>
                            <div class="m-t-20 text-center">
								<button destino="caixa/inserir" class="novo-ajax btn btn-primary submit-btn" type="button">Cadastrar Registro</button>	
                            </div>
                        </form>
						
                    </div>
                </div>
            </div>

	
	
<div class="sidebar-overlay" data-reff=""></div>