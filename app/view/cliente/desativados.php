<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">Clientes Desativados</h4>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0">
								<thead>
									<tr>
										<th>Nome</th>
										<th>Email</th>
										<th>Celular</th>
										<th>Telefone</th>
										<th>Data Nascimento</th>
										<th>Satisfação</th>
										<th class="text-right">Opções</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($clienteLista as $cliente) { ?>
									<tr id="linha<?php echo $cliente->idCliente; ?>">
										<td>
											<a href="<?php echo URL; ?>cliente/perfil/<?php echo $cliente->idCliente; ?>">
												<!--<img width="28" height="28" src="<?php							   
											if ($cliente->cliFoto == "") {
												if($cliente->cliWebcam == ""){
													echo URL."assets/img/cliente/user.jpg";
												}else{
													echo $cliente->cliWebcam;
												}
											}else{
												echo URL."assets/img/cliente/".$cliente->cliFoto;
										   	} 
											?>" title="Perfil <?php if (isset($cliente->cliNome)) echo htmlspecialchars($cliente->cliNome, ENT_QUOTES, 'UTF-8'); ?>" class="rounded-circle m-r-5">-->
											<?php if (isset($cliente->cliNome)) echo htmlspecialchars($cliente->cliNome, ENT_QUOTES, 'UTF-8'); ?>
											</a>
										</td>
										<td><?php if (isset($cliente->cliEmail)) echo htmlspecialchars($cliente->cliEmail, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($cliente->cliCel)) echo htmlspecialchars($cliente->cliCel, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($cliente->cliCPF)) echo htmlspecialchars($cliente->cliCPF, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($cliente->cliNasci)) echo htmlspecialchars($cliente->cliNasci, ENT_QUOTES, 'UTF-8'); ?></td>
										<td>Pesquisa de Satisfação</td>
										<td class="text-right"><a class="dropdown-item desativar-ajax" destino="cliente/ativarDesativar/1" idobjeto="<?php echo $cliente->idCliente; ?>" href="#"><i class="fa fa-check m-r-5"></i>Ativar</a></td>
										
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
            

<div class="sidebar-overlay" data-reff=""></div>