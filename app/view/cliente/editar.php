<div class="page-wrapper">
            <div class="content">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:<?php if(isset($_SESSION['alerta']) && $_SESSION['alerta']=="erro"){echo 'block';$_SESSION['alerta']="alerta";}else{echo 'none';}; ?>">
					Erro ao Atualizar Cliente. 
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:<?php if(isset($_SESSION['alerta']) && $_SESSION['alerta']=="sucesso"){echo 'block';$_SESSION['alerta']="alerta";}else{echo 'none';}; ?>">
					Cliente Atualizado com Sucesso.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Editar Cliente</h4>
                    </div>
                </div>
				<?php foreach ($clienteLista as $linha) { ?>
                <form id="formularios">
                    <div class="card-box">
                        <h3 class="card-title">Informação básica</h3>
                        <div class="row">
                            <div class="col-md-12">
								<div class="profile-img-wrap">
									<div class="profile" style="background-image: url('<?php							   
											if ($linha->cliFoto == "") {
												if($linha->cliWebcam == ""){
													echo URL."assets/img/cliente/user.jpg";
                                                    $imgFoto = "";
                                                    $imgWebcam = "";
												}else{
                                                    echo $linha->cliWebcam;
                                                    $imgWebcam = $linha->cliWebcam;
                                                    $imgFoto = "";
												}
											}else{
                                                echo URL."assets/img/cliente/".$linha->cliFoto;
                                                $imgFoto = URL."assets/img/cliente/".$linha->cliFoto;
                                                $imgWebcam = "";
										   	} 
											?>');">
										<label class="edit">
											<span><i class="mdi mdi-upload"></i></span>
											<input type="file" size="32" name="cliFoto" id="inputImagem" value="<?php echo $imgFoto;?>">
											<input type="text" name="cliWebcam" id="inputWebcam" value="<?php echo $imgWebcam;?>" hidden>
										</label>
									</div>
									<button id="editarImagem" type="button" class="btn btn-primary btn-sm">
										<i class="fa fa-file-image-o"></i>
									</button>
									<button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
										<i class="fa fa-camera"></i>
									</button>
								</div>
                                
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Nome:*</label>
                                                <input type="text" name="cliNome" class="form-control floating" value="<?php echo $linha->cliNome;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">CPF:*</label>
                                                <input id="cpf" type="text" name="cliCPF" class="form-control floating" value="<?php echo $linha->cliCPF;?>" required>
                                            </div>
                                        </div>
										<div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Email:*</label>
                                                <input type="text" name="cliEmail" class="form-control floating" value="<?php echo $linha->cliEmail;?>" required>
                                            </div>
                                        </div>
										<div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Senha:*</label>
                                                <input type="password" name="cliSenha" class="form-control floating" value="">
                                            </div>

                                        </div>
                                        <div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Data de Nascimento:</label>
                                                <div class="cal-icon">
                                                    <input class="form-control floating" type="text" name="cliNasci" value="<?php echo $linha->cliNasci;?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Sexo</label>
                                                <select class="select form-control floating" name="cliSexo">
                                                    <option></option>
                                                    <option value="Masculino" <?php if($linha->cliSexo == "Masculino") {echo "selected";};?>>Masculino</option>
                                                    <option value="Feminino" <?php if($linha->cliSexo == "Feminino") {echo "selected";};?>>Feminino</option>
                                                </select>
                                            </div>
                                        </div>
										<div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">RG:</label>
                                                <input type="text" name="cliRG" class="form-control floating" value="<?php echo $linha->cliRG;?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Estado Civil:</label>
                                                <input type="text" name="cliEstadoCivil" class="form-control floating" value="<?php echo $linha->cliEstadoCivil;?>">
                                            </div>
                                        </div>
										<div class="col-sm-5 col-md-4 col-lg-4">
                                           <div class="form-group form-focus">
                                                <label class="focus-label">Telefone:</label>
                                                <input type="text" name="cliFone" class="form-control floating" value="<?php echo $linha->cliFone;?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Celular:*</label>
                                                <input id="cel" type="text" name="cliCel" class="form-control floating" value="<?php echo $linha->cliCel;?>" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
                        </div>
                    </div>
                    <div class="card-box">
                        <h3 class="card-title">Informações de Contato</h3>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Endereço:</label>
                                    <input type="text" name="cliEnd" class="form-control floating" value="<?php echo $linha->cliEnd;?>">
                                </div>
                            </div>
							<div class="col-md-2">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Número:</label>
                                    <input type="text" name="cliEndNumero" class="form-control floating" value="<?php echo $linha->cliEndNumero;?>">
                                </div>
                            </div>
							<div class="col-md-2">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Complemento:</label>
                                    <input type="text" name="cliEndComp" class="form-control floating" value="<?php echo $linha->cliEndComp;?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Cidade:</label>
                                    <input type="text" name="cliCidade" class="form-control floating" value="<?php echo $linha->cliCidade;?>">
                                </div>
                            </div>
							<div class="col-md-3">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Estado:</label>
                                    <input type="text" name="cliEstado" class="form-control floating" value="<?php echo $linha->cliEstado;?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-focus">
                                    <label class="focus-label">CEP:</label>
                                    <input type="text" name="cliCEP" class="form-control floating" value="<?php echo $linha->cliCEP;?>">
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="card-box">
						<h3 class="card-title">Objetivo do Tratamento</h3>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<textarea class="form-control floating cliDepoimento" rows="5" cols="30" name="cliTratamento"><?php echo $linha->cliTratamento;?></textarea>
								</div>								
							</div>                       
						</div>
					</div>
					<div class="card-box">
                        <h3 class="card-title">Informações Institucionais</h3>
                        <div class="row">
							<div class="col-md-6">                                
								<div class="form-group">
									<div class="profile-img-wrap assinatura">
										<img id="Assinatura-img" class="inline-block" src="<?php 
														 if($linha->cliAssinatura == null){
															echo URL."assets/img/cliente/assinatura.png";
														 }else{
															 echo $linha->cliAssinatura;
														 } ?>" alt="user">
										<input id="Assinatura-input" type="text" value="<?php echo $linha->cliAssinatura;?>" name="cliAssinatura" hidden> 
									</div>
								</div>								
                            </div>
							<div class="col-md-6">
                                <div class="form-group">
                                    <label class="focus-label">Como conheceu a Clínica Belizes Time</label>
									<textarea class="form-control floating" rows="3" cols="30" name="cliComoConheceu"><?php echo $linha->cliComoConheceu;?></textarea>
                                </div>								
                            </div>                            
                        </div>
                       
                    </div>
                    <?php if(isset($_SESSION['idUsuario'])){?>
                    <div class="card-box">
						<h3 class="card-title">Assinatura Digital</h3>
						<div class="row">
							<div class="col">
								<div class="tabs">
									<div class="tab">
										<input type="checkbox" id="chck1">
										<label class="tab-label" for="chck1">Abrir assinatura</label>
										<div class="tab-content">
											<div class="assinatura-corpo" onselectstart="return false">
												<div id="signature-pad" class="signature-pad">
													<div class="signature-pad--body">
														<canvas></canvas>
													</div>
													<div class="signature-pad--footer">
														<div class="description">Assine Acima</div>
														<div class="signature-pad--actions">
															<div>
																<button type="button" class="button clear" data-action="clear">Limpar</button>
																<button type="button" class="button" data-action="undo">Desfazer</button>
															</div>
															<div>
																<button type="button" class="button save salvar-assinatura" data-action="save-png">SALVAR</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <?php }?>
                </form>
                <div class="text-center m-t-20">
                    <button destino="cliente/atualizar/<?php echo $linha->idCliente;?>" class="atualizar-ajax btn btn-primary submit-btn" disabled>Atualizar Cliente</button>
                </div>
				<?php } ?>

            </div>

                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:<?php if(isset($_SESSION['alerta']) && $_SESSION['alerta']=="erro"){echo 'block';$_SESSION['alerta']="alerta";}else{echo 'none';}; ?>">
					Erro ao Atualizar Cliente. 
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:<?php if(isset($_SESSION['alerta']) && $_SESSION['alerta']=="sucesso"){echo 'block';$_SESSION['alerta']="alerta";}else{echo 'none';}; ?>">
					Cliente Atualizado com Sucesso.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
<!-- Modal Web Cam -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Foto Web Cam</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="camera">
					<video id="video" class="videoWebcam">Vídeo indisponível.</video>
				</div>
				<canvas id="canvas" style="display:none"></canvas>
				<div class="output"></div>
			</div>
			<div class="modal-footer">
				<button type="button" id="startbutton" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-camera"></i></button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Web Cam -->
<div class="sidebar-overlay" data-reff=""></div>