
								<?php if(isset($fichaCovidLista)){foreach ($fichaCovidLista as $linha) {  ?>
								<form id="formCovid">
									<div class="card-box">
										<h3 class="card-title">FICHA ANAMNESE COVID-19</h3>											
										<div class="row ficha">
											<label class="col-md-2 col-form-label form-group">Já Fez Teste Covid-19</label>
											<div class="col-md-2">
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19JaFezTesteCovid19" id="" value="1" <?php if($linha->fichaAnamneseCovid19JaFezTesteCovid19){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?> >
													<label class="form-check-label" for="fichaAnamneseCovid19JaFezTesteCovid19">
													Sim
													</label>
												</div>
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19JaFezTesteCovid19" id="" value="0" <?php if(!$linha->fichaAnamneseCovid19JaFezTesteCovid19){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?> >
													<label class="form-check-label" for="fichaAnamneseCovid19JaFezTesteCovid19">
													Não
													</label>
												</div>
											</div>
											<div class="col-md-2">
												<div class="form-group ">
													<label class="focus-label">Qual?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19QualTesteCovid19" value="<?php echo $linha->fichaAnamneseCovid19QualTesteCovid19;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<div class="col-md-2">
												<div class="form-group ">
													<label class="focus-label">Resultado?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19Resultado" value="<?php echo $linha->fichaAnamneseCovid19Resultado;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group ">
													<label class="focus-label">Quanto Tempo Faz Que Fez Teste?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19QuantoTempoFazQueFezTeste" value="<?php echo $linha->fichaAnamneseCovid19QuantoTempoFazQueFezTeste;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group ">
													<label class="focus-label">Observações:</label>
													<textarea class="form-control floating" rows="3" cols="30" name="fichaAnamneseCovid19ObsTesteCovid19" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>><?php echo $linha->fichaAnamneseCovid19ObsTesteCovid19;?></textarea>
												</div>								
											</div>                         


											<label class="col-md-4 col-form-label form-group">Teve Contato Com Pessoas Teste Covid-19 Positivo?</label>
											<div class="col-md-2">
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo" id="" value="1" <?php if($linha->fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo){echo "checked";};if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo">
													Sim
													</label>
												</div>
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo" id="" value="0" <?php if(!$linha->fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo){echo "checked";};if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo">
													Não
													</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group ">
													<label class="focus-label">Quanto Tempo Faz Que Fez Teste?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19QuantoTempoFazContatoComPessoas" value="<?php echo $linha->fichaAnamneseCovid19QuantoTempoFazContatoComPessoas;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
										</div>
									</div>
									<div class="card-box">
										<h3 class="card-title">Marque a caixa se Estiver Sentindo Qualquer Desses Sintomas</h3>
										<div class="row ficha">
											<!--<div class="col-md-1">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19Febre" value="0" checked hidden>
													<input type="checkbox" name="fichaAnamneseCovid19Febre" value="1" <?php if($linha->fichaAnamneseCovid19Febre){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Febre:	
												</label>
											</div>-->
											<div class="col-md-2">
												<div class="form-group ">
													<label class="focus-label">Febre? QuantoTempoFaz?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19FebreQuantoTempoFaz" value="<?php echo $linha->fichaAnamneseCovid19FebreQuantoTempoFaz;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<!--<div class="col-md-1">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19Tosse" value="0" checked hidden>
													<input type="checkbox" name="fichaAnamneseCovid19Tosse" value="1" <?php if($linha->fichaAnamneseCovid19TosseQuantoTempoFaz){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Tosse:
												</label>
											</div>-->
											<div class="col-md-2">
												<div class="form-group ">
													<label class="focus-label">Tosse? Quanto Tempo Faz?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19TosseQuantoTempoFaz" value="<?php echo $linha->fichaAnamneseCovid19TosseQuantoTempoFaz;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<!--<div class="col-md-1">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19DorDeGarganta" value="0" checked hidden>
													<input type="checkbox" name="fichaAnamneseCovid19DorDeGarganta" value="1" <?php if($linha->fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Dor De Garganta:
												</label>
											</div>-->
											<div class="col-md-2">
												<div class="form-group ">
													<label class="focus-label">Dor De Garganta? Quanto Tempo Faz?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz" value="<?php echo $linha->fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<!--<div class="col-md-1">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19FaltaDeAr"  value="0" checked hidden>
													<input type="checkbox" name="fichaAnamneseCovid19FaltaDeAr"  value="1" <?php if($linha->fichaAnamneseCovid19FaltaDeArQuantoTempoFaz){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Falta De Ar:
												</label>
											</div>-->
											<div class="col-md-2">
												<div class="form-group ">
													<label class="focus-label">Falta De Ar? Quanto Tempo Faz?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19FaltaDeArQuantoTempoFaz"  value="<?php echo $linha->fichaAnamneseCovid19FaltaDeArQuantoTempoFaz;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<!--<div class="col-md-1">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19Diarreia"  value="0" hidden>
													<input type="checkbox" name="fichaAnamneseCovid19Diarreia"  value="1" <?php if($linha->fichaAnamneseCovid19DiarreiaQuantoTempoFaz){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Diarreia:
												</label>
											</div>-->
											<div class="col-md-2">
												<div class="form-group ">
													<label class="focus-label">Diarreia? Quanto Tempo Faz?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19DiarreiaQuantoTempoFaz" value="<?php echo $linha->fichaAnamneseCovid19DiarreiaQuantoTempoFaz;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<!--<div class="col-md-1">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19Coriza"  value="0" hidden>
													<input type="checkbox" name="fichaAnamneseCovid19Coriza"  value="1" <?php if($linha->fichaAnamneseCovid19CorizaQuantoTempoFaz){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Coriza:
												</label>
											</div>-->
											<div class="col-md-2">
												<div class="form-group ">
													<label class="focus-label">Coriza? Quanto Tempo Faz?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19CorizaQuantoTempoFaz" value="<?php echo $linha->fichaAnamneseCovid19CorizaQuantoTempoFaz;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<!--<div class="col-md-1">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19DorDeCabeca"  value="0" hidden>
													<input type="checkbox" name="fichaAnamneseCovid19DorDeCabeca"  value="1" <?php if($linha->fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Dor De Cabeça:
												</label>
											</div>-->
											<div class="col-md-2">
												<div class="form-group ">
													<label class="focus-label">Dor De Cabeça? Quanto Tempo Faz?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz" value="<?php echo $linha->fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<!--<div class="col-md-1">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19DoresNoCorpo"  value="0" hidden>
													<input type="checkbox" name="fichaAnamneseCovid19DoresNoCorpo"  value="1" <?php if($linha->fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Dores No Corpo:
												</label>
											</div>-->
											<div class="col-md-2">
												<div class="form-group ">
													<label class="focus-label">Dores No Corpo? Quanto Tempo Faz?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz" value="<?php echo $linha->fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<!--<div class="col-md-1">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19DoresNasCostas"  value="0" hidden>
													<input type="checkbox" name="fichaAnamneseCovid19DoresNasCostas"  value="1" <?php if($linha->fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Dores Nas Costas:
												</label>
											</div>-->
											<div class="col-md-2">
												<div class="form-group ">
													<label class="focus-label">Dores Nas Costas? Quanto Tempo Faz?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz" value="<?php echo $linha->fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<!--<div class="col-md-1">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19Cansaco"  value="0" checked hidden>
													<input type="checkbox" name="fichaAnamneseCovid19Cansaco"  value="1" <?php if($linha->fichaAnamneseCovid19CansacoQuantoTempoFaz){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Cansaço:
												</label>
											</div>-->
											<div class="col-md-2">
												<div class="form-group ">
													<label class="focus-label">Cansaço? Quanto Tempo Faz?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19CansacoQuantoTempoFaz" value="<?php echo $linha->fichaAnamneseCovid19CansacoQuantoTempoFaz;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<div class="col-md-6">
											</div>


											<label class="col-md-1 col-form-label form-group">É Fumante?</label>
											<div class="col-md-1">
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19eFumante" id="" value="1" <?php if($linha->fichaAnamneseCovid19eFumante){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaHipotireoidismo">
													Sim
													</label>
												</div>
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19eFumante" id="" value="0" <?php if(!$linha->fichaAnamneseCovid19eFumante){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaAnamneseCovid19eFumante">
													Não
													</label>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group ">
													<label class="focus-label">Quanto Tempo?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19QuantoTempoEFumante" value="<?php echo $linha->fichaAnamneseCovid19QuantoTempoEFumante;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>

											<label class="col-md-1 col-form-label form-group">Tem Algum Vicio?</label>
											<div class="col-md-1">
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TemAlgumVicio" id="" value="1" <?php if($linha->fichaAnamneseCovid19TemAlgumVicio){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaAnamneseCovid19TemAlgumVicio">
													Sim
													</label>
												</div>
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TemAlgumVicio" id="" value="0" <?php if(!$linha->fichaAnamneseCovid19TemAlgumVicio){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaAnamneseCovid19TemAlgumVicio">
													Não
													</label>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group ">
													<label class="focus-label">Qual?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19QualAlgumVicio" value="<?php echo $linha->fichaAnamneseCovid19QualAlgumVicio;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<div class="col-md-12">
												<hr>
											</div>

											<label class="col-md-3 col-form-label form-group">Tem Algum Problema De Saúde?</label>
											<div class="col-md-2">
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TemAlgumProblemaDeSaude" id="" value="1" <?php if($linha->fichaAnamneseCovid19TemAlgumProblemaDeSaude){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaAnamneseCovid19TemAlgumProblemaDeSaude">
													Sim
													</label>
												</div>
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TemAlgumProblemaDeSaude" id="" value="0" <?php if(!$linha->fichaAnamneseCovid19TemAlgumProblemaDeSaude){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaAnamneseCovid19TemAlgumProblemaDeSaude">
													Não
													</label>
												</div>
											</div>
											<div class="col-md-7">
												<div class="form-group ">
													<label class="focus-label">Qual?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19QualProblemaDeSaude" value="<?php echo $linha->fichaAnamneseCovid19QualProblemaDeSaude;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<div class="col-md-1">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19Diabete" value="0" checked hidden>
													<input type="checkbox" name="fichaAnamneseCovid19Diabete" value="1" <?php if($linha->fichaAnamneseCovid19Diabete){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Diabete
												</label>
											</div>
											<div class="col-md-2">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19DoencaCardiaca" value="0" checked hidden>
													<input type="checkbox" name="fichaAnamneseCovid19DoencaCardiaca" value="1" <?php if($linha->fichaAnamneseCovid19DoencaCardiaca){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Doença Cardíaca
												</label>
											</div>
											<div class="col-md-2">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19ProblemaRenal" value="0" checked hidden>
													<input type="checkbox" name="fichaAnamneseCovid19ProblemaRenal" value="1"  <?php if($linha->fichaAnamneseCovid19ProblemaRenal){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Problema Renal
												</label>
											</div>
											<div class="col-md-2">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19DoencaRespiratotiaCronica" value="0" checked hidden>
													<input type="checkbox" name="fichaAnamneseCovid19DoencaRespiratotiaCronica" value="1" <?php if($linha->fichaAnamneseCovid19DoencaRespiratotiaCronica){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Doença Respiratória Crônica
												</label>
											</div>
											<div class="col-md-2">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19RiniteSinusite" value="0" checked hidden>
													<input type="checkbox" name="fichaAnamneseCovid19RiniteSinusite" value="1" <?php if($linha->fichaAnamneseCovid19RiniteSinusite){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Rinite / Sinusite
												</label>
											</div>
											<div class="col-md-2">
												<label>
													<input type="checkbox" name="fichaAnamneseCovid19PressaoAlta" value="0" checked hidden>
													<input type="checkbox" name="fichaAnamneseCovid19PressaoAlta" value="1" <?php if($linha->fichaAnamneseCovid19PressaoAlta){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Pressão Alta	
												</label>
											</div>
											<div class="col-md-1">
												<label>
												<input type="checkbox" name="fichaAnamneseCovid19Asma" value="0" checked hidden>
												<input type="checkbox" name="fichaAnamneseCovid19Asma" value="1" <?php if($linha->fichaAnamneseCovid19Asma){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>> Asma
													 
												</label>
											</div>
											<div class="col-md-12">
												<hr>
											</div>


											<label class="col-md-4 col-form-label form-group">Nos Últimos Três Meses, Seguiu Quarenta Corretamente?</label>
											<div class="col-md-2">
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament" id="" value="1" <?php if($linha->fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament">
													Sim
													</label>
												</div>
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament" id="" value="0" <?php if(!$linha->fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament">
													Não
													</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group ">
												<label class="focus-label">Se Trabalha, Já Voltou As Atividades Normais?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais" value="<?php echo $linha->fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
											<label class="col-md-4 col-form-label form-group">Mora Com Familiares? Os Mesmo Já Voltaram Para Suas Atividades Normal?</label>
											<div class="col-md-2">
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19MoraComFamiliares" id="" value="1" <?php if($linha->fichaAnamneseCovid19MoraComFamiliares){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaAnamneseCovid19MoraComFamiliares">
													Sim
													</label>
												</div>
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19MoraComFamiliares" id="" value="0" <?php if(!$linha->fichaAnamneseCovid19MoraComFamiliares){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaAnamneseCovid19MoraComFamiliares">
													Não
													</label>
												</div>
											</div>
											<div class="col-md-6">
											</div>

											<label class="col-md-4 col-form-label form-group">Você ou Familiar que moram na mesma residência trabalham aa Área Da Saúde?</label>
											<div class="col-md-2">
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude" id="" value="1" <?php if($linha->fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude">
													Sim
													</label>
												</div>
												<div class="form-group form-check form-check-inline">
													<input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude" id="" value="0" <?php if(!$linha->fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude){echo "checked";}; if (!$this->permitirAtualizacaoConvid){echo ' disabled';};?>>
													<label class="form-check-label" for="fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude">
													Não
													</label>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group ">
													<label class="focus-label">Qual?</label>
													<input type="text" class="form-control floating" name="fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude" value="<?php echo $linha->fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude;?>" <?php if (!$this->permitirAtualizacaoConvid){echo ' readonly';};?>>
												</div>
											</div>
										</div>
									</div>
									<div class="card-box">
										<h3 class="card-title">Eu: </h3>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group ">
													<label class="focus-label">Nome:*</label>
													<input type="text" class="form-control floating" value="<?php echo $linha->cliNome;?>" readonly>
												</div>
											</div>
											<div class="col-md-12">
												<p>Respondi com total sinceridade, estando ciente que o mesmo, caso eu tenha falado algo que não procede, sou responsável por qualquer irresponsabilidade que venha ocorrer nesse estabelecimento, por erro que eu venha causar, assim não colocando em risco a minha saúde e nem a saúde dos profissionais. As declarações acima são expressão da verdade, não cabendo ao estabelecimento nenhuma responsabilidade por fatos omitido ou falso.</p>
												<blockquote>
													<p class="mb-0">Código Penal - Decreto-Lei No 2.848, De 7 De Dezembro De 1940.<br>
																	Infração De Medida Sanitária Preventiva<br>
																	Art. 268 - Infringir Determinação Do Poder Público, Destinada A Impedir Introdução Ou Propagação De Doença Contagiosa:</p>
												</blockquote>
												<blockquote>
													<p class="mb-0">Pena - Detenção, De Um Mês A Um Ano, E Multa.<br>
																	Parágrafo Único - A Pena É Aumentada De Um Terço, Se O Agente É Funcionário Da Saúde Pública Ou Exerce A Profissão Na Área Da Saúde.</p>
												</blockquote>
											</div>

										</div>
									</div>
									<div class="card-box">
										<h3 class="card-title">Li E Concordo Com Os Termos Descritos Nesta Ficha De Anamnese Em Relação Covid-19.</h3>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group profile-img-wrap fichaAssinatura covid19">
													<img class="inline-block" src="<?php if(!empty($linha->cliAssinatura)){echo $linha->cliAssinatura;}else{echo URL . 'assets/img/assinatura.png';}?>" alt="user" readonly>
													
												</div>							
											</div>
											<div class="col-md-6">
													<div class="form-group ">
														<label class="focus-label">CPF:*</label>
														<input type="text" class="form-control floating" value="<?php echo $linha->cliCPF;?>" readonly>
													</div>
													<div class="form-group ">
														<label class="focus-label">RG:*</label>
														<input type="text" class="form-control floating" value="<?php echo $linha->cliRG;?>" readonly>
														<input type="text" name="idCliente" value="<?php echo $linha->idCliente;?>" hidden>
													</div>
											</div>
										</div>                       
									</div>
									<div class="card-box">
										<div class="text-center m-t-20">
										<?php if ($this->permitirAtualizacaoConvid){echo " <button destino='ficha/inserirFichaAnamneseCovid19' class='inserir-ajax-ficha-covid btn btn-primary submit-btn' type='button'>Atualizar Ficha Anamnese Covid-19</button>";}; ?>
										</div>
									</div>
								</form>
								<?php };?>
								<?php };?>