<form id="formCovid">
                                        <div class="card-box">
                                            <h3 class="card-title">FICHA ANAMNESE COVID-19</h3>											
                                            <div class="row ficha">
                                                
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Já Fez Teste Covid-19</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19JaFezTesteCovid19" id="" value="1" >
                                                            <label class="form-check-label" for="fichaAnamneseCovid19JaFezTesteCovid19">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19JaFezTesteCovid19" id="" value="0" >
                                                            <label class="form-check-label" for="fichaAnamneseCovid19JaFezTesteCovid19">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Qual?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QualTesteCovid19" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Resultado?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19Resultado" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Quanto Tempo Faz Que Fez Teste?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QuantoTempoFazQueFezTeste" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Observações:</label>
                                                        <textarea class="form-control floating" rows="3" cols="30" name="fichaAnamneseCovid19ObsTesteCovid19" ></textarea>
                                                    </div>								
                                                </div>                         

                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Teve Contato Com Pessoas Teste Covid-19 Positivo?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo" id="" value="1">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo" id="" value="0" >
                                                            <label class="form-check-label" for="fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Quanto Tempo Faz Que Fez Teste?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QuantoTempoFazContatoComPessoas" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-box">
                                            <h3 class="card-title">Marque a caixa se Estiver Sentindo Qualquer Desses Sintomas</h3>
                                            <div class="row ficha">
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19FebreQuantoTempoFaz"> Febre:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Febre? QuantoTempoFaz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19FebreQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19TosseQuantoTempoFaz" > Tosse:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Tosse? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19TosseQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz" > Dor De Garganta:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Dor De Garganta? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19FaltaDeArQuantoTempoFaz"> Falta De Ar:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Falta De Ar? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19FaltaDeArQuantoTempoFaz"  value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DiarreiaQuantoTempoFaz"> Diarreia:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Diarreia? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19DiarreiaQuantoTempoFaz" value="" >
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19CorizaQuantoTempoFaz"> Coriza:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Coriza? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19CorizaQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz"> Dor De Cabeça:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Dor De Cabeça? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz" value="" >
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz" > Dores No Corpo:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Dores No Corpo? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz"> Dores Nas Costas:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Dores Nas Costas? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19CansacoQuantoTempoFaz"> Cansaço:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Cansaço? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19CansacoQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                </div>
                                                
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">É Fumante?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19eFumante" value="1">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19eFumante">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19eFumante" value="0">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19eFumante">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Quanto Tempo?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QuantoTempoEFumante" value="">
                                                    </div>
                                                </div>

                                                
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Tem Algum Vicio?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TemAlgumVicio" value="1">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19TemAlgumVicio">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TemAlgumVicio" value="0">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19TemAlgumVicio">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Qual?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QualAlgumVicio" value="" >
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <hr>
                                                </div>

                                                
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Tem Algum Problema De Saúde?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TemAlgumProblemaDeSaude"  value="1">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19TemAlgumProblemaDeSaude">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TemAlgumProblemaDeSaude" value="0">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19TemAlgumProblemaDeSaude">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Qual?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QualProblemaDeSaude" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19Diabete" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19Diabete" value="1" > Diabete
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DoencaCardiaca" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DoencaCardiaca" value="1" > Doença Cardíaca
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19ProblemaRenal" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19ProblemaRenal" value="1" > Problema Renal
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DoencaRespiratotiaCronica" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DoencaRespiratotiaCronica" value="1" > Doença Respiratória Crônica
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19RiniteSinusite" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19RiniteSinusite" value="1" > Rinite / Sinusite
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19PressaoAlta" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19PressaoAlta" value="1" > Pressão Alta
                                                    </label>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19Asma" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19Asma" value="1" > Asma
                                                    </label>
                                                </div>
                                                <div class="col-md-12">
                                                    <hr>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Nos Últimos Três Meses, Seguiu Quarenta Corretamente?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament" id="" value="1" >
                                                            <label class="form-check-label" for="">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament" id="" value="0">
                                                            <label class="form-check-label" for="">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Se Trabalha, Já Voltou As Atividades Normais?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais" value="">
                                                    </div>
                                                </div>
                                               
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Mora Com Familiares? Os Mesmo Já Voltaram Para Suas Atividades Normal?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19MoraComFamiliares" id="" value="1">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19MoraComFamiliares">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19MoraComFamiliares" id="" value="0" >
                                                            <label class="form-check-label" for="fichaAnamneseCovid19MoraComFamiliares">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                </div>

                                                
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Você ou Familiar que moram na mesma residência trabalham aa Área Da Saúde?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude" id="" value="1" >
                                                            <label class="form-check-label" for="fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude" id="" value="0" >
                                                            <label class="form-check-label" for="fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Qual?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-box">
                                            <h3 class="card-title">Eu: </h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Nome:*</label>
                                                        <input type="text" class="form-control floating" value="<?php echo $perfilLista->cliNome;?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <p>Respondi com total sinceridade, estando ciente que o mesmo, caso eu tenha falado algo que não procede, sou responsável por qualquer irresponsabilidade que venha ocorrer nesse estabelecimento, por erro que eu venha causar, assim não colocando em risco a minha saúde e nem a saúde dos profissionais. As declarações acima são expressão da verdade, não cabendo ao estabelecimento nenhuma responsabilidade por fatos omitido ou falso.</p>
                                                    <blockquote>
                                                        <p class="mb-0">Código Penal - Decreto-Lei No 2.848, De 7 De Dezembro De 1940.<br>
                                                                        Infração De Medida Sanitária Preventiva<br>
                                                                        Art. 268 - Infringir Determinação Do Poder Público, Destinada A Impedir Introdução Ou Propagação De Doença Contagiosa:</p>
                                                    </blockquote>
                                                    <blockquote>
                                                        <p class="mb-0">Pena - Detenção, De Um Mês A Um Ano, E Multa.<br>
                                                                        Parágrafo Único - A Pena É Aumentada De Um Terço, Se O Agente É Funcionário Da Saúde Pública Ou Exerce A Profissão Na Área Da Saúde.</p>
                                                    </blockquote>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="card-box">
                                            <h3 class="card-title">Li E Concordo Com Os Termos Descritos Nesta Ficha De Anamnese Em Relação Covid-19.</h3>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group profile-img-wrap fichaAssinatura covid19">
                                                        <img class="inline-block" src="<?php if(!empty($perfilLista->cliAssinatura)){echo $perfilLista->cliAssinatura;}else{echo URL . 'assets/img/assinatura.png';}?>" alt="user">
                                                        
                                                    </div>							
                                                </div>
                                                <div class="col-md-6">
                                                        <div class="form-group ">
                                                            <label class="focus-label">CPF:*</label>
                                                            <input type="text" class="form-control floating" value="<?php echo $perfilLista->cliCPF;?>">
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="focus-label">RG:*</label>
                                                            <input type="text" class="form-control floating" value="<?php echo $perfilLista->cliRG;?>">
                                                            <input type="text" class="form-control floating" name="idCliente" value="<?php echo $perfilLista->idCliente;?>" hidden>
                                                        </div>
                                                </div>
                                            </div>                       
                                        </div>
                                        <div class="card-box">
                                            <div class="text-center m-t-20">
                                                <button destino="ficha/inserirFichaAnamneseCovid19" class="inserir-ajax-ficha-covid btn btn-primary submit-btn" type="button">Cadastrar Avaliação Geral</button>
                                            </div>
                                        </div>
                                    </form>