
                        <form id="formFichaGeral">
                            <div class="card-box">
                                <h3 class="card-title">AVALIAÇÃO GERAL ( ANAMSESE )</h3>
                                <div class="row">
                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="label-block">Tem histórico de dermatite ou câncer de pele? </label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="" value="1" <?php //if($linha->fichaHistoricoDeDermatiteOuCancerDePele){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for=""> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="" value="0" <?php //if(!$linha->fichaHistoricoDeDermatiteOuCancerDePele){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for=""> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="focus-label">Em qual local do corpo?</label>
                                            <input type="text" class="form-control floating" name="fichaEmQualLocalDoCorpo" value="<?php // echo $linha->fichaEmQualLocalDoCorpo;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="focus-label">Faz quanto tempo?</label>
                                            <input type="text" class="form-control floating" name="fichaFazQuantoTempo" value="<?php // echo $linha->fichaFazQuantoTempo;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="label-block">Foi liberado pelo médico para tratamento estético? </label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" id="" value="1" <?php // if($linha->fichaFoiLiberadoPeloMedicoParaTratamentoEstetico){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" id="" value="0" <?php // if(!$linha->fichaFoiLiberadoPeloMedicoParaTratamentoEstetico){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h3 class="card-title">Histórico de doenças</h3>
                                <div class="row ">
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Hipertireoidismo?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaHipertireoidismo" id="" value="1" <?php // if($linha->fichaHipertireoidismo){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaHipertireoidismo"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaHipertireoidismo" id="" value="0" <?php // if(!$linha->fichaHipertireoidismo){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaHipertireoidismo"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo" value="<?php // echo $linha->fichaTratamentoHipertireoidismo;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Hipotireoidismo?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaHipotireoidismo" id="" value="1" <?php // if($linha->fichaHipotireoidismo){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaHipotireoidismo"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaHipotireoidismo" id="" value="0" <?php // if(!$linha->fichaHipotireoidismo){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaHipotireoidismo"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoHipotireoidismo" value="<?php // echo $linha->fichaTratamentoHipotireoidismo;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="col-form-label form-group">Diabetes?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaDiabetes" id="" value="1" <?php // if($linha->fichaDiabetes){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaDiabetes"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaDiabetes" id="" value="0" <?php // if(!$linha->fichaDiabetes){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaDiabetes"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoDiabetes" value="<?php // echo $linha->fichaTratamentoDiabetes;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Hipertensão?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaHipertensao" id="" value="1" <?php // if($linha->fichaHipertensao){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaHipertensao"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaHipertensao" id="" value="0" <?php // if(!$linha->fichaHipertensao){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaHipertensao"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoHipertensao" value="<?php // echo $linha->fichaTratamentoHipertensao;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Eplepsia?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaEplepsia" id="" value="1" <?php // if($linha->fichaEplepsia){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaEplepsia"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaEplepsia" id="" value="0" <?php // if(!$linha->fichaEplepsia){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaEplepsia"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoEplepsia" value="<?php // echo $linha->fichaTratamentoEplepsia;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Psoríase?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaPsoriase" id="" value="1" <?php // if($linha->fichaPsoriase){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaPsoriase"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaPsoriase" id="" value="0" <?php // if(!$linha->fichaPsoriase){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaPsoriase"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoPsoriase" value="<?php // echo $linha->fichaTratamentoPsoriase;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="card-box">
                                <div class="row">
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Tem Implantes metálicos?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaTemImplantesMetalicos" id="" value="1" <?php // if($linha->fichaTemImplantesMetalicos){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaTemImplantesMetalicos"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaTemImplantesMetalicos" id="" value="0" <?php // if(!$linha->fichaTemImplantesMetalicos){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaTemImplantesMetalicos"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Local?</label>
                                            <input type="text" class="form-control floating" name="fichaLocalTemImplantesMetalicos" value="<?php // echo $linha->fichaLocalTemImplantesMetalicos;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Portador de marcapasso?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaPortadorDeMarcapasso" id="" value="1" <?php // if($linha->fichaPortadorDeMarcapasso){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaPortadorDeMarcapasso"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaPortadorDeMarcapasso" id="" value="0" <?php // if(!$linha->fichaPortadorDeMarcapasso){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaPortadorDeMarcapasso"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">A quanto tempo?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantoTempoPortadorDeMarcapasso" value="<?php // echo $linha->fichaQuantoTempoPortadorDeMarcapasso;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Portador de HIV?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaPortadorDeHIV" id="" value="1" <?php // if($linha->fichaPortadorDeHIV){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaPortadorDeHIV"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaPortadorDeHIV" id="" value="0" <?php // if(!$linha->fichaPortadorDeHIV){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaPortadorDeHIV"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoPortadorDeHIV" value="<?php // echo $linha->fichaTratamentoPortadorDeHIV;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Portador de Hepatite?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaPortadorDeHepatite" id="" value="1" <?php // if($linha->fichaPortadorDeHepatite){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaPortadorDeHepatite"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaPortadorDeHepatite" id="" value="0" <?php // if(!$linha->fichaPortadorDeHepatite){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaPortadorDeHepatite"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualPortadorDeHepatite" value="<?php // echo $linha->fichaQualPortadorDeHepatite;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Alteração hormonal?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaAlteracaoHormonal" id="" value="1" <?php // if($linha->fichaAlteracaoHormonal){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaAlteracaoHormonal"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaAlteracaoHormonal" id="" value="0" <?php // if(!$linha->fichaAlteracaoHormonal){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaAlteracaoHormonal"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualAlteracaoHormonal" value="<?php // echo $linha->fichaQualAlteracaoHormonal;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Alergia a algum alimento?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaAlergiaAlgumAlimento" id="" value="1" <?php // if($linha->fichaAlergiaAlgumAlimento){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaAlergiaAlgumAlimento"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaAlergiaAlgumAlimento" id="" value="0" <?php // if(!$linha->fichaAlergiaAlgumAlimento){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaAlergiaAlgumAlimento"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualAlergiaAlgumAlimento" value="<?php // echo $linha->fichaQualAlergiaAlgumAlimento;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Alergia a algum medicamento?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaAlergiaAlgumMedicamento" id="" value="1" <?php // if($linha->fichaAlergiaAlgumMedicamento){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaAlergiaAlgumMedicamento"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaAlergiaAlgumMedicamento" id="" value="0" <?php // if(!$linha->fichaAlergiaAlgumMedicamento){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaAlergiaAlgumMedicamento"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualAlergiaAlgumMedicamento" value="<?php // echo $linha->fichaQualAlergiaAlgumMedicamento;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Faz uso de medicamento?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaFazUsoDeMedicamento" id="" value="1" <?php // if($linha->fichaFazUsoDeMedicamento){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaFazUsoDeMedicamento"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaFazUsoDeMedicamento" id="" value="0" <?php // if(!$linha->fichaFazUsoDeMedicamento){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaFazUsoDeMedicamento"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualFazUsoDeMedicamento" value="<?php // echo $linha->fichaQualFazUsoDeMedicamento;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Presença de queloides?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaPresencaDeQueloides" id="" value="1" <?php // if($linha->fichaPresencaDeQueloides){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaPresencaDeQueloides"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaPresencaDeQueloides" id="" value="0" <?php // if(!$linha->fichaPresencaDeQueloides){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaPresencaDeQueloides"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQueLocalPresencaDeQueloides" value="<?php // echo $linha->fichaQueLocalPresencaDeQueloides;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Gestante?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaGestante" id="" value="1" <?php // if($linha->fichaGestante){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaGestante"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaGestante" id="" value="0" <?php // if(!$linha->fichaGestante){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaGestante"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Quanto tempo?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantoTempoGestante" value="<?php // echo $linha->fichaQuantoTempoGestante;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Filhos?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaFilhos" id="" value="1" <?php // if($linha->fichaFilhos){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaFilhos"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaFilhos" id="" value="0" <?php // if(!$linha->fichaFilhos){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaFilhos"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Quantos e que idade?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantosFilhosIdade" value="<?php // echo $linha->fichaQuantosFilhosIdade;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Fez alguma cirurgia ou plástica?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaFezAlgumaCirurgiaPlastica" id="" value="1" <?php // if($linha->fichaFezAlgumaCirurgiaPlastica){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaFezAlgumaCirurgiaPlastica"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaFezAlgumaCirurgiaPlastica" id="" value="0" <?php // if(!$linha->fichaFezAlgumaCirurgiaPlastica){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaFezAlgumaCirurgiaPlastica"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Que local?</label>
                                            <input type="text" class="form-control floating" name="fichaQualFezAlgumaCirurgiaPlastica" value="<?php // echo $linha->fichaQualFezAlgumaCirurgiaPlastica;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Realizou procedimentos estéticos anteriores?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaRealizouProcedimentosEsteticosAnteriores" id="" value="1" <?php // if($linha->fichaRealizouProcedimentosEsteticosAnteriores){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaRealizouProcedimentosEsteticosAnteriores"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaRealizouProcedimentosEsteticosAnteriores" id="" value="0" <?php // if(!$linha->fichaRealizouProcedimentosEsteticosAnteriores){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaRealizouProcedimentosEsteticosAnteriores"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualRealizouProcedimentosEsteticosAnteriores" value="<?php // echo $linha->fichaQualRealizouProcedimentosEsteticosAnteriores;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Faz uso de maquiagem?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaFazUsoDeMaquiagem" id="" value="1" <?php // if($linha->fichaFazUsoDeMaquiagem){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaFazUsoDeMaquiagem"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaFazUsoDeMaquiagem" id="" value="0" <?php // if(!$linha->fichaFazUsoDeMaquiagem){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaFazUsoDeMaquiagem"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Como retira?</label>
                                            <input type="text" class="form-control floating" name="fichaQualFazUsoDeMaquiagem" value="<?php // echo $linha->fichaQualFazUsoDeMaquiagem;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Faz atividade física?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaFazAtividadeFisica" id="" value="1" <?php // if($linha->fichaFazAtividadeFisica){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaFazAtividadeFisica"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaFazAtividadeFisica" id="" value="0" <?php // if(!$linha->fichaFazAtividadeFisica){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaFazAtividadeFisica"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Quantas vezes por semana?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantasVezesPorSemanaAtividadeFisica">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Já fumou ou fuma?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaJaFumouOuFuma" id="" value="1" <?php // if($linha->fichaJaFumouOuFuma){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaJaFumouOuFuma"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaJaFumouOuFuma" id="" value="0" <?php // if(!$linha->fichaJaFumouOuFuma){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaJaFumouOuFuma"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Quantos cigarros por dia?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantosCigarrosPorDia" value="<?php // echo $linha->fichaQuantosCigarrosPorDia;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    <label class="col-md-2 col-form-label form-group">Consumo de água?</label>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label class="focus-label">Ingere quantos copos ao dia?</label>
                                            <input type="text" class="form-control floating" name="fichaConsumoAguaIngereQuantosCoposDia" value="<?php // echo $linha->fichaConsumoAguaIngereQuantosCoposDia;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Ingere bebida alcoólica?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaIngereBebidaAlcoolica" id="" value="1" <?php // if($linha->fichaIngereBebidaAlcoolica){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaIngereBebidaAlcoolica"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaIngereBebidaAlcoolica" id="" value="0" <?php // if(!$linha->fichaIngereBebidaAlcoolica){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaIngereBebidaAlcoolica"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Quantas doses por semana?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantasDosesPorSemanaBebidaAlcoolica" value="<?php // echo $linha->fichaQuantasDosesPorSemanaBebidaAlcoolica;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="label-block">Ingere bebida gaseificada?</label>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaIngereBebidaGaseificada" id="" value="1" <?php // if($linha->fichaIngereBebidaGaseificada){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaIngereBebidaGaseificada"> Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <input class="validar form-check-input" type="radio" name="fichaIngereBebidaGaseificada" id="" value="0" <?php // if(!$linha->fichaIngereBebidaGaseificada){echo "checked";}; if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <label class="form-check-label" for="fichaIngereBebidaGaseificada"> Não </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="focus-label">Quantas doses por semana?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantasDosesPorSemanaBebidaGaseificada" value="<?php // echo $linha->fichaQuantasDosesPorSemanaBebidaGaseificada;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group select-focus">
                                            <label class="focus-label">Estresse</label>
                                            <select class="select form-control floating" name="fichaEstresse" <?php // if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <option></option>
                                                <option value="Não" <?php // if($linha->fichaEstresse == "Não"){echo "selected";};?>>Não</option>
                                                <option value="Pouco" <?php // if($linha->fichaEstresse == "Pouco"){echo "selected";};?>>Pouco</option>
                                                <option value="Moderado" <?php // if($linha->fichaEstresse == "Moderado"){echo "selected";};?>>Moderado</option>
                                                <option value="Elevado" <?php // if($linha->fichaEstresse == "Elevado"){echo "selected";};?>>Elevado</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group select-focus">
                                            <label class="focus-label">Posição que permanece por mais tempo</label>
                                            <select class="select form-control floating" name="fichaPosicaoQuePermanecePorMaisTempo" <?php // if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <option></option>
                                                <option value="Sentado" <?php // if($linha->fichaPosicaoQuePermanecePorMaisTempo == "Sentado"){echo "selected";};?>>Sentado</option>
                                                <option value="Em pé" <?php // if($linha->fichaPosicaoQuePermanecePorMaisTempo == "Em pé"){echo "selected";};?>>Em pé</option>
                                                <option value="Alternado" <?php // if($linha->fichaPosicaoQuePermanecePorMaisTempo == "Alternado"){echo "selected";};?>>Alternado</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group select-focus">
                                            <label class="focus-label">Sono</label>
                                            <select class="select form-control floating" name="fichaSono" <?php // if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <option></option>
                                                <option value="Inquieto" <?php // if($linha->fichaSono == "Inquieto"){echo "selected";};?>>Inquieto</option>
                                                <option value="Tranquilo" <?php // if($linha->fichaSono == "Tranquilo"){echo "selected";};?>>Tranquilo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="focus-label">Quantas horas por noite?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantasHorasPorNoiteSono" value="<?php // echo $linha->fichaQuantasHorasPorNoiteSono;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group select-focus">
                                            <label class="focus-label">Alimentação</label>
                                            <select class="select form-control floating" name="fichaAlimentacao"  <?php // if (!$this->permitirAtualizacaoGeral){echo ' disabled';};?>>
                                                <option></option>
                                                <option value="Balanceada e dá preferência por frutas, verduras e legumes"  <?php // if($linha->fichaAlimentacao == "Balanceada e dá preferência por frutas, verduras e legumes"){echo "selected";};?>>Balanceada e dá preferência por frutas, verduras e legumes</option>
                                                <option value="Não Balanceada com restrição alimentar" <?php // if($linha->fichaAlimentacao == "Não Balanceada com restrição alimentar"){echo "selected";};?>>Não Balanceada com restrição alimentar</option>
                                                <option value="Não balanceada e come frituras, massas e doces" <?php // if($linha->fichaAlimentacao == "Não balanceada e come frituras, massas e doces"){echo "selected";};?>>Não balanceada e come frituras, massas e doces</option>
                                                <option value="Elevado" <?php // if($linha->fichaAlimentacao == "Elevado"){echo "selected";};?>>Faz dieta</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="focus-label">Faz dieta - Qual e a quanto tempo?</label>
                                            <input type="text" class="form-control floating" name="fichaFazDietaQualQuantoTempo" value="<?php // echo $linha->fichaFazDietaQualQuantoTempo;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="card-box">
                                <h3 class="card-title">Observações Gerais</h3>
                                
								
								<div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="focus-label">O que te incomoda</label>
											<input type="text" class="form-control floating" name="fichaOqueTeIncomoda" value="<?php // echo $linha->fichaOqueTeIncomoda;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
									<div class="col-md-3">
                                        <div class="form-group">
                                            <label class="focus-label">Qual seu time?</label>
											<input type="text" class="form-control floating" name="fichaQualSeuTime" value="<?php // echo $linha->fichaQualSeuTime;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                    
										</div>
									</div>
									<div class="col-md-3">
                                        <div class="form-group">
                                            <label class="focus-label">O que você mudaria em você</label>
											<input type="text" class="form-control floating" name="fichaOqueVcMudariaEmVc" value="<?php // echo $linha->fichaOqueVcMudariaEmVc;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
									<div class="col-md-3">
                                        <div class="form-group">
                                            <label class="focus-label">Qual seu estilo de musica</label>
											<input type="text" class="form-control floating" name="fichaQualSeuEstiloMusica" value="<?php // echo $linha->fichaQualSeuEstiloMusica;?>" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>>
                                        </div>
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="focus-label">Observações:</label>
                                            <textarea class="form-control floating" rows="3" cols="30" name="fichaOutrasDoencas" <?php // if (!$this->permitirAtualizacaoGeral){echo ' readonly';};?>><?php // echo $linha->fichaOutrasDoencas;?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="text" class="form-control floating" name="idCliente" value="<?php echo $_SESSION['idCliente'];?>" hidden>
                        </form>
                        <div class="text-center m-t-20">
                            <button destino="ficha/inserirAvaliacaoGeral" idform="formFichaGeral" class="ficha-geral-cliente-ajax btn btn-primary submit-btn" type="button">Cadastrar Avaliação Geral</button>
                            <!--<button destino="ficha/atualizarAvaliacaoGeral/<?php // echo $linha->idFichaGeral;?>" class="atualizar-ajax-ficha-geral btn btn-primary submit-btn">Atualizar Avaliação Geral</button>-->					
                        </div>

