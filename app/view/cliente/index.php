
<div class="page-wrapper">
            <div class="content">
                <div class="row">
					<div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
                        <span class="texto-ajax"></span>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
						<span class="texto-ajax"></span>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title"><?php echo $qtdCliente; ?> - Clientes Ativos</h4>
                    </div>
                    <div class="col-sm-8 col-9 text-right m-b-20">
						<a href="<?php echo URL; ?>cliente/novo" class="btn btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i> Cliente</a>
						<a href="<?php echo URL; ?>cliente/desativado" class="btn btn btn-primary btn-rounded float-right">Clientes Desativados</a>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0">
								<thead>
									<tr>
										<th>Nome</th>
										<th>Email</th>
										<th>Celular</th>
										<th>CPF</th>
										<th>Data Nascimento</th>
										<th class="text-right">Opções</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($clienteLista as $cliente) { ?>
									<tr id="linha<?php echo $cliente->idCliente; ?>">
										<td>
											<a href="<?php echo URL; ?>cliente/perfil/<?php echo $cliente->idCliente; ?>">
												<!--<img width="28" height="28" src="<?php							   
											if ($cliente->cliFoto == "") {
												if($cliente->cliWebcam == ""){
													echo URL."assets/img/cliente/user.jpg";
												}else{
													echo $cliente->cliWebcam;
												}
											}else{
												echo URL."assets/img/cliente/".$cliente->cliFoto;
										   	} 
											?>" title="Perfil <?php if (isset($cliente->cliNome)) echo htmlspecialchars($cliente->cliNome, ENT_QUOTES, 'UTF-8'); ?>" class="rounded-circle m-r-5">-->
											<?php if (isset($cliente->cliNome)) echo htmlspecialchars($cliente->cliNome, ENT_QUOTES, 'UTF-8'); ?>
											</a>
										</td>
										<td><?php if (isset($cliente->cliEmail)) echo htmlspecialchars($cliente->cliEmail, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($cliente->cliCel)) echo htmlspecialchars($cliente->cliCel, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($cliente->cliCPF)) echo htmlspecialchars($cliente->cliCPF, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($cliente->cliNasci)) echo htmlspecialchars($cliente->cliNasci, ENT_QUOTES, 'UTF-8'); ?></td>
										
										
										<!--<td>
										<?php
											/*foreach ($mediaPesquisa as $qtd) {	
												
												$MuitoMal = 0;
												$Mal = 0;
												$Medio = 0;
												$Bom = 0;
												$Excelente = 0;
												
												if($qtd->pesquisaQualidadeComoServicoFoiPrestado == "Muito Mal"){$MuitoMal= $MuitoMal + 1;}
												if($qtd->pesquisaQualidadeAtendimentoRecepcao == "Muito Mal"){$MuitoMal= $MuitoMal + 1;}
												if($qtd->pesquisaQualidadeAtendimentoBiomedico == "Muito Mal"){$MuitoMal= $MuitoMal + 1;}
												if($qtd->pesquisaQualidadeAtendimentoEsteticista == "Muito Mal"){$MuitoMal= $MuitoMal + 1;}
												if($qtd->pesquisaQualidadeAtendimentoGeral == "Muito Mal"){$MuitoMal= $MuitoMal + 1;}
												if($qtd->pesquisaQualidadeLimpezaDosBanheiros == "Muito Mal"){$MuitoMal= $MuitoMal + 1;}
												if($qtd->pesquisaQualidadeLimpezaDasSalas == "Muito Mal"){$MuitoMal= $MuitoMal + 1;}
												if($qtd->pesquisaQualidadeConfortoDoEspaco == "Muito Mal"){$MuitoMal= $MuitoMal + 1;}
												if($qtd->pesquisaQualidadeSeuTratamento == "Muito Mal"){$MuitoMal= $MuitoMal + 1;}

												if($qtd->pesquisaQualidadeComoServicoFoiPrestado == "Excelente"){$Excelente= $Excelente + 1;}
												if($qtd->pesquisaQualidadeAtendimentoRecepcao == "Excelente"){$Excelente= $Excelente + 1;}
												if($qtd->pesquisaQualidadeAtendimentoBiomedico == "Excelente"){$Excelente= $Excelente + 1;}
												if($qtd->pesquisaQualidadeAtendimentoEsteticista == "Excelente"){$Excelente= $Excelente + 1;}
												if($qtd->pesquisaQualidadeAtendimentoGeral == "Excelente"){$Excelente= $Excelente + 1;}
												if($qtd->pesquisaQualidadeLimpezaDosBanheiros == "Excelente"){$Excelente= $Excelente + 1;}
												if($qtd->pesquisaQualidadeLimpezaDasSalas == "Excelente"){$Excelente= $Excelente + 1;}
												if($qtd->pesquisaQualidadeConfortoDoEspaco == "Excelente"){$Excelente= $Excelente + 1;}
												if($qtd->pesquisaQualidadeSeuTratamento == "Excelente"){$Excelente= $Excelente + 1;}

												if($qtd->pesquisaQualidadeComoServicoFoiPrestado == "Bom"){$Bom = $Bom + 1;}
												if($qtd->pesquisaQualidadeAtendimentoRecepcao == "Bom"){$Bom = $Bom + 1;}
												if($qtd->pesquisaQualidadeAtendimentoBiomedico == "Bom"){$Bom = $Bom + 1;}
												if($qtd->pesquisaQualidadeAtendimentoEsteticista == "Bom"){$Bom = $Bom + 1;}
												if($qtd->pesquisaQualidadeAtendimentoGeral == "Bom"){$Bom = $Bom + 1;}
												if($qtd->pesquisaQualidadeLimpezaDosBanheiros == "Bom"){$Bom = $Bom + 1;}
												if($qtd->pesquisaQualidadeLimpezaDasSalas == "Bom"){$Bom = $Bom + 1;}
												if($qtd->pesquisaQualidadeConfortoDoEspaco == "Bom"){$Bom = $Bom + 1;}
												if($qtd->pesquisaQualidadeSeuTratamento == "Bom"){$Bom = $Bom + 1;}

												if($qtd->pesquisaQualidadeComoServicoFoiPrestado == "Mal"){$Mal = $Mal + 1;}
												if($qtd->pesquisaQualidadeAtendimentoRecepcao == "Mal"){$Mal = $Mal + 1;}
												if($qtd->pesquisaQualidadeAtendimentoBiomedico == "Mal"){$Mal = $Mal + 1;}
												if($qtd->pesquisaQualidadeAtendimentoEsteticista == "Mal"){$Mal = $Mal + 1;}
												if($qtd->pesquisaQualidadeAtendimentoGeral == "Mal"){$Mal = $Mal + 1;}
												if($qtd->pesquisaQualidadeLimpezaDosBanheiros == "Mal"){$Mal = $Mal + 1;}
												if($qtd->pesquisaQualidadeLimpezaDasSalas == "Mal"){$Mal = $Mal + 1;}
												if($qtd->pesquisaQualidadeConfortoDoEspaco == "Mal"){$Mal = $Mal + 1;}
												if($qtd->pesquisaQualidadeSeuTratamento == "Mal"){$Mal = $Mal + 1;}

												if($qtd->pesquisaQualidadeComoServicoFoiPrestado == "Medio"){$Medio = $Medio + 1;}
												if($qtd->pesquisaQualidadeAtendimentoRecepcao == "Medio"){$Medio = $Medio + 1;}
												if($qtd->pesquisaQualidadeAtendimentoBiomedico == "Medio"){$Medio = $Medio + 1;}
												if($qtd->pesquisaQualidadeAtendimentoEsteticista == "Medio"){$Medio = $Medio + 1;}
												if($qtd->pesquisaQualidadeAtendimentoGeral == "Medio"){$Medio = $Medio + 1;}
												if($qtd->pesquisaQualidadeLimpezaDosBanheiros == "Medio"){$Medio = $Medio + 1;}
												if($qtd->pesquisaQualidadeLimpezaDasSalas == "Medio"){$Medio = $Medio + 1;}
												if($qtd->pesquisaQualidadeConfortoDoEspaco == "Medio"){$Medio = $Medio + 1;}
												if($qtd->pesquisaQualidadeSeuTratamento == "Medio"){$Medio = $Medio + 1;}
											
												if($cliente->idCliente == $qtd->idCliente){

													echo "Muito Mal: " .$MuitoMal. "<br>";
													echo "Mal: " .$Mal. "<br>";
													echo "Médio: " .$Medio. "<br>";
													echo "Bom: " .$Bom. "<br>";
													echo "Excelente: " .$Excelente. "<br>";
												}	
											}*/
																			   
											
											
										?>
										</td>-->
										
										
										
										<td class="text-right">	
											<div class="dropdown dropdown-action show">
												<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="<?php echo URL; ?>cliente/editar/<?php echo $cliente->idCliente; ?>"><i class="fa fa-pencil m-r-5"></i>Editar</a>
													<a class="dropdown-item desativar-ajax" destino="cliente/ativarDesativar/0" idobjeto="<?php echo $cliente->idCliente; ?>" href="#"><i class="fa fa-close m-r-5"></i>Desativar</a>
													<a  class="dropdown-item permissao-atualizacao-geral-ajax" destino="cliente/permissaoAtualizarGeral" idobjeto="<?php echo $cliente->idCliente; ?>" boleano="<?php echo 1 - $cliente->cliPermitirAtualizacaoGeral; ?>" href="#" ></i><?php if($cliente->cliPermitirAtualizacaoGeral == 1){ echo "<i class='fa fa-close m-r-5'></i>Bloquear Atualizar Geral";}else{echo " <i class='fa fa-check m-r-5'></i>Permissão Atualizar Geral";}; ?></a>
													<a  class="dropdown-item permissao-atualizacao-covid-ajax" destino="cliente/permissaoAtualizarCovid" idobjeto="<?php echo $cliente->idCliente; ?>" boleano="<?php echo 1 - $cliente->cliPermitirAtualizacaoCovid; ?>" href="#" ></i><?php if($cliente->cliPermitirAtualizacaoCovid == 1){ echo "<i class='fa fa-close m-r-5'></i>Bloquear Atualizar Covid";}else{echo " <i class='fa fa-check m-r-5'></i>Permissão Atualizar Covid";}; ?></a>
													<a class="carregar-form dropdown-item" destino="contatoadmin/formContato/<?php echo $cliente->idCliente;?>" href="#" data-toggle="modal" data-target=".bd-example-modal-lg"> <i class="fa fa-envelope-o m-r-5"></i>Enviar Mensagem</a>
													<a class="dropdown-item"  href="<?php echo URL; ?>pesquisa/listacliente/<?php echo $cliente->idCliente; ?>"> <i class="fa fa-smile-o m-r-5"></i>Pesquisa</a>
												</div>
											</div>										
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
            

<div class="sidebar-overlay" data-reff=""></div>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Enviar mensagem</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body modal-body-formgeral">
        ...
        </div>
    </div>
  </div>
</div>