
<div class="page-wrapper">
            <div class="content">
                <div class="row">
					<div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
                        <span class="texto-ajax"></span>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
						<span class="texto-ajax"></span>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title"><?php echo $qtdCliente; ?> - Clientes Ativos</h4>
                    </div>
                    
				</div>
				<div class="row filter-row">                    
					<div class="col-sm-3 col-3  m-b-30">
						<div class="dataTables_length" id="DataTables_Table_0_length">
							<label>Pesquisa por CPF:
								<input type="text" class="filtrocpf form-control form-control-sm">
							</label>
						</div>
					</div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0">
								<thead>
									<tr>
										<th>Nome</th>
										<th>CPF</th>
										<th>Data Nascimento</th>
										<th>Profissão</th>
										<th>Sexo</th>
										<th class="text-right">Opções</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($clienteLista as $cliente) { ?>
									<tr id="linha<?php echo $cliente->idCliente; ?>">
										<td>
											
											<img width="28" height="28" src="<?php							   
											if ($cliente->cliFoto == "") {
												if($cliente->cliWebcam == ""){
													echo URL."assets/img/cliente/usuario.jpg";
												}else{
													echo $cliente->cliWebcam;
												}
											}else{
												echo URL."assets/img/cliente/".$cliente->cliFoto;
										   	} 
											?>" title="Perfil <?php if (isset($cliente->cliNome)) echo htmlspecialchars($cliente->cliNome, ENT_QUOTES, 'UTF-8'); ?>" class="rounded-circle m-r-5">
											<?php if (isset($cliente->cliNome)) echo htmlspecialchars($cliente->cliNome, ENT_QUOTES, 'UTF-8'); ?>
											
										</td>
										<td><?php if (isset($cliente->cliCPF)) echo htmlspecialchars($cliente->cliCPF, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($cliente->cliNasci)) echo htmlspecialchars($cliente->cliNasci, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php echo $cliente->cliProfissao; ?></td>
										<td><?php echo $cliente->cliSexo; ?></td>
														
										<td class="text-right">	
										<a class="dropdown-item" href="<?php echo URL; ?>cliente/perfilhistorico/<?php echo $cliente->idCliente; ?>"><i class="fa fa-eye m-r-5"></i>Visualizar</a>
																					
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
            

<div class="sidebar-overlay" data-reff=""></div>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Enviar mensagem</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body modal-body-formgeral">
        ...
        </div>
    </div>
  </div>
</div>