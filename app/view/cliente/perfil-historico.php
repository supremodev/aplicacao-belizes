<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="alert alert-danger alert-dismissible notificacao-false fade show" role="alert" style="display:none;width: 100%;">
                        <span class="texto-ajax">Erro ao Cadastrar Avaliação Geral.</span>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
                    </div>
                    
					<div class="alert alert-success alert-dismissible notificacao-true fade show" role="alert" style="display:none;width: 100%;">
						<span class="texto-ajax">Registro Cadastrado com Sucesso.</span>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
                    <div class="col-sm-7 col-4">
                        <h4 class="page-title">Perfil: <?php echo htmlspecialchars($perfilLista->cliNome, ENT_QUOTES, 'UTF-8'); ?></h4>
                    </div>

                   
                </div>
                <div class="card-box profile-header">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-view">
                                <div class="profile-img-wrap">
                                    <div class="profile-img">
                                        <a href="#">
											<img class="avatar" src="<?php echo URL; ?>assets/img/cliente/<?php 
																		 if($perfilLista->cliFoto == ""){
																			 echo "user.jpg";
																		 }else{
																			echo $perfilLista->cliFoto; 
																		 }
																	 ?>" alt="<?php echo htmlspecialchars($perfilLista->cliNome, ENT_QUOTES, 'UTF-8'); ?>">
										</a>
									</div>
                                </div>
                                <div class="profile-basic">
                                    <div class="row">                                        
                                        <div class="col-md-6">
                                            <ul class="personal-info profile-info-left">
                                                <li>
                                                    <span class="title">CPF:</span>
                                                    <span class="text"><?php echo $perfilLista->cliCPF; ?></span>
                                                </li>
                                                <li>
                                                    <span class="title">Nascimento:</span>
                                                    <span class="text"><?php echo htmlspecialchars($perfilLista->cliNasci, ENT_QUOTES, 'UTF-8'); ?></span>
                                                </li>
                                                <li>
                                                    <span class="title">Sexo:</span>
                                                    <span class="text"><?php echo htmlspecialchars($perfilLista->cliSexo, ENT_QUOTES, 'UTF-8'); ?></span>
                                                </li>
												<li>
													<span class="title">Estado Civil:</span>
													<span class="text"><?php echo htmlspecialchars($perfilLista->cliEstadoCivil, ENT_QUOTES, 'UTF-8'); ?></span>
												</li>
                                                <li>
                                                    <span class="title">Profissão:</span>
                                                    <span class="text"><?php echo $perfilLista->cliProfissao; ?></span>
                                                </li>
                                            </ul>
                                        </div>
										<div class="col-md-6">
											
										</div>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                    </div>
                </div>
				
				<div class="profile-tabs">
					<ul class="nav nav-tabs nav-tabs-bottom">						
						<li class="nav-item"><a class="nav-link" href="#tabHistorico" data-toggle="tab">Histórico</a></li>					
						<li class="nav-item"><a class="nav-link" href="#tabAvaliacaoGeral" data-toggle="tab">Avaliação Geral</a></li>
                        <li class="nav-item"><a class="nav-link" href="#tabCovid19" data-toggle="tab">Ficha Anamnese Covid-19</a></li>
                        <li class="nav-item"><a class="nav-link" href="#tabProntuario" data-toggle="tab">Prontuário</a></li>												
					</ul>

					<div class="tab-content">
						<div class="tab-pane show active" id="tabCompromissos">
							<div class="row">
								<div class="col-md-12">
									<div class="card-box">
                                        <h3 class="card-title">Compromissos <?php echo $DataAtual;?></h3> 
                                        <div class="table-responsive">
                                        <table class="table table-border table-striped custom-table datatable m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Especialista</th>
                                                    <th>Data / Hora</th>
                                                    <th>Status</th>
                                                    <th>Visualizar</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($agendaLista  as $agenda) { ?>
                                                <tr>
                                                    <td><?php if (isset($agenda->funcNome)) echo htmlspecialchars($agenda->funcNome, ENT_QUOTES, 'UTF-8'); ?></td>
                                                    <td>
														<?php echo htmlspecialchars(date("d/m/Y", strtotime($agenda->agendaData))); ?>
                                                        - 
                                                        <?php if (isset($agenda->agendaHora)) echo htmlspecialchars($agenda->agendaHora, ENT_QUOTES, 'UTF-8'); ?>
                                                    </td>
													 
                                                     
                                                    <td>
                                                            <?php if($agenda->agendaStatus == 1){ ?>
                                                                <span class="custom-badge status-green">Confirmado</span>
                                                            <?php }else if($agenda->agendaStatus == 2){ ?>
                                                                <span class="custom-badge status-orange">Em Andamento</span>
                                                            <?php }else if($agenda->agendaStatus == 3){ ?>
                                                                <span class="custom-badge status-grey">Finalizado</span>
                                                            <?php }else { ?>
                                                                <span class="custom-badge status-red">Cancelado</span>
                                                            <?php }; ?>
                                                    </td>
                                                    <td>
                                                        <a class="dropdown-item"  href="<?php echo URL . "agenda/visualizarcliente/" . $agenda->idAgenda;?>">
                                                            <i class="fa fa-eye m-r-5"></i>Observar
                                                        </a>
                                                    </td>
                                                    
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>                                       
									</div>
								</div>
							</div>							
						</div>	
                        <div class="tab-pane show" id="tabHistorico">
							<div class="row">
								
							<!-- 
							#############################################################################
							#############################################################################
							###################		AVALIAÇÃO GERAL ( ANAMSESE )     ####################
							#############################################################################
							############################################################################# 
							-->
								<div class="col-md-12">
									<div class="card-box">
										<h3 class="card-title">Histórico Avaliação Geral</h3>
                                        <div class="table-responsive">
                                            <table class="table table-border table-striped custom-table datatable m-b-0">
                                                <thead>
                                                    <tr>
                                                        <th>Data</th>
                                                        <th>Hora</th>
                                                        <th class="text-right">Ver</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($FichaGeralListaData as $ficha) { ?>
                                                    <tr>
                                                        <td><?php echo htmlspecialchars(date("d/m/Y", strtotime($ficha->fichaData))); ?></td>
                                                        <td><?php if (isset($ficha->fichaHora)) echo htmlspecialchars($ficha->fichaHora, ENT_QUOTES, 'UTF-8'); ?></td>
                                                        
                                                        <td class="text-right">
                                                            <a class="dropdown-item carregar-form" titulo="Avaliação Geral Histórico" destino="cliente/formGeralHistorico/<?php echo $ficha->idFichaGeral; ?>" href="#" data-toggle="modal" data-target=".bd-example-modal-lg" >
                                                                <i class="fa fa-eye m-r-5"></i>Observar
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
									</div>
								</div>
							<!-- 
							#############################################################################
							#############################################################################
							#######################		AVALIAÇÃO CORPORAL        #######################
							#############################################################################
							############################################################################# 
							-->
								<div class="col-md-12">
									<div class="card-box">
										<h3 class="card-title">Histórico Avaliação Corporal</h3>
                                        <div class="table-responsive">
                                            <table class="table table-border table-striped custom-table datatable m-b-0">
                                                <thead>
                                                    <tr>
                                                        <th>Data</th>
                                                        <th>Hora</th>
                                                        <th class="text-right">Ver</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($FichaCorporalListaData as $ficha) { ?>
                                                    <tr>
                                                        <td><?php echo htmlspecialchars(date("d/m/Y", strtotime($ficha->fichaCorporalData))); ?></td>
                                                        <td><?php if (isset($ficha->fichaCorporalHora)) echo htmlspecialchars($ficha->fichaCorporalHora, ENT_QUOTES, 'UTF-8'); ?></td>
                                                        
                                                        <td class="text-right">
                                                            <a class="dropdown-item carregar-form" titulo="Avaliação Corporal Histórico" destino="cliente/visualizarcorporal/<?php echo $ficha->idFichaCorporal; ?>" href="#" data-toggle="modal" data-target=".bd-example-modal-lg" >
                                                                <i class="fa fa-eye m-r-5"></i>Observar
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
									</div>
								</div>
								
							<!-- 
							#############################################################################
							#############################################################################
							#######################		 AVALIAÇÃO FACIAL     ###########################
							#############################################################################
							############################################################################# 
							-->
								<div class="col-md-12">
									<div class="card-box">
										<h3 class="card-title">Histórico Avaliação Facial</h3>
                                        <div class="table-responsive">
                                            <table class="table table-border table-striped custom-table datatable m-b-0">
                                                <thead>
                                                    <tr>
                                                        <th>Data</th>
                                                        <th>Hora</th>
                                                        <th class="text-right">Ver</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($FichaFacialListaData as $ficha) { ?>
                                                    <tr>
                                                        <td><?php echo htmlspecialchars(date("d/m/Y", strtotime($ficha->fichaFacilData))); ?></td>
                                                        <td><?php if (isset($ficha->fichaFacilHora)) echo htmlspecialchars($ficha->fichaFacilHora, ENT_QUOTES, 'UTF-8'); ?></td>
                                                        
                                                        <td class="text-right">
                                                            <a class="dropdown-item carregar-form" titulo="Avaliação Facial Histórico" destino="cliente/visualizarfacial/<?php echo $ficha->idFichaFacil; ?>" href="#" data-toggle="modal" data-target=".bd-example-modal-lg" >
                                                                <i class="fa fa-eye m-r-5"></i>Observar
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
									</div>
								</div>
							<!--
							#############################################################################
							#############################################################################
							#######################		ANAMNESE CRIOLIPOLISE     #######################
							#############################################################################
							############################################################################# 
							-->
								<div class="col-md-12">
									<div class="card-box">
										<h3 class="card-title">Histórico Avaliação Anamnese Criolipolise </h3>
                                        <div class="table-responsive">
                                            <table class="table table-border table-striped custom-table datatable m-b-0">
                                                <thead>
                                                    <tr>
                                                        <th>Data</th>
                                                        <th>Hora</th>
                                                        <th class="text-right">Ver</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($FichaAnamneseListaData as $ficha) { ?>
                                                    <tr>
                                                        <td><?php echo htmlspecialchars(date("d/m/Y", strtotime($ficha->fichaAnamneseData))); ?></td>
                                                        <td><?php if (isset($ficha->fichaAnamneseHora)) echo htmlspecialchars($ficha->fichaAnamneseHora, ENT_QUOTES, 'UTF-8'); ?></td>
                                                        
                                                        <td class="text-right">
                                                            <a class="dropdown-item carregar-form" titulo="Avaliação Anamnese Criolipolise" destino="cliente/visualizarAnamneseCriolipolise/<?php echo $ficha->idFichaAnamneseCriolipolise; ?>" href="#" data-toggle="modal" data-target=".bd-example-modal-lg" >
                                                                <i class="fa fa-eye m-r-5"></i>Observar
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
									</div>
								</div>
							<!--
							#############################################################################
							#############################################################################
							#########################		ANAMNESE COVID-19    ########################
							#############################################################################
							############################################################################# 
							-->
                                <div class="col-md-12">
									<div class="card-box">
										<h3 class="card-title">Histórico Ficha Anamnese Covid-19</h3>
                                        <div class="table-responsive">
                                            <table class="table table-border table-striped custom-table datatable m-b-0">
                                                <thead>
                                                    <tr>
                                                        <th>Data</th>
                                                        <th>Hora</th>
                                                        <th class="text-right">Ver</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($fichaCovidListaData as $ficha) { ?>
                                                    <tr>
                                                        <td><?php echo htmlspecialchars(date("d/m/Y", strtotime($ficha->fichaAnamneseCovid19Data))); ?></td>
                                                        <td><?php if (isset($ficha->fichaAnamneseCovid19Hora)) echo htmlspecialchars($ficha->fichaAnamneseCovid19Hora, ENT_QUOTES, 'UTF-8'); ?></td>
                                                        
                                                        <td class="text-right">
                                                            <a class="dropdown-item carregar-form" titulo="Covid Histórico" destino="cliente/formCovidHistorico/<?php echo $ficha->idFichaAnamneseCovid19; ?>" href="#" data-toggle="modal" data-target=".bd-example-modal-lg" >
                                                                <i class="fa fa-eye m-r-5"></i>Observar
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
									</div>
								</div>
								
							<!--
							#############################################################################
							#############################################################################
							############################		CONTRATO    #############################
							#############################################################################
							############################################################################# 
							-->
								<div class="col-md-12">
									<div class="card-box">
										<h3 class="card-title">Histórico Contrato</h3>
                                        <div class="table-responsive">
                                            <table class="table table-border table-striped custom-table datatable m-b-0">
                                                <thead>
                                                    <tr>
                                                        <th>Data</th>
                                                        <th class="text-right">Ver</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($contratoListaData as $contrato) { ?>
                                                    <tr>
                                                        <td><?php if (isset($contrato->contratoData)) echo htmlspecialchars($contrato->contratoData, ENT_QUOTES, 'UTF-8'); ?></td>
                                                                                                                
                                                        <td class="text-right">
                                                            <a class="dropdown-item carregar-form" titulo="Contrato Histórico" destino="ficha/formFichaContratoCliente/<?php echo $contrato->idContrato; ?>" href="#" data-toggle="modal" data-target=".bd-example-modal-lg" >
                                                                <i class="fa fa-eye m-r-5"></i>Observar
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
									</div>
								</div>
                                
							</div>							
						</div>	
						<div class="tab-pane" id="tabAvaliacaoGeral">
							<div class="row">
								<div class="col-md-12">
                                <?php $this->formGeral($id);?>
									<div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
										Erro ao Cadastrar Avaliação Geral.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
									<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
										Registro Cadastrado com Sucesso.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tabCovid19">
							<div class="row">
								<div class="col-md-12">
                                    <?php if (empty($fichaCovidLista)) {?>
									
                                    <form id="formCovid">
                                        <div class="card-box">
                                            <h3 class="card-title">FICHA ANAMNESE COVID-19</h3>											
                                            <div class="row ficha">
                                                
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Já Fez Teste Covid-19</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19JaFezTesteCovid19" id="" value="1" >
                                                            <label class="form-check-label" for="fichaAnamneseCovid19JaFezTesteCovid19">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19JaFezTesteCovid19" id="" value="0" >
                                                            <label class="form-check-label" for="fichaAnamneseCovid19JaFezTesteCovid19">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Qual?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QualTesteCovid19" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Resultado?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19Resultado" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Quanto Tempo Faz Que Fez Teste?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QuantoTempoFazQueFezTeste" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Observações:</label>
                                                        <textarea class="form-control floating" rows="3" cols="30" name="fichaAnamneseCovid19ObsTesteCovid19" ></textarea>
                                                    </div>								
                                                </div>                         

                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Teve Contato Com Pessoas Teste Covid-19 Positivo?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo" id="" value="1">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo" id="" value="0" >
                                                            <label class="form-check-label" for="fichaAnamneseCovid19TeveContatoComPessoasTesteCovid19Positivo">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Quanto Tempo Faz Que Fez Teste?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QuantoTempoFazContatoComPessoas" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-box">
                                            <h3 class="card-title">Marque a caixa se Estiver Sentindo Qualquer Desses Sintomas</h3>
                                            <div class="row ficha">
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19FebreQuantoTempoFaz"> Febre:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Febre? QuantoTempoFaz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19FebreQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19TosseQuantoTempoFaz" > Tosse:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Tosse? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19TosseQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz" > Dor De Garganta:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Dor De Garganta? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19DorDeGargantaQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19FaltaDeArQuantoTempoFaz"> Falta De Ar:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Falta De Ar? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19FaltaDeArQuantoTempoFaz"  value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DiarreiaQuantoTempoFaz"> Diarreia:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Diarreia? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19DiarreiaQuantoTempoFaz" value="" >
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19CorizaQuantoTempoFaz"> Coriza:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Coriza? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19CorizaQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz"> Dor De Cabeça:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Dor De Cabeça? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19DorDeCabecaQuantoTempoFaz" value="" >
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz" > Dores No Corpo:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Dores No Corpo? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19DoresNoCorpoQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz"> Dores Nas Costas:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Dores Nas Costas? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19DoresNasCostasQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19CansacoQuantoTempoFaz"> Cansaço:
                                                    </label>
                                                </div>-->
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Cansaço? Quanto Tempo Faz?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19CansacoQuantoTempoFaz" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                </div>
                                                
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">É Fumante?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19eFumante" id="" value="1" >
                                                            <label class="form-check-label" for="fichaHipotireoidismo">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19eFumante" id="" value="0">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19eFumante">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Quanto Tempo?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QuantoTempoEFumante" value="">
                                                    </div>
                                                </div>

                                                
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Tem Algum Vicio?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TemAlgumVicio" id="" value="1">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19TemAlgumVicio">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TemAlgumVicio" id="" value="0">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19TemAlgumVicio">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Qual?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QualAlgumVicio" value="" >
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <hr>
                                                </div>

                                                
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Tem Algum Problema De Saúde?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TemAlgumProblemaDeSaude" id="" value="1">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19TemAlgumProblemaDeSaude">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19TemAlgumProblemaDeSaude" id="" value="0">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19TemAlgumProblemaDeSaude">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Qual?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QualProblemaDeSaude" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19Diabete" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19Diabete" value="1" > Diabete
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DoencaCardiaca" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DoencaCardiaca" value="1" > Doença Cardíaca
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19ProblemaRenal" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19ProblemaRenal" value="1" > Problema Renal
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DoencaRespiratotiaCronica" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19DoencaRespiratotiaCronica" value="1" > Doença Respiratória Crônica
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19RiniteSinusite" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19RiniteSinusite" value="1" > Rinite / Sinusite
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19PressaoAlta" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19PressaoAlta" value="1" > Pressão Alta
                                                    </label>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>
                                                        <input type="checkbox" name="fichaAnamneseCovid19Asma" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaAnamneseCovid19Asma" value="1" > Asma
                                                    </label>
                                                </div>
                                                <div class="col-md-12">
                                                    <hr>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Nos Últimos Três Meses, Seguiu Quarenta Corretamente?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament" id="" value="1" >
                                                            <label class="form-check-label" for="">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19NosUltimosTresMesesSeguiuQuarentaCorretament" id="" value="0">
                                                            <label class="form-check-label" for="">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Se Trabalha, Já Voltou As Atividades Normais?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19SeTrabalhaJaVoltouAsAtividadesNormais" value="">
                                                    </div>
                                                </div>
                                               
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Mora Com Familiares? Os Mesmo Já Voltaram Para Suas Atividades Normal?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19MoraComFamiliares" id="" value="1">
                                                            <label class="form-check-label" for="fichaAnamneseCovid19MoraComFamiliares">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19MoraComFamiliares" id="" value="0" >
                                                            <label class="form-check-label" for="fichaAnamneseCovid19MoraComFamiliares">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                </div>

                                                
                                                <div class="col-md-2">
                                                    <div class="form-group ">
                                                        <label class="label-block">Você ou Familiar que moram na mesma residência trabalham aa Área Da Saúde?</label>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude" id="" value="1" >
                                                            <label class="form-check-label" for="fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude">
                                                            Sim
                                                            </label>
                                                        </div>
                                                        <div class="form-group form-check form-check-inline">
                                                            <input class="validar form-check-input" type="radio" name="fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude" id="" value="0" >
                                                            <label class="form-check-label" for="fichaAnamneseCovid19FamiliarMoramNaResidenciaAreaDaSaude">
                                                            Não
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Qual?</label>
                                                        <input type="text" class="form-control floating" name="fichaAnamneseCovid19QualTrabalhamNaAreaDaSaude" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-box">
                                            <h3 class="card-title">Eu: </h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group ">
                                                        <label class="focus-label">Nome:*</label>
                                                        <input type="text" class="form-control floating" value="<?php echo $perfilLista->cliNome;?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <p>Respondi com total sinceridade, estando ciente que o mesmo, caso eu tenha falado algo que não procede, sou responsável por qualquer irresponsabilidade que venha ocorrer nesse estabelecimento, por erro que eu venha causar, assim não colocando em risco a minha saúde e nem a saúde dos profissionais. As declarações acima são expressão da verdade, não cabendo ao estabelecimento nenhuma responsabilidade por fatos omitido ou falso.</p>
                                                    <blockquote>
                                                        <p class="mb-0">Código Penal - Decreto-Lei No 2.848, De 7 De Dezembro De 1940.<br>
                                                                        Infração De Medida Sanitária Preventiva<br>
                                                                        Art. 268 - Infringir Determinação Do Poder Público, Destinada A Impedir Introdução Ou Propagação De Doença Contagiosa:</p>
                                                    </blockquote>
                                                    <blockquote>
                                                        <p class="mb-0">Pena - Detenção, De Um Mês A Um Ano, E Multa.<br>
                                                                        Parágrafo Único - A Pena É Aumentada De Um Terço, Se O Agente É Funcionário Da Saúde Pública Ou Exerce A Profissão Na Área Da Saúde.</p>
                                                    </blockquote>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="card-box">
                                            <h3 class="card-title">Li E Concordo Com Os Termos Descritos Nesta Ficha De Anamnese Em Relação Covid-19.</h3>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group profile-img-wrap fichaAssinatura covid19">
                                                        <img class="inline-block" src="<?php if(!empty($perfilLista->cliAssinatura)){echo $perfilLista->cliAssinatura;}else{echo URL . 'assets/img/assinatura.png';}?>" alt="user">
                                                        
                                                    </div>							
                                                </div>
                                                <div class="col-md-6">
                                                        <div class="form-group ">
                                                            <label class="focus-label">CPF:*</label>
                                                            <input type="text" class="form-control floating" value="<?php echo $perfilLista->cliCPF;?>">
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="focus-label">RG:*</label>
                                                            <input type="text" class="form-control floating" value="<?php echo $perfilLista->cliRG;?>">
                                                            <input type="text" class="form-control floating" name="idCliente" value="<?php echo $perfilLista->idCliente;?>" hidden>
                                                        </div>
                                                </div>
                                            </div>                       
                                        </div>
                                        <div class="card-box">
                                            <div class="text-center m-t-20">
                                                <button destino="ficha/inserirFichaAnamneseCovid19" class="inserir-ajax-ficha-covid btn btn-primary submit-btn" type="button">Cadastrar Avaliação Geral</button>
                                            </div>
                                        </div>
                                    </form>

                                    <?php } elseif($perfilLista->cliPermitirAtualizacaoCovid){
                                        $this->permitirAtualizacaoConvid = true;
                                        $this->formCovid($id);
                                    }else {
                                        $this->permitirAtualizacaoConvid = false;
                                        $this->formCovid($id);
                                    }; ?>
								</div>
							</div>							
                        </div>
                       
						<div class="tab-pane" id="tabDepoimento">
							<div class="row">
								<div class="col-md-12">
                                    <?php if(empty($depoimentoLista)) {?>
									<form id="formulariosDepo">									
									<div class="card-box">
										<input type="hidden" name="idCliente" value="<?php echo $perfilLista->idCliente; ?>">
										<h3 class="card-title">Deixe um depoimento para a Belize's Time</h3>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="focus-label">Depoimento: </label>
													<textarea class="form-control floating cliDepoimento" rows="5" cols="30" name="depoimentoTexto"></textarea>
												</div>								
											</div>
											<div class="col-md-12">
												<div>
													<label class="col-form-label form-group">Podemos publicar o depoimento?</label>
													<div class="col-md-2">
														<div class="form-group form-check form-check-inline">
															<input class="form-check-input" type="radio" name="depoimentoStatus" id="depoimentoStatus" value="1" checked>
															<label class="form-check-label" for="depoimentoStatus">
															Sim
															</label>
														</div>
														<div class="form-group form-check form-check-inline">
															<input class="form-check-input" type="radio" name="depoimentoStatus" id="depoimentoStatus" value="0" checked>
															<label class="form-check-label" for="depoimentoStatus">
															Não
															</label>
														</div>
													</div>
												</div>								
											</div>                        
										</div>
									</div>
									
                                    </form>
                                    <div class="card-box">
										<div class="text-center m-t-20">
											<button destino="cliente/depoimento" class="depoimento-ajax btn btn-primary submit-btn">Cadastrar Depoimento</button>
										</div>										
                                    </div>
                                    <?php } else {?>
                                    <form id="formularios">
                                    <?php foreach ($depoimentoLista as $linha) { ?>									
									<div class="card-box">
										
										<h3 class="card-title">Deixe um depoimento para a Belize's Time</h3>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="focus-label">Depoimento: </label>
													<textarea class="form-control floating cliDepoimento" rows="5" cols="30" name="depoimentoTexto" readonly><?php echo $linha->depoimentoTexto;?></textarea>
												</div>								
											</div>
											                       
										</div>
									</div>
									<?php }?>
                                    </form>
                                    <?php }?>
									
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tabProntuario">
							<div class="row">
								<div class="col-md-12">
                                <div class="card-box">
                                        <button class="btn" data-toggle="modal" data-target="#exampleModalCenter" style="margin-bottom: 15px;">
                                            <i class="fa fa-plus"></i> Novo prontuário
                                        </button>
										<h3 class="card-title">Histórico do prontuário</h3>
                                        <div class="table-responsive">
                                            <table class="table table-border table-striped custom-table datatable m-b-0">
                                                <thead>
                                                    <tr>
                                                        <th>Profissional</th>
                                                        <th>Data</th>
                                                        <th>Hora</th>
                                                        <th class="text-right">Opções</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($listaProntuario as $ficha) { ?>
                                                    <tr>
                                                        <td><?php echo $ficha->funcNome;?> </td>
                                                        <td><?php echo htmlspecialchars(date("d/m/Y", strtotime($ficha->prontuarioData))); ?></td>
                                                        <td>
                                                        <?php $date = date_create($ficha->prontuarioHora);
                                                        echo date_format($date, 'H:i:s'); ?>
                                                        </td>
                                                        <td class="text-right">
                                                            <?php if($ficha->idFunc == $_SESSION['idUsuario']) {?>
                                                            <a class="dropdown-item carregar-prontuario" titulo="Atualizar Prontuário" destino="prontuario/editar/<?php echo $ficha->id; ?>" href="#" data-toggle="modal" data-target=".bd-editar-modal-lg" >
                                                                <i class="fa fa-edit m-r-5"></i>Editar
                                                            </a>
                                                            <?php }?>
                                                            <a class="dropdown-item carregar-form" titulo="Prontuário Histórico" destino="prontuario/show/<?php echo $ficha->id; ?>" href="#" data-toggle="modal" data-target=".bd-example-modal-lg" >
                                                                <i class="fa fa-eye m-r-5"></i>Observar
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
									</div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>

<div class="sidebar-overlay" data-reff=""></div>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="z-index:99999">
  <div class="modal-dialog modal-lg" style="max-width: 1280px;">
    <div class="modal-content">
        <div class="modal-header"  style="background: #3e7943;color: #fff">
            <h5 class="modal-title" id="exampleModalLongTitle">Ficha Geral</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body modal-body-formgeral">
        ...
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
    </div>
  </div>
</div>
<div class="modal fade bd-editar-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="max-width: 1280px;">
    <div class="modal-content">
        <div class="modal-header"  style="background: #3e7943;color: #fff">
            <h5 class="modal-title" id="exampleModalLongTitle">Ficha Geral</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-danger atualizar-pro-danger alert-dismissible fade show" role="alert" style="display:none">
                        Erro ao atualizar prontuário.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
        </div>
        <div class="alert alert-success atualizar-pro-success alert-dismissible fade show" role="alert" style="display:none">
                    Prontuário atualizado com sucesso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
        </div>
        <div class="modal-body modal-body-prontuario">
        ...
        </div>
        
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="button" class="atualizar-prontuario-ajax btn btn-primary">Atualizar</button>
        </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade show" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #3e7943;color: #fff">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Novo prontuário</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
                        Erro ao registrar prontuário.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
                    Prontuário registrado com sucesso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div> 
                        <form id="prontuarioform" style="margin-top: 10px;">
                            <div class="roww">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="focus-label">Profissional</label>
										<input type="text" class="form-control floating" name="fichaOqueTeIncomoda" value="<?php echo $_SESSION['userNome']?>" disabled>
                                        <input type="text" class="form-control floating" name="idFunc" value="<?php echo $_SESSION['idUsuario']?>" hidden>
                                        <input type="text" class="form-control floating" name="idCliente" value="<?php echo $id?>" hidden>
                                    </div>
                                </div>
								<div class="col-md-12">
                                    <div class="form-group">
                                        <label class="focus-label">Descrição</label>
                                        <textarea name="descricao" class="form-control floating" cols="30" rows="10"></textarea>
									</div>
								</div>
                            </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" class="novo-prontuario-ajax btn btn-primary">Salvar</button>
                </div>
            </div>
        </div>
    </div> 
    <div class="modal fade show" id="editarprontuario" tabindex="-1" role="dialog" aria-labelledby="editarprontuario">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #3e7943;color: #fff">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Atualizar prontuário</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
                        Erro ao atualizar prontuário.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
                    Prontuário atualizar com sucesso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div> 
                        <form id="prontuarioform" style="margin-top: 10px;">
                            <div class="roww">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="focus-label">Profissional</label>
										<input type="text" class="form-control floating" name="fichaOqueTeIncomoda" value="<?php echo $_SESSION['userNome']?>" disabled>dden>
                                        <input type="text" class="form-control floating" name="idProntuario" value="" hidden>
                                    </div>
                                </div>
								<div class="col-md-12">
                                    <div class="form-group">
                                        <label class="focus-label">Descrição</label>
                                        <textarea name="descricao-editar" class="form-control floating" cols="30" rows="10"></textarea>
									</div>
								</div>
                            </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" class="novo-prontuario-ajax btn btn-primary">Atualizar</button>
                </div>
            </div>
        </div>
    </div> 