
            <div class="content">

				<?php foreach ($prontuLista as $linha) { ?>
                    <form id="prontuarioform">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nome do Paciente</label>
                                    <input id="nome" type="text" name="cliNome" value="<?php echo $linha->cliNome;?>" class="form-control floating" readonly="">
                                    <input type="text" name="idProntuario" value="<?php echo $linha->id;?>" hidden>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Especialista</label>
                                    <div>
                                        <input type="text" class="form-control" name="especialista" value="<?php echo $linha->funcNome;?>" readonly="">
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Data</label>
                                    <div>
                                        <input type="text" class="form-control" name="" value="<?php echo date("d/m/Y", strtotime($linha->prontuarioData)); ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Hora</label>
                                    <div>
                                        <input type="time" class="form-control" name="" value="<?php $date = date_create($linha->prontuarioHora);
                                                        echo date_format($date, 'H:i:s'); ?>" readonly="">
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Descrição:</label>
                                    <textarea cols="30" rows="8" id="teste" class="form-control" name="descricao"><?php echo $linha->descricao;?></textarea>
                                   
                                </div>
                            </div>
                        </div>
                    </form>
				<?php } ?>
            </div>
			
			
