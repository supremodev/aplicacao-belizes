<div class="card-box">
    <h3 class="card-title">CORPO POSIÇÃO ANATÔMICA: </h3>
    <div class="row blococorporal">
        <div id="corpo-ajax" class="col-md-6">
            <?php if ($FichaGeralLista[0]->cliSexo == "Masculino") {
                require APP . 'view/corpo/homem.php';
            } else {
                require APP . 'view/corpo/mulher.php';
            } ?>

        </div>
        <div class="col-md-6">
            <ol class="list-number-rounded lista-corpo">
                <?php foreach ($listaMarcacaoCorporal as $linha) { ?>
                    <li class="obsMarcacao"><a><?php echo $linha->obsCorporal; ?></a></li>
                <?php } ?>
            </ol>
        </div>

    </div>
</div>