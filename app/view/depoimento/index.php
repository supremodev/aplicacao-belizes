<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">Depoimento</h4>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0">
								<thead>
									<tr>
										<th>Nome</th>
										<th>Depoimento</th>
										<th>Status</th>
										<th class="text-right">Opções</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($depoimentoLista as $linha) { ?>
									<tr>
										<td><?php echo $linha->cliNome; ?></td>
										<td><?php echo $linha->depoimentoTexto; ?></td>
										
										<td>
											<?php if($linha->depoimentoStatus == 1){ ?>
												<span class="custom-badge status-green">Aprovado</span>
											<?php }else{ ?>
												<span class="custom-badge status-red">Reprovado</span>
											<?php } ?>
										</td>
										<td class="text-right">											
											<div>
												<a class="dropdown-item" href="depoimento/analisar/<?php echo $linha->idDepo; ?>" >
													<i class="fa fa-eye m-r-5"></i>Analisar
												</a>
											</div>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>
            

<div class="sidebar-overlay" data-reff=""></div>