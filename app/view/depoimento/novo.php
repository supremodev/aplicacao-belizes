<div class="page-wrapper">
            <div class="content">
            <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
				<span class="texto-ajax">Erro ao salvar.</span>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
                <span class="texto-ajax">Salvo com sucesso.</span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
                <div class="row">
                    <div class="col-md-8 ">
                        <h4 class="page-title">Analise do depoimento</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 ">
                        <form id="formularios">
                        <?php foreach ($depoimentoLista as $linha) { ?>
                        <div class="row">
                            <div class="col-md-12">
								<div class="form-group">
									<label class="focus-label">Nome:</label>
									<input class="form-control floating" name="clinome" value="<?php echo $linha->cliNome; ?>" readonly>
								</div>								
							</div>
											<div class="col-md-12">
												<div class="form-group">
													<label class="focus-label">Depoimento:</label>
													<textarea class="form-control floating cliDepoimento" rows="5" cols="30" name="depoimentoTexto" readonly><?php echo $linha->depoimentoTexto; ?></textarea>
												</div>								
											</div>
											<div class="col-md-12">
												<div>
													<label class="col-form-label form-group">Depoimento aprovado?</label>
													<div class="col-md-2">
														<div class="form-group form-check form-check-inline">
															<input class="form-check-input" type="radio" name="depoimentoStatus" id="depoimentoStatus" value="1" >
															<label class="form-check-label" for="depoimentoStatus">
															Sim
															<i class="input-helper"></i></label>
														</div>
														<div class="form-group form-check form-check-inline">
															<input class="form-check-input" type="radio" name="depoimentoStatus" id="depoimentoStatus" value="0" >
															<label class="form-check-label" for="depoimentoStatus">
															Não
															<i class="input-helper"></i></label>
														</div>
													</div>
												</div>								
											</div>  
                                                                  
							</div>  
                        <?php } ?>     
						</form>
						<div class="col-md-12">
												<div class="form-group">
                                                <div class="m-t-20 text-center">
                                                    <button destino="depoimento/atualizarEstatus/<?php echo $linha->idDepo; ?>" class="atualizar-ajax btn btn-primary submit-btn">Salvar</button>
                                                </div>
												</div>								
											</div>                            
                    </div>
                    </div>
                </div>
            </div>

	
	
	
<div class="sidebar-overlay" data-reff=""></div>   
