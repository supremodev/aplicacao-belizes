<html lang="pt-BR">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>Cadastro cliente</title>
</head>

<body translate="no" bgcolor="#eeeeee" style="width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; margin: 0; padding: 0;background: #efefef;">
    <title>Cadastro Cliente</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1; maximum-scale=1.0">
    <style type="text/css">
        @media only screen and (max-width: 640px) {
            table[class="devicewidth"] {
                width: 440px !important;
                text-align: center !important;
            }

            table[class="devicewidthinner"] {
                width: 380px !important;
                text-align: center !important;
            }

            table[class="sthide"] {
                display: none !important;
            }

            img[class="bigimage"] {
                width: 100% !important;
                height: auto !important;
            }

            img[class="col2img"] {
                width: 420px !important;
                height: 258px !important;
            }

            img[class="image-banner"] {
                width: 440px !important;
                height: 106px !important;
            }

            td[class="menu"] {
                text-align: center !important;
                padding: 10px 0 10px 0 !important;
            }

            td[class="logo"] {
                padding: 0px 0 20px 0 !important;
                margin: 0 auto !important;
            }

            img[class="logo"] {
                padding: 0 !important;
                margin: 0 auto !important;
            }
        }

        @media only screen and (max-width: 480px) {
            table[class="devicewidth"] {
                width: 320px !important;
                text-align: center !important;
            }

            table[class="devicewidthinner"] {
                width: 260px !important;
                text-align: center !important;
            }

            table[class="sthide"] {
                display: none !important;
            }

            img[class="bigimage"] {
                width: 100% !important;
                height: auto !important;
            }

            img[class="col2img"] {
                width: 260px !important;
                height: 160px !important;
            }

            img[class="image-banner"] {
                width: 280px !important;
                height: 68px !important;
            }
        }
    </style>
    <div class="block" style="background: #3E7943;">
        <table width="100%" bgcolor="#eeeeee" cellpadding="0" cellspacing="0" border="0" id="backgroundTable"
            st-sortable="header"
            style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin: 0; padding: 0; width: 100% !important; line-height: 100% !important; ">
            <tbody>
                <tr>
                    <td width="100%" style="border-collapse: collapse; "></td>
                </tr>

            </tbody>
        </table>
        <table width="50%" bgcolor="#3E7943" cellpadding="0" cellspacing="0" border="0" align="center"
            class="devicewidth" hlitebg="edit" shadow="edit"
            style="border-collapse: collapse; border-radius: 3px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;background:#3E7943;">
            <tbody>
                <tr>
                    <td align="center" valign="middle" width="270"
                        style="border-collapse: collapse; padding: 30px 0 30px 0px;">
                        <a title="OneTrust"
                            style="color: white; border-bottom: 1px solid transparent; text-decoration: none; font-family: 'Open Sans', arial, sans-serif; font-weight: 400; line-height: 25px;"
                            href="https://www.belizestime.com.br/">
                            <span style="font-size: 50px;">Belize's Time</span>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="block">
            <table bgcolor="#ffffff" width="50%" cellpadding="30" cellspacing="0" border="0" align="center" class="devicewidth" modulebg="edit" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-radius: 3px;">
                <tbody>
                    <tr>
                        <td style="border-collapse: collapse; font-family: OpenSans, arial, sans-serif; font-size: 30px; color: #333333; text-align:center;line-height: 34px; padding-bottom: 0px; padding-top: 30px;" st-content="fulltext-paragraph">Seja bem-vindo a Belize's Time</td>
                    </tr>
                    <tr>
                        <td style="border-collapse: collapse; font-family: OpenSans, arial, sans-serif; font-size: 17px; color: #999999; text-align:center;line-height: 30px; padding-bottom: 0px; padding-top: 30px;" st-content="fulltext-paragraph">Ol&aacute; [nome], Esperamos que tenha uma &oacute;tima experi&ecirc;ncia com nossa aplica&ccedil;&atilde;o.
                        </td>
                    </tr>
                    <tr>
                        <td style="border-collapse: collapse; font-family: OpenSans, arial, sans-serif; font-size: 17px; color: #999999; text-align:center;line-height: 30px; padding-bottom: 0px; padding-top: 30px;" st-content="fulltext-paragraph">Acesso a aplica&ccedil;&atilde;o, dados a abaixo:
                        </td>
                    </tr>
                    <tr>
                        <td style="border-collapse: collapse; font-family: OpenSans, arial, sans-serif; font-size: 17px; color: #999999; text-align:center;line-height: 30px; padding-bottom: 0px; padding-top: 30px;" st-content="fulltext-paragraph">Usu&aacute;rio: [nome] </br> Senha: [senha] </br> <a href="[link]" target="_blank" rel="noopener noreferrer">Fazer Login</a>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-collapse: collapse; font-family: OpenSans, arial, sans-serif; font-size: 14px; color: #999999; line-height: 30px; padding-bottom: 0px; padding-top: 30px;" st-content="fulltext-paragraph">
                            Qualquer d&uacute;vida estamos &agrave; disposi&ccedil;&atilde;o.</br>

                            Abraços e sucesso!</br>

                            Equipe de sucesso,
                            Belize's Time
                        </td>
                    </tr>
                    <tr>
                        <td style="border-collapse: collapse; font-family: OpenSans, arial, sans-serif; font-size: 14px; color: #999999; line-height: 30px; padding-bottom: 0px; padding-top: 30px;" st-content="fulltext-paragraph">Esse é um e-mail automático. Não responda esse e-mail, caso tenha alguma d&uacute;vida estamos a disposi&ccedil;&atilde;o.
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" height="40" style="border-collapse:collapse"></td>
                    </tr>
            </table>
            <table width="100%" bgcolor="#eeeeee" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin: 0; padding: 0; width: 100% !important; line-height: 100% !important; ">
                <tbody>
                    <tr>
                        <td width="100%" height="40" style="border-collapse: collapse; "></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>