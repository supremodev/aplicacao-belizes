<h3 class="card-title">Informação básica para envio.</h3>
                <div class="row">
                    <div class="col-md-12">	
                    <div class="modal-body">
                        <form id="formcontato">
                            <div class="row">
                                <div class="col-md-4 ">
                                    <div class="form-group">
                                        <label>Cliente/Paciente</label>
                                        <input type="text" id="nome" name="nome" class="form-control contatoEmail" value="<?php echo $Cliente[0]->cliNome;?>">
                                    </div>
                                </div>
                                <div class="col-md-4 ">
                                    <label class="focus-label">Para (WhatsApp ou Email)</label>
                                    <select id="selecionarEnvioContato" class="select form-control floating" name="email">
                                        <option value=""></option>
                                        <option value="<?php echo $Cliente[0]->cliCel;?>">WhatsApp</option>
                                        <option value="<?php echo $Cliente[0]->cliEmail;?>">Email</option>
                                    </select>
                                    
                                </div>
                                <div class="col-md-4 ">
                                    <div class="form-group">
                                        <label>Assunto</label>
                                        <input id="assunto" type="text" name="assunto" class="form-control contatoAssunto">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label>Mensagem</label>
                                        <textarea id="mens" name="msg" class="form-control" rows="4"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>    
                    </div>
                    <div class="modal-footer">
                        <button id="enviocontato" class="btn btn-outline-success" style="display:none">Enviar</button>
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
                    </div>					
                        

                    </div>
                </div>