<div class="page-wrapper">
    <div class="content">
        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
        Erro ao enviar E-mail.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
        E-mail enviado com Sucess.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">E-mail</h4>
            </div>
        </div>

            <div class="card-box">
                <h3 class="card-title">Informação básica para envio.</h3>
                <div class="row">
                    <div class="col-md-12">	
                    <div class="modal-body">
                        <form id="formulariosContato">
                            <div class="row">
                                <div class="col-md-4 ">
                                    <div class="form-group">
                                        <label>Cliente/Paciente</label>
                                        <input type="text" name="email" class="form-control contatoEmail">
                                    </div>
                                </div>
                                <div class="col-md-4 ">
                                    <label class="focus-label">Para (WhatsApp ou Email)</label>
                                    <select class="select form-control floating" name="funcSexo">
                                        <option></option>
                                        <option value="Masculino">WhatsApp</option>
                                        <option value="Feminino">Email</option>
                                    </select>
                                    
                                </div>
                                <div class="col-md-4 ">
                                    <div class="form-group">
                                        <label>Assunto</label>
                                        <input type="text" name="assunto" class="form-control contatoAssunto">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label>Mensagem</label>
                                        <textarea name="msg" class="form-control" rows="4"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>    
                    </div>
                    <div class="modal-footer">
                        <button id="enviarEmail" class="btn btn-outline-success">Enviar</button>
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
                    </div>					
                        

                    </div>
                </div>
            </div>		

    </div>
   
<div class="sidebar-overlay" data-reff=""></div>