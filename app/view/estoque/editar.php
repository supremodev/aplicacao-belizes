<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <h4 class="page-title">Editar Registro no Estoque</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
						<?php foreach ($estoqueLista as $linha) { ?>
                        <form id="formularios">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label>Produto</label>
                                        <input class="form-control" type="text" name="estoqueProduto" value="<?php echo $linha->estoqueProduto; ?>">
                                    </div>
                                </div>
								<div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Tipo</label>
                                        <input class="form-control" name="estoqueTipo" type="text"  value="<?php echo $linha->estoqueTipo; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
								<div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Quantidade</label>
                                        <input class="form-control" type="text" name="estoqueQtd" value="<?php echo $linha->estoqueQtd; ?>">
                                    </div>
                                </div>
								<div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Usar em 15 dias</label>
                                        <input class="form-control" type="text" name="estoqueUsar15Dias" value="<?php echo $linha->estoqueUsar15Dias; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Medida</label>
                                        <select class="select" name="estoqueMedida">
                                            <option></option>
                                            <option value="ML" <?php if($linha->estoqueMedida == "ML") {echo "selected";};?>>ML</option>
                                            <option value="G" <?php if($linha->estoqueMedida == "G") {echo "selected";};?>>G</option>
                                        </select>
                                    </div>
                                </div>
								<div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Quantidade de Unidades</label>
                                        <input class="form-control" type="text" name="estoqueQtdUnidade" value="<?php echo $linha->estoqueQtdUnidade; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Data de Vencimento (Mês/Ano)</label>
                                        <div class="cal-icon">
                                            <input class="form-control" type="text" name="estoqueVencimento" value="<?php echo $linha->estoqueVencimento; ?>">
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="m-t-20 text-center">
								<button destino="estoque/atualizar/<?php echo $linha->idEstoque; ?>" class="atualizar-ajax btn btn-primary submit-btn" disabled>Atualizar Registro</button>
                            </div>
                        </form>
                    	<?php } ?>
						<div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
							Erro ao Atualizar a Registro.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
							Registro Atualizar com sucesso.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
					</div>
                </div>
            </div>

<div class="sidebar-overlay" data-reff=""></div>