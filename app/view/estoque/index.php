<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-5 col-4">
                        <h4 class="page-title">Estoque - Qtd Itens: <?php echo $qtdEstoque; ?> (ML: <?php echo $qtdEstoqueML; ?> / G: <?php echo $qtdEstoqueG; ?>)</h4>
					</div>

                    <div class="col-sm-7 col-8 text-right m-b-30">
                        <a href="<?php echo URL; ?>estoque/novo" class="btn btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i> Incluir Lançamento</a>
					</div>
					<div class="col-sm-3 col-3  m-b-30">
						<div class="dataTables_length" id="DataTables_Table_0_length">
							<label>Nome do produto:
								<input type="text" class="filtroproduto form-control form-control-sm">
							</label>
						</div>
					</div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0">
								<thead>
									<tr> 
										<th>Material</th>
                                        <th>Quantidade</th>
                                        <th>Usar em 15 dias</th>
                                        <th>Medida ( ML / G )</th>
										<th>Estoque</th>
                                        <th>Data de Vencimento</th>
										<th class="text-right">Ação</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($estoqueLista as $lista) { ?>
									<tr>
										<td><?php if (isset($lista->estoqueProduto)) echo htmlspecialchars($lista->estoqueProduto, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($lista->estoqueQtd)) echo htmlspecialchars($lista->estoqueQtd, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($lista->estoqueUsar15Dias)) echo htmlspecialchars($lista->estoqueUsar15Dias, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($lista->estoqueMedida)) echo htmlspecialchars($lista->estoqueMedida, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($lista->estoqueQtdUnidade)) echo htmlspecialchars($lista->estoqueQtdUnidade, ENT_QUOTES, 'UTF-8'); ?></td>
                                        <td>
                                            
                                            
                                            <?php
                                                // Tratar valor
                                                $mes = substr($lista->estoqueVencimento, 0, 2);
                                                $ano = substr($lista->estoqueVencimento, -4, 4);
                                                
                                                                             
												if ($ano == date("Y") and $mes - 1 == date("m")){
                                                    echo "<span class='custom-badge status-red'>" .$lista->estoqueVencimento. "</span>";
												}else{
                                                    echo "<span class='custom-badge status-green'>" .$lista->estoqueVencimento. "</span>";				
                                                }													
											?>
                                        
                                        
                                        </td>
										
                                        
                                        
                                        <td class="text-right">											
											<div>
												<a class="dropdown-item" href="<?php echo URL; ?>estoque/editar/<?php echo $lista->idEstoque; ?>">
													<i class="fa fa-pencil m-r-5"></i>Editar
												</a>
												
											</div>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>

<div class="sidebar-overlay" data-reff=""></div>