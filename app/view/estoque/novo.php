<div class="page-wrapper">
            <div class="content">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
								Erro ao cadastrar a Registro.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
								Registro cadastrada com sucesso.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <h4 class="page-title">Novo Registro no Estoque</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <form id="formularios">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label>Produto</label>
                                        <input class="form-control" name="estoqueProduto" type="text">
                                    </div>
                                </div>
								<div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Tipo</label>
                                        <input class="form-control" name="estoqueTipo" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
								<div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Quantidade</label>
                                        <input class="form-control" name="estoqueQtd" type="text">
                                    </div>
                                </div>
								<div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Usar em 15 dias</label>
                                        <input class="form-control" name="estoqueUsar15Dias" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Medida</label>
                                        <select class="select" name="estoqueMedida">
                                            <option>Selecione a Medida</option>
                                            <option value="ML">ML</option>
                                            <option value="G">G</option>
                                        </select>
                                    </div>
                                </div>
								<div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Quantidade de Unidades</label>
                                        <input class="form-control" name="estoqueQtdUnidade" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Data de Vencimento (Mês/Ano</label>
                                        <div class="cal-icon">
                                            <input class="form-control" name="estoqueVencimento" type="text">						
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="m-t-20 text-center">
								<button destino="estoque/inserir" class="novo-ajax btn btn-primary submit-btn" type="button">Cadastrar Registro</button>
                            </div>
							
                        </form>
                    </div>
                </div>
            </div>

<div class="sidebar-overlay" data-reff=""></div>   
