<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">CRIAR FICHA ANAMNESE COVID-19</h4>
                    </div>
                </div>
                <form>
                    <div class="card-box">
                        <h3 class="card-title">Informações do Paciente</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-img-wrap">
                                    <img class="inline-block" src="<?php echo URL; ?>belizer-time/admin/assets/img/user.jpg" alt="user">
                                    <div class="fileupload btn">
                                        <span class="btn-text">adicionar</span>
                                        <input class="upload" type="file">
                                    </div>
                                </div>
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Nome:*</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">CPF:*</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Email:*</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">RG:</label>
                                                <input type="text" class="form-control floating">
                                            </div>
                                        </div>
										
                                        <div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Data de Nascimento:</label>
                                                <div class="cal-icon">
                                                    <input class="form-control floating datetimepicker" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Sexo</label>
                                                <select class="select form-control floating">
                                                    <option></option>
                                                    <option value="Masculino">Masculino</option>
                                                    <option value="Feminino">Feminino</option>
                                                </select>
                                            </div>
                                        </div>
										<div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Profissão:</label>
                                                <input type="text" class="form-control floating">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
											<div class="form-group form-focus">
												<label class="focus-label">Estado Civil:</label>
												<input type="text" class="form-control floating">
											</div>
										</div>
										<div class="col-md-2">
                                           <div class="form-group form-focus">
                                                <label class="focus-label">Telefone:</label>
                                                <input type="text" class="form-control floating">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Celular:*</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										
										<div class="col-md-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Endereço:</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">CEP:</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Cidade:</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Estado:</label>
                                                <input type="text" class="form-control floating">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>							
                        </div>
                    </div>
					<div class="card-box">
						<h3 class="card-title">FICHA ANAMNESE COVID-19</h3>											
						<div class="row ficha">
							<label class="col-md-2 col-form-label form-group">Já Fez Teste Covid-19</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaEmQualLocalDoCorpo">
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Resultado?</label>
									<input type="text" class="form-control floating" name="fichaFazQuantoTempo">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto Tempo Faz Que Fez Teste?</label>
									<input type="text" class="form-control floating" name="fichaFazQuantoTempo">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group form-focus">
									<label class="focus-label">Observações:</label>
									<textarea class="form-control floating" rows="3" cols="30" name="fichaOutrasDoencas"></textarea>
								</div>								
							</div>                         


							<label class="col-md-4 col-form-label form-group">Teve Contato Com Pessoas Teste Covid-19 Positivo?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" id="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" value="1">
									<label class="form-check-label" for="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" id="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" value="0">
									<label class="form-check-label" for="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto Tempo Faz Que Fez Teste?</label>
									<input type="text" class="form-control floating" name="fichaFazQuantoTempo">
								</div>
							</div>
						</div>
					</div>
					<div class="card-box">
						<h3 class="card-title">Marque a caixa se Estiver Sentindo Qualquer Desses Sintomas</h3>
						<div class="row ficha">
							<div class="col-md-1">
								<label>
									<input type="checkbox" name="checkbox"> Febre:
								</label>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto Tempo Faz?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo">
								</div>
							</div>
							<div class="col-md-1">
								<label>
									<input type="checkbox" name="checkbox"> Tosse:
								</label>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto Tempo Faz?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo">
								</div>
							</div>
							<div class="col-md-1">
								<label>
									<input type="checkbox" name="checkbox"> Dor De Garganta:
								</label>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto Tempo Faz?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo">
								</div>
							</div>
							<div class="col-md-1">
								<label>
									<input type="checkbox" name="checkbox"> Falta De Ar:
								</label>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto Tempo Faz?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo">
								</div>
							</div>
							<div class="col-md-1">
								<label>
									<input type="checkbox" name="checkbox"> Diarreia:
								</label>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto Tempo Faz?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo">
								</div>
							</div>
							<div class="col-md-1">
								<label>
									<input type="checkbox" name="checkbox"> Coriza:
								</label>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto Tempo Faz?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo">
								</div>
							</div>
							<div class="col-md-1">
								<label>
									<input type="checkbox" name="checkbox"> Dor De Cabeça:
								</label>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto Tempo Faz?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo">
								</div>
							</div>
							<div class="col-md-1">
								<label>
									<input type="checkbox" name="checkbox"> Dores No Corpo:
								</label>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto Tempo Faz?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo">
								</div>
							</div>
							<div class="col-md-1">
								<label>
									<input type="checkbox" name="checkbox"> Dores Nas Costas:
								</label>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto Tempo Faz?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo">
								</div>
							</div>
							<div class="col-md-1">
								<label>
									<input type="checkbox" name="checkbox"> Cansaço:
								</label>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto Tempo Faz?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo">
								</div>
							</div>
							<div class="col-md-6">
							</div>
							

							<label class="col-md-1 col-form-label form-group">É Fumante?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHipotireoidismo" id="fichaHipotireoidismo" value="1">
									<label class="form-check-label" for="fichaHipotireoidismo">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHipotireoidismo" id="fichaHipotireoidismo" value="0">
									<label class="form-check-label" for="fichaHipotireoidismo">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto Tempo?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipotireoidismo">
								</div>
							</div>

							<label class="col-md-1 col-form-label form-group">Tem Algum Vicio?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaDiabetes" id="fichaDiabetes" value="1">
									<label class="form-check-label" for="fichaDiabetes">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaDiabetes" id="fichaDiabetes" value="0">
									<label class="form-check-label" for="fichaDiabetes">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoDiabetes">
								</div>
							</div>
							<div class="col-md-12">
								<hr>
							</div>

							<label class="col-md-3 col-form-label form-group">Tem Algum Problema De Saúde?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHipertensao" id="fichaHipertensao" value="1">
									<label class="form-check-label" for="fichaHipertensao">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHipertensao" id="fichaHipertensao" value="0">
									<label class="form-check-label" for="fichaHipertensao">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-7">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipertensao">
								</div>
							</div>
							<div class="col-md-1">
								<label>
									<input type="checkbox" name="checkbox"> Diabete
								</label>
							</div>
							<div class="col-md-2">
								<label>
									<input type="checkbox" name="checkbox"> Doença Cardíaca
								</label>
							</div>
							<div class="col-md-2">
								<label>
									<input type="checkbox" name="checkbox"> Problema Renal
								</label>
							</div>
							<div class="col-md-2">
								<label>
									<input type="checkbox" name="checkbox"> Doença Respiratória Crônica
								</label>
							</div>
							<div class="col-md-2">
								<label>
									<input type="checkbox" name="checkbox"> Rinite / Sinusite
								</label>
							</div>
							<div class="col-md-2">
								<label>
									<input type="checkbox" name="checkbox"> Pressão Alta
								</label>
							</div>
							<div class="col-md-1">
								<label>
									<input type="checkbox" name="checkbox"> Asma
								</label>
							</div>
							<div class="col-md-12">
								<hr>
							</div>
							
	
							<label class="col-md-4 col-form-label form-group">Nos Últimos Três Meses, Seguiu Quarenta Corretamente?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaEplepsia" id="fichaEplepsia" value="1">
									<label class="form-check-label" for="fichaEplepsia">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaEplepsia" id="fichaEplepsia" value="0">
									<label class="form-check-label" for="fichaEplepsia">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group form-focus">
									<label class="focus-label">Se Trabalha, Já Voltou As Atividades Normais?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoEplepsia">
								</div>
							</div>
							<label class="col-md-4 col-form-label form-group">Mora Com Familiares? Os Mesmo Já Voltaram Para Suas Atividades Normal?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPsoriase" id="fichaPsoriase" value="1">
									<label class="form-check-label" for="fichaPsoriase">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPsoriase" id="fichaPsoriase" value="0">
									<label class="form-check-label" for="fichaPsoriase">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-6">
							</div>
							
							<label class="col-md-4 col-form-label form-group">Você ou Familiar que moram na mesma residência trabalham aa Área Da Saúde?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPsoriase" id="fichaPsoriase" value="1">
									<label class="form-check-label" for="fichaPsoriase">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPsoriase" id="fichaPsoriase" value="0">
									<label class="form-check-label" for="fichaPsoriase">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoPsoriase">
								</div>
							</div>
						</div>
					</div>
					
					
					<div class="card-box">
						<h3 class="card-title">Eu: </h3>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group form-focus">
									<label class="focus-label">Nome:*</label>
									<input type="text" class="form-control floating" required>
								</div>
							</div>
							<div class="col-md-12">
								<p>Respondi com total sinceridade, estando ciente que o mesmo, caso eu tenha falado algo que não procede, sou responsável por qualquer irresponsabilidade que venha ocorrer nesse estabelecimento, por erro que eu venha causar, assim não colocando em risco a minha saúde e nem a saúde dos profissionais. As declarações acima são expressão da verdade, não cabendo ao estabelecimento nenhuma responsabilidade por fatos omitido ou falso.</p>
								<blockquote>
									<p class="mb-0">Código Penal - Decreto-Lei No 2.848, De 7 De Dezembro De 1940.<br>
													Infração De Medida Sanitária Preventiva<br>
													Art. 268 - Infringir Determinação Do Poder Público, Destinada A Impedir Introdução Ou Propagação De Doença Contagiosa:</p>
								</blockquote>
								<blockquote>
									<p class="mb-0">Pena - Detenção, De Um Mês A Um Ano, E Multa.<br>
													Parágrafo Único - A Pena É Aumentada De Um Terço, Se O Agente É Funcionário Da Saúde Pública Ou Exerce A Profissão Na Área Da Saúde.</p>
								</blockquote>
							</div>
							
						</div>
					</div>
					
                    <div class="card-box">
                        <h3 class="card-title">Li E Concordo Com Os Termos Descritos Nesta Ficha De Anamnese Em Relação Covid-19.</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group profile-img-wrap fichaAssinatura covid19">
									<img class="inline-block" src="<?php echo URL; ?>belizer-time/admin/assets/img/assinatura.png" alt="user">
									<div class="fileupload btn">
										<span class="btn-text">Assinatura Cliente</span>
										<input class="upload" type="file">
									</div>
								</div>							
                            </div>
							<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="focus-label">CPF:*</label>
										<input type="text" class="form-control floating" required>
									</div>
									<div class="form-group form-focus">
										<label class="focus-label">RG:*</label>
										<input type="text" class="form-control floating" required>
									</div>
                            </div>
                        </div>                       
                    </div>
                    <div class="text-center m-t-20">
                        <button class="btn btn-primary submit-btn" type="button">Cadastrar Ficha Anamnese Covid-19</button>
                    </div>
                </form>
            </div>            

<div class="sidebar-overlay" data-reff=""></div>