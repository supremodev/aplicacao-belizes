<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">EDITAR FICHA ANAMNESE CRIOLIPÓLISE</h4>
                    </div>
                </div>
                <form>
                    <div class="card-box">
                        <h3 class="card-title">Informações do Paciente</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-img-wrap">
                                    <img class="inline-block" src="<?php echo URL; ?>assets/img/user.jpg" alt="user">
                                    <div class="fileupload btn">
                                        <span class="btn-text">adicionar</span>
                                        <input class="upload" type="file">
                                    </div>
                                </div>
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Nome:*</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">CPF:*</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Email:*</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">RG:</label>
                                                <input type="text" class="form-control floating">
                                            </div>
                                        </div>
										
                                        <div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Data de Nascimento:</label>
                                                <div class="cal-icon">
                                                    <input class="form-control floating datetimepicker" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Sexo</label>
                                                <select class="select form-control floating">
                                                    <option></option>
                                                    <option value="Masculino">Masculino</option>
                                                    <option value="Feminino">Feminino</option>
                                                </select>
                                            </div>
                                        </div>
										<div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Profissão:</label>
                                                <input type="text" class="form-control floating">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
											<div class="form-group form-focus">
												<label class="focus-label">Estado Civil:</label>
												<input type="text" class="form-control floating">
											</div>
										</div>
										<div class="col-md-2">
                                           <div class="form-group form-focus">
                                                <label class="focus-label">Telefone:</label>
                                                <input type="text" class="form-control floating">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Celular:*</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										
										<div class="col-md-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Endereço:</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">CEP:</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Cidade:</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Estado:</label>
                                                <input type="text" class="form-control floating">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>							
                        </div>
                    </div>
					<div class="card-box">
						<h3 class="card-title">ANAMNESE</h3>											
						<div class="row ficha">
							<label class="col-md-2 col-form-label form-group">Pratica atividade física</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaEmQualLocalDoCorpo">
								</div>
							</div>
							
							<label class="col-md-2 col-form-label form-group">Tem algum tipo de alergia</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaEmQualLocalDoCorpo">
								</div>
							</div>
							
							<label class="col-md-2 col-form-label form-group">Tem algum tipo de alteração de hormônios</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaEmQualLocalDoCorpo">
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group form-focus">
									<label class="focus-label">Peso:</label>
									<input type="text" class="form-control floating" name="fichaFazQuantoTempo">
								</div>
							</div>
						</div>
					</div>
					
					<div class="card-box">
						<h3 class="card-title">HISTÓRICO MÉDICO</h3>											
						<div class="row ficha">
							<label class="col-md-2 col-form-label form-group">Doenças relacionadas ao frio?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>							
							<label class="col-md-2 col-form-label form-group">Hérnias (umbilical ou inguinal)?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>							
							<label class="col-md-2 col-form-label form-group">Diástase Abdominal?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>
							<label class="col-md-2 col-form-label form-group">Lesão aberta na pele (furúnculo/dermatites,eczema)?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>
							<label class="col-md-2 col-form-label form-group">Lúpus eritematoso sistêmico?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>
							<label class="col-md-2 col-form-label form-group">Câncer de Pele?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>
							<label class="col-md-2 col-form-label form-group">Insuficiência circulatória periférica (varizes graves)?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>
							<label class="col-md-2 col-form-label form-group">Uso de anticoagulante?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>
							<label class="col-md-2 col-form-label form-group">Cirurgia recente na área a ser tratada?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>
							<label class="col-md-2 col-form-label form-group">Gestante e ou Amamentando?</label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>
							
							
							<div class="col-md-8">
								<div class="form-group form-focus">
									<label class="focus-label">Observações:</label>
									<textarea class="form-control floating" rows="3" cols="30"></textarea>
								</div>
							</div>
							
						</div>
					</div>				
					
					<div class="card-box">
						<h3 class="card-title">Eu: </h3>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group form-focus">
									<label class="focus-label">Nome:*</label>
									<input type="text" class="form-control floating" required>
								</div>
							</div>
							<div class="col-md-12">
								<p>Respondi com total sinceridade, estando ciente que o mesmo, caso eu tenha falado algo que não procede, sou responsável por qualquer irresponsabilidade que venha ocorrer nesse estabelecimento, por erro que eu venha causar, assim não colocando em risco a minha saúde e nem a saúde dos profissionais. As declarações acima são expressão da verdade, não cabendo ao estabelecimento nenhuma responsabilidade por fatos omitido ou falso.</p>
								<blockquote>
									<p class="mb-0">Código Penal - Decreto-Lei No 2.848, De 7 De Dezembro De 1940.<br>
													Infração De Medida Sanitária Preventiva<br>
													Art. 268 - Infringir Determinação Do Poder Público, Destinada A Impedir Introdução Ou Propagação De Doença Contagiosa:</p>
								</blockquote>
								<blockquote>
									<p class="mb-0">Pena - Detenção, De Um Mês A Um Ano, E Multa.<br>
													Parágrafo Único - A Pena É Aumentada De Um Terço, Se O Agente É Funcionário Da Saúde Pública Ou Exerce A Profissão Na Área Da Saúde.</p>
								</blockquote>
							</div>
							
						</div>
					</div>
					
                    <div class="card-box">
                        <h3 class="card-title">Li E Concordo Com Os Termos Descritos Nesta Ficha De Anamnese Em Relação Covid-19.</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group profile-img-wrap fichaAssinatura covid19">
									<img class="inline-block" src="<?php echo URL; ?>assets/img/assinatura.png" alt="user">
									<div class="fileupload btn">
										<span class="btn-text">Assinatura Cliente</span>
										<input class="upload" type="file">
									</div>
								</div>							
                            </div>
							<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="focus-label">CPF:*</label>
										<input type="text" class="form-control floating" required>
									</div>
									<div class="form-group form-focus">
										<label class="focus-label">RG:*</label>
										<input type="text" class="form-control floating" required>
									</div>
                            </div>
                        </div>                       
                    </div>
                    <div class="text-center m-t-20">
                        <button class="btn btn-primary submit-btn" type="button">Cadastrar Ficha Anamnese Covid-19</button>
                    </div>
                </form>
            </div>            

<div class="sidebar-overlay" data-reff=""></div>