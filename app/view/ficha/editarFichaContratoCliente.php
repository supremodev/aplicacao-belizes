<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">EDITAR CONTRATO DE PRESTAÇÃO DE SERVIÇOS ESTÉTICOS</h4>
                    </div>
                </div>
                <form>
                    <div class="card-box">
                        <h3 class="card-title">Informações do Profissional</h3>
						<div class="row contrato">
							<div class="col-md-1">
								<h5>Eu,</h5>
							</div>
							<div class="col-md-5">
								<div class="form-group form-focus">
									<label class="focus-label">Nome:*</label>
									<input type="text" class="form-control floating" required>
								</div>
							</div>
							<div class="col-md-6">
								<p>Profissional em Estética corporal e facial, presto serviço na área de estética no espaço Belize´s Time estética facial e corporal, assim registro para os devidos fins minha responsabilidade na prestação destes serviços.</p>
							</div>
							<div class="col-md-1">
								<h5>Eu,</h5>
							</div>
							<div class="col-md-5">
								<div class="form-group form-focus">
									<label class="focus-label">Nome:*</label>
									<input type="text" class="form-control floating" required>
								</div>
							</div>
							<div class="col-md-3">
								<p>cliente do espaço Belize´s Time portador(a) do RG:</p>
							</div>							
							<div class="col-md-3">
								<div class="form-group form-focus">
									<label class="focus-label">RG:</label>
									<input type="text" class="form-control floating">
								</div>
							</div>
							<div class="col-md-1">
								<p>Data De Nasc.:</p>
							</div>
							<div class="col-md-5">
								<div class="form-group form-focus">
									<label class="focus-label">Data de Nascimento:</label>
									<div class="cal-icon">
										<input class="form-control floating datetimepicker" type="text">
									</div>
								</div>
							</div>
							<div class="col-md-1">
								<p>CPF:</p>
							</div>
							<div class="col-md-5">
								<div class="form-group form-focus">
									<label class="focus-label">CPF:</label>
									<input type="text" class="form-control floating">
								</div>
							</div>
							<div class="col-md-1">
								<p>Endereço:</p>
							</div>
							<div class="col-md-5">
								<div class="form-group form-focus">
									<label class="focus-label">Endereço:</label>
									<input type="text" class="form-control floating">
								</div>
							</div>
							<div class="col-md-6">
								<p>venho através deste realizar tratamento estético, nas seguintes condições desse espaço.</p>
							</div>
						</div>
                                
                    </div>
					<div class="card-box">
						<h3 class="card-title">Regras gerais:</h3>											
						<div class="row ficha">
							<div class="col-md-12">
								<ol class="contratoLista">
									<li>O não comparecimento em sessenta dias, sem aviso prévio, para a realização do tratamento estético, o mesmo será cancelado e não será devolvido o valor já pago. <br> <strong>OBS:</strong> Caso o espaço venha fazer a devolução do valor, por algum motivo específico, será devolvido o mesmo, com desconto da máquina de cartão, se pago com cartão, pagamento da comissão do profissional, entre outros.</li>
									
									<li>O plano de tratamento não pode ser transferido, porém podendo ser analisado. Caso a/o cliente opte pela desistência do tratamento após sete dias do início do mesmo, não será devolvido o valor do primeiro mês de tratamento, ou primeira parcela. Caso queira mudar de um tratamento que já faz, para outro, avisar com antecedência, informando qual motivo e qual novo tratamento, pagando a diferença se houver, do novo tratamento. </li>
									
									<li>O não comparecimento nas sessões sem aviso com pelo menos uma hora de antecedência, será descontado como sessão realizada.</li>
									
									<li>Não será permitido chegar com atraso na sessão, caso ocorra algum imprevisto avisar, para que a sessão seja remarcada para o próximo horário caso houver vaga.</li>
									
									<li>A partir da data de pagamento em um tratamento pacote, casos de doença, entre outros, será válido por 6 meses, assim conversado com o profissional para melhor solução, ou até renovação do mesmo, atualizado em valores.</li>
									
									<li>O tratamento realizado inclui procedimentos específicos para a recuperação e manutenção do cliente, fica o cliente ciente das obrigações e orientações passadas pelo profissional, bem como os cuidados diários home care.</li>
									
									<li>Paciente está ciente que tratamento estético, é um tratamento, com resultados dependendo muito de paciente para paciente: como qualidade de vida, alimentação saudável, atividade física, uso constante de filtro solar, manutenção em dia dos tratamentos estético, entre outros. Podendo ver resultados logo de início ou meses depois.</li>
									
									<li>Os profissionais atuantes no tratamento se comprometem em realizar tudo o que está descrito nas cláusulas acima citadas desde que os clientes cumpram com suas responsabilidades e obrigações.</li>
									
									<li>Cada equipamento estético tem seu tempo para um resultado satisfatório, sendo que alguns só terá um bom resultado, em conjunto com outro equipamento.</li>
									
									<li>O cliente fica ciente que terá que seguir as orientações do profissional, ficando assim o profissional livre de quaisquer danos decorrentes ao mau uso dos produtos e o não cumprimento das orientações.</li>
									
									<li>O tratamento não seguido dias corretos para próxima sessão, pode não gerar um bom resultado, assim não tendo cobrança do paciente, da clínica, nenhuma insatisfação.</li>
									
									<li>O espaço de estética não se responsabiliza por produtos facial e corporal em home care no qual não foi indicado.</li>
									
									<li>Agendamento para próximas sessões, o cliente deve entrar em contato com profissional no celular do estabelecimento ou telefone fixo, com uma semana antes, pois a agenda é feita todos os dias, ás vezes não havendo horário se não for com antecedência, passando do prazo da próxima sessão.</li>
									
									<li>Fica o cliente ciente de que o não esclarecimento e a omissão de antecedentes de qualquer tipo, deixa o profissional isento de qualquer responsabilidade.</li>
									
								</ol>
								<h5>OBS.: Caso não esteja de acordo com algum item citado, favor nos informar antecipadamente.</h5>
								<h5>Li, e estou totalmente de acordo.</h5>
								<h5>Tratamento contratado:</h5>
							</div>
							<div class="col-md-12">
									<div class="form-group form-focus">
										<label class="focus-label">Tratamento contratado:</label>
										<textarea class="form-control floating" rows="3" cols="30"></textarea>
									</div>								
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="focus-label">Início do tratamento em:</label>
										<div class="cal-icon">
											<input class="form-control floating datetimepicker" type="text">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-focus">
										<label class="focus-label">Quantidade de sessões contratadas:</label>
										<input type="text" class="form-control floating">
									</div>
								</div>
						</div>
					</div>
					
                    <div class="card-box">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group profile-img-wrap fichaAssinatura covid19">
									<img class="inline-block" src="<?php echo URL; ?>assets/img/assinatura.png" alt="user">
									<div class="fileupload btn">
										<span class="btn-text">Assinatura Cliente</span>
										<input class="upload" type="file">
									</div>
								</div>							
                            </div>
							<div class="col-md-6">
                                <div class="form-group profile-img-wrap fichaAssinatura covid19">
									<img class="inline-block" src="<?php echo URL; ?>assets/img/assinatura.png" alt="user">
									<div class="fileupload btn">
										<span class="btn-text">Assinatura Profissional</span>
										<input class="upload" type="file">
									</div>
								</div>							
                            </div>
                        </div>                       
                    </div>
                    <div class="text-center m-t-20">
                        <button class="btn btn-primary submit-btn" type="button">CONTRATO DE PRESTAÇÃO DE SERVIÇOS</button>
                    </div>
                </form>
            </div>            

<div class="sidebar-overlay" data-reff=""></div>