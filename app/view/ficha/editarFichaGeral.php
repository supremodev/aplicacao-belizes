<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">EDITAR FICHA DE AVALIAÇÃO GERAL</h4>
                    </div>
                </div>
				<?php foreach ($FichaGeralLista as $linha) { ?>
                <form>
                    <div class="card-box">
                        <h3 class="card-title">Informações do Paciente</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-img-wrap">
                                    <img class="inline-block" src="<?php echo URL; ?>assets/img/user.jpg" alt="user">
                                    <div class="fileupload btn">
                                        <span class="btn-text">adicionar</span>
                                        <input class="upload" type="file">
                                    </div>
                                </div>
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Nome:*</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">CPF:*</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Email:*</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">RG:</label>
                                                <input type="text" class="form-control floating">
                                            </div>
                                        </div>
										
                                        <div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Data de Nascimento:</label>
                                                <div class="cal-icon">
                                                    <input class="form-control floating datetimepicker" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Sexo</label>
                                                <select class="select form-control floating">
                                                    <option></option>
                                                    <option value="Masculino">Masculino</option>
                                                    <option value="Feminino">Feminino</option>
                                                </select>
                                            </div>
                                        </div>
										<div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Profissão:</label>
                                                <input type="text" class="form-control floating">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
											<div class="form-group form-focus">
												<label class="focus-label">Estado Civil:</label>
												<input type="text" class="form-control floating">
											</div>
										</div>
										<div class="col-md-2">
                                           <div class="form-group form-focus">
                                                <label class="focus-label">Telefone:</label>
                                                <input type="text" class="form-control floating">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Celular:*</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										
										<div class="col-md-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Endereço:</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">CEP:</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Cidade:</label>
                                                <input type="text" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Estado:</label>
                                                <input type="text" class="form-control floating">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>							
                        </div>
                    </div>
                    <div class="card-box">
                        <h3 class="card-title">Informações da Sessão</h3>
                        <div class="row ficha">
                            <label class="col-md-4 col-form-label form-group">Tem histórico de dermatite ou câncer de pele? </label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="1">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="fichaHistoricoDeDermatiteOuCancerDePele" value="0">
									<label class="form-check-label" for="fichaHistoricoDeDermatiteOuCancerDePele">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group form-focus">
									<label class="focus-label">Em qual local do corpo?</label>
									<input type="text" class="form-control floating" name="fichaEmQualLocalDoCorpo">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group form-focus">
									<label class="focus-label">Faz quanto tempo?</label>
									<input type="text" class="form-control floating" name="fichaFazQuantoTempo">
								</div>
							</div>
							
							<label class="col-md-4 col-form-label form-group">Foi liberado pelo médico para tratamento estético? </label>
							<div class="col-md-2">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" id="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" value="1">
									<label class="form-check-label" for="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" id="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" value="0">
									<label class="form-check-label" for="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico">
									Não
									</label>
								</div>
							</div>
                        </div>
                    </div>
					<div class="card-box">
                        <h3 class="card-title">Histórico de doenças</h3>
                        <div class="row ficha">
                            <label class="col-md-1 col-form-label form-group">Hipertireoidismo?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHipertireoidismo" id="fichaHipertireoidismo" value="1">
									<label class="form-check-label" for="fichaHipertireoidismo">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHipertireoidismo" id="fichaHipertireoidismo" value="0">
									<label class="form-check-label" for="fichaHipertireoidismo">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Tratamento?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Hipotireoidismo?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHipotireoidismo" id="fichaHipotireoidismo" value="1">
									<label class="form-check-label" for="fichaHipotireoidismo">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHipotireoidismo" id="fichaHipotireoidismo" value="0">
									<label class="form-check-label" for="fichaHipotireoidismo">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Tratamento?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipotireoidismo">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Diabetes?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaDiabetes" id="fichaDiabetes" value="1">
									<label class="form-check-label" for="fichaDiabetes">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaDiabetes" id="fichaDiabetes" value="0">
									<label class="form-check-label" for="fichaDiabetes">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Tratamento?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoDiabetes">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Hipertensão?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHipertensao" id="fichaHipertensao" value="1">
									<label class="form-check-label" for="fichaHipertensao">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaHipertensao" id="fichaHipertensao" value="0">
									<label class="form-check-label" for="fichaHipertensao">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Tratamento?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoHipertensao">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Eplepsia?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaEplepsia" id="fichaEplepsia" value="1">
									<label class="form-check-label" for="fichaEplepsia">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaEplepsia" id="fichaEplepsia" value="0">
									<label class="form-check-label" for="fichaEplepsia">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Tratamento?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoEplepsia">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Psoríase?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPsoriase" id="fichaPsoriase" value="1">
									<label class="form-check-label" for="fichaPsoriase">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPsoriase" id="fichaPsoriase" value="0">
									<label class="form-check-label" for="fichaPsoriase">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Tratamento?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoPsoriase">
								</div>
							</div>
						</div>
					</div>
					<div class="card-box">
                        <h3 class="card-title">Outras doenças</h3>
                        <div class="row">
							<div class="col-md-12">
                                <div class="form-group form-focus">
									<label class="focus-label">Observações:</label>
									<textarea class="form-control floating" rows="3" cols="30" name="fichaOutrasDoencas"></textarea>
                                </div>								
                            </div>                         
						</div>
                    </div>
					<div class="card-box">
                        <div class="row">		
							<label class="col-md-1 col-form-label form-group">Tem Implantes metálicos?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaTemImplantesMetalicos" id="fichaTemImplantesMetalicos" value="1">
									<label class="form-check-label" for="fichaTemImplantesMetalicos">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaTemImplantesMetalicos" id="fichaTemImplantesMetalicos" value="0">
									<label class="form-check-label" for="fichaTemImplantesMetalicos">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Local?</label>
									<input type="text" class="form-control floating" name="fichaLocalTemImplantesMetalicos">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Portador de marcapasso?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPortadorDeMarcapasso" id="fichaPortadorDeMarcapasso" value="1">
									<label class="form-check-label" for="fichaPortadorDeMarcapasso">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPortadorDeMarcapasso" id="fichaPortadorDeMarcapasso" value="0">
									<label class="form-check-label" for="fichaPortadorDeMarcapasso">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">A quanto tempo?</label>
									<input type="text" class="form-control floating" name="fichaQuantoTempoPortadorDeMarcapasso">
								</div>
							</div>
														
							<label class="col-md-1 col-form-label form-group">Portador de HIV?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPortadorDeHIV" id="fichaPortadorDeHIV" value="1">
									<label class="form-check-label" for="fichaPortadorDeHIV">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPortadorDeHIV" id="fichaPortadorDeHIV" value="0">
									<label class="form-check-label" for="fichaPortadorDeHIV">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Tratamento?</label>
									<input type="text" class="form-control floating" name="fichaTratamentoPortadorDeHIV">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Portador de Hepatite?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPortadorDeHepatite" id="fichaPortadorDeHepatite" value="1">
									<label class="form-check-label" for="fichaPortadorDeHepatite">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPortadorDeHepatite" id="fichaPortadorDeHepatite" value="0">
									<label class="form-check-label" for="fichaPortadorDeHepatite">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaQualPortadorDeHepatite">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Alteração hormonal?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaAlteracaoHormonal" id="fichaAlteracaoHormonal" value="1">
									<label class="form-check-label" for="fichaAlteracaoHormonal">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaAlteracaoHormonal" id="fichaAlteracaoHormonal" value="0">
									<label class="form-check-label" for="fichaAlteracaoHormonal">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaQualAlteracaoHormonal">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Alergia a algum alimento?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaAlergiaAlgumAlimento" id="fichaAlergiaAlgumAlimento" value="1">
									<label class="form-check-label" for="fichaAlergiaAlgumAlimento">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaAlergiaAlgumAlimento" id="fichaAlergiaAlgumAlimento" value="0">
									<label class="form-check-label" for="fichaAlergiaAlgumAlimento">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaQualAlergiaAlgumAlimento">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Alergia a algum medicamento?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaAlergiaAlgumMedicamento" id="fichaAlergiaAlgumMedicamento" value="1">
									<label class="form-check-label" for="fichaAlergiaAlgumMedicamento">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaAlergiaAlgumMedicamento" id="fichaAlergiaAlgumMedicamento" value="0">
									<label class="form-check-label" for="fichaAlergiaAlgumMedicamento">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaQualAlergiaAlgumMedicamento">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Faz uso de medicamento?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFazUsoDeMedicamento" id="fichaFazUsoDeMedicamento" value="1">
									<label class="form-check-label" for="fichaFazUsoDeMedicamento">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFazUsoDeMedicamento" id="fichaFazUsoDeMedicamento" value="0">
									<label class="form-check-label" for="fichaFazUsoDeMedicamento">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaQualFazUsoDeMedicamento">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Presença de queloides?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPresencaDeQueloides" id="fichaPresencaDeQueloides" value="1">
									<label class="form-check-label" for="fichaPresencaDeQueloides">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaPresencaDeQueloides" id="fichaPresencaDeQueloides" value="0">
									<label class="form-check-label" for="fichaPresencaDeQueloides">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaQueLocalPresencaDeQueloides">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Gestante?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaGestante" id="fichaGestante" value="1">
									<label class="form-check-label" for="fichaGestante">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaGestante" id="fichaGestante" value="0">
									<label class="form-check-label" for="fichaGestante">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Quanto tempo?</label>
									<input type="text" class="form-control floating" name="fichaQuantoTempoGestante">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Filhos?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFilhos" id="fichaFilho" value="1">
									<label class="form-check-label" for="fichaFilhos">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFilhos" id="fichaFilhos" value="0">
									<label class="form-check-label" for="fichaFilhos">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Quantos e que idade?</label>
									<input type="text" class="form-control floating" name="fichaQuantosFilhosIdade">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Fez alguma cirurgia ou plástica?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFezAlgumaCirurgiaPlastica" id="fichaFezAlgumaCirurgiaPlastica" value="1">
									<label class="form-check-label" for="fichaFezAlgumaCirurgiaPlastica">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFezAlgumaCirurgiaPlastica" id="fichaFezAlgumaCirurgiaPlastica" value="0">
									<label class="form-check-label" for="fichaFezAlgumaCirurgiaPlastica">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Que local?</label>
									<input type="text" class="form-control floating" name="fichaQualFezAlgumaCirurgiaPlastica">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Realizou procedimentos estéticos anteriores?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaRealizouProcedimentosEsteticosAnteriores" id="fichaRealizouProcedimentosEsteticosAnteriores" value="1">
									<label class="form-check-label" for="fichaRealizouProcedimentosEsteticosAnteriores">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaRealizouProcedimentosEsteticosAnteriores" id="fichaRealizouProcedimentosEsteticosAnteriores" value="0">
									<label class="form-check-label" for="fichaRealizouProcedimentosEsteticosAnteriores">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Qual?</label>
									<input type="text" class="form-control floating" name="fichaQualRealizouProcedimentosEsteticosAnteriores">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Faz uso de maquiagem?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFazUsoDeMaquiagem" id="fichaFazUsoDeMaquiagem" value="1">
									<label class="form-check-label" for="fichaFazUsoDeMaquiagem">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFazUsoDeMaquiagem" id="fichaFazUsoDeMaquiagem" value="0">
									<label class="form-check-label" for="fichaFazUsoDeMaquiagem">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Como retira?</label>
									<input type="text" class="form-control floating" name="fichaQualFazUsoDeMaquiagem">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Faz atividade física?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFazAtividadeFisica" id="fichaFazAtividadeFisica" value="1">
									<label class="form-check-label" for="fichaFazAtividadeFisica">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaFazAtividadeFisica" id="fichaFazAtividadeFisica" value="0">
									<label class="form-check-label" for="fichaFazAtividadeFisica">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Quantas vezes por semana?</label>
									<input type="text" class="form-control floating" name="fichaQuantasVezesPorSemanaAtividadeFisica">
								</div>
							</div>
														
							<label class="col-md-1 col-form-label form-group">Já fumou ou fuma?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaJaFumouOuFuma" id="fichaJaFumouOuFuma" value="1">
									<label class="form-check-label" for="fichaJaFumouOuFuma">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaJaFumouOuFuma" id="fichaJaFumouOuFuma" value="0">
									<label class="form-check-label" for="fichaJaFumouOuFuma">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Quantos cigarros por dia?</label>
									<input type="text" class="form-control floating" name="fichaQuantosCigarrosPorDia">
								</div>
							</div>
							<label class="col-md-2 col-form-label form-group">Consumo de água?</label>
							<div class="col-md-10">
								<div class="form-group form-focus">
									<label class="focus-label">Ingere quantos copos ao dia?</label>
									<input type="text" class="form-control floating" name="fichaConsumoAguaIngereQuantosCoposDia">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Ingere bebida alcoólica?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaIngereBebidaAlcoolica" id="fichaIngereBebidaAlcoolica" value="1">
									<label class="form-check-label" for="fichaIngereBebidaAlcoolica">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaIngereBebidaAlcoolica" id="fichaIngereBebidaAlcoolica" value="0">
									<label class="form-check-label" for="fichaIngereBebidaAlcoolica">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Quantas doses por semana?</label>
									<input type="text" class="form-control floating" name="fichaQuantasDosesPorSemanaBebidaAlcoolica">
								</div>
							</div>
							
							<label class="col-md-1 col-form-label form-group">Ingere bebida gaseificada?</label>
							<div class="col-md-1">
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaIngereBebidaGaseificada" id="fichaIngereBebidaGaseificada" value="1">
									<label class="form-check-label" for="fichaIngereBebidaGaseificada">
									Sim
									</label>
								</div>
								<div class="form-group form-check form-check-inline">
									<input class="form-check-input" type="radio" name="fichaIngereBebidaGaseificada" id="fichaIngereBebidaGaseificada" value="0">
									<label class="form-check-label" for="fichaIngereBebidaGaseificada">
									Não
									</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group form-focus">
									<label class="focus-label">Quantas doses por semana?</label>
									<input type="text" class="form-control floating" name="fichaQuantasDosesPorSemanaBebidaGaseificada">
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="form-group form-focus select-focus">
									<label class="focus-label">Estresse</label>
									<select class="select form-control floating" name="fichaEstresse">
										<option></option>
										<option value="Não">Não</option>
										<option value="Pouco">Pouco</option>
										<option value="Moderado">Moderado</option>
										<option value="Elevado">Elevado</option>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group form-focus select-focus">
									<label class="focus-label">Posição que permanece por mais tempo</label>
									<select class="select form-control floating" name="fichaPosicaoQuePermanecePorMaisTempo">
										<option></option>
										<option value="Sentado">Sentado</option>
										<option value="Em pé">Em pé</option>
										<option value="Alternado">Alternado</option>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group form-focus select-focus">
									<label class="focus-label">Sono</label>
									<select class="select form-control floating" name="fichaSono">
										<option></option>
										<option value="Inquieto">Inquieto</option>
										<option value="Tranquilo">Tranquilo</option>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group form-focus">
									<label class="focus-label">Quantas horas por noite?</label>
									<input type="text" class="form-control floating" name="fichaQuantasHorasPorNoiteSono">
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group form-focus select-focus">
									<label class="focus-label">Alimentação</label>
									<select class="select form-control floating" name="fichaAlimentacao">
										<option></option>
										<option value="Balanceada e dá preferência por frutas, verduras e legumes">Balanceada e dá preferência por frutas, verduras e legumes</option>
										<option value="Não Balanceada com restrição alimentar">Não Balanceada com restrição alimentar</option>
										<option value="Não balanceada e come frituras, massas e doces">Não balanceada e come frituras, massas e doces</option>
										<option value="Elevado">Faz dieta</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group form-focus">
									<label class="focus-label">Faz dieta - Qual e a quanto tempo?</label>
									<input type="text" class="form-control floating" name="fichaFazDietaQualQuantoTempo">
								</div>
							</div>
                        </div>
                    </div>
                    <div class="card-box">
                        <h3 class="card-title">Termo;</h3>
                        <div class="row">
                            <div class="col-md-12">
								<h5>Estou de acordo com a veracidade das informações acima relacionadas, não cabendo ao profissional esteta nenhuma responsabilidade por fatos omitidos ou falsos.</h5>                                
                            </div>
							
							
							<div class="col-md-6">
                                <div class="form-group profile-img-wrap fichaAssinatura">
									<img class="inline-block" src="<?php echo URL; ?>assets/img/assinatura.png" alt="user">
									<div class="fileupload btn">
										<span class="btn-text">Assinatura Biomédico</span>
										<input class="upload" type="file">
									</div>
								</div>								
                            </div>
							<div class="col-md-6">
                                <div class="form-group profile-img-wrap fichaAssinatura">
									<img class="inline-block" src="<?php echo URL; ?>assets/img/assinatura.png" alt="user">
									<div class="fileupload btn">
										<span class="btn-text">Assinatura Cliente</span>
										<input class="upload" type="file">
									</div>
								</div>								
                            </div>
							
							<div class="col-md-8">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Tratamento estético indicado:</label>
									<textarea class="form-control floating" rows="3" cols="30"></textarea>
                                </div>								
                            </div>
							<div class="col-md-4">
								<div class="form-group form-focus">	
									<label class="focus-label">Número de sessões?</label>								
									<input type="text" class="form-control floating" name="fichaFazDietaQualQuantoTempo">
								</div>
							</div>
                        </div>                       
                    </div>
                    
                    <div class="text-center m-t-20">
                        <button class="btn btn-primary submit-btn" type="button">Cadastrar Ficha Geral</button>
                    </div>
                </form>
				<?php } ?>
            </div>            

<div class="sidebar-overlay" data-reff=""></div>