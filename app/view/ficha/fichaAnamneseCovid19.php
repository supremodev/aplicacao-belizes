<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title"><?php echo $qtdFichaAnamneseCovid19; ?> - FICHA ANAMNESE COVID-19</h4>
                    </div>
                    <div class="col-sm-8 col-9 text-right m-b-20">
                        <a href="<?php echo URL; ?>ficha/novoFichaAnamneseCovid19" class="btn btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i> CRIAR FICHA ANAMNESE COVID-19</a>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0">
								<thead>
									<tr>
										<th>Nome Cliente</th>
										<th>Nome Especialista</th>
										<th>Data - Hora</th>
										<th>Telefone Cliente</th>
										<th>E-mail Cliente</th>
										<th>Status</th>
										<th class="text-right">Editar</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($fichaAnamneseCovid19 as $ficha) { ?>
									<tr>
										<td><?php if (isset($ficha->cliNome)) echo htmlspecialchars($ficha->cliNome, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($ficha->funcNome)) echo htmlspecialchars($ficha->funcNome, ENT_QUOTES, 'UTF-8'); ?></td>
										<td>
											<?php if (isset($ficha->fichaAnamneseCovid19Data)) echo htmlspecialchars($ficha->fichaAnamneseCovid19Data, ENT_QUOTES, 'UTF-8'); ?> - 
											<?php if (isset($ficha->fichaAnamneseCovid19Hora)) echo htmlspecialchars($ficha->fichaAnamneseCovid19Hora, ENT_QUOTES, 'UTF-8'); ?>
										</td>
										<td>
											<?php if (isset($ficha->cliCel)) echo htmlspecialchars($ficha->cliCel, ENT_QUOTES, 'UTF-8'); ?> / 
											<?php if (isset($ficha->cliFone)) echo htmlspecialchars($ficha->cliFone, ENT_QUOTES, 'UTF-8'); ?>
										</td>
										<td><?php if (isset($ficha->cliEmail)) echo htmlspecialchars($ficha->cliEmail, ENT_QUOTES, 'UTF-8'); ?></td>
										<td>
											<?php 
												if (isset($ficha->fichaAnamneseCovid19Status)){
													if($ficha->fichaAnamneseCovid19Status == "1"){
														echo("Ativa");
													}else{
														echo("Desativado");
													}														
												}													
											?>										
										</td>
										<td class="text-right">
											<div>
												<a class="dropdown-item" href="<?php echo URL; ?>ficha/editarFichaAnamneseCovid19/<?php echo $ficha->idFichaAnamneseCovid19; ?>">
													<i class="fa fa-pencil m-r-5"></i>Editar
												</a>
												<a class="dropdown-item" href="#" data-toggle="modal" data-target="#delete_patient">
													<i class="fa fa-trash-o m-r-5"></i> Desativar
												</a>
											</div>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>            

	<div class="sidebar-overlay" data-reff=""></div>