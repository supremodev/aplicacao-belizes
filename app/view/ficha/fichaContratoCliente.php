<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">CONTRATO SERVIÇOS ESTÉTICOS</h4>
                    </div>
                    <div class="col-sm-8 col-9 text-right m-b-20">
                        <a href="<?php echo URL; ?>ficha/novoFichaContratoCliente" class="btn btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i> NOVO CONTRATO</a>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0">
								<thead>
									<tr>
										<th>Nome Cliente</th>
										<th>Data</th>
										<th>Telefone Cliente</th>
										<th>E-mail Cliente</th>
										<th>Status</th>
										<th class="text-right">Editar</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($ContratoCliente as $ficha) { ?>
									<tr>
										<td>
											<?php echo $ficha->cliNome; ?>
										</td>
										<td>
											<?php echo date("d/m/Y", strtotime($ficha->contratoData)); ?>
										</td>
										<td>
											<?php echo $ficha->cliCel; ?>
										</td>
										<td>
											<?php echo $ficha->cliEmail; ?>
										</td>
										<td>
											<?php 
												if (isset($ficha->contratoStatus)){
													if($ficha->contratoStatus == "1"){
														echo("Ativa");
													}else{
														echo("Desativado");
													}														
												}													
											?>										
										</td>
										<td class="text-right">
											<div>
												<!--<a class="dropdown-item" href="<?php echo URL; ?>ficha/editarFichaContratoCliente/<?php echo $ficha->idContrato; ?>">
													<i class="fa fa-pencil m-r-5"></i>Editar
												</a>-->
												<a class="dropdown-item" href="<?php echo URL; ?>ficha/verFichaContratoCliente/<?php echo $ficha->idContrato; ?>" d>
													<i class="fa fa-eye m-r-5"></i> Ver Contrato
												</a>
											</div>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>            

    <div class="sidebar-overlay" data-reff=""></div>