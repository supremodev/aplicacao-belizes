<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">FICHA DE AVALIAÇÃO GERAL ( ANAMSESE )</h4>
                    </div>
                    <div class="col-sm-8 col-9 text-right m-b-20"><!--
                        <a href="<?php echo URL; ?>ficha/novoFichaGeral" class="btn btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i> CRIAR FICHA GERAL</a> -->
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0">
								<thead>
									<tr>
										<th>Nome Cliente</th>
										<th>Data</th>
										<th>Hora</th>
										<th class="text-right">Preencher as Fichas</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($fichaGeral as $ficha) { ?>
									<tr>
										<td><?php if (isset($ficha->cliNome)) echo htmlspecialchars($ficha->cliNome, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php echo htmlspecialchars(date("d/m/Y", strtotime($ficha->agendaData))); ?></td> 
										<td><?php echo $ficha->agendaHora; ?></td>
										<td class="text-right">
											<div>
												<a class="dropdown-item" href="<?php echo URL; ?>ficha/novoFichaGeral/<?php echo $ficha->cliCPF; ?>">
													<i class="fa fa-pencil m-r-5"></i>Preencher Ficha
												</a>
												
											</div>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>            

	<div class="sidebar-overlay" data-reff=""></div>