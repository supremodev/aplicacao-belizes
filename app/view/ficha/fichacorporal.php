            <!-- TAB FICHA CORPORAL -->
        
            <div class="tab-pane" id="tabFichaCorporal">
                <div class="row fichaCorporal">
                    <div class="col-md-12">
                        <?php foreach ($fichacorporal as $ficha) { ?>
                        <form id="formulario-corporal">
                            <div class="card-box">
                                <h3 class="card-title">AVALIAÇÃO CORPORAL</h3>
                                <h4 class="card-title">Hipolipodistrofia Ginoide (HLDG)</h4>
                                <div class="row ficha">
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Tipo:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalHipolipodistrofia" <?php if($ficha->fichaCorporalHipolipodistrofia == "Flácida"){echo "checked";};?>>
                                                        Flácida </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalHipolipodistrofia" <?php if($ficha->fichaCorporalHipolipodistrofia == "Edematosa"){echo "checked";};?>>
                                                        Edematosa </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalHipolipodistrofia" <?php if($ficha->fichaCorporalHipolipodistrofia == "Compacta"){echo "checked";};?>>
                                                        Compacta </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalHipolipodistrofia" <?php if($ficha->fichaCorporalHipolipodistrofia == "Mista"){echo "checked";};?>>
                                                        Mista </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Grau:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalGrau" value="1" <?php if($ficha->fichaCorporalGrau == "1"){echo "checked";};?>>
                                                        I </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalGrau" value="2" <?php if($ficha->fichaCorporalGrau == "2"){echo "checked";};?>>
                                                        II </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalGrau" value="3" <?php if($ficha->fichaCorporalGrau == "3"){echo "checked";};?>>
                                                        III </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalGrau" value="4" <?php if($ficha->fichaCorporalGrau == "4"){echo "checked";};?>>
                                                        IV </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label form-group">Temperatura:</label>
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input  class="validar" type="radio" name="fichaCorporalTemperatura"  value="Fria" <?php if($ficha->fichaCorporalTemperatura == "Fria"){echo "checked";};?>>
                                                        Fria </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input  class="validar" type="radio" name="fichaCorporalTemperatura" value="Quente" <?php if($ficha->fichaCorporalTemperatura == "Quente"){echo "checked";};?>>
                                                        Quente </label>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label form-group">Presença de dor à palpação:</label>
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalPalpacao" value="sim" <?php if($ficha->fichaCorporalPalpacao == "sim"){echo "checked";};?>>
                                                        Sim </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input  class="validar" type="radio" name="fichaCorporalPalpacao" value="não" <?php if($ficha->fichaCorporalPalpacao == "não"){echo "checked";};?>>
                                                        Não </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Localização:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalTemperaturaLocalizacao" value="<?php echo $ficha->fichaCorporalTemperaturaLocalizacao;?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Coloração do tecido:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalTemperaturaColoracaoDoTecido" value="<?php echo $ficha->fichaCorporalTemperaturaColoracaoDoTecido;?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">Edema</h4>
                                <div class="row ficha">
                                    <label class="col-md-2 col-form-label form-group">Teste de cacifo:</label>
                                    <div class="col-md-10">
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for=""> 
                                                <input class="form-check-input validar" type="radio" name="fichaCorporalEdemaTesteDeCacifo" id="" value="1" <?php if($ficha->fichaCorporalEdemaTesteDeCacifo == "1"){echo "checked";};?>>
                                            Positivo </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                                <input class="form-check-input validar" type="radio" name="fichaCorporalEdemaTesteDeCacifo" id="" value="0" <?php if($ficha->fichaCorporalEdemaTesteDeCacifo == "0"){echo "checked";};?>>
                                             Negativo </label>
                                        </div>
                                    </div>
                                    <label class="col-md-2 col-form-label form-group">Teste de digito-pressão:</label>
                                    <div class="col-md-10">
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                                <input class="form-check-input validar" type="radio" name="fichaCorporalEdemaTesteDeDigitoPressao" id="" value="1"  <?php if($ficha->fichaCorporalEdemaTesteDeDigitoPressao == "1"){echo "checked";};?>>
                                             Positivo </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                                <input class="form-check-input validar" type="radio" name="fichaCorporalEdemaTesteDeDigitoPressao" id="" value="0"  <?php if($ficha->fichaCorporalEdemaTesteDeDigitoPressao == "0"){echo "checked";};?>>
                                             Negativo </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Sensação de peso / Cansaço em MMII:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalEdemaSensacaoDePesoCansacoEmMMII" value="<?php echo $ficha->fichaCorporalEdemaSensacaoDePesoCansacoEmMMII;?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Observações:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalEdemaObservacoes" value="<?php echo $ficha->fichaCorporalEdemaObservacoes;?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">Lipodistrofia</h4>
                                <div class="row ficha">
                                    <label class="col-md-2 col-form-label form-group">Gordura:</label>
                                    <div class="col-md-10">
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                            <input class="form-check-input validar" type="radio" name="fichaCorporalLipodistrofiaGorduraCompactaFlacida" id="" value="1" <?php if($ficha->fichaCorporalLipodistrofiaGorduraCompactaFlacida == "1"){echo "checked";};?>>
                                             Compacta </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                            <input class="form-check-input validar" type="radio" name="fichaCorporalLipodistrofiaGorduraCompactaFlacida" id="" value="0" <?php if($ficha->fichaCorporalLipodistrofiaGorduraCompactaFlacida == "0"){echo "checked";};?>>
                                             Flácida </label>
                                        </div>
                                    </div>
                                    <label class="col-md-2 col-form-label form-group">Distribuição de gordura:</label>
                                    <div class="col-md-10">
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                                <input class="form-check-input validar" type="radio" name="fichaCorporalLipodistrofiaDistribuicaoGordura" id="" value="1" <?php if($ficha->fichaCorporalLipodistrofiaDistribuicaoGordura == "1"){echo "checked";};?>>
                                            Localizada </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                                <input class="form-check-input validar" type="radio" name="fichaCorporalLipodistrofiaDistribuicaoGordura" id="" value="0" <?php if($ficha->fichaCorporalLipodistrofiaDistribuicaoGordura == "0"){echo "checked";};?>>
                                             Generalizada </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Localização:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalLipodistrofiaLocalizacao" value="<?php echo $ficha->fichaCorporalLipodistrofiaLocalizacao;?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">Biótipo</h4>
                                <div class="row ficha">
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Tipo:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalBiotipo" value="Ginoide" <?php if($ficha->fichaCorporalBiotipo == "Ginoide"){echo "checked";};?>>
                                                        Ginoide </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalBiotipo" value="Androide" <?php if($ficha->fichaCorporalBiotipo == "Androide"){echo "checked";};?>>
                                                        Androide </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalBiotipo" value="Normolíneo" <?php if($ficha->fichaCorporalBiotipo == "Normolíneo"){echo "checked";};?>>
                                                        Normolíneo </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Peso:</label>
                                                <input type="text" class="form-control floating imc" name="fichaCorporalBiotipoPeso" value="<?php echo $ficha->fichaCorporalBiotipoPeso;?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Altura:</label>
                                                <input type="text" id="imc" class="form-control floating imc" name="fichaCorporalBiotipoAltura" value="<?php echo $ficha->fichaCorporalBiotipoAltura;?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">IMC:</label>
                                                <input readonly type="text" class="form-control floating" value="0.0" name="fichaCorporalBiotipoIMC" value="<?php echo $ficha->fichaCorporalBiotipoIMC;?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Peso min:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalBiotipoPesoMin" value="<?php echo $ficha->fichaCorporalBiotipoPesoMin;?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Peso máx:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalBiotipoPesoMax" value="<?php echo $ficha->fichaCorporalBiotipoPesoMax;?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Situação:</label>
                                                <input readonly value="Exibir mensagem após o resultado do IMC" type="text" class="form-control floating" name="fichaCorporalBiotipoIMCSituacao" value="<?php echo $ficha->fichaCorporalBiotipoIMCSituacao;?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Observações:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalBiotipoObservacoes" value="<?php echo $ficha->fichaCorporalBiotipoObservacoes;?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">Flacidez</h4>
                                <h5>Quantificar os itens abaixo: (+leve, ++moderado, +++Intenso, ++++Grave)</h5>
                                <div class="row ficha">
                                    <div class="col-md-2">
                                        <div class="form-group checkbox checkboxCorporal">
                                            <label>
                                                <input type="checkbox" name="fichaCorporalFlacidezTissular" value="0" checked hidden>
                                                <input type="checkbox" name="fichaCorporalFlacidezTissular" value="1" <?php if($ficha->fichaCorporalFlacidezTissular == "1"){echo "checked";};?>>
                                                Tissular </label>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Localização da flacidez tissular:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalFlacidezLocalizacaoDaFlacidezTissular" value="<?php echo $ficha->fichaCorporalFlacidezLocalizacaoDaFlacidezTissular;?>">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group checkbox checkboxCorporal">
                                            <label>
                                                <input type="checkbox" name="fichaCorporalFlacidezMuscular" value="0" checked hidden>
                                                <input type="checkbox" name="fichaCorporalFlacidezMuscular" value="1" <?php if($ficha->fichaCorporalFlacidezMuscular == "1"){echo "checked";};?>>
                                                Muscular </label>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Localização da flacidez muscular:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalFlacidezLocalizacaoDaFlacidezMuscular" value="<?php echo $ficha->fichaCorporalFlacidezLocalizacaoDaFlacidezMuscular;?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">Estrias</h4>
                                <div class="row ficha">
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Cor:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalEstriasCor" value="Rubra / violácea" <?php if($ficha->fichaCorporalEstriasCor == "Rubra / violácea"){echo "checked";};?>>
                                                        Rubra / violácea </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalEstriasCor" value="Alba" <?php if($ficha->fichaCorporalEstriasCor == "Alba"){echo "checked";};?>>
                                                        Alba </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Largura:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalEstriasLargura" value="Fina" <?php if($ficha->fichaCorporalEstriasLargura == "Fina"){echo "checked";};?>>
                                                        Fina </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalEstriasLargura" value="Larga" <?php if($ficha->fichaCorporalEstriasLargura == "Larga"){echo "checked";};?>>
                                                        Larga </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantidade: (+leve, ++moderado, +++Intenso, ++++Grave)</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalEstriasQuantidade" value="<?php echo $ficha->fichaCorporalEstriasQuantidade;?>">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Região:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalEstriasRegiao" value="<?php echo $ficha->fichaCorporalEstriasRegiao;?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">Alterações Posturais</h4>
                                <div class="row ficha">
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Ombros:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisOmbrosAnteriorizacao" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisOmbrosAnteriorizacao" value="1" <?php if($ficha->fichaCorporalAlteracoesPosturaisOmbrosAnteriorizacao == "1"){echo "checked";};?>>
                                                        Anteriorização </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisOmbrosHiperextensao" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisOmbrosHiperextensao" value="1" <?php if($ficha->fichaCorporalAlteracoesPosturaisOmbrosAnteriorizacao == "1"){echo "checked";};?>>
                                                        Hiperextensão </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Coluna:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisColunaEscolioseEmC" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisColunaEscolioseEmC" value="1" <?php if($ficha->fichaCorporalAlteracoesPosturaisColunaEscolioseEmC == "1"){echo "checked";};?>>
                                                        Escoliose em C </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="ColunaEscolioseEmS" value="0" checked hidden>
                                                        <input type="checkbox" name="ColunaEscolioseEmS" value="1" <?php if($ficha->fichaCorporalAlteracoesPosturaisColunaEscolioseEmS == "1"){echo "checked";};?>>
                                                        Escoliose em S </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="ColunaHipercifose" value="0" checked hidden>
                                                        <input type="checkbox" name="ColunaHipercifose" value="1" <?php if($ficha->fichaCorporalAlteracoesPosturaisColunaHipercifose == "1"){echo "checked";};?>>
                                                        Hipercifose </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="ColunaHiperlordose" value="0" checked hidden>
                                                        <input type="checkbox" name="ColunaHiperlordose" value="1" <?php if($ficha->fichaCorporalAlteracoesPosturaisColunaHiperlordose == "1"){echo "checked";};?>>
                                                        Hiperlordose </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="ColunaRetificacaoDaCifose" value="0" checked hidden>
                                                        <input type="checkbox" name="ColunaRetificacaoDaCifose" value="1" <?php if($ficha->fichaCorporalAlteracoesPosturaisColunaRetificacaoDaCifose == "1"){echo "checked";};?>>
                                                        Retificação da cifose </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="ColunaRetificacaoDaLordose" value="0" checked hidden>
                                                        <input type="checkbox" name="ColunaRetificacaoDaLordose" value="1" <?php if($ficha->fichaCorporalAlteracoesPosturaisColunaRetificacaoDaLordose == "1"){echo "checked";};?>>
                                                        Retificação da lordose </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Quadril:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="QuadrilAntiversao" value="0" checked hidden>
                                                        <input type="checkbox" name="QuadrilAntiversao" value="1" <?php if($ficha->fichaCorporalAlteracoesPosturaisQuadrilAntiversao == "1"){echo "checked";};?>>
                                                        Antiversão </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="QuadrilRetroversao" value="0" checked hidden>
                                                        <input type="checkbox" name="QuadrilRetroversao" value="1" <?php if($ficha->fichaCorporalAlteracoesPosturaisQuadrilRetroversao == "1"){echo "checked";};?>>
                                                        Retroversão </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Joelhos:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisJoelhosGenovalgo" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisJoelhosGenovalgo" value="1" <?php if($ficha->fichaCorporalAlteracoesPosturaisJoelhosGenovalgo == "1"){echo "checked";};?>>
                                                        Genovalgo </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="JoelhosGenovaro" value="0" checked hidden>
                                                        <input type="checkbox" name="JoelhosGenovaro" value="1" <?php if($ficha->fichaCorporalAlteracoesPosturaisJoelhosGenovaro == "1"){echo "checked";};?>>
                                                        Genovaro </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="JoelhosHiperextensao" value="0" checked hidden>
                                                        <input type="checkbox" name="JoelhosHiperextensao" value="1" <?php if($ficha->fichaCorporalAlteracoesPosturaisJoelhosHiperextensao == "1"){echo "checked";};?>>
                                                        Hiperextensão </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Outros:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalAlteracoesPosturaisOutros" value="<?php echo $ficha->fichaCorporalAlteracoesPosturaisOutros;?>">
                                            <input type="text" class="idFicha form-control floating" name="idFichaGeral" value="" hidden>
                                        </div>
                                    </div>

                                   
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">TRATAMENTO INDICADO</h4>
                                <div class="row ficha">
                                    <div class="col-md-8">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Obs:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalObsTratamentoIndicado" value="<?php echo $ficha->fichaCorporalObsTratamentoIndicado;?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Número de sessões?</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalQtdSessoes" value="<?php echo $ficha->fichaCorporalQtdSessoes;?>">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </form>
                        
                        
                    <!-- FIM FASE 01 DA TAB FICHA CORPORAL
                         APÓS O CADASTRO, DEVE LIBERAR OS CAMPOS A BAIXO E RETORNAR 
                         O ID DA FICHA QUE ACABOU DE SER CADASTRADA.
                    -->
                    
                        <form id="formPerimetria">
                            <div class="card-box">
                                <h4 class="card-title">Perimetria - Altura da fita</h4>
                                <div class="row ficha fichaCorporalPerimetria">
                                <?php foreach ($fichacorporalaperimetria as $fichaperimetria) { ?>
                                    <div class="col-md-4">
                                        <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Braço D:</label>
                                                <input type="text" class="idFichaCorporal" name="idFichaCorporal" hidden>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaBracoD" value="<?php echo $fichaperimetria->fichaCorporalPerimetriaBracoD;?>">
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Braço E:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaBracoE" value="<?php echo $fichaperimetria->fichaCorporalPerimetriaBracoE;?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Abd. Sup. Cintura:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaAbdSupCintura" value="<?php echo $fichaperimetria->fichaCorporalPerimetriaAbdSupCintura;?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Abd Inf. Quadril:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaAbdInfQuadril" value="<?php echo $fichaperimetria->fichaCorporalPerimetriaAbdInfQuadril;?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Coxa Sup. D:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaCoxaSupD" value="<?php echo $fichaperimetria->fichaCorporalPerimetriaCoxaSupD;?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Coxa Sup. E:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaCoxaSupE" value="<?php echo $fichaperimetria->fichaCorporalPerimetriaCoxaSupE;?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Coxa Inf. D:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaCoxaInfD" value="<?php echo $fichaperimetria->fichaCorporalPerimetriaCoxaInfD;?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Coxa Inf. E:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaCoxaInfE" value="<?php echo $fichaperimetria->fichaCorporalPerimetriaCoxaInfE;?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Joelho D:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaJoelhoD" value="<?php echo $fichaperimetria->fichaCorporalPerimetriaJoelhoD;?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Joelho E:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaJoelhoE" value="<?php echo $fichaperimetria->fichaCorporalPerimetriaJoelhoE;?>">
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <?php }?>
                                    <div class="col-md-8">
                                        <div class="row" id="imgPerimetria">
                                        <?php foreach ($fichacorporalfoto as $fichafoto) { ?>
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="blog grid-blog">
                                                    <div class="blog-image">
                                                        <img class="img-fluid" src="<?php echo URL;?>assets/img/ficha/<?php echo $fichafoto->fichaCorporalFotoNome;?>" alt="">
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        <?php }?>
                                            <div class="col-sm-12 col-md-3 col-lg-3" data-toggle="modal" data-target="#exampleModalCenter" style="cursor: pointer;">
                                                <div class="blog grid-blog btn-fotos" style="display: none;">
                                                    <div class="blog-image">
                                                        <img class="img-fluid" src="<?php echo URL;?>assets/img/ficha/add-img.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?php }?>
                        <form id="formAdipometria">
                            <div class="card-box">
                                <h4 class="card-title">Adipometria</h4>
                                <?php foreach ($fichacorporaladipometria as $fichadipometria) { ?>
                                <div class="row fichaCorporalAdipometria" >
                                    <div class="col-md-5">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Região do Corpo:</label>
                                            <input type="text" class="idFichaCorporal form-control floating" name="idFichaCorporal" value="<?php echo $fichadipometria->fichaCorporalAdipometriaRegiaoCorpo;?>">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Prega em CM:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalAdipometriaPregaCM[]" value="<?php echo $fichadipometria->fichaCorporalAdipometriaPregaCM;?>">
                                        </div>
                                    </div>                    
                                </div>
                                <?php }?>
                            </div>
                        </form>
                            <!-- ADD CORPO POSIÇÃO ANATÔMICA: -->
                            <?php require APP . 'view/corpo/visualizar-corpo.php';?>  
                    </div>
                </div>
            </div>
