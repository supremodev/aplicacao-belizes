<div class="tab-pane" id="tabFichaFacil">
            <div class="row">
                <div class="col-md-12">
                <?php foreach ($FichaFacialLista as $ficha) { ?>
                    <form id="formFacial">
                        <div class="card-box">
                            <h3 class="card-title">AVALIAÇÃO FACIAL</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row linhaFacil">
                                        <label class="col-md-2 col-form-label form-group">Queixa Principal:</label>
                                        <div class="col-md-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilLinhasDeExpressao" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilLinhasDeExpressao" value="1" <?php if($ficha->fichaFacilLinhasDeExpressao == "1"){echo "checked";};?>>
                                                    Linhas de expressão </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilCicatrizesDeAcneCicatrizAtrofica"  value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilCicatrizesDeAcneCicatrizAtrofica"  value="1" <?php if($ficha->fichaFacilCicatrizesDeAcneCicatrizAtrofica == "1"){echo "checked";};?>>
                                                    Cicatrizes de Acne / Cicatriz Atrófica </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilEnvelhecimentoPrecoce" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilEnvelhecimentoPrecoce" value="1" <?php if($ficha->fichaFacilEnvelhecimentoPrecoce == "1"){echo "checked";};?>>
                                                    Envelhecimento Precoce </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilFlacidez" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilFlacidez" value="1" <?php if($ficha->fichaFacilFlacidez == "1"){echo "checked";};?>>
                                                    Flacidez </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilDesidratacao" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilDesidratacao" value="1" <?php if($ficha->fichaFacilDesidratacao == "1"){echo "checked";};?>>
                                                    Desidratação </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilManchas" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilManchas" value="1" <?php if($ficha->fichaFacilManchas == "1"){echo "checked";};?>>
                                                    Manchas </label>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Outra:</label>
                                                <input type="text" class="form-control floating" name="fichaFacilOutra" value="<?php echo $ficha->fichaFacilOutra;?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        <label class="col-md-6 col-form-label form-group">Fototipo de pele:</label>
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilFototipoDePele" value="1" <?php if($ficha->fichaFacilFototipoDePele == "1"){echo "checked";};?>>
                                                    I </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilFototipoDePele" value="2" <?php if($ficha->fichaFacilFototipoDePele == "2"){echo "checked";};?>>
                                                    II </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilFototipoDePele" value="3" <?php if($ficha->fichaFacilFototipoDePele == "3"){echo "checked";};?>>
                                                    III </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilFototipoDePele" value="4" <?php if($ficha->fichaFacilFototipoDePele == "4"){echo "checked";};?>>
                                                    IV </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilFototipoDePele" value="5" <?php if($ficha->fichaFacilFototipoDePele == "5"){echo "checked";};?>>
                                                    V </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        <label class="col-md-6 col-form-label form-group">Biotipo cutâneo:</label>
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilBiotipoCutaneo" value="Eudérmica" <?php if($ficha->fichaFacilBiotipoCutaneo == "Eudérmica"){echo "checked";};?>>
                                                    Eudérmica </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilBiotipoCutaneo" value="Lipídica" <?php if($ficha->fichaFacilBiotipoCutaneo == "Lipídica"){echo "checked";};?>>
                                                    Lipídica </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilBiotipoCutaneo" value="Alípica" <?php if($ficha->fichaFacilBiotipoCutaneo == "Alípica"){echo "checked";};?>> 
                                                    Alípica </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilBiotipoCutaneo" value="Mista" <?php if($ficha->fichaFacilBiotipoCutaneo == "Mista"){echo "checked";};?>>
                                                    Mista </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        <label class="col-md-6 col-form-label form-group">Hidratação cutânea:</label>
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilHidratadaCutanea" value="Hidratada" <?php if($ficha->fichaFacilHidratadaCutanea == "Hidratada"){echo "checked";};?>>
                                                    Hidratada </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilHidratadaCutanea" value="Desidratada" <?php if($ficha->fichaFacilHidratadaCutanea == "Desidratada"){echo "checked";};?>>
                                                    Desidratada </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        <label class="col-md-6 col-form-label form-group">Desiquilíbrio da pele:</label>
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilComedao" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilComedao" value="1" <?php if($ficha->fichaFacilComedao == "1"){echo "checked";};?>>
                                                    Comedão </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilMilium" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilMilium" value="1" <?php if($ficha->fichaFacilMilium == "1"){echo "checked";};?>>
                                                    Mílium </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilPapula" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilPapula" value="1" <?php if($ficha->fichaFacilPapula == "1"){echo "checked";};?>>
                                                    Pápula </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilPustula" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilPustula" value="1" <?php if($ficha->fichaFacilPustula == "1"){echo "checked";};?>>
                                                    Pústula </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilMicrocisto" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilMicrocisto" value="1" <?php if($ficha->fichaFacilMicrocisto == "1"){echo "checked";};?>>
                                                    Microcisto </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilNodulo" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilNodulo" value="1" <?php if($ficha->fichaFacilNodulo == "1"){echo "checked";};?>>
                                                    Nódulo </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilQueratoseActinica" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilQueratoseActinica" value="1" <?php if($ficha->fichaFacilQueratoseActinica == "1"){echo "checked";};?>>
                                                    Queratose Actínica </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilDermatosePapulosaNigra" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilDermatosePapulosaNigra" value="1" <?php if($ficha->fichaFacilDermatosePapulosaNigra == "1"){echo "checked";};?>>
                                                    Dermatose Papulosa Nigra </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Obs:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilObsDesiquilibrioDaPele" value="<?php echo $ficha->fichaFacilObsDesiquilibrioDaPele;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">Sequela de Acne</h4>
                            <div class="row ficha">
                                <div class="col-md-2">
                                    <div class="form-group form-check form-check-inline">
                                        <label class="form-check-label" for=""> 
                                            <input class="form-check-input validar" type="radio" name="fichaFacilSequelaDeAcne" id="" value="1" <?php if($ficha->fichaFacilSequelaDeAcne == "1"){echo "checked";};?>>
                                        Sim </label>
                                    </div>
                                    <div class="form-group form-check form-check-inline">
                                        <label class="form-check-label" for="">
                                            <input class="form-check-input validar" type="radio" name="fichaFacilSequelaDeAcne" id="" value="0" <?php if($ficha->fichaFacilSequelaDeAcne == "0"){echo "checked";};?>>
                                         Não </label>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Região:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilRegiaoSequelaDeAcne" value="<?php echo $ficha->fichaFacilRegiaoSequelaDeAcne;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">Exposição solar atual</h4>
                            <div class="row ficha">
                                <div class="col-md-4">
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilExposiccaoSolar" value="pouco" <?php if($ficha->fichaFacilExposiccaoSolar == "pouco"){echo "checked";};?>>
                                            Pouco </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilExposiccaoSolar" value="Médio" <?php if($ficha->fichaFacilExposiccaoSolar == "Médio"){echo "checked";};?>>
                                            Médio </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilExposiccaoSolar" value="Alto" <?php if($ficha->fichaFacilExposiccaoSolar == "Alto"){echo "checked";};?>>
                                            Alto </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Frequência:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilFrequenciaProtecaoSolar" value="<?php echo $ficha->fichaFacilFrequenciaProtecaoSolar;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">Hábito de proteção solar</h4>
                            <div class="row ficha">
                                <div class="col-md-4">
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input type="checkbox" name="fichaFacilNaoUsa"  value="0" hidden checked>
                                            <input type="checkbox" name="fichaFacilNaoUsa"  value="1" <?php if($ficha->fichaFacilNaoUsa == "1"){echo "checked";};?>>
                                            Não usa </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input type="checkbox" name="fichaFacilUsoSomenteQuandoExpostoAoSol" value="0" hidden checked>
                                            <input type="checkbox" name="fichaFacilUsoSomenteQuandoExpostoAoSol" value="1" <?php if($ficha->fichaFacilUsoSomenteQuandoExpostoAoSol == "1"){echo "checked";};?>>
                                            Somente quando exposto ao sol </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Frequência:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilFrequencia" value="<?php echo $ficha->fichaFacilFrequencia;?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input type="checkbox" name="fichaFacilProtecaoSolarUsoEsporadico" value="0" hidden checked>
                                            <input type="checkbox" name="fichaFacilProtecaoSolarUsoEsporadico" value="1" <?php if($ficha->fichaFacilProtecaoSolarUsoEsporadico == "1"){echo "checked";};?>>
                                            Uso esporádico </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input type="checkbox" name="fichaFacilProtecaoSolarUsoDiario" value="0" hidden checked>
                                            <input type="checkbox" name="fichaFacilProtecaoSolarUsoDiario" value="1" <?php if($ficha->fichaFacilProtecaoSolarUsoDiario == "1"){echo "checked";};?>>
                                            Uso diário </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Frequência:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilProtecaoSolarFrequencia"  value="<?php echo $ficha->fichaFacilProtecaoSolarFrequencia;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">Histórico de cicatrização</h4>
                            <div class="row ficha">
                                <div class="col-md-4">
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHistorico" value="Sem histórico" <?php if($ficha->fichaFacilHistorico == "Sem histórico"){echo "checked";};?> >
                                            Sem histórico </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHistorico" value="Cicatriz hipertrófica" <?php if($ficha->fichaFacilHistorico == "Cicatriz hipertrófica"){echo "checked";};?>>
                                            Cicatriz hipertrófica </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHistorico" value="Cicatriz hipotrófica" <?php if($ficha->fichaFacilHistorico == "Cicatriz hipotrófica"){echo "checked";};?>>
                                            Cicatriz hipotrófica </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">Hipercromia</h4>
                            <div class="row ficha">
                                <div class="col-md-4">
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHipercromia" value="Não" <?php if($ficha->fichaFacilHipercromia == "Não"){echo "checked";};?>>
                                            Não </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHipercromia" value="Hipercromia pós inflamatória / pós traumática"  <?php if($ficha->fichaFacilHipercromia == "Hipercromia pós inflamatória / pós traumática"){echo "checked";};?>>
                                            Hipercromia pós inflamatória / pós traumática </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHipercromia" value="Mancha de gravidez / Cloasma" <?php if($ficha->fichaFacilHipercromia == "Mancha de gravidez / Cloasma"){echo "checked";};?>>
                                            Mancha de gravidez / Cloasma </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHipercromia" value="Mancha solares" <?php if($ficha->fichaFacilHipercromia == "Mancha solares"){echo "checked";};?>>
                                            Mancha solares </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHipercromia" value="Melasma Profundidade" <?php if($ficha->fichaFacilHipercromia == "Melasma Profundidade"){echo "checked";};?>>
                                            Melasma Profundidade </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Região:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilRegiaoHipercromia" value="<?php echo $ficha->fichaFacilRegiaoHipercromia;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">Pele sensível</h4>
                            <div class="row ficha">
                                <div class="col-md-2">
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilPeleSensivel" value="Não" <?php if($ficha->fichaFacilPeleSensivel == "Não"){echo "checked";};?>>
                                            Não </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilPeleSensivel" value="Dermatite seborreica" <?php if($ficha->fichaFacilPeleSensivel == "Dermatite seborreica"){echo "checked";};?>>
                                            Dermatite seborreica </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilPeleSensivel" value="Rosácea" <?php if($ficha->fichaFacilPeleSensivel == "Rosácea"){echo "checked";};?>>
                                            Rosácea </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilPeleSensivel" value="Telangiectasia" <?php if($ficha->fichaFacilPeleSensivel == "Telangiectasia"){echo "checked";};?>>
                                            Telangiectasia </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilPeleSensivel" value="Eritema" <?php if($ficha->fichaFacilPeleSensivel == "Eritema"){echo "checked";};?>>
                                            Eritema </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">TRATAMENTO INDICADO</h4>
                            <div class="row ficha">
                                <div class="col-md-8">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Obs:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilObsTratamentoIndicado" value="<?php echo $ficha->fichaFacilObsTratamentoIndicado;?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Número de sessões?</label>
                                        <input type="text" class="form-control floating" name="fichaFacilQtdSessoes" value="<?php echo $ficha->fichaFacilQtdSessoes;?>">
                                        <input type="text" class="idFicha form-control floating" name="idFichaGeral" value="" hidden>
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                        <!-- ADD FACIL MARCAÇÃO ANATÔMICA:  -->
                       <?php require APP . 'view/corpo/visualizar-facial.php';?>
                        </div>  
                    </form>
                <?php }?>       
                </div>
            </div>
        </div>