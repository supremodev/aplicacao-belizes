<div class="page-wrapper">
<div class="content">
    <div class="alert alert-danger alert-dismissible fade show " role="alert" style="display:none"> Erro ao Cadastrar Anamnese.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
    <div class="alert alert-success alert-dismissible fade show " role="alert" style="display:none"> Ficha anamnese cadastrado com Sucesso.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">CRIAR FICHA ANAMNESE CRIOLIPÓLISE</h4>
        </div>
    </div>
    
        <div class="card-box">
            <h3 class="card-title">Informações do Paciente</h3>
            <div class="row">
                <div class="col-md-12">
                <div class="profile-img-wrap">
                     <img id="img" class="inline-block" src="<?php echo URL; ?>assets/img/user.jpg" alt="user">
                </div>
                <div class="profile-basic">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Nome:*</label>
                                <input id="nome" type="text" class="form-control floating" readonly>
                                
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Pesquisa por CPF:*</label>
                                <input id="cpf" type="text" name="" class="ajax-consulta-ficha form-control floating" max="11" maxlength="11" pattern="([0-9]{11})" value="<?php echo $CPF;?>" required>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Data de Nascimento:</label>
                                <div class="cal-icon">
                                    <input id="nasci" class="form-control floating" type="text" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Profissão:</label>
                                <input id="profissao" type="text" class="form-control floating" readonly>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Sexo</label>
                                <input id="sexo" type="text" class="form-control floating" readonly>
                            </div>
                        </div>
                  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="profile-tabs">
            <ul class="nav nav-tabs nav-tabs-bottom">
                <li class="nav-item"><a class="nav-link active" href="#tabFichaGeral" data-toggle="tab">AVALIAÇÃO GERAL</a></li>
                <li class="nav-item" ><a class="nav-link" href="#tabAnamnese" data-toggle="tab">ANAMNESE  CRIOLIPOLISE</a></li>
            </ul>
            <div class="tab-content">
            <div class="tab-pane show active" id="tabFichaGeral">
                <div class="row">
                    <div class="col-md-12">
                        
                            <div class="card-box">
                                <h3 class="card-title">AVALIAÇÃO GERAL ( ANAMSESE )</h3>
                                <div class="row ficha">
                                    <label class="col-md-4 col-form-label form-group">Tem histórico de dermatite ou câncer de pele? </label>
                                    <div class="col-md-2">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="" value="1" disabled>
                                            <label class="form-check-label" for=""> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="" value="0" disabled>
                                            <label class="form-check-label" for=""> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Em qual local do corpo?</label>
                                            <input type="text" class="form-control floating" name="fichaEmQualLocalDoCorpo" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Faz quanto tempo?</label>
                                            <input type="text" class="form-control floating" name="fichaFazQuantoTempo" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-4 col-form-label form-group">Foi liberado pelo médico para tratamento estético? </label>
                                    <div class="col-md-2">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" id="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" value="1" disabled>
                                            <label class="form-check-label" for="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" id="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" value="0" disabled>
                                            <label class="form-check-label" for="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico"> Não </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h3 class="card-title">Histórico de doenças</h3>
                                <div class="row ficha">
                                    <label class="col-md-1 col-form-label form-group">Hipertireoidismo?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHipertireoidismo" id="fichaHipertireoidismo" value="1" disabled>
                                            <label class="form-check-label" for="fichaHipertireoidismo"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHipertireoidismo" id="fichaHipertireoidismo" value="0" disabled>
                                            <label class="form-check-label" for="fichaHipertireoidismo"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Hipotireoidismo?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHipotireoidismo" id="fichaHipotireoidismo" value="1" disabled>
                                            <label class="form-check-label" for="fichaHipotireoidismo"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHipotireoidismo" id="fichaHipotireoidismo" value="0" disabled>
                                            <label class="form-check-label" for="fichaHipotireoidismo"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoHipotireoidismo" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Diabetes?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaDiabetes" id="fichaDiabetes" value="1" disabled>
                                            <label class="form-check-label" for="fichaDiabetes"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaDiabetes" id="fichaDiabetes" value="0" disabled>
                                            <label class="form-check-label" for="fichaDiabetes"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoDiabetes" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Hipertensão?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHipertensao" id="fichaHipertensao" value="1" disabled>
                                            <label class="form-check-label" for="fichaHipertensao"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHipertensao" id="fichaHipertensao" value="0" disabled>
                                            <label class="form-check-label" for="fichaHipertensao"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoHipertensao" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Eplepsia?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaEplepsia" id="fichaEplepsia" value="1" disabled>
                                            <label class="form-check-label" for="fichaEplepsia"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaEplepsia" id="fichaEplepsia" value="0" disabled>
                                            <label class="form-check-label" for="fichaEplepsia"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoEplepsia" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Psoríase?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPsoriase" id="fichaPsoriase" value="1" disabled>
                                            <label class="form-check-label" for="fichaPsoriase"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPsoriase" id="fichaPsoriase" value="0" disabled>
                                            <label class="form-check-label" for="fichaPsoriase"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoPsoriase" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <div class="row">
                                    <label class="col-md-1 col-form-label form-group">Tem Implantes metálicos?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaTemImplantesMetalicos" id="fichaTemImplantesMetalicos" value="1" disabled>
                                            <label class="form-check-label" for="fichaTemImplantesMetalicos"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaTemImplantesMetalicos" id="fichaTemImplantesMetalicos" value="0" disabled>
                                            <label class="form-check-label" for="fichaTemImplantesMetalicos"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Local?</label>
                                            <input type="text" class="form-control floating" name="fichaLocalTemImplantesMetalicos" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Portador de marcapasso?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPortadorDeMarcapasso" id="fichaPortadorDeMarcapasso" value="1" disabled>
                                            <label class="form-check-label" for="fichaPortadorDeMarcapasso"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPortadorDeMarcapasso" id="fichaPortadorDeMarcapasso" value="0" disabled>
                                            <label class="form-check-label" for="fichaPortadorDeMarcapasso"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">A quanto tempo?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantoTempoPortadorDeMarcapasso" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Portador de HIV?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPortadorDeHIV" id="fichaPortadorDeHIV" value="1" disabled>
                                            <label class="form-check-label" for="fichaPortadorDeHIV"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPortadorDeHIV" id="fichaPortadorDeHIV" value="0" disabled>
                                            <label class="form-check-label" for="fichaPortadorDeHIV"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoPortadorDeHIV" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Portador de Hepatite?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPortadorDeHepatite" id="fichaPortadorDeHepatite" value="1" disabled>
                                            <label class="form-check-label" for="fichaPortadorDeHepatite"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPortadorDeHepatite" id="fichaPortadorDeHepatite" value="0" disabled>
                                            <label class="form-check-label" for="fichaPortadorDeHepatite"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualPortadorDeHepatite" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Alteração hormonal?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaAlteracaoHormonal" id="fichaAlteracaoHormonal" value="1" disabled>
                                            <label class="form-check-label" for="fichaAlteracaoHormonal"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaAlteracaoHormonal" id="fichaAlteracaoHormonal" value="0" disabled>
                                            <label class="form-check-label" for="fichaAlteracaoHormonal"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualAlteracaoHormonal" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Alergia a algum alimento?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaAlergiaAlgumAlimento" id="fichaAlergiaAlgumAlimento" value="1" disabled>
                                            <label class="form-check-label" for="fichaAlergiaAlgumAlimento"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaAlergiaAlgumAlimento" id="fichaAlergiaAlgumAlimento" value="0" disabled>
                                            <label class="form-check-label" for="fichaAlergiaAlgumAlimento"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualAlergiaAlgumAlimento" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Alergia a algum medicamento?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaAlergiaAlgumMedicamento" id="fichaAlergiaAlgumMedicamento" value="1" disabled>
                                            <label class="form-check-label" for="fichaAlergiaAlgumMedicamento"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaAlergiaAlgumMedicamento" id="fichaAlergiaAlgumMedicamento" value="0" disabled>
                                            <label class="form-check-label" for="fichaAlergiaAlgumMedicamento"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualAlergiaAlgumMedicamento" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Faz uso de medicamento?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFazUsoDeMedicamento" id="fichaFazUsoDeMedicamento" value="1" disabled>
                                            <label class="form-check-label" for="fichaFazUsoDeMedicamento"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFazUsoDeMedicamento" id="fichaFazUsoDeMedicamento" value="0" disabled>
                                            <label class="form-check-label" for="fichaFazUsoDeMedicamento"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualFazUsoDeMedicamento" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Presença de queloides?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPresencaDeQueloides" id="fichaPresencaDeQueloides" value="1" disabled>
                                            <label class="form-check-label" for="fichaPresencaDeQueloides"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPresencaDeQueloides" id="fichaPresencaDeQueloides" value="0" disabled>
                                            <label class="form-check-label" for="fichaPresencaDeQueloides"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQueLocalPresencaDeQueloides" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Gestante?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaGestante" id="fichaGestante" value="1" disabled>
                                            <label class="form-check-label" for="fichaGestante"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaGestante" id="fichaGestante" value="0" disabled>
                                            <label class="form-check-label" for="fichaGestante"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quanto tempo?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantoTempoGestante" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Filhos?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFilhos" id="fichaFilhos" value="1" disabled>
                                            <label class="form-check-label" for="fichaFilhos"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFilhos" id="fichaFilhos" value="0" disabled>
                                            <label class="form-check-label" for="fichaFilhos"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantos e que idade?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantosFilhosIdade" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Fez alguma cirurgia ou plástica?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFezAlgumaCirurgiaPlastica" id="fichaFezAlgumaCirurgiaPlastica" value="1" disabled>
                                            <label class="form-check-label" for="fichaFezAlgumaCirurgiaPlastica"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFezAlgumaCirurgiaPlastica" id="fichaFezAlgumaCirurgiaPlastica" value="0" disabled>
                                            <label class="form-check-label" for="fichaFezAlgumaCirurgiaPlastica"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Que local?</label>
                                            <input type="text" class="form-control floating" name="fichaQualFezAlgumaCirurgiaPlastica" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Realizou procedimentos estéticos anteriores?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaRealizouProcedimentosEsteticosAnteriores" id="fichaRealizouProcedimentosEsteticosAnteriores" value="1" disabled>
                                            <label class="form-check-label" for="fichaRealizouProcedimentosEsteticosAnteriores"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaRealizouProcedimentosEsteticosAnteriores" id="fichaRealizouProcedimentosEsteticosAnteriores" value="0" disabled>
                                            <label class="form-check-label" for="fichaRealizouProcedimentosEsteticosAnteriores"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualRealizouProcedimentosEsteticosAnteriores" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Faz uso de maquiagem?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFazUsoDeMaquiagem" id="fichaFazUsoDeMaquiagem" value="1" disabled>
                                            <label class="form-check-label" for="fichaFazUsoDeMaquiagem"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFazUsoDeMaquiagem" id="fichaFazUsoDeMaquiagem" value="0" disabled>
                                            <label class="form-check-label" for="fichaFazUsoDeMaquiagem"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Como retira?</label>
                                            <input type="text" class="form-control floating" name="fichaQualFazUsoDeMaquiagem" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Faz atividade física?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFazAtividadeFisica" id="fichaFazAtividadeFisica" value="1" disabled>
                                            <label class="form-check-label" for="fichaFazAtividadeFisica"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFazAtividadeFisica" id="fichaFazAtividadeFisica" value="0" disabled>
                                            <label class="form-check-label" for="fichaFazAtividadeFisica"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantas vezes por semana?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantasVezesPorSemanaAtividadeFisica" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Já fumou ou fuma?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaJaFumouOuFuma" id="fichaJaFumouOuFuma" value="1" disabled>
                                            <label class="form-check-label" for="fichaJaFumouOuFuma"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaJaFumouOuFuma" id="fichaJaFumouOuFuma" value="0" disabled>
                                            <label class="form-check-label" for="fichaJaFumouOuFuma"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantos cigarros por dia?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantosCigarrosPorDia" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-2 col-form-label form-group">Consumo de água?</label>
                                    <div class="col-md-10">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Ingere quantos copos ao dia?</label>
                                            <input type="text" class="form-control floating" name="fichaConsumoAguaIngereQuantosCoposDia" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Ingere bebida alcoólica?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaIngereBebidaAlcoolica" id="fichaIngereBebidaAlcoolica" value="1" disabled>
                                            <label class="form-check-label" for="fichaIngereBebidaAlcoolica"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaIngereBebidaAlcoolica" id="fichaIngereBebidaAlcoolica" value="0" disabled>
                                            <label class="form-check-label" for="fichaIngereBebidaAlcoolica"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantas doses por semana?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantasDosesPorSemanaBebidaAlcoolica" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Ingere bebida gaseificada?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaIngereBebidaGaseificada" id="fichaIngereBebidaGaseificada" value="1" disabled>
                                            <label class="form-check-label" for="fichaIngereBebidaGaseificada"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaIngereBebidaGaseificada" id="fichaIngereBebidaGaseificada" value="0" disabled>
                                            <label class="form-check-label" for="fichaIngereBebidaGaseificada"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantas doses por semana?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantasDosesPorSemanaBebidaGaseificada" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-focus select-focus select-focus">
                                            <label class="focus-label">Estresse</label>
                                            <input type="text" class="form-control floating" name="fichaEstresse" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-focus select-focus select-focus">
                                            <label class="focus-label">Posição que permanece por mais tempo</label>
                                            <input type="text" class="form-control floating" name="fichaPosicaoQuePermanecePorMaisTempo" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-focus select-focus select-focus">
                                            <label class="focus-label">Sono</label>
                                            <input type="text" class="form-control floating" name="fichaSono" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantas horas por noite?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantasHorasPorNoiteSono" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus select-focus">
                                            <label class="focus-label">Alimentação</label>
                                            <input type="text" class="form-control floating" name="fichaAlimentacao" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Faz dieta - Qual e a quanto tempo?</label>
                                            <input type="text" class="form-control floating" name="fichaFazDietaQualQuantoTempo" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h3 class="card-title">Observações Gerais</h3>
                                
								
								<div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group form-focus focused">
                                            <label class="focus-label">O que te incomoda</label>
											<input type="text" class="form-control floating" name="fichaOqueTeIncomoda" value="" readonly>
                                        </div>
                                    </div>
									<div class="col-md-3">
                                        <div class="form-group form-focus focused">
                                            <label class="focus-label">Qual seu time?</label>
											<input type="text" class="form-control floating" name="fichaQualSeuTime" value="" readonly>
                                    
										</div>
									</div>
									<div class="col-md-3">
                                        <div class="form-group form-focus focused">
                                            <label class="focus-label">O que você mudaria em você</label>
											<input type="text" class="form-control floating" name="fichaOqueVcMudariaEmVc" value="" readonly>
                                        </div>
                                    </div>
									<div class="col-md-3">
                                        <div class="form-group form-focus focused">
                                            <label class="focus-label">Qual seu estilo de musica</label>
											<input type="text" class="form-control floating" name="fichaQualSeuEstiloMusica" value="" readonly>
                                        </div>
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-focus focused">
                                            <label class="focus-label">Observações:</label>
                                            <textarea class="form-control floating" rows="3" cols="30" name="fichaOutrasDoencas"  readonly></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        
                    </div>
                </div>
            </div>
                <div class="tab-pane show " id="tabAnamnese">
                    <div class="row">
                        <div class="col-md-12">
                        <form id="formularios">
                                <div class="card-box">
                                    <h3 class="card-title">ANAMNESE</h3>
                                    <div class="row ficha">
                                        <label class="col-md-2 col-form-label form-group">Pratica atividade física</label>
                                        <div class="col-md-2">
                                            <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="fichaAnamnesePraticaAtividadeFísica">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamnesePraticaAtividadeFisica" id="" value="1" >
                                                Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamnesePraticaAtividadeFísica">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamnesePraticaAtividadeFisica" id="" value="0" >
                                                Não </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Qual?</label>
                                                <input type="text" class="form-control floating" name="fichaAnamneseQualPraticaAtividadeFisica">
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-form-label form-group">Tem algum tipo de alergia</label>
                                        <div class="col-md-2">
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseTemAlgumTipoDeAlergia	">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseTemAlgumTipoDeAlergia" id="" value="1" >
                                                Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseTemAlgumTipoDeAlergia	">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseTemAlgumTipoDeAlergia" id="" value="0" >
                                                Não </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Qual?</label>
                                                <input type="text" class="form-control floating" name="fichaAnamneseQualAlgumTipoDeAlergia">
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-form-label form-group">Tem algum tipo de alteração hormonal</label>
                                        <div class="col-md-2">
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseTemAlgumtipodehormonal">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseTemAlgumtipodehormonal" id="" value="1" >
                                                Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseTemAlgumtipodehormonal">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseTemAlgumtipodehormonal" id="" value="0" >
                                                Não </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Qual?</label>
                                                <input type="text" class="form-control floating" name="fichaAnamneseQualAlgumtipodehormonal">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Peso:</label>
                                                <input type="text" class="form-control floating" name="fichaAnamnesePeso">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-box">
                                    <h3 class="card-title">HISTÓRICO MÉDICO</h3>
                                    <div class="row ficha">
                                        <label class="col-md-2 col-form-label form-group">Doenças relacionadas ao frio?</label>
                                        <div class="col-md-2">
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseDoencasRelacionadasAoFrio">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseDoencasRelacionadasAoFrio" id="" value="1" >
                                                Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseDoencasRelacionadasAoFrio">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseDoencasRelacionadasAoFrio" id="" value="0" >
                                                Não </label>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-form-label form-group">Hérnias (umbilical ou inguinal)?</label>
                                        <div class="col-md-2">
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseHerniasUmbilicalOuInguinal">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseHerniasUmbilicalOuInguinal" id="" value="1" >
                                                Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseDoencasRelacionadasAoFrio">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseHerniasUmbilicalOuInguinal" id="" value="0" >
                                                    Não </label>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-form-label form-group">Diástase Abdominal?</label>
                                        <div class="col-md-2">
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseDiastaseAbdominal">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseDiastaseAbdominal" id="" value="1" >
                                                Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseDiastaseAbdominal">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseDiastaseAbdominal" id="" value="0" >
                                                Não </label>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-form-label form-group">Lesão aberta na pele (furúnculo/dermatites,eczema)?</label>
                                        <div class="col-md-2">
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseLesaoAbertaNaPele	">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseLesaoAbertaNaPele" id="" value="1" >
                                                Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseLesaoAbertaNaPele	">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseLesaoAbertaNaPele" id="" value="0" >
                                                Não </label>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-form-label form-group">Lúpus eritematoso sistêmico?</label>
                                        <div class="col-md-2">
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseLupusEritematosoSistemico	">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseLupusEritematosoSistemico" id="" value="1" >
                                                Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseLupusEritematosoSistemico	">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseLupusEritematosoSistemico" id="" value="0" >
                                                Não </label>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-form-label form-group">Câncer de Pele?</label>
                                        <div class="col-md-2">
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseCancerDePele">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseCancerDePele" id="" value="1" >
                                                Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseCancerDePele">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseCancerDePele" id="" value="0" >
                                                Não </label>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-form-label form-group">Insuficiência circulatória periférica (varizes graves)?</label>
                                        <div class="col-md-2">
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves" id="" value="1" >
                                                Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves" id="" value="0" >
                                                Não </label>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-form-label form-group">Uso de anticoagulante?</label>
                                        <div class="col-md-2">
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseUsoDeAnticoagulante">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseUsoDeAnticoagulante" id="" value="1" >
                                                Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseUsoDeAnticoagulante">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseUsoDeAnticoagulante" id="" value="0" >
                                                Não </label>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-form-label form-group">Cirurgia recente na área a ser tratada?</label>
                                        <div class="col-md-2">
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseCirurgiaRecenteNaAreaTratada">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseCirurgiaRecenteNaAreaTratada" id="" value="1" >
                                                Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseCirurgiaRecenteNaAreaTratada"> 
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseCirurgiaRecenteNaAreaTratada" id="" value="0" >
                                                Não </label>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-form-label form-group">Gestante e ou Amamentando?</label>
                                        <div class="col-md-2">
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseGestanteeOuAmamentando">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseGestanteeOuAmamentando" id="" value="1" >
                                                Sim </label>
                                            </div>
                                            <div class="form-group form-check form-check-inline">
                                                <label class="form-check-label" for="fichaAnamneseGestanteeOuAmamentando">
                                                <input class="form-check-input validar" type="radio" name="fichaAnamneseGestanteeOuAmamentando" id="" value="0" >
                                                Não </label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Observações:</label>
                                                <textarea class="form-control floating" rows="3" cols="30" name='fichaAnamneseObservacoes'></textarea>
                                                <input type="text" id="idCliente" class="form-control floating" name="idCliente" hidden>
                                                <input type="text" class="floating" name="idFunc" value="<?php echo $_SESSION['idUsuario'];?>" hidden>
                                            </div>
                                        </div>
                                    </div>
								</div>
                                
								<div class="card-box">
									<h4 class="card-title">TRATAMENTO INDICADO</h4>
									<div class="row ficha">
										<div class="col-md-8">
											<div class="form-group form-focus select-focus">
												<label class="focus-label">Obs:</label>
												<input type="text" class="form-control floating" name="fichaAnamneseObsTratamentoIndicado">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group form-focus select-focus">
												<label class="focus-label">Número de sessões?</label>
												<input type="text" class="form-control floating" name="fichaAnamneseQtdSessoes">
												<input type="text" class="idFicha form-control floating" name="idFichaGeral" value="" hidden>
											</div>
										</div>
										<div class="col-md-12">
											<div class="text-center m-t-20">
												<button class="anamenese-ajax btn btn-primary submit-btn" type="button">Cadastrar Ficha Anamnese Criolipólise</button>
											</div>
										</div>
									</div>
								</div>
								
								<div class="card-box">
                                    <h3 class="card-title">CORPO POSIÇÃO ANATÔMICA: </h3>
                                        <div class="row blococorporal" style="display:none">
                                            <div id="corpo-ajax" class="col-md-6" >
                                            </div>
                                            <div class="col-md-6">
                                                <ol class="list-number-rounded lista-corpo">
                                                </ol>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="text-center m-t-20">
                                                    <button class="add-marcacao-corporal-anamenese-ajax btn btn-primary submit-btn" type="button">Cadastrar Corpo Anamnese</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            
        
    
</div>
<div class="sidebar-overlay" data-reff=""></div>
