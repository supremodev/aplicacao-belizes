<div class="page-wrapper">
<div class="content">
    <div class="alert alert-danger alert-dismissible fade show notificao-corporal-false" role="alert" style="display:none"> Erro ao Cadastrar Avaliação Corporal.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
    <div class="alert alert-success alert-dismissible fade show notificao-corporal-true" role="alert" style="display:none"> Avaliação Corporal cadastrado com Sucesso.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
    <div class="alert alert-danger alert-dismissible fade show  notificao-facial-false" role="alert" style="display:none"> Erro ao Cadastrar Avaliação Facial.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
    <div class="alert alert-success alert-dismissible fade show notificao-facial-true" role="alert" style="display:none">Avaliação Facial cadastrado com Sucesso.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">CRIAR FICHA DE AVALIAÇÃO GERAL ( ANAMSESE )</h4>
        </div>
    </div>
    <div class="card-box">
        <h3 class="card-title">Informações do Paciente</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="profile-img-wrap">
                     <img id="img" class="inline-block" src="<?php echo URL; ?>assets/img/user.jpg" alt="user">
                </div>
                <div class="profile-basic">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Nome:*</label>
                                <input id="nome" type="text" class="form-control floating" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Pesquisa por CPF:*</label>
                                <input id="cpf" type="text" name="" class="ajax-consulta-ficha form-control floating" max="11" maxlength="11" pattern="([0-9]{11})" value="<?php echo $CPF;?>" required>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Data de Nascimento:</label>
                                <div class="cal-icon">
                                    <input id="nasci" class="form-control floating" type="text" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Profissão:</label>
                                <input id="profissao" type="text" class="form-control floating" readonly>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Sexo</label>
                                <input id="sexo" type="text" class="form-control floating" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="profile-tabs">
        <ul class="nav nav-tabs nav-tabs-bottom">
            <li class="nav-item"><a class="nav-link active" href="#tabFichaGeral" data-toggle="tab">AVALIAÇÃO GERAL ( ANAMSESE )</a></li>
            <li class="nav-item" style="display:none"><a class="nav-link" href="#tabFichaCorporal" data-toggle="tab">AVALIAÇÃO CORPORAL</a></li>
            <li class="nav-item" style="display:none"><a class="nav-link" href="#tabFichaFacil" data-toggle="tab">AVALIAÇÃO FACIAL</a></li>
            <!--<li class="nav-item" style="display:none"><a class="nav-link" href="#tabTratamentoIndicado" data-toggle="tab">TRATAMENTO INDICADO</a></li>-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane show active" id="tabFichaGeral">
                <div class="row">
                    <div class="col-md-12">
                        <form id="formularioss">
                            <div class="card-box">
                                <h3 class="card-title">AVALIAÇÃO GERAL ( ANAMSESE )</h3>
                                <div class="row ficha">
                                    <label class="col-md-4 col-form-label form-group">Tem histórico de dermatite ou câncer de pele? </label>
                                    <div class="col-md-2">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="" value="1" disabled>
                                            <label class="form-check-label" for=""> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="" value="0" disabled>
                                            <label class="form-check-label" for=""> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Em qual local do corpo?</label>
                                            <input type="text" class="form-control floating" name="fichaEmQualLocalDoCorpo" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Faz quanto tempo?</label>
                                            <input type="text" class="form-control floating" name="fichaFazQuantoTempo" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-4 col-form-label form-group">Foi liberado pelo médico para tratamento estético? </label>
                                    <div class="col-md-2">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" id="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" value="1" disabled>
                                            <label class="form-check-label" for="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" id="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" value="0" disabled>
                                            <label class="form-check-label" for="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico"> Não </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h3 class="card-title">Histórico de doenças</h3>
                                <div class="row ficha">
                                    <label class="col-md-1 col-form-label form-group">Hipertireoidismo?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHipertireoidismo" id="fichaHipertireoidismo" value="1" disabled>
                                            <label class="form-check-label" for="fichaHipertireoidismo"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHipertireoidismo" id="fichaHipertireoidismo" value="0" disabled>
                                            <label class="form-check-label" for="fichaHipertireoidismo"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoHipertireoidismo" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Hipotireoidismo?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHipotireoidismo" id="fichaHipotireoidismo" value="1" disabled>
                                            <label class="form-check-label" for="fichaHipotireoidismo"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHipotireoidismo" id="fichaHipotireoidismo" value="0" disabled>
                                            <label class="form-check-label" for="fichaHipotireoidismo"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoHipotireoidismo" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Diabetes?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaDiabetes" id="fichaDiabetes" value="1" disabled>
                                            <label class="form-check-label" for="fichaDiabetes"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaDiabetes" id="fichaDiabetes" value="0" disabled>
                                            <label class="form-check-label" for="fichaDiabetes"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoDiabetes" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Hipertensão?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHipertensao" id="fichaHipertensao" value="1" disabled>
                                            <label class="form-check-label" for="fichaHipertensao"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaHipertensao" id="fichaHipertensao" value="0" disabled>
                                            <label class="form-check-label" for="fichaHipertensao"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoHipertensao" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Eplepsia?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaEplepsia" id="fichaEplepsia" value="1" disabled>
                                            <label class="form-check-label" for="fichaEplepsia"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaEplepsia" id="fichaEplepsia" value="0" disabled>
                                            <label class="form-check-label" for="fichaEplepsia"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoEplepsia" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Psoríase?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPsoriase" id="fichaPsoriase" value="1" disabled>
                                            <label class="form-check-label" for="fichaPsoriase"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPsoriase" id="fichaPsoriase" value="0" disabled>
                                            <label class="form-check-label" for="fichaPsoriase"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoPsoriase" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <div class="row">
                                    <label class="col-md-1 col-form-label form-group">Tem Implantes metálicos?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaTemImplantesMetalicos" id="fichaTemImplantesMetalicos" value="1" disabled>
                                            <label class="form-check-label" for="fichaTemImplantesMetalicos"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaTemImplantesMetalicos" id="fichaTemImplantesMetalicos" value="0" disabled>
                                            <label class="form-check-label" for="fichaTemImplantesMetalicos"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Local?</label>
                                            <input type="text" class="form-control floating" name="fichaLocalTemImplantesMetalicos" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Portador de marcapasso?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPortadorDeMarcapasso" id="fichaPortadorDeMarcapasso" value="1" disabled>
                                            <label class="form-check-label" for="fichaPortadorDeMarcapasso"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPortadorDeMarcapasso" id="fichaPortadorDeMarcapasso" value="0" disabled>
                                            <label class="form-check-label" for="fichaPortadorDeMarcapasso"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">A quanto tempo?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantoTempoPortadorDeMarcapasso" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Portador de HIV?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPortadorDeHIV" id="fichaPortadorDeHIV" value="1" disabled>
                                            <label class="form-check-label" for="fichaPortadorDeHIV"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPortadorDeHIV" id="fichaPortadorDeHIV" value="0" disabled>
                                            <label class="form-check-label" for="fichaPortadorDeHIV"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Tratamento?</label>
                                            <input type="text" class="form-control floating" name="fichaTratamentoPortadorDeHIV" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Portador de Hepatite?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPortadorDeHepatite" id="fichaPortadorDeHepatite" value="1" disabled>
                                            <label class="form-check-label" for="fichaPortadorDeHepatite"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPortadorDeHepatite" id="fichaPortadorDeHepatite" value="0" disabled>
                                            <label class="form-check-label" for="fichaPortadorDeHepatite"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualPortadorDeHepatite" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Alteração hormonal?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaAlteracaoHormonal" id="fichaAlteracaoHormonal" value="1" disabled>
                                            <label class="form-check-label" for="fichaAlteracaoHormonal"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaAlteracaoHormonal" id="fichaAlteracaoHormonal" value="0" disabled>
                                            <label class="form-check-label" for="fichaAlteracaoHormonal"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualAlteracaoHormonal" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Alergia a algum alimento?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaAlergiaAlgumAlimento" id="fichaAlergiaAlgumAlimento" value="1" disabled>
                                            <label class="form-check-label" for="fichaAlergiaAlgumAlimento"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaAlergiaAlgumAlimento" id="fichaAlergiaAlgumAlimento" value="0" disabled>
                                            <label class="form-check-label" for="fichaAlergiaAlgumAlimento"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualAlergiaAlgumAlimento" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Alergia a algum medicamento?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaAlergiaAlgumMedicamento" id="fichaAlergiaAlgumMedicamento" value="1" disabled>
                                            <label class="form-check-label" for="fichaAlergiaAlgumMedicamento"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaAlergiaAlgumMedicamento" id="fichaAlergiaAlgumMedicamento" value="0" disabled>
                                            <label class="form-check-label" for="fichaAlergiaAlgumMedicamento"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualAlergiaAlgumMedicamento" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Faz uso de medicamento?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFazUsoDeMedicamento" id="fichaFazUsoDeMedicamento" value="1" disabled>
                                            <label class="form-check-label" for="fichaFazUsoDeMedicamento"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFazUsoDeMedicamento" id="fichaFazUsoDeMedicamento" value="0" disabled>
                                            <label class="form-check-label" for="fichaFazUsoDeMedicamento"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualFazUsoDeMedicamento" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Presença de queloides?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPresencaDeQueloides" id="fichaPresencaDeQueloides" value="1" disabled>
                                            <label class="form-check-label" for="fichaPresencaDeQueloides"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaPresencaDeQueloides" id="fichaPresencaDeQueloides" value="0" disabled>
                                            <label class="form-check-label" for="fichaPresencaDeQueloides"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQueLocalPresencaDeQueloides" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Gestante?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaGestante" id="fichaGestante" value="1" disabled>
                                            <label class="form-check-label" for="fichaGestante"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaGestante" id="fichaGestante" value="0" disabled>
                                            <label class="form-check-label" for="fichaGestante"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quanto tempo?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantoTempoGestante" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Filhos?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFilhos" id="fichaFilhos" value="1" disabled>
                                            <label class="form-check-label" for="fichaFilhos"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFilhos" id="fichaFilhos" value="0" disabled>
                                            <label class="form-check-label" for="fichaFilhos"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantos e que idade?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantosFilhosIdade" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Fez alguma cirurgia ou plástica?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFezAlgumaCirurgiaPlastica" id="fichaFezAlgumaCirurgiaPlastica" value="1" disabled>
                                            <label class="form-check-label" for="fichaFezAlgumaCirurgiaPlastica"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFezAlgumaCirurgiaPlastica" id="fichaFezAlgumaCirurgiaPlastica" value="0" disabled>
                                            <label class="form-check-label" for="fichaFezAlgumaCirurgiaPlastica"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Que local?</label>
                                            <input type="text" class="form-control floating" name="fichaQualFezAlgumaCirurgiaPlastica" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Realizou procedimentos estéticos anteriores?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaRealizouProcedimentosEsteticosAnteriores" id="fichaRealizouProcedimentosEsteticosAnteriores" value="1" disabled>
                                            <label class="form-check-label" for="fichaRealizouProcedimentosEsteticosAnteriores"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaRealizouProcedimentosEsteticosAnteriores" id="fichaRealizouProcedimentosEsteticosAnteriores" value="0" disabled>
                                            <label class="form-check-label" for="fichaRealizouProcedimentosEsteticosAnteriores"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual?</label>
                                            <input type="text" class="form-control floating" name="fichaQualRealizouProcedimentosEsteticosAnteriores"  readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Faz uso de maquiagem?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFazUsoDeMaquiagem" id="fichaFazUsoDeMaquiagem" value="1" disabled>
                                            <label class="form-check-label" for="fichaFazUsoDeMaquiagem"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFazUsoDeMaquiagem" id="fichaFazUsoDeMaquiagem" value="0" disabled>
                                            <label class="form-check-label" for="fichaFazUsoDeMaquiagem"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Como retira?</label>
                                            <input type="text" class="form-control floating" name="fichaQualFazUsoDeMaquiagem"  readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Faz atividade física?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFazAtividadeFisica" id="fichaFazAtividadeFisica" value="1" disabled>
                                            <label class="form-check-label" for="fichaFazAtividadeFisica"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaFazAtividadeFisica" id="fichaFazAtividadeFisica" value="0" disabled>
                                            <label class="form-check-label" for="fichaFazAtividadeFisica"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantas vezes por semana?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantasVezesPorSemanaAtividadeFisica"  readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Já fumou ou fuma?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaJaFumouOuFuma" id="fichaJaFumouOuFuma" value="1" disabled>
                                            <label class="form-check-label" for="fichaJaFumouOuFuma"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaJaFumouOuFuma" id="fichaJaFumouOuFuma" value="0" disabled>
                                            <label class="form-check-label" for="fichaJaFumouOuFuma"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantos cigarros por dia?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantosCigarrosPorDia" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-2 col-form-label form-group">Consumo de água?</label>
                                    <div class="col-md-10">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Ingere quantos copos ao dia?</label>
                                            <input type="text" class="form-control floating" name="fichaConsumoAguaIngereQuantosCoposDia" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Ingere bebida alcoólica?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaIngereBebidaAlcoolica" id="fichaIngereBebidaAlcoolica" value="1" disabled>
                                            <label class="form-check-label" for="fichaIngereBebidaAlcoolica"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaIngereBebidaAlcoolica" id="fichaIngereBebidaAlcoolica" value="0" disabled>
                                            <label class="form-check-label" for="fichaIngereBebidaAlcoolica"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantas doses por semana?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantasDosesPorSemanaBebidaAlcoolica" readonly>
                                        </div>
                                    </div>
                                    <label class="col-md-1 col-form-label form-group">Ingere bebida gaseificada?</label>
                                    <div class="col-md-1">
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaIngereBebidaGaseificada" id="fichaIngereBebidaGaseificada" value="1" disabled>
                                            <label class="form-check-label" for="fichaIngereBebidaGaseificada"> Sim </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="fichaIngereBebidaGaseificada" id="fichaIngereBebidaGaseificada" value="0" disabled>
                                            <label class="form-check-label" for="fichaIngereBebidaGaseificada"> Não </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantas doses por semana?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantasDosesPorSemanaBebidaGaseificada" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-focus select-focus select-focus">
                                            <label class="focus-label">Estresse</label>
                                            <input type="text" class="form-control floating" name="fichaEstresse" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-focus select-focus select-focus">
                                            <label class="focus-label">Posição que permanece por mais tempo</label>
                                            <input type="text" class="form-control floating" name="fichaPosicaoQuePermanecePorMaisTempo" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-focus select-focus select-focus">
                                            <label class="focus-label">Sono</label>
                                            <input type="text" class="form-control floating" name="fichaSono" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantas horas por noite?</label>
                                            <input type="text" class="form-control floating" name="fichaQuantasHorasPorNoiteSono" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus select-focus">
                                            <label class="focus-label">Alimentação</label>
                                            <input type="text" class="form-control floating" name="fichaAlimentacao" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Faz dieta - Qual e a quanto tempo?</label>
                                            <input type="text" class="form-control floating" name="fichaFazDietaQualQuantoTempo" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h3 class="card-title">Observações Gerais</h3>
                                
								
								<div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">O que te incomoda</label>
											<input type="text" class="form-control floating" name="fichaOqueTeIncomoda" value="" readonly>
                                        </div>
                                    </div>
									<div class="col-md-3">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual seu time?</label>
											<input type="text" class="form-control floating" name="fichaQualSeuTime" value="" readonly>
                                    
										</div>
									</div>
									<div class="col-md-3">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">O que você mudaria em você</label>
											<input type="text" class="form-control floating" name="fichaOqueVcMudariaEmVc" value="" readonly>
                                        </div>
                                    </div>
									<div class="col-md-3">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Qual seu estilo de musica</label>
											<input type="text" class="form-control floating" name="fichaQualSeuEstiloMusica" value="" readonly>
                                        </div>
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-focus focused">
                                            <label class="focus-label">Observações:</label>
                                            <textarea class="form-control floating" rows="3" cols="30" name="fichaOutrasDoencas" readonly></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
            <!-- FIM TAB FICHA GERAL -->
            
            <!-- TAB FICHA CORPORAL -->
            <div class="tab-pane" id="tabFichaCorporal">
                <div class="row fichaCorporal">
                    <div class="col-md-12">
                        <form id="formulario-corporal">
                            <div class="card-box">
                                <h3 class="card-title">AVALIAÇÃO CORPORAL</h3>
                                <h4 class="card-title">Hipolipodistrofia Ginoide (HLDG)</h4>
                                <div class="row ficha">
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Tipo:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalHipolipodistrofia" value="Flácida">
                                                        Flácida </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalHipolipodistrofia" value="Edematosa">
                                                        Edematosa </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalHipolipodistrofia" value="Compacta">
                                                        Compacta </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalHipolipodistrofia" value="Mista">
                                                        Mista </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Grau:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalGrau" value="1">
                                                        I </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalGrau" value="2">
                                                        II </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalGrau" value="3">
                                                        III </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalGrau" value="4">
                                                        IV </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label form-group">Temperatura:</label>
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input  class="validar" type="radio" name="fichaCorporalTemperatura"  value="Fria">
                                                        Fria </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input  class="validar" type="radio" name="fichaCorporalTemperatura" value="Quente">
                                                        Quente </label>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label form-group">Presença de dor à palpação:</label>
                                            <div class="col-md-9">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalPalpacao" value="sim">
                                                        Sim </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input  class="validar" type="radio" name="fichaCorporalPalpacao" value="não">
                                                        Não </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Localização:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalTemperaturaLocalizacao">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Coloração do tecido:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalTemperaturaColoracaoDoTecido">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">Edema</h4>
                                <div class="row ficha">
                                    <label class="col-md-2 col-form-label form-group">Teste de cacifo:</label>
                                    <div class="col-md-10">
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for=""> 
                                                <input class="form-check-input validar" type="radio" name="fichaCorporalEdemaTesteDeCacifo" id="" value="1">
                                            Positivo </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                                <input class="form-check-input validar" type="radio" name="fichaCorporalEdemaTesteDeCacifo" id="" value="0">
                                             Negativo </label>
                                        </div>
                                    </div>
                                    <label class="col-md-2 col-form-label form-group">Teste de digito-pressão:</label>
                                    <div class="col-md-10">
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                                <input class="form-check-input validar" type="radio" name="fichaCorporalEdemaTesteDeDigitoPressao" id="" value="1">
                                             Positivo </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                                <input class="form-check-input validar" type="radio" name="fichaCorporalEdemaTesteDeDigitoPressao" id="" value="0">
                                             Negativo </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Sensação de peso / Cansaço em MMII:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalEdemaSensacaoDePesoCansacoEmMMII">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Observações:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalEdemaObservacoes">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">Lipodistrofia</h4>
                                <div class="row ficha">
                                    <label class="col-md-2 col-form-label form-group">Gordura:</label>
                                    <div class="col-md-10">
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                            <input class="form-check-input validar" type="radio" name="fichaCorporalLipodistrofiaGorduraCompactaFlacida" id="" value="1">
                                             Compacta </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                            <input class="form-check-input validar" type="radio" name="fichaCorporalLipodistrofiaGorduraCompactaFlacida" id="" value="0">
                                             Flácida </label>
                                        </div>
                                    </div>
                                    <label class="col-md-2 col-form-label form-group">Distribuição de gordura:</label>
                                    <div class="col-md-10">
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                                <input class="form-check-input validar" type="radio" name="fichaCorporalLipodistrofiaDistribuicaoGordura" id="" value="1">
                                            Localizada </label>
                                        </div>
                                        <div class="form-group form-check form-check-inline">
                                            <label class="form-check-label" for="">
                                                <input class="form-check-input validar" type="radio" name="fichaCorporalLipodistrofiaDistribuicaoGordura" id="" value="0">
                                             Generalizada </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Localização:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalLipodistrofiaLocalizacao">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">Biótipo</h4>
                                <div class="row ficha">
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Tipo:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalBiotipo" value="Ginoide">
                                                        Ginoide </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalBiotipo" value="Androide">
                                                        Androide </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalBiotipo" value="Normolíneo">
                                                        Normolíneo </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Peso:</label>
                                                <input type="text" class="form-control floating imc" name="fichaCorporalBiotipoPeso">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Altura:</label>
                                                <input type="text" id="imc" class="form-control floating imc" name="fichaCorporalBiotipoAltura">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">IMC:</label>
                                                <input readonly type="text" class="form-control floating" value="0.0" name="fichaCorporalBiotipoIMC">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Peso min:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalBiotipoPesoMin">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Peso máx:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalBiotipoPesoMax">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Situação:</label>
                                                <input readonly value="Exibir mensagem após o resultado do IMC" type="text" class="form-control floating" name="fichaCorporalBiotipoIMCSituacao">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Observações:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalBiotipoObservacoes">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">Flacidez</h4>
                                <h5>Quantificar os itens abaixo: (+leve, ++moderado, +++Intenso, ++++Grave)</h5>
                                <div class="row ficha">
                                    <div class="col-md-2">
                                        <div class="form-group checkbox checkboxCorporal">
                                            <label>
                                                <input type="checkbox" name="fichaCorporalFlacidezTissular" value="0" checked hidden>
                                                <input type="checkbox" name="fichaCorporalFlacidezTissular" value="1">
                                                Tissular </label>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Localização da flacidez tissular:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalFlacidezLocalizacaoDaFlacidezTissular">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group checkbox checkboxCorporal">
                                            <label>
                                                <input type="checkbox" name="fichaCorporalFlacidezMuscular" value="0" checked hidden>
                                                <input type="checkbox" name="fichaCorporalFlacidezMuscular" value="1">
                                                Muscular </label>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Localização da flacidez muscular:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalFlacidezLocalizacaoDaFlacidezMuscular">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">Estrias</h4>
                                <div class="row ficha">
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Cor:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalEstriasCor" value="Rubra / violácea">
                                                        Rubra / violácea </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalEstriasCor" value="Alba">
                                                        Alba </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Largura:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalEstriasLargura" value="Fina">
                                                        Fina </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="validar" type="radio" name="fichaCorporalEstriasLargura" value="Larga">
                                                        Larga </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Quantidade: (+leve, ++moderado, +++Intenso, ++++Grave)</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalEstriasQuantidade">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Região:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalEstriasRegiao">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">Alterações Posturais</h4>
                                <div class="row ficha">
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Ombros:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisOmbrosAnteriorizacao" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisOmbrosAnteriorizacao" value="1">
                                                        Anteriorização </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisOmbrosHiperextensao" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisOmbrosHiperextensao" value="1">
                                                        Hiperextensão </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Coluna:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisColunaEscolioseEmC" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisColunaEscolioseEmC" value="1">
                                                        Escoliose em C </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="ColunaEscolioseEmS" value="0" checked hidden>
                                                        <input type="checkbox" name="ColunaEscolioseEmS" value="1">
                                                        Escoliose em S </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="ColunaHipercifose" value="0" checked hidden>
                                                        <input type="checkbox" name="ColunaHipercifose" value="1">
                                                        Hipercifose </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="ColunaHiperlordose" value="0" checked hidden>
                                                        <input type="checkbox" name="ColunaHiperlordose" value="1">
                                                        Hiperlordose </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="ColunaRetificacaoDaCifose" value="0" checked hidden>
                                                        <input type="checkbox" name="ColunaRetificacaoDaCifose" value="1">
                                                        Retificação da cifose </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="ColunaRetificacaoDaLordose" value="0" checked hidden>
                                                        <input type="checkbox" name="ColunaRetificacaoDaLordose" value="1">
                                                        Retificação da lordose </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Quadril:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="QuadrilAntiversao" value="0" checked hidden>
                                                        <input type="checkbox" name="QuadrilAntiversao" value="1">
                                                        Antiversão </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="QuadrilRetroversao" value="0" checked hidden>
                                                        <input type="checkbox" name="QuadrilRetroversao" value="1">
                                                        Retroversão </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label form-group">Joelhos:</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisJoelhosGenovalgo" value="0" checked hidden>
                                                        <input type="checkbox" name="fichaCorporalAlteracoesPosturaisJoelhosGenovalgo" value="1">
                                                        Genovalgo </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="JoelhosGenovaro" value="0" checked hidden>
                                                        <input type="checkbox" name="JoelhosGenovaro" value="1">
                                                        Genovaro </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="JoelhosHiperextensao" value="0" checked hidden>
                                                        <input type="checkbox" name="JoelhosHiperextensao" value="1">
                                                        Hiperextensão </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Outros:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalAlteracoesPosturaisOutros">
                                            <input type="text" class="idFicha form-control floating" name="idFichaGeral" value="" hidden>
                                        </div>
                                    </div>

                                   
                                </div>
                            </div>
                            <div class="card-box">
                                <h4 class="card-title">TRATAMENTO INDICADO</h4>
                                <div class="row ficha">
                                    <div class="col-md-8">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Obs:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalObsTratamentoIndicado">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Número de sessões?</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalQtdSessoes">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="text-center m-t-20">
                                            <button destino="ficha/inserirAvaliacaoCorporal" class="corporal-ajax btn btn-primary submit-btn" type="button">Cadastrar Avaliação Corporal</button>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                    <!-- FIM FASE 01 DA TAB FICHA CORPORAL
                         APÓS O CADASTRO, DEVE LIBERAR OS CAMPOS A BAIXO E RETORNAR 
                         O ID DA FICHA QUE ACABOU DE SER CADASTRADA.
                    -->
                        <form id="formPerimetria">
                            <div class="card-box">
                                <h4 class="card-title">Perimetria - Altura da fita</h4>
                                <div class="row ficha fichaCorporalPerimetria" style="display:none">
                                    <div class="col-md-4">
                                        <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Braço D:</label>
                                                <input type="text" class="idFichaCorporal" name="idFichaCorporal" hidden>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaBracoD">
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Braço E:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaBracoE">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Abd. Sup. Cintura:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaAbdSupCintura">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Abd Inf. Quadril:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaAbdInfQuadril">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Coxa Sup. D:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaCoxaSupD">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Coxa Sup. E:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaCoxaSupE">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Coxa Inf. D:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaCoxaInfD">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Coxa Inf. E:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaCoxaInfE">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Joelho D:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaJoelhoD">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Joelho E:</label>
                                                <input type="text" class="form-control floating" name="fichaCorporalPerimetriaJoelhoE">
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row" id="imgPerimetria">
                                            <!--<div class="col-sm-12 col-md-6 col-lg-4">
                                                <div class="blog grid-blog">
                                                    <div class="blog-image">
                                                        <img class="img-fluid" src="<?php echo URL;?>assets/img/ficha/modelo.jpg" alt="">
                                                    </div>
                                                    <div class="blog-content">
                                                        <p>Lorem ipsum dolor sit amet, consectetur em adipiscing elit, sed do eiusmod tempor incididunt ut labore etmis dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco sit laboris.</p>
                                                    </div>
                                                </div>
                                            </div>-->
                                            <div class="col-sm-12 col-md-3 col-lg-3" data-toggle="modal" data-target="#exampleModalCenter" style="cursor: pointer;">
                                                <div class="blog grid-blog btn-fotos" style="display: none;">
                                                    <div class="blog-image">
                                                        <img class="img-fluid" src="<?php echo URL;?>assets/img/ficha/add-img.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="text-center m-t-20">
                                            <button destino="ficha/inserirPerimetria" class="perimetria-ajax btn btn-primary submit-btn" type="button">Cadastrar Perimetria</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form id="formAdipometria">
                            <div class="card-box">
                                <h4 class="card-title">Adipometria</h4>
                                <div class="row fichaCorporalAdipometria" style="display:none">
                                    <div class="col-md-5">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Região do Corpo:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalAdipometriaRegiaoCorpo[]">
                                            <input type="text" class="idFichaCorporal form-control floating" name="idFichaCorporal" value="" hidden>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Prega em CM:</label>
                                            <input type="text" class="form-control floating" name="fichaCorporalAdipometriaPregaCM[]">
                                            <button class="addCampoAdipometria btn btn-primary" type="button">+</button>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="text-center m-t-20">
                                            <button destino="ficha/inserirAdipometria" class="adipometria-ajax btn btn-primary submit-btn" type="button">Cadastrar Adipometria</button>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </form>
                            <!-- ADD CORPO POSIÇÃO ANATÔMICA: -->
                            <div class="card-box">
                                <h4 class="card-title">CORPO POSIÇÃO ANATÔMICA:</h4>
                                <div class="row ficha corpo-anatomica" style="display:none">
                                    <div id="corpo-ajax" class="col-md-6">
                                    </div>
                                    <div class="col-md-6">
                                        <ol class="list-number-rounded lista-corpo">
                                        </ol>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="text-center m-t-20">
                                        <button class="add-marcacao-corpotal-ajax btn btn-primary submit-btn" type="button">Cadastrar Detalhes Avaliação Corporal</button>
                                        </div>
                                    </div>
                                </div>
                            </div>      
                    </div>
                </div>
            </div>
        <div class="tab-pane" id="tabFichaFacil">
            <div class="row">
                <div class="col-md-12">
                    <form id="formFacial">
                        <div class="card-box">
                            <h3 class="card-title">AVALIAÇÃO FACIAL</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row linhaFacil">
                                        <label class="col-md-2 col-form-label form-group">Queixa Principal:</label>
                                        <div class="col-md-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilLinhasDeExpressao" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilLinhasDeExpressao" value="1">
                                                    Linhas de expressão </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilCicatrizesDeAcneCicatrizAtrofica"  value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilCicatrizesDeAcneCicatrizAtrofica"  value="1">
                                                    Cicatrizes de Acne / Cicatriz Atrófica </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilEnvelhecimentoPrecoce" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilEnvelhecimentoPrecoce" value="1">
                                                    Envelhecimento Precoce </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilFlacidez" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilFlacidez" value="1">
                                                    Flacidez </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilDesidratacao" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilDesidratacao" value="1">
                                                    Desidratação </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilManchas" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilManchas" value="1">
                                                    Manchas </label>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Outra:</label>
                                                <input type="text" class="form-control floating" name="fichaFacilOutra" value=" ">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        <label class="col-md-6 col-form-label form-group">Fototipo de pele:</label>
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilFototipoDePele" value="1">
                                                    I </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilFototipoDePele" value="2">
                                                    II </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilFototipoDePele" value="3">
                                                    III </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilFototipoDePele" value="4">
                                                    IV </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilFototipoDePele" value="5">
                                                    V </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        <label class="col-md-6 col-form-label form-group">Biotipo cutâneo:</label>
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilBiotipoCutaneo" value="Eudérmica">
                                                    Eudérmica </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilBiotipoCutaneo" value="Lipídica">
                                                    Lipídica </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilBiotipoCutaneo" value="Alípica">
                                                    Alípica </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilBiotipoCutaneo" value="Mista">
                                                    Mista </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        <label class="col-md-6 col-form-label form-group">Hidratação cutânea:</label>
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilHidratadaCutanea" value="Hidratada">
                                                    Hidratada </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="validar" type="radio" name="fichaFacilHidratadaCutanea" value="Desidratada">
                                                    Desidratada </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group row">
                                        <label class="col-md-6 col-form-label form-group">Desiquilíbrio da pele:</label>
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilComedao" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilComedao" value="1">
                                                    Comedão </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilMilium" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilMilium" value="1">
                                                    Mílium </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilPapula" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilPapula" value="1">
                                                    Pápula </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilPustula" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilPustula" value="1">
                                                    Pústula </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilMicrocisto" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilMicrocisto" value="1">
                                                    Microcisto </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilNodulo" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilNodulo" value="1">
                                                    Nódulo </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilQueratoseActinica" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilQueratoseActinica" value="1">
                                                    Queratose Actínica </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="fichaFacilDermatosePapulosaNigra" value="0" hidden checked>
                                                    <input type="checkbox" name="fichaFacilDermatosePapulosaNigra" value="1">
                                                    Dermatose Papulosa Nigra </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Obs:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilObsDesiquilibrioDaPele">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">Sequela de Acne</h4>
                            <div class="row ficha">
                                <div class="col-md-2">
                                    <div class="form-group form-check form-check-inline">
                                        <label class="form-check-label" for=""> 
                                            <input class="form-check-input validar" type="radio" name="fichaFacilSequelaDeAcne" id="" value="1">
                                        Sim </label>
                                    </div>
                                    <div class="form-group form-check form-check-inline">
                                        <label class="form-check-label" for="">
                                            <input class="form-check-input validar" type="radio" name="fichaFacilSequelaDeAcne" id="" value="0">
                                         Não </label>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Região:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilRegiaoSequelaDeAcne">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">Exposição solar atual</h4>
                            <div class="row ficha">
                                <div class="col-md-4">
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilExposiccaoSolar" value="pouco">
                                            Pouco </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilExposiccaoSolar" value="Médio">
                                            Médio </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilExposiccaoSolar" value="Alto">
                                            Alto </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Frequência:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilFrequenciaProtecaoSolar">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">Hábito de proteção solar</h4>
                            <div class="row ficha">
                                <div class="col-md-4">
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            
                                            <input type="checkbox" name="fichaFacilNaoUsa"  value="1">
                                            Não usa </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                           
                                            <input type="checkbox" name="fichaFacilUsoSomenteQuandoExpostoAoSol" value="1">
                                            Somente quando exposto ao sol </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Frequência:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilFrequencia">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input type="checkbox" name="fichaFacilProtecaoSolarUsoEsporadico" value="0" hidden checked>
                                            <input type="checkbox" name="fichaFacilProtecaoSolarUsoEsporadico" value="1">
                                            Uso esporádico </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input type="checkbox" name="fichaFacilProtecaoSolarUsoDiario" value="0" hidden checked>
                                            <input type="checkbox" name="fichaFacilProtecaoSolarUsoDiario" value="1">
                                            Uso diário </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Frequência:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilProtecaoSolarFrequencia">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">Histórico de cicatrização</h4>
                            <div class="row ficha">
                                <div class="col-md-4">
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHistorico" value="Sem histórico">
                                            Sem histórico </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHistorico" value="Cicatriz hipertrófica">
                                            Cicatriz hipertrófica </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHistorico" value="Cicatriz hipotrófica">
                                            Cicatriz hipotrófica </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">Hipercromia</h4>
                            <div class="row ficha">
                                <div class="col-md-4">
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHipercromia" value="Não">
                                            Não </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHipercromia" value="Hipercromia pós inflamatória / pós traumática">
                                            Hipercromia pós inflamatória / pós traumática </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHipercromia" value="Mancha de gravidez / Cloasma">
                                            Mancha de gravidez / Cloasma </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHipercromia" value="Mancha solares">
                                            Mancha solares </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilHipercromia" value="Melasma Profundidade">
                                            Melasma Profundidade </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Região:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilRegiaoHipercromia">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">Pele sensível</h4>
                            <div class="row ficha">
                                <div class="col-md-2">
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilPeleSensivel" value="Não">
                                            Não </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilPeleSensivel" value="Dermatite seborreica">
                                            Dermatite seborreica </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilPeleSensivel" value="Rosácea">
                                            Rosácea </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilPeleSensivel" value="Telangiectasia">
                                            Telangiectasia </label>
                                    </div>
                                    <div class="form-group checkbox checkboxCorporal">
                                        <label>
                                            <input class="validar" type="radio" name="fichaFacilPeleSensivel" value="Eritema">
                                            Eritema </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">TRATAMENTO INDICADO</h4>
                            <div class="row ficha">
                                <div class="col-md-8">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Obs:</label>
                                        <input type="text" class="form-control floating" name="fichaFacilObsTratamentoIndicado">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Número de sessões?</label>
                                        <input type="text" class="form-control floating" name="fichaFacilQtdSessoes">
                                        <input type="text" class="idFicha form-control floating" name="idFichaGeral" value="" hidden>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="text-center m-t-20">
                                        <button destino="ficha/inserirAvaliacaoFacial" class="facial-ajax btn btn-primary submit-btn" type="button">Cadastrar Avaliação Facial</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- ADD FACIL MARCAÇÃO ANATÔMICA:  -->
                        <div class="card-box">
                            <h4 class="card-title">FACIAL MARCAÇÃO ANATÔMICA:</h4>
                            <div class="row ficha blocofacial" style="display:none">
                                <div id="facial-ajax" class="col-md-6"> </div>
                                <div class="col-md-6">
                                    <ol class="list-number-rounded lista-facial">
                                    </ol>
                                    <input type="text" class="idFichaFacil" name="idFichaFacial" value="" hidden>
                                </div>
                            </div>
                        </div>
                        <div class="card-box blocofacial" style="display:none">
                            <div class="text-center m-t-20">
                                <button class="add-marcacao-facial-ajax btn btn-primary submit-btn" type="button">Cadastrar marcação facial</button>
                            </div>
                        </div>
                        
                    </form>
                    
                    
                </div>
            </div>
        </div>
        <div class="tab-pane" id="tabTratamentoIndicado">
            <div class="row">
                <div class="col-md-12">
                    <form classe="TratamentoIndicado">
                        <div class="card-box">
                            <h4 class="card-title">AVALIAÇÃO CORPORAL</h4>
                            <div class="row ficha">
                                <div class="col-md-2">
                                    <div class="form-group form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="tratamentoIndicadoCorporal" id="tratamentoIndicadoCorporal" value="1">
                                        <label class="form-check-label" for="tratamentoIndicadoCorporal"> Sim </label>
                                    </div>
                                    <div class="form-group form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="tratamentoIndicadoCorporal" id="tratamentoIndicadoCorporal" value="0">
                                        <label class="form-check-label" for="tratamentoIndicadoCorporal"> Não </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Região:</label>
                                        <input type="text" class="form-control floating" name="tratamentoIndicadoCorporalRegiao">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Número de sessões?</label>
                                        <input type="text" class="form-control floating" name="tratamentoIndicadoCorporalNumeroSessoes">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h4 class="card-title">AVALIAÇÃO FACIAL</h4>
                            <div class="row ficha">
                                <div class="col-md-2">
                                    <div class="form-group form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="tratamentoIndicadoFacial" id="tratamentoIndicadoFacial" value="1">
                                        <label class="form-check-label" for="tratamentoIndicadoFacial"> Sim </label>
                                    </div>
                                    <div class="form-group form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="tratamentoIndicadoFacial" id="tratamentoIndicadoFacial" value="0">
                                        <label class="form-check-label" for="tratamentoIndicadoFacial"> Não </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Região:</label>
                                        <input type="text" class="form-control floating" name="tratamentoIndicadoFacialRegiao">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group form-focus select-focus">
                                        <label class="focus-label">Número de sessões?</label>
                                        <input type="text" class="form-control floating" name="tratamentoIndicadoFacialNumeroSessoes">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <h3 class="card-title">Termo;</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Estou de acordo com a veracidade das informações acima relacionadas, não cabendo ao profissional esteta nenhuma responsabilidade por fatos omitidos ou falsos.</h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group profile-img-wrap fichaAssinatura"> <img id="assinaturacliente" class="inline-block" src="<?php echo URL; ?>assets/img/assinatura.png" alt="user"><!--
                                        <div class="fileupload btn"> <span class="btn-text">Assinatura Biomédico</span>
                                            <input class="upload" type="file" >
                                        </div> -->
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group profile-img-wrap fichaAssinatura"> <img class="inline-block" src="<?php echo URL; ?>assets/img/assinatura.png" alt="user"><!--
                                        <div class="fileupload btn"> <span class="btn-text">Assinatura Cliente</span>
                                            <input class="upload" type="file">-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="card-box">
                            <div class="text-center m-t-20">
                                <button destino="ficha/inserirTratamentoIndicado" class="indicado-ajax btn btn-primary submit-btn" type="button">Cadastrar Tratamento Indicado</button>
                            </div>
                        </div>
                    </form>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none"> Erro ao Cadastrar Tratamento Indicado.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none"> Registro Cadastrado com Sucesso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
<div class="sidebar-overlay" data-reff=""></div>

 <!-- Modal -->
 <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Adicionar Foto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-success alert-dismissible fade notificacao-foto-true show" role="alert" style="display:none">
                        Curso cadastrado com sucesso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="alert alert-danger alert-dismissible notificacao-foto-false fade show" role="alert" style="display:none">
                        Erro ao cadastrar Curso. 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div> 

                    <form id="formImgFicha">
                            <div class="card-box">
                                <h3 class="card-title">Imagem</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="profile-img-wrap" style="position: relative;width: 100%;">
                                            <div class="profile" style="background-image: url('<?php echo URL; ?>assets/img/pacote/sem-foto.jpg');">
                                                <label class="edit">
                                                    <span><i class="mdi mdi-upload"></i></span>
                                                    <input type="text" class="idFichaCorporal" name="idFichaCorporal" hidden>
                                                    <input type="file" size="32" name="foto" id="inputImagem">
                                                </label>
                                            </div>
                                            <button id="editarImagem" type="button" class="btn btn-primary btn-sm">
                                                <i class="fa fa-file-image-o"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" class="novo-img-ficha-ajax btn btn-primary">Salvar</button>
                </div>
            </div>
        </div>
    </div>
