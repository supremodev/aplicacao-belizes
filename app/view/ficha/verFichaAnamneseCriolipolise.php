<div class="page-wrapper">
<div class="content">
    <div class="alert alert-danger alert-dismissible fade show " role="alert" style="display:none"> Erro ao Cadastrar Anamnese.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
    <div class="alert alert-success alert-dismissible fade show " role="alert" style="display:none"> Ficha anamnese cadastrado com Sucesso.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">FICHA ANAMNESE CRIOLIPÓLISE</h4>
        </div>
    </div>

    <?php foreach ($listaAnamnese as $linha) {?>
    <form id="formularios">
        <div class="card-box">
            <h3 class="card-title">Informações do Paciente</h3>
            <div class="row">
                <div class="col-md-12">
                <div class="profile-img-wrap">
                     <img id="img" class="inline-block" src="<?php 
                                                    if (!empty($linha->cliFoto)){
														echo URL."/assets/img/cliente/".$linha->cliFoto;
                                                    }elseif(!empty($pesquisa->cliWebcam)){
                                                        echo $linha->cliWebcam;
                                                    }else{
                                                        echo URL."/assets/img/user.jpg";
                                                    }
                                                    ?>" alt="user">
                </div>
                <div class="profile-basic">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Nome:*</label>
                                <input id="nome" type="text" class="form-control floating" value="<?php echo $linha->cliNome;?>" readonly>
                                <input type="text" id="idCliente" class="form-control floating" name="idCliente" hidden>
                                <input type="text" class="floating" name="idFunc" hidden>
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Pesquisa por CPF:*</label>
                                <input type="text" name="" value="<?php echo $linha->cliCPF;?>" class="ajax-consulta-ficha-necessario form-control floating" max="11" maxlength="11" pattern="([0-9]{11})" required>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Data de Nascimento:</label>
                                <div class="cal-icon">
                                    <input id="nasci" class="form-control floating" type="text" value="<?php echo $linha->cliNasci;?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Profissão:</label>
                                <input id="profissao" type="text" class="form-control floating" value="<?php echo $linha->cliProfissao;?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group form-focus select-focus select-focus">
                                <label class="focus-label">Sexo</label>
                                <input id="sexo" type="text" class="form-control floating" value="<?php echo $linha->cliSexo;?>" readonly>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-box">
            <h3 class="card-title">ANAMNESE</h3>
            <div class="row ficha">
                <label class="col-md-2 col-form-label form-group">Pratica atividade física</label>
                <div class="col-md-2">
                    <div class="form-group form-check form-check-inline">
                    <label class="form-check-label" for="fichaAnamnesePraticaAtividadeFísica">
                        <input class="form-check-input validar" type="radio" name="fichaAnamnesePraticaAtividadeFisica" id="" value="1" <?php if($linha->fichaAnamnesePraticaAtividadeFisica == "1"){echo "checked";};?>>
                         Sim </label>
                    </div>
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamnesePraticaAtividadeFísica">
                        <input class="form-check-input validar" type="radio" name="fichaAnamnesePraticaAtividadeFisica" id="" value="0" <?php if($linha->fichaAnamnesePraticaAtividadeFisica == "0"){echo "checked";};?>>
                         Não </label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group form-focus">
                        <label class="focus-label">Qual?</label>
                        <input type="text" class="form-control floating" name="fichaAnamneseQualPraticaAtividadeFisica" value="<?php echo $linha->fichaAnamneseQualPraticaAtividadeFisica;?>">
                    </div>
                </div>
                <label class="col-md-2 col-form-label form-group">Tem algum tipo de alergia</label>
                <div class="col-md-2">
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseTemAlgumTipoDeAlergia	">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseTemAlgumTipoDeAlergia" id="" value="1"<?php if($linha->fichaAnamneseTemAlgumTipoDeAlergia == "1"){echo " checked";};?>>
                         Sim </label>
                    </div>
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseTemAlgumTipoDeAlergia	">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseTemAlgumTipoDeAlergia" id="" value="0"<?php if($linha->fichaAnamneseTemAlgumTipoDeAlergia == "0"){echo " checked";};?>>
                         Não </label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group form-focus">
                        <label class="focus-label">Qual?</label>
                        <input type="text" class="form-control floating" name="fichaAnamneseQualAlgumTipoDeAlergia" value="<?php echo $linha->fichaAnamneseQualAlgumTipoDeAlergia;?>">
                    </div>
                </div>
                <label class="col-md-2 col-form-label form-group">Tem algum tipo de hormonal</label>
                <div class="col-md-2">
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseTemAlgumtipodehormonal">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseTemAlgumtipodehormonal" id="" value="1"<?php if($linha->fichaAnamneseTemAlgumtipodehormonal == "1"){echo " checked";};?>>
                         Sim </label>
                    </div>
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseTemAlgumtipodehormonal">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseTemAlgumtipodehormonal" id="" value="0"<?php if($linha->fichaAnamneseTemAlgumtipodehormonal == "0"){echo " checked";};?>>
                         Não </label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group form-focus">
                        <label class="focus-label">Qual?</label>
                        <input type="text" class="form-control floating" name="fichaAnamneseQualAlgumtipodehormonal" value="<?php echo $linha->fichaAnamneseQualAlgumtipodehormonal;?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group form-focus">
                        <label class="focus-label">Peso:</label>
                        <input type="text" class="form-control floating" name="fichaAnamnesePeso" value="<?php echo $linha->fichaAnamnesePeso;?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="card-box">
            <h3 class="card-title">HISTÓRICO MÉDICO</h3>
            <div class="row ficha">
                <label class="col-md-2 col-form-label form-group">Doenças relacionadas ao frio?</label>
                <div class="col-md-2">
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseDoencasRelacionadasAoFrio">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseDoencasRelacionadasAoFrio" id="" value="1"<?php if($linha->fichaAnamneseDoencasRelacionadasAoFrio == "1"){echo " checked";};?>
                         Sim </label>
                    </div>
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseDoencasRelacionadasAoFrio">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseDoencasRelacionadasAoFrio" id="" value="0"<?php if($linha->fichaAnamneseDoencasRelacionadasAoFrio == "0"){echo " checked";};?>>
                         Não </label>
                    </div>
                </div>
                <label class="col-md-2 col-form-label form-group">Hérnias (umbilical ou inguinal)?</label>
                <div class="col-md-2">
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseHerniasUmbilicalOuInguinal">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseHerniasUmbilicalOuInguinal" id="" value="1"<?php if($linha->fichaAnamneseHerniasUmbilicalOuInguinal == "1"){echo " checked";};?>>
                         Sim </label>
                    </div>
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseDoencasRelacionadasAoFrio">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseHerniasUmbilicalOuInguinal" id="" value="0"<?php if($linha->fichaAnamneseHerniasUmbilicalOuInguinal == "0"){echo " checked";};?>>
                            Não </label>
                    </div>
                </div>
                <label class="col-md-2 col-form-label form-group">Diástase Abdominal?</label>
                <div class="col-md-2">
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseDiastaseAbdominal">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseDiastaseAbdominal" id="" value="1"<?php if($linha->fichaAnamneseDiastaseAbdominal == "1"){echo " checked";};?>>
                         Sim </label>
                    </div>
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseDiastaseAbdominal">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseDiastaseAbdominal" id="" value="0"<?php if($linha->fichaAnamneseDiastaseAbdominal == "0"){echo " checked";};?>>
                         Não </label>
                    </div>
                </div>
                <label class="col-md-2 col-form-label form-group">Lesão aberta na pele (furúnculo/dermatites,eczema)?</label>
                <div class="col-md-2">
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseLesaoAbertaNaPele	">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseLesaoAbertaNaPele" id="" value="1"<?php if($linha->fichaAnamneseLesaoAbertaNaPele == "1"){echo " checked";};?>>
                         Sim </label>
                    </div>
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseLesaoAbertaNaPele	">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseLesaoAbertaNaPele" id="" value="0"<?php if($linha->fichaAnamneseLesaoAbertaNaPele == "0"){echo " checked";};?>>
                         Não </label>
                    </div>
                </div>
                <label class="col-md-2 col-form-label form-group">Lúpus eritematoso sistêmico?</label>
                <div class="col-md-2">
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseLupusEritematosoSistemico	">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseLupusEritematosoSistemico" id="" value="1"<?php if($linha->fichaAnamneseLupusEritematosoSistemico == "1"){echo " checked";};?>>
                         Sim </label>
                    </div>
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseLupusEritematosoSistemico	">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseLupusEritematosoSistemico" id="" value="0"<?php if($linha->fichaAnamneseLupusEritematosoSistemico == "0"){echo " checked";};?>>
                         Não </label>
                    </div>
                </div>
                <label class="col-md-2 col-form-label form-group">Câncer de Pele?</label>
                <div class="col-md-2">
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseCancerDePele">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseCancerDePele" id="" value="1"<?php if($linha->fichaAnamneseCancerDePele == "1"){echo " checked";};?>>
                         Sim </label>
                    </div>
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseCancerDePele">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseCancerDePele" id="" value="0"<?php if($linha->fichaAnamneseCancerDePele == "0"){echo " checked";};?>>
                         Não </label>
                    </div>
                </div>
                <label class="col-md-2 col-form-label form-group">Insuficiência circulatória periférica (varizes graves)?</label>
                <div class="col-md-2">
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves" id="" value="1"<?php if($linha->fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves == "1"){echo " checked";};?>>
                         Sim </label>
                    </div>
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves" id="" value="0"<?php if($linha->fichaAnamneseInsuficienciaCirculatoriaPerifericaVarizesGraves == "0"){echo " checked";};?>>
                         Não </label>
                    </div>
                </div>
                <label class="col-md-2 col-form-label form-group">Uso de anticoagulante?</label>
                <div class="col-md-2">
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseUsoDeAnticoagulante">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseUsoDeAnticoagulante" id="" value="1"<?php if($linha->fichaAnamneseUsoDeAnticoagulante == "1"){echo " checked";};?>>
                         Sim </label>
                    </div>
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseUsoDeAnticoagulante">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseUsoDeAnticoagulante" id="" value="0"<?php if($linha->fichaAnamneseUsoDeAnticoagulante == "0"){echo " checked";};?>>
                         Não </label>
                    </div>
                </div>
                <label class="col-md-2 col-form-label form-group">Cirurgia recente na área a ser tratada?</label>
                <div class="col-md-2">
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseCirurgiaRecenteNaAreaTratada">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseCirurgiaRecenteNaAreaTratada" id="" value="1"<?php if($linha->fichaAnamneseCirurgiaRecenteNaAreaTratada == "1"){echo " checked";};?>>
                         Sim </label>
                    </div>
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseCirurgiaRecenteNaAreaTratada"> 
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseCirurgiaRecenteNaAreaTratada" id="" value="0"<?php if($linha->fichaAnamneseCirurgiaRecenteNaAreaTratada == "0"){echo " checked";};?>>
                        Não </label>
                    </div>
                </div>
                <label class="col-md-2 col-form-label form-group">Gestante e ou Amamentando?</label>
                <div class="col-md-2">
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseGestanteeOuAmamentando">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseGestanteeOuAmamentando" id="" value="1"<?php if($linha->fichaAnamneseGestanteeOuAmamentando == "1"){echo " checked";};?>>
                         Sim </label>
                    </div>
                    <div class="form-group form-check form-check-inline">
                        <label class="form-check-label" for="fichaAnamneseGestanteeOuAmamentando">
                        <input class="form-check-input validar" type="radio" name="fichaAnamneseGestanteeOuAmamentando" id="" value="0"<?php if($linha->fichaAnamneseGestanteeOuAmamentando == "0"){echo " checked";};?>>
                         Não </label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group form-focus">
                        <label class="focus-label">Observações:</label>
                        <textarea class="form-control floating" rows="3" cols="30" name='fichaAnamneseObservacoes'><?php echo $linha->fichaAnamneseObservacoes;?></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-box">
            <h3 class="card-title">CORPO POSIÇÃO ANATÔMICA: </h3>
                <div class="row blococorporal">
                    <div id="corpo-ajax" class="col-md-6" >
                    <?php if ($linha->cliSexo =="Masculino") {
                        require APP . 'view/corpo/homem.php';
                    } else {
                        require APP . 'view/corpo/mulher.php';
                    } ?>

                    </div>
                    <div class="col-md-6">
                        <ol class="list-number-rounded lista-corpo">
                        <?php foreach ($listaMarcacao as $linha) {?>
                            <li class="obsMarcacao"><a><?php echo $linha->obsAnamneseCriolipolise;?></a></li>
                        <?php } ?>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
    </form>
    <?php }?>
</div>
<div class="sidebar-overlay" data-reff=""></div> 
