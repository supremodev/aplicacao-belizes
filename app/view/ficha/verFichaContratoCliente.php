<div class="page-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">CONTRATO DE PRESTAÇÃO DE SERVIÇOS ESTÉTICOS</h4>
            </div>
        </div>

        <?php foreach ($fichaContratoCliente as $linha) { ?>
            <form id="formularios">
                <div class="card-box">
                    <h3 class="card-title">Informações do Profissional</h3>
                    <div class="row contrato">
                        <div class="col-md-1">
                            <h5>Eu,</h5>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group ">
                                <input id="nome" type="text" class="form-control floating" value="<?php echo $linha->funcNome; ?>" readonly>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <p>Profissional em Estética corporal e facial, presto serviço na área de estética no espaço Belize´s Time estética facial e corporal, assim registro para os devidos fins minha responsabilidade na prestação destes serviços.</p>
                        </div>
                        <div class="col-md-1">
                            <h5>Eu,</h5>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group form-focus">
                                <input id="idCliente" name="idCliente" type="text" class="form-control floating" hidden>
                                <input id="nome" type="text" value="<?php echo $linha->cliNome; ?>" class="form-control floating" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <p>cliente do espaço Belize´s Time portador(a) do RG:</p>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-focus">
                                <input id="rg" type="text" class="form-control floating" value="<?php echo $linha->cliRG; ?>" readonly>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <p>Data De Nasc.:</p>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group form-focus">

                                <div class="cal-icon">
                                    <input id="nasci" class="form-control floating" type="text" value="<?php echo $linha->cliNasci; ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <p>CPF:</p>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group form-focus">
                                <input id="cpf" type="text" value="<?php echo $linha->cliCPF; ?>" class="ajax-consulta-ficha-contrato form-control floating" readonly>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <p>Endereço:</p>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group form-focus">
                                <input id="end" type="text" value="<?php echo $linha->cliEnd; ?>" class="form-control floating" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <p>venho através deste realizar tratamento estético, nas seguintes condições desse espaço.</p>
                        </div>
                    </div>
                </div>
                <div class="card-box">
                    <h3 class="card-title">Regras gerais:</h3>
                    <div class="row ficha">
                        <div class="col-md-12">
                            <ol class="contratoLista">
                                <li>O não comparecimento em sessenta dias, sem aviso prévio, para a realização do tratamento estético, o mesmo será cancelado e não será devolvido o valor já pago. <br>
                                    <strong>OBS:</strong> Caso o espaço venha fazer a devolução do valor, por algum motivo específico, será devolvido o mesmo, com desconto da máquina de cartão, se pago com cartão, pagamento da comissão do profissional, entre outros.</li>
                                <li>O plano de tratamento não pode ser transferido, porém podendo ser analisado. Caso a/o cliente opte pela desistência do tratamento após sete dias do início do mesmo, não será devolvido o valor do primeiro mês de tratamento, ou primeira parcela. Caso queira mudar de um tratamento que já faz, para outro, avisar com antecedência, informando qual motivo e qual novo tratamento, pagando a diferença se houver, do novo tratamento. </li>
                                <li>O não comparecimento nas sessões sem aviso com pelo menos uma hora de antecedência, será descontado como sessão realizada.</li>
                                <li>Não será permitido chegar com atraso na sessão, caso ocorra algum imprevisto avisar, para que a sessão seja remarcada para o próximo horário caso houver vaga.</li>
                                <li>A partir da data de pagamento em um tratamento pacote, casos de doença, entre outros, será válido por 6 meses, assim conversado com o profissional para melhor solução, ou até renovação do mesmo, atualizado em valores.</li>
                                <li>O tratamento realizado inclui procedimentos específicos para a recuperação e manutenção do cliente, fica o cliente ciente das obrigações e orientações passadas pelo profissional, bem como os cuidados diários home care.</li>
                                <li>Paciente está ciente que tratamento estético, é um tratamento, com resultados dependendo muito de paciente para paciente: como qualidade de vida, alimentação saudável, atividade física, uso constante de filtro solar, manutenção em dia dos tratamentos estético, entre outros. Podendo ver resultados logo de início ou meses depois.</li>
                                <li>Os profissionais atuantes no tratamento se comprometem em realizar tudo o que está descrito nas cláusulas acima citadas desde que os clientes cumpram com suas responsabilidades e obrigações.</li>
                                <li>Cada equipamento estético tem seu tempo para um resultado satisfatório, sendo que alguns só terá um bom resultado, em conjunto com outro equipamento.</li>
                                <li>O cliente fica ciente que terá que seguir as orientações do profissional, ficando assim o profissional livre de quaisquer danos decorrentes ao mau uso dos produtos e o não cumprimento das orientações.</li>
                                <li>O tratamento não seguido dias corretos para próxima sessão, pode não gerar um bom resultado, assim não tendo cobrança do paciente, da clínica, nenhuma insatisfação.</li>
                                <li>O espaço de estética não se responsabiliza por produtos facial e corporal em home care no qual não foi indicado.</li>
                                <li>Agendamento para próximas sessões, o cliente deve entrar em contato com profissional no celular do estabelecimento ou telefone fixo, com uma semana antes, pois a agenda é feita todos os dias, ás vezes não havendo horário se não for com antecedência, passando do prazo da próxima sessão.</li>
                                <li>Fica o cliente ciente de que o não esclarecimento e a omissão de antecedentes de qualquer tipo, deixa o profissional isento de qualquer responsabilidade.</li>
                            </ol>
                            <h5>OBS.: Caso não esteja de acordo com algum item citado, favor nos informar antecipadamente.</h5>
                            <h5>Li, e estou totalmente de acordo.</h5>
                            <h5>Tratamento contratado:</h5>
                        </div>
                        <div class="col-md-12">
                        <?php if(!empty($listaServicos)){ ?>
                            <div class="form-group">
                                <label>Serviços </label>
                                    <span class="select2 select2-container select2-container--default select2-container--below select2-container--focus" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1">
                                        <ul class="select2-selection__rendered">       
                                            <?php foreach ($listaServicos as $servico) { ?>
                                                <li class="select2-selection__choice" title="Maquiagens"><?php echo $servico->servicoNome;?></li>
                                            <?php } ?>
                                            <li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="" style="width: 0.75em;"></li>
                                        </ul>
                                    </span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                            <?php } else { ?>
                            <div class="form-group">
                                <label>Tratamento </label>
                                    <span class="select2 select2-container select2-container--default select2-container--below select2-container--focus" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1">
                                        <ul class="select2-selection__rendered">       
                                            <?php foreach ($listaPacotes as $pacote) { ?>
                                                <li class="select2-selection__choice" title="Maquiagens"><?php echo $pacote->pacoteNome;?></li>
                                            <?php } ?>
                                            <li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" role="textbox" aria-autocomplete="list" placeholder="" style="width: 0.75em;"></li>
                                        </ul>
                                    </span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                            <?php } ?>   

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="focus-label">Início do tratamento em:</label>
                                <div class="cal-icon">
                                    <input name="contratoData" id="contratoData" value="<?php echo date("d/m/Y", strtotime($linha->contratoData)); ?>" class="form-control floating datetimepicker" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="focus-label">Quantidade de sessões contratadas:</label>
                                <input type="text" id="qtdSessoes" name="qtdSessoes" value="<?php echo $linha->qtdSessoes; ?>" class="form-control floating">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group profile-img-wrap fichaAssinatura covid19">
                                <label class="focus-label">Assinatura contratante:</label>
                                <img id="assinaturacliente" class="inline-block" src="<?php echo $linha->cliAssinatura; ?>" alt="user">
                                <!--
                        <div class="fileupload btn"> <span class="btn-text">Assinatura Cliente</span>
                            <input class="upload" type="file">
                        </div> -->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group profile-img-wrap fichaAssinatura covid19">
                                <label class="focus-label">Assinatura contratado:</label>
                                <img id="assinaturafunc" class="inline-block" src="<?php echo $linha->funcAssinatura; ?>" alt="user">
                                <!--
                        <div class="fileupload btn"> <span class="btn-text">Assinatura Profissional</span>
                            <input class="upload" type="file">
                        </div> -->
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        <?php } ?>

    </div>
    <div class="sidebar-overlay" data-reff=""></div>