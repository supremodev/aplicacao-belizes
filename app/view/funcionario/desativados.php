<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">Funcionário Desativados</h4>
                    </div>      
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0">
								<thead>
									<tr>
										<th>Nome</th>
										<th>Cargo / Nível</th>
										<th>Status</th>
										<th>Telefone</th>
										<th>E-mail</th>
										<th class="text-right">Editar</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($funcionarios as $funcionario) { ?>
									<tr id="linha<?php echo $funcionario->idFunc; ?>">
										<td>
											<a href="<?php echo URL; ?>funcionario/perfil/<?php echo $funcionario->idFunc; ?>">
												<!--<img width="28" height="28" src="<?php							   
											if ($funcionario->funcFoto == "") {
												if($funcionario->funcWebcam == ""){
													echo URL."assets/img/func/user.jpg";
												}else{
													echo $funcionario->funcWebcam;
												}
											}else{
												echo URL."assets/img/func/".$funcionario->funcFoto;
										   	} 
											?>" title="Perfil <?php if (isset($funcionario->funcNome)) echo htmlspecialchars($funcionario->funcNome, ENT_QUOTES, 'UTF-8'); ?>" class="rounded-circle m-r-5"> -->
												<?php if (isset($funcionario->funcNome)) echo htmlspecialchars($funcionario->funcNome, ENT_QUOTES, 'UTF-8'); ?>
											</a>
										</td>
										<td><?php if (isset($funcionario->funcNivel)) echo htmlspecialchars($funcionario->funcNivel, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($funcionario->funcStatus)) echo htmlspecialchars($funcionario->funcStatus, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($funcionario->funcCpf)) echo htmlspecialchars($funcionario->funcCpf, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($funcionario->funcEmail)) echo htmlspecialchars($funcionario->funcEmail, ENT_QUOTES, 'UTF-8'); ?></td>
										<td class="text-right"><a class="dropdown-item desativar-ajax" destino="funcionario/ativarDesativar/1" idobjeto="<?php echo $funcionario->idFunc; ?>" href="#"><i class="fa fa-check m-r-5"></i>Ativar</a></td>
										
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>   
			
			
<div class="sidebar-overlay" data-reff=""></div>