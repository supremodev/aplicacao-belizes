<div class="page-wrapper">
            <div class="content">
				<div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
                    <span class="texto-ajax"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
					<span class="texto-ajax"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title"><?php echo $amount_of_funcionario; ?> - Funcionário Ativos</h4>
                    </div>
                    <div class="col-sm-8 col-9 text-right m-b-20">
                        <a href="<?php echo URL; ?>funcionario/novo" class="btn btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i>Funcionário</a>
						<a href="<?php echo URL; ?>funcionario/desativado" class="btn btn btn-primary btn-rounded float-right">Funcionário Desativados</a>
					</div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-border table-striped custom-table datatable m-b-0">
								<thead>
									<tr>
										<th>Nome</th>
										<th>Cargo / Nível</th>
										<th>Telefone</th>
										<th>E-mail</th>
										<th class="text-right">Editar</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($funcionarios as $funcionario) { ?>
									<tr id="linha<?php echo $funcionario->idFunc; ?>">
										<td>
											<a href="<?php echo URL; ?>funcionario/perfil/<?php echo $funcionario->idFunc; ?>">
												<!--<img width="28" height="28" src="<?php							   
											if ($funcionario->funcFoto == "") {
												if($funcionario->funcWebcam == ""){
													echo URL."assets/img/func/user.jpg";
												}else{
													echo $funcionario->funcWebcam;
												}
											}else{
												echo URL."assets/img/func/".$funcionario->funcFoto;
										   	} 
											?>" title="Perfil <?php if (isset($funcionario->funcNome)) echo htmlspecialchars($funcionario->funcNome, ENT_QUOTES, 'UTF-8'); ?>" class="rounded-circle m-r-5"> -->
												<?php if (isset($funcionario->funcNome)) echo htmlspecialchars($funcionario->funcNome, ENT_QUOTES, 'UTF-8'); ?>
											</a>
										</td>
										<td><?php if (isset($funcionario->funcNivel)) echo htmlspecialchars($funcionario->funcNivel, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($funcionario->funcCel)) echo htmlspecialchars($funcionario->funcCel, ENT_QUOTES, 'UTF-8'); ?></td>
										<td><?php if (isset($funcionario->funcEmail)) echo htmlspecialchars($funcionario->funcEmail, ENT_QUOTES, 'UTF-8'); ?></td>
										<td class="text-right">
											<div class="dropdown dropdown-action show">
												<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="<?php echo URL; ?>funcionario/editar/<?php echo $funcionario->idFunc; ?>"><i class="fa fa-pencil m-r-5"></i>Editar</a>
													<a class="dropdown-item desativar-ajax" destino="funcionario/ativarDesativar/0" idobjeto="<?php echo $funcionario->idFunc; ?>" href="#"><i class="fa fa-close m-r-5"></i>Desativar</a>
												</div>
											</div>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>   
			
			
<div class="sidebar-overlay" data-reff=""></div>