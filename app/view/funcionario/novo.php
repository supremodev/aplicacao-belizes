<div class="page-wrapper">
            <div class="content">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
                    Erro ao cadsatrar Funcionário.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
					Funcionário cadastrado com sucesso.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div> 
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Cadastro Funcionário</h4>
                    </div>
                </div>
                <form id="formularios">
                    <div class="card-box">
                        <h3 class="card-title">Informação básica</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-img-wrap">
									<div class="profile" style="background-image: url('<?php echo URL; ?>images/usuario.png');">
										<label class="edit">
											<span><i class="mdi mdi-upload"></i></span>
											<input type="file" size="32" name="funcFoto" id="inputImagem">
											<input type="text" name="funcWebcam" id="inputWebcam" hidden>
										</label>
									</div>
									<button id="editarImagem" type="button" class="btn btn-primary btn-sm">
										<i class="fa fa-file-image-o"></i>
									</button>
									<button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
										<i class="fa fa-camera"></i>
									</button>
								</div>
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Nome:*</label>
                                                <input type="text" name="funcNome" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">CPF:*</label>
                                                <input type="text" name="funcCpf" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Email:*</label>
                                                <input type="text" name="funcEmail" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Senha:*</label>
                                                <input type="password" name="funcSenha" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Data de Nascimento:</label>
                                                <div class="cal-icon">
                                                    <input class="form-control floating" type="text" name="funcNasci">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Sexo</label>
                                                <select class="select form-control floating" name="funcSexo">
                                                    <option></option>
                                                    <option value="Masculino">Masculino</option>
                                                    <option value="Feminino">Feminino</option>
                                                </select>
                                            </div>
                                        </div>
										<div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">RG:</label>
                                                <input type="text" name="funcRg" class="form-control floating">
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-md-4 col-lg-4">
                                            <div class="form-group form-focus select-focus">
                                                <label class="focus-label">Cargo (Nível):*</label>
                                                <select class="select form-control floating" required name="funcNivel">
                                                    <option></option>
                                                    <option value="Atendimento">Atendimento</option>
                                                    <option value="Profissional">Profissional</option>
                                                    <option value="Gerente">Gerente</option>
                                                    <option value="Diretor">Diretor/a</option>
                                                    <option value="Admin">Admin</option>
                                                </select>
                                            </div>
                                        </div>
										<div class="col-sm-5 col-md-3 col-lg-3">
                                           <div class="form-group form-focus">
                                                <label class="focus-label">Telefone:</label>
                                                <input type="text" name="funcFone" class="form-control floating">
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-md-3 col-lg-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Celular:*</label>
                                                <input type="text" name="funcCel" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-md-3 col-lg-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Cédula Profissional</label>
                                                <input type="text" name="funcCedulaProfissional" class="form-control floating">
                                            </div>
                                        </div>
										<div class="col-sm-5 col-md-3 col-lg-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Comissão Profissional</label>
                                                <input type="text" name="funcComissao" class="form-control floating">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
                        </div>
                    </div>
                    <div class="card-box">
                        <h3 class="card-title">Informações de Contato</h3>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Endereço:</label>
                                    <input type="text" name="funcEnd" class="form-control floating">
                                </div>
                            </div>
							<div class="col-md-2">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Número:</label>
                                    <input type="text" name="funcNumero" class="form-control floating">
                                </div>
                            </div>
							<div class="col-md-2">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Complemento:</label>
                                    <input type="text" name="funcEndComp" class="form-control floating">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Cidade:</label>
                                    <input type="text" name="funcCidade" class="form-control floating">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-focus">
                                    <label class="focus-label">CEP:</label>
                                    <input type="text" name="funcCEP" class="form-control floating">
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="card-box">
                        <h3 class="card-title">Informações Institucionais</h3>
                        <div class="row">
                            <div class="col-md-6">
								<div class="form-group">
									<label class="focus-label">Biografia</label>
									<textarea class="form-control floating" rows="3" cols="30" name="funcBiografia"></textarea>
								</div>								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<div class="profile-img-wrap assinatura">
										<img id="Assinatura-img" class="inline-block" src="<?php echo URL; ?>assets/img/assinatura.png" alt="user">
										<input id="Assinatura-input" type="text" name="funcAssinatura" hidden>                 
									</div>
								</div>
							</div>
                        </div>                       
                    </div>
					<div class="card-box">
						<h3 class="card-title">Assinatura Digital</h3>
						<div class="row">
							<div class="col">
								<div class="tabs">
									<div class="tab">
										<input type="checkbox" id="chck1">
										<label class="tab-label" for="chck1">Abrir assinatura</label>
										<div class="tab-content">
											<div class="assinatura-corpo" onselectstart="return false">
												<div id="signature-pad" class="signature-pad">
													<div class="signature-pad--body">
														<canvas></canvas>
													</div>
													<div class="signature-pad--footer">
														<div class="description">Assine Acima</div>
														<div class="signature-pad--actions">
															<div>
																<button type="button" class="button clear" data-action="clear">Limpar</button>
																<button type="button" class="button" data-action="undo">Desfazer</button>
															</div>
															<div>
																<button type="button" class="button save" data-action="save-png">SALVAR</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    
                    <div class="text-center m-t-20">
                        <button destino="funcionario/inserir" class="novo-ajax btn btn-primary submit-btn" type="button">Cadastrar Funcionário</button>
                    </div>
                </form>
                            
				                         
            </div>
			<!-- Modal Web Cam -->
			<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Foto Web Cam</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="camera">
								<video id="video" class="videoWebcam">Vídeo indisponível.</video>
							</div>
							<canvas id="canvas" style="display:none"></canvas>
							<div class="output"></div>
						</div>
						<div class="modal-footer">
							<button type="button" id="startbutton" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-camera"></i></button>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal Web Cam -->


<div class="sidebar-overlay" data-reff=""></div>