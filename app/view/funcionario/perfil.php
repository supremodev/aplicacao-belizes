<div class="page-wrapper">
            <div class="content">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
					<span class="texto-ajax">Erro ao deletar especialidade.</span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
					<span class="texto-ajax">Especialidade deletada com sucesso.</span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
                <div class="row">
                    <div class="col-sm-7 col-4">
                        <h4 class="page-title">Perfil: <?php echo htmlspecialchars($profissionaisLista->funcNome, ENT_QUOTES, 'UTF-8'); ?></h4>
                    </div>

                    <div class="col-sm-5 col-8 text-right m-b-30">
                        <a href="<?php echo URL; ?>funcionario/editar/<?php echo $profissionaisLista->idFunc; ?>" class="btn btn-primary btn-rounded">
							<i class="fa fa-plus"></i> Editar Perfil 
						</a>
                    </div>
                </div>
                <div class="card-box profile-header"> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-view">
                                <div class="profile-img-wrap">
                                    <div class="profile-img">
                                        <a href="#">
                                            <img class="avatar" src="<?php 
                                                    if (!empty($profissionaisLista->funcFoto)){
														echo URL."/assets/img/func/".$profissionaisLista->funcFoto;
                                                    }elseif(!empty($profissionaisLista->funcWebcam)){
                                                        echo $profissionaisLista->funcWebcam;
                                                    }else{
                                                        echo URL."/assets/img/func/user.jpg";
                                                    }
                                                    ?>" alt="<?php echo htmlspecialchars($profissionaisLista->funcNome, ENT_QUOTES, 'UTF-8'); ?>">
                                        </a>
                                    </div>
                                </div>
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="profile-info-left">
                                                <h3 class="user-name m-t-0 m-b-0"><?php echo htmlspecialchars($profissionaisLista->funcNome, ENT_QUOTES, 'UTF-8'); ?></h3>
                                                <small class="text-muted"><?php echo htmlspecialchars($profissionaisLista->funcNivel, ENT_QUOTES, 'UTF-8'); ?></small>
                                                <div class="staff-id"><?php echo htmlspecialchars($profissionaisLista->funcBiografia, ENT_QUOTES, 'UTF-8'); ?></div>
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <ul class="personal-info">
                                                <li>
                                                    <span class="title">Celular:</span>
                                                    <span class="text"><?php echo htmlspecialchars($profissionaisLista->funcCel, ENT_QUOTES, 'UTF-8'); ?></span>
                                                </li>
                                                <li>
                                                    <span class="title">Email:</span>
                                                    <span class="text"><?php echo htmlspecialchars($profissionaisLista->funcEmail, ENT_QUOTES, 'UTF-8'); ?></span>
                                                </li>
                                                <li>
                                                    <span class="title">Aniversário:</span>
                                                    <span class="text"><?php echo htmlspecialchars($profissionaisLista->funcNasci, ENT_QUOTES, 'UTF-8'); ?></span>
                                                </li>
                                                <li>
                                                    <span class="title">Endereço:</span>
                                                    <span class="text"><?php echo htmlspecialchars($profissionaisLista->funcEnd, ENT_QUOTES, 'UTF-8'); ?></span>
                                                </li>
                                                <li>
                                                    <span class="title">Sexo:</span>
                                                    <span class="text"><?php echo htmlspecialchars($profissionaisLista->funcSexo, ENT_QUOTES, 'UTF-8'); ?></span>
                                                </li>
												<li>
                                                    <span class="title">Comissão:</span>
                                                    <span class="text"><?php echo htmlspecialchars($profissionaisLista->funcComissao, ENT_QUOTES, 'UTF-8'); ?></span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                    </div>
                </div>
				
				<div class="profile-tabs">
					<ul class="nav nav-tabs nav-tabs-bottom">
						<?php if($profissionaisLista->funcNivel == 'Profissional'){ ?>
						<li class="nav-item"><a class="nav-link <?php if(!isset($_GET['idusuario'])){ echo "active"; } ?>" href="#profissional" data-toggle="tab">Especialidade</a></li>						
						<?php } ?>
						
						<?php if($profissionaisLista->funcNivel == 'Profissional'){ ?>
						<li class="nav-item"><a class="nav-link" href="#comissao" data-toggle="tab">Comissão</a></li>						
						<?php } ?>
						
						<li class="nav-item"><a class="nav-link <?php if(isset($_GET['idusuario'])){ echo "active"; } ?>" href="#bottom-tab2" data-toggle="tab">Compromissos</a></li>
						
					</ul>

					<div class="tab-content">
						<?php if($profissionaisLista->funcNivel == 'Profissional'){ ?>
						<div class="tab-pane <?php if(!isset($_GET['idusuario'])){ echo "show active"; } ?>" id="profissional">
							<div class="row">
								<div class="col-md-12">
									<div class="card-box">
										<h3 class="card-title">Especialidade</h3>
										<div class="experience-box">

                                        <button class="btn" data-toggle="modal" data-target="#exampleModalCenter">
                                            <i class="fa fa-plus"></i> Adicionar Especialidade
                                        </button>
											<ul class="experience-list">								
												<?php foreach ($especialidadeLista as $especialidade) { ?>
												<li id="linha<?php echo $especialidade->idEspecialidade;?>">
													<div class="experience-user">
														<div class="before-circle"></div>
													</div>
													<div class="experience-content">
														<div class="timeline-content">
															<h3 class="name">
																<?php echo htmlspecialchars($especialidade->espeNome, ENT_QUOTES, 'UTF-8'); ?>
                                                                <button type="button" destino="funcionario/deleteEspecialidade" idObjeto="<?php echo $especialidade->idEspecialidade;?>" class="deletar-ajax btn btn-danger" style="padding: 0px 5px;"><i class="fa fa-close"></i></button>
                                                            </h3>
															<div>
																<?php echo htmlspecialchars($especialidade->espeDescricao, ENT_QUOTES, 'UTF-8'); ?>
															</div>
														</div>
													</div>
												</li>
												<?php } ?>												
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<?php } ?>
						
						<?php if($profissionaisLista->funcNivel == 'Profissional'){ ?>
						<div class="tab-pane" id="comissao">
							<div class="row">
								<div class="col-md-12">
									<div class="card-box">
                                        <h3 class="card-title">Comissão</h3>
                                        <div class="table-responsive">
                                            <table class="table table-border table-striped custom-table datatable m-b-0">
                                                <thead>
                                                    <tr>
                                                        <th>Serviço</th>
                                                        <th>Data Atendimento</th>
                                                        <th>Valor a Receber</th>
                                                        <th>Status do Serviço</th>
                                                        <th>Pagamento</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($comissaoProfissional  as $comissao) { ?>
                                                    <tr>
                                                        <td><?php echo $comissao->servicoNome; ?></td>
                                                        <td><?php if (isset($comissao->agendaData)) echo htmlspecialchars(date("d/m/Y", strtotime($comissao->agendaData)), ENT_QUOTES, 'UTF-8'); ?></td>
                                                        <td>R$ <?php echo number_format($comissao->servicoValor * $comissao->funcComissao,2,",","."); ?></td>
														<td>
                                                            <?php
																if ($comissao->agendaStatus == 1) {
																	echo "<span class='custom-badge status-green'>Confirmado</span>";
																} else if ($comissao->agendaStatus == 2) {
																	echo "<span class='custom-badge status-orange'>Em Atendimento</span>";
																} else if ($comissao->agendaStatus == 3) {
																	echo "<span class='custom-badge status-grey'>Finalizado</span>";
																} else {
																	echo "<span class='custom-badge status-red'>Desmarcado</span>";
																}
															?>
                                                        </td>
														<th>
															<?php 
																if (isset($comissao->agendaPG)){
																	if($comissao->agendaPG){
																		echo "<span class='custom-badge status-green'> Pago </span>";
																	}else{
																		echo "<span class='custom-badge status-red'> Pendente </span>";		
																	}														
																}													
															?>	
														</th>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
									</div>
								</div>
								
								
							</div>
						</div>
						
						<?php } ?>
						
						
						<div class="tab-pane <?php if(isset($_GET['idusuario'])){ echo "show active"; } ?>" id="bottom-tab2">
							<div class="row">
								<div class="col-md-12">
									<div class="card-box">
                                        <h3 class="card-title">Compromissos</h3>
                                        <div class="dataTables_length page-title" id="DataTables_Table_0_length">
                                            <label>Realizar filtro por:
                                                <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" id="filtroCompromisso" class="form-control form-control-sm">
                                                    <option value=""></option>
                                                    <!--<option value="tudo">Tudo</option>-->
                                                    <option value="dia">Dia</option>
                                                    <option value="semana">Semana</option>
                                                    <option value="mes">Mês</option>
                                                </select>
                                                <input type="text" id="idusuario" value="<?php echo $_SESSION['idUsuario'];?>" hidden>
                                            </label>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-border table-striped custom-table  datatable-compromisso-pro  m-b-0">
                                                <thead>
                                                    <tr>
                                                        <th>Cliente</th>
                                                        <th>Data / Hora</th>

                                                        <th>Status</th>
                                                        <th class="text-right">Editar</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($agendaLista  as $agenda) { ?>
                                                    <tr>
                                                        <td><?php echo $agenda->cliNome; ?></td>
                                                        <td>
                                                            <?php if (isset($agenda->agendaData)) echo htmlspecialchars(date("d/m/Y", strtotime($agenda->agendaData)), ENT_QUOTES, 'UTF-8'); ?> - 
                                                            <?php if (isset($agenda->agendaHora)) echo htmlspecialchars($agenda->agendaHora, ENT_QUOTES, 'UTF-8'); ?>
                                                        </td>
                                                        
                                                        <td>
                                                            <?php if($agenda->agendaStatus == 1){ ?>
                                                                <span class="custom-badge status-green">Confirmado</span>
                                                            <?php }else if($agenda->agendaStatus == 2){ ?>
                                                                <span class="custom-badge status-orange">Em Andamento</span>
                                                            <?php }else if($agenda->agendaStatus == 3){ ?>
                                                                <span class="custom-badge status-grey">Finalizado</span>
                                                            <?php }else { ?>
                                                                <span class="custom-badge status-red">Cancelado</span>
                                                            <?php }; ?>
                                                        </td>

                                                        <td class="text-right">											
                                                            <div>
                                                                <a class="dropdown-item" href="<?php echo URL; ?>agenda/visualizar/<?php echo $agenda->idAgenda; ?>">
                                                                    <i class="fa fa-eye m-r-5"></i>Visualizar
                                                                </a>
                                                                
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>

<div class="sidebar-overlay" data-reff=""></div>

<!-- Modal -->
<div class="modal fade show" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Adicionar Especialidade</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
                        Erro ao cadastrar especialidade.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
                        Especialidade cadastrado com sucesso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div> 
                        <form id="cursoform">
                            <div class="card-box">
                                <h3 class="card-title">Nome do curso</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-focus focused">
                                            <input type="text" class="form-control floating" name="idfunc" value="<?php echo $id;?>" hidden>
                                            <input type="text" class="form-control floating" name="cursonome">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h3 class="card-title">Descrição</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-focus focused">
                                            <textarea class="form-control floating" rows="3" cols="30" name="cursodescricao" style="min-height: 80px;"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" class="novo-curso-ajax btn btn-primary">Salvar</button>
                </div>
            </div>
        </div>
    </div>