<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">Profissionais</h4>
                    </div>
                    
                </div>
				<div class="row doctor-grid">
					<?php foreach ($profissionais as $profissional) { ?>
                    <div class="col-md-6 col-sm-6 col-xs-6 col-lg-4">
                        <div class="profile-widget">
                            <div class="doctor-img">
                                <a class="avatar" href="<?php echo URL; ?>funcionario/perfil/<?php echo $profissional->idFunc; ?>">
									<img alt="<?php if (isset($profissional->funcNome)) echo htmlspecialchars($profissional->funcNome, ENT_QUOTES, 'UTF-8'); ?>" src="<?php echo URL; ?>assets/img/func/<?php if (isset($profissional->funcFoto)) echo htmlspecialchars($profissional->funcFoto, ENT_QUOTES, 'UTF-8'); ?>">
								</a>
                            </div>
                            
                            <h4 class="doctor-name text-ellipsis">
								<a href="profile.html">
								<?php if (isset($profissional->funcNome)) echo htmlspecialchars($profissional->funcNome, ENT_QUOTES, 'UTF-8'); ?>
								</a>
							</h4>
                            <div class="doc-prof"><?php if (isset($profissional->funcNivel)) echo htmlspecialchars($profissional->funcNivel, ENT_QUOTES, 'UTF-8'); ?></div>
                            <div class="user-country">
                                <i class="fa fa-mobile"></i>
								<?php if (isset($profissional->funcCel)) echo htmlspecialchars($profissional->funcCel, ENT_QUOTES, 'UTF-8'); ?>
                            </div>
							<div class="user-country">
                                <i class="fa fa-envelope-open-o"></i>
								<?php if (isset($profissional->funcEmail)) echo htmlspecialchars($profissional->funcEmail, ENT_QUOTES, 'UTF-8'); ?>
                            </div>
                        </div>
                    </div>
					<?php } ?>
                </div>
            </div>


<div class="sidebar-overlay" data-reff=""></div>