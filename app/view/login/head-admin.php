<!DOCTYPE html>
<html lang="pt-br">

<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Login - Belizes Time</title>
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="belizer-time/images/logoSupremoDigital.png">
		
    <style>

/*! CSS Used from: http://localhost/weepy/dashboard-supremo/vendors/mdi/css/materialdesignicons.min.css */
.mdi:before{display:inline-block;font:normal normal normal 24px/1 "Material Design Icons";font-size:inherit;text-rendering:auto;line-height:inherit;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
.mdi-account-outline:before{content:"\F013";}
.mdi-eye-off:before{content:"\F209";}
.mdi-lock-outline:before{content:"\F341";}
/*! CSS Used from: http://localhost/weepy/dashboard-supremo/css/style.css */
*,*::before,*::after{box-sizing:border-box;}
body{margin:0;font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";font-size:1rem;font-weight:400;line-height:1.5;color:#000;text-align:left;background-color:#fff;}
h4,h6{margin-top:0;margin-bottom:0.5rem;}
p{margin-top:0;margin-bottom:1rem;}
a{color:#007bff;text-decoration:none;background-color:transparent;}
a:hover{color:#0056b3;text-decoration:underline;}
img{vertical-align:middle;border-style:none;}
label{display:inline-block;margin-bottom:0.5rem;}
button{border-radius:0;}
button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color;}
input,button,textarea{margin:0;font-family:inherit;font-size:inherit;line-height:inherit;}
button,input{overflow:visible;}
button{text-transform:none;}
button,[type="submit"]{-webkit-appearance:button;}
button::-moz-focus-inner,[type="submit"]::-moz-focus-inner{padding:0;border-style:none;}
input[type="checkbox"]{box-sizing:border-box;padding:0;}
textarea{overflow:auto;resize:vertical;}
h4,h6{margin-bottom:0.5rem;font-weight:500;line-height:1.2;}
h4{font-size:1.5rem;}
h6{font-size:1rem;}
.container-fluid{width:100%;padding-right:10px;padding-left:10px;margin-right:auto;margin-left:auto;}
.row{display:flex;flex-wrap:wrap;margin-right:-10px;margin-left:-10px;}
.col-lg-6{position:relative;width:100%;padding-right:10px;padding-left:10px;}
@media (min-width: 992px){
.col-lg-6{flex:0 0 50%;max-width:50%;}
}
.form-control{display:block;width:100%;height:2.875rem;padding:0.875rem 1.375rem;font-size:0.875rem;font-weight:400;line-height:1;color:#495057;background-color:#ffffff;background-clip:padding-box;border:1px solid #ced4da;border-radius:2px;transition:border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;}
@media (prefers-reduced-motion: reduce){
.form-control{transition:none;}
}
.form-control::-ms-expand{background-color:transparent;border:0;}
.form-control:focus{color:#495057;background-color:#fff;border-color:#80bdff;outline:0;box-shadow:0 0 0 0.2rem rgba(0, 123, 255, 0.25);}
.form-control::placeholder{color:#c9c8c8;opacity:1;}
.form-control:disabled{background-color:#e9ecef;opacity:1;}
.form-control-lg{height:3.5rem;padding:0.94rem 1.94rem;font-size:1.25rem;line-height:1.5;border-radius:0.3rem;}
.form-group{margin-bottom:1rem;}
.form-check{position:relative;display:block;padding-left:1.25rem;}
.form-check-input{position:absolute;margin-top:0.3rem;margin-left:-1.25rem;}
.form-check-label{margin-bottom:0;}
.btn{display:inline-block;font-weight:400;color:#000;text-align:center;vertical-align:middle;user-select:none;background-color:transparent;border:1px solid transparent;padding:0.75rem 1.5rem;font-size:0.875rem;line-height:1;border-radius:0.1875rem;transition:color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;}
@media (prefers-reduced-motion: reduce){
.btn{transition:none;}
}
.btn:hover{color:#000;text-decoration:none;}
.btn:focus{outline:0;box-shadow:0 0 0 0.2rem rgba(0, 123, 255, 0.25);}
.btn:disabled{opacity:0.65;}
.btn-primary{color:#fff;background-color:#4d83ff;border-color:#4d83ff;}
.btn-primary:hover{color:#fff;background-color:#2768ff;border-color:#1a5fff;}
.btn-primary:focus{box-shadow:0 0 0 0.2rem rgba(104, 150, 255, 0.5);}
.btn-primary:disabled{color:#fff;background-color:#4d83ff;border-color:#4d83ff;}
.btn-lg{padding:1rem 3rem;font-size:0.875rem;line-height:1.5;border-radius:0.1875rem;}
.btn-block{display:block;width:100%;}
.input-group{position:relative;display:flex;flex-wrap:wrap;align-items:stretch;width:100%;}
.input-group > .form-control{position:relative;flex:1 1 auto;width:1%;margin-bottom:0;}
.input-group > .form-control:focus{z-index:3;}
.input-group > .form-control:not(:last-child){border-top-right-radius:0;border-bottom-right-radius:0;}
.input-group > .form-control:not(:first-child){border-top-left-radius:0;border-bottom-left-radius:0;}
.input-group-prepend,.input-group-append{display:flex;}
.input-group-prepend{margin-right:-1px;}
.input-group-append{margin-left:-1px;}
.input-group-text{display:flex;align-items:center;padding:0.875rem 1.375rem;margin-bottom:0;font-size:0.875rem;font-weight:400;line-height:1;color:#495057;text-align:center;white-space:nowrap;background-color:#e9ecef;border:1px solid #ced4da;border-radius:2px;}
.input-group > .input-group-prepend > .input-group-text{border-top-right-radius:0;border-bottom-right-radius:0;}
.input-group > .input-group-append > .input-group-text{border-top-left-radius:0;border-bottom-left-radius:0;}
.bg-transparent{background-color:transparent!important;}
.border-right-0{border-right:0!important;}
.border-left-0{border-left:0!important;}
.d-flex{display:flex!important;}
.flex-row{flex-direction:row!important;}
.justify-content-center{justify-content:center!important;}
.justify-content-between{justify-content:space-between!important;}
.align-items-center{align-items:center!important;}
.align-items-stretch{align-items:stretch!important;}
.align-self-end{align-self:flex-end!important;}
.my-2{margin-top:0.5rem!important;}
.my-2{margin-bottom:0.5rem!important;}
.my-3{margin-top:1rem!important;}
.my-3{margin-bottom:1rem!important;}
.p-3{padding:1rem!important;}
.pt-3{padding-top:1rem!important;}
.text-left{text-align:left!important;}
.text-center{text-align:center!important;}
.font-weight-light{font-weight:300!important;}
.text-white{color:#ffffff!important;}
.text-primary{color:#4d83ff!important;}
.text-muted{color:#686868!important;}
@media print{
*,*::before,*::after{text-shadow:none!important;box-shadow:none!important;}
a:not(.btn){text-decoration:underline;}
img{page-break-inside:avoid;}
p{orphans:3;widows:3;}
body{min-width:992px!important;}
}
body{padding:0;margin:0;overflow-x:hidden;}
.form-control,.form-control:focus{-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;outline:0;}
a,div,h4,p,span{text-shadow:none;}
a:active,a:focus,a:visited,button::-moz-focus-inner{outline:0;}
input,.form-control:focus,input:focus,textarea:focus,button:focus{outline:none;outline-width:0;outline-color:transparent;box-shadow:none;outline-style:none;}
textarea{resize:none;overflow-x:hidden;}
.btn,.btn:active,.btn:focus,.btn:hover,.btn:visited,a,a:active,a:checked,a:focus,a:hover,a:visited,body,button,button:active,button:hover,button:visited,div,input,input:active,input:focus,input:hover,input:visited,textarea,textarea:active,textarea:focus,textarea:hover,textarea:visited{-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;}
.btn:active:focus,.btn:focus,button,button:active,button:checked,button:focus,button:hover,button:visited{outline:0;outline-offset:0;}
a:focus,input:focus{border-color:transparent;outline:none;}
body{font-size:1rem;font-family:"Roboto", sans-serif;font-weight:initial;line-height:normal;-webkit-font-smoothing:antialiased;}
h4,h6{font-weight:500;line-height:1;}
p{font-size:0.875rem;margin-bottom:.5rem;line-height:1.3rem;}
h4{font-size:1.25rem;}
h6{font-size:.937rem;}
.font-weight-medium{font-weight:500;}
body{overflow-x:hidden;padding-right:0!important;}
*:-moz-full-screen,*:-webkit-full-screen,*:fullscreen *:-ms-fullscreen{overflow:auto;}
.container-scroller{overflow:hidden;}
.flex-grow{flex-grow:1;}
.border-right-0{border-right:0;}
.border-left-0{border-left:0;}
.btn{font-size:0.875rem;line-height:1;font-weight:400;}
.btn.btn-lg{font-size:0.875rem;}
.btn-primary,.btn-primary:hover{box-shadow:0 2px 2px 0 rgba(77, 131, 255, 0.14), 0 3px 1px -2px rgba(77, 131, 255, 0.2), 0 1px 5px 0 rgba(77, 131, 255, 0.12);}
.form-check{position:relative;display:block;margin-top:10px;margin-bottom:10px;padding-left:0;}
.form-check .form-check-label{min-height:18px;display:block;margin-left:1.75rem;font-size:0.875rem;line-height:1.5;}
.form-check .form-check-label input{position:absolute;top:0;left:0;margin-left:0;margin-top:0;z-index:1;cursor:pointer;opacity:0;filter:alpha(opacity=0);}
.form-group{margin-bottom:1.5rem;}
.input-group-append,.input-group-prepend{color:#c9c8c8;width:auto;border:none;}
.input-group-append .input-group-text,.input-group-prepend .input-group-text{border-color:#f3f3f3;padding:0.875rem 0.75rem;color:#c9c8c8;}
.form-control{border:1px solid #f3f3f3;font-weight:400;font-size:0.875rem;}
.form-group label{font-size:0.875rem;line-height:1.4rem;vertical-align:top;margin-bottom:.5rem;}
.icon-md{font-size:1.875rem;}
.auth .login-half-bg{background:url("images/slider/slide_03.png");background-size:cover;}
.auth .auth-form-transparent{background:transparent;}
.auth .auth-form-transparent .form-control,.auth .auth-form-transparent .input-group-text{border-color:#686868;}
.auth .auth-form-transparent .form-control:focus,.auth .auth-form-transparent .form-control:active,.auth .auth-form-transparent .input-group-text:focus,.auth .auth-form-transparent .input-group-text:active{border-color:#686868;}
.auth.auth-img-bg{padding:0;}
@media (min-width: 768px){
.auth.auth-img-bg .auth-form-transparent{width:55%;margin:auto;}
}
.auth .brand-logo{margin-bottom:2rem;}
.auth .brand-logo img{width:250px;}
.auth form .form-group{margin-bottom:1.5rem;}
.auth form .form-group label{font-size:.8125rem;}
.auth form .form-group .form-control{background:transparent;border-radius:0;font-size:.9375rem;}
.auth form .auth-form-btn{height:50px;line-height:1.5;}
.auth form .auth-link{font-size:0.875rem;}
.auth form .auth-link:hover{color:initial;}
.page-body-wrapper{min-height:calc(100vh - 60px);display:-webkit-flex;display:flex;-webkit-flex-direction:row;flex-direction:row;padding-left:0;padding-right:0;padding-top:60px;}
.page-body-wrapper.full-page-wrapper{width:100%;min-height:100vh;padding-top:0;}
.content-wrapper{background:#f3f3f3;padding:2.1rem 1.04rem;width:100%;-webkit-flex-grow:1;flex-grow:1;}
@media (max-width: 767px){
.content-wrapper{padding:1.5rem 1.5rem;}
}
@media (max-width: 480px){
.content-wrapper{padding:5rem 15px 1.5rem;}
}
.olhar-senha{padding:0 5px 0 0!important;border-left:0px solid white!important;border-radius:2px;position:relative;background:white;}
/*! CSS Used fontfaces */
@font-face{
    font-family:"Material Design Icons";
    src:url("<?php echo URL; ?>fonts/materialdesignicons-webfont.eot?v=3.6.95");
    src:url("<?php echo URL; ?>fonts/materialdesignicons-webfont.eot#iefix&v=3.6.95") format("embedded-opentype"),url("<?php echo URL; ?>fonts/materialdesignicons-webfont.woff2?v=3.6.95") format("woff2"),url("<?php echo URL; ?>fonts/materialdesignicons-webfont.woff?v=3.6.95") format("woff"),url("<?php echo URL; ?>fonts/materialdesignicons-webfont.ttf?v=3.6.95") format("truetype"),url("<?php echo URL; ?>fonts/materialdesignicons-webfont.svg?v=3.6.95#materialdesigniconsregular") format("svg");font-weight:normal;font-style:normal;
}
@font-face{
    font-family:'Roboto';
    src:url("<?php echo URL; ?>fonts/Roboto/Roboto-Light.eot");
    src:url("<?php echo URL; ?>fonts/Roboto/Roboto-Light.eot#iefix") format("embedded-opentype"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Light.woff2") format("woff2"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Light.woff") format("woff"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Light.ttf") format("truetype");
    font-weight:300;
    font-style:normal;
}
@font-face{
    font-family:'Roboto';
    src:url("<?php echo URL; ?>fonts/Roboto/Roboto-Bold.eot");
    src:url("<?php echo URL; ?>fonts/Roboto/Roboto-Bold.eot#iefix") format("embedded-opentype"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Bold.woff2") format("woff2"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Bold.woff") format("woff"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Bold.ttf") format("truetype");
    font-weight:bold;
    font-style:normal;
}
@font-face{
    font-family:'Roboto';
    src:url("<?php echo URL; ?>fonts/Roboto/Roboto-Medium.eot");
    src:url("<?php echo URL; ?>fonts/Roboto/Roboto-Medium.eot#iefix") format("embedded-opentype"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Medium.woff2") format("woff2"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Medium.woff") format("woff"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Medium.ttf") format("truetype");
    font-weight:500;font-style:normal;
}
@font-face{
    font-family:'Roboto';
    src:url("<?php echo URL; ?>fonts/Roboto/Roboto-Regular.eot");
    src:url("<?php echo URL; ?>fonts/Roboto/Roboto-Regular.eot#iefix") format("embedded-opentype"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Regular.woff2") format("woff2"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Regular.woff") format("woff"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Regular.ttf") format("truetype");
    font-weight:normal;
    font-style:normal;
}
@font-face{
    font-family:'Roboto';
    src:url("<?php echo URL; ?>fonts/Roboto/Roboto-Black.eot");
    src:url("<?php echo URL; ?>fonts/Roboto/Roboto-Black.eot#iefix") format("embedded-opentype"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Black.woff2") format("woff2"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Black.woff") format("woff"), url("<?php echo URL; ?>fonts/Roboto/Roboto-Black.ttf") format("truetype");
    font-weight:900;
    font-style:normal;
}
    </style>
    </head>

<body>