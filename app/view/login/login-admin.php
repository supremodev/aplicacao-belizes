<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
        <div class="row flex-grow">
          <div class="col-lg-6 d-flex align-items-center justify-content-center">
            <div class="auth-form-transparent text-left p-3">
              <div class="brand-logo">
                <img src="<?php echo URL; ?>images/logo.png" alt="Belize's Time">
              </div>
              <div id="teste"></div>
              <h4>Seja bem vindo!</h4>
              <h6 class="font-weight-light">Feliz em ver você de novo!</h6>
              <script src='https://www.google.com/recaptcha/api.js'></script>
              <form class="pt-3" id="formLogin" method="POST" action="<?php echo URL; ?>login/acessoadmin">
                <div class="form-group">
                  <label for="exampleInputEmail">Nome ou E-mail</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-account-outline text-primary"></i>
                      </span>
                    </div>
                    <input type="text" name="nome_email" class="form-control form-control-lg border-left-0" id="exampleInputEmail" placeholder="Email ou nome" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword">Senha</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-lock-outline text-primary"></i>
                      </span>
                    </div>
                    <input type="password" name="senha" class="form-control input-senha form-control-lg border-left-0" id="exampleInputPassword" placeholder="Senha" required>
                    <div class="input-group-append">
                      <span class="input-group-text olhar-senha"><i class="mdi mdi-eye-off icon-md"></i></span>
                    </div>                        
                  </div>
                </div>
                <?php if(isset($msgErro)){ ?>
                  <div class="" style="
                      background: #FFC107;
                      padding: 15px;
                  ">
                  <?php echo $msgErro; ?>
                </div>
                <?php }; ?>
                <div class="my-3">
                  <div class="g-recaptcha" data-sitekey="6LeLq64ZAAAAAOgeDah4LymHsCA-XcU04SQjWjl7"></div>
                </div>
                <div class="my-3">
                  <button type="submit" name="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">LOGIN</button>
                </div>
                <div class="my-2 d-flex justify-content-between align-items-center">
                  <a href="<?php echo URL; ?>login/recuperarSenha" class="auth-link .text-branco">Esqueceu a senha?</a>
                </div>
                <!--<div class="mb-2 d-flex">
                  <button type="button" class="btn btn-facebook auth-form-btn flex-grow mr-1">
                    <i class="mdi mdi-facebook mr-2"></i>Facebook
                  </button>
                  <button type="button" class="btn btn-google auth-form-btn flex-grow ml-1">
                    <i class="mdi mdi-google mr-2"></i>Google
                  </button>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  Não tem uma conta? <a href="register-2.html" class="text-primary">Crie</a>
                </div>-->
              </form>
            </div>
          </div>
          <div class="col-lg-6 login-half-bg d-flex flex-row">
            <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy; 2020 - Supremo Digital.</p>
          </div>
        </div>