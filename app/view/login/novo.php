<div class="container-fluid" style="background-image: url(../images/fundo-login.jpg);">
    <div class="container">
        <div class="">
            <div class="alert erro-cpf alert-danger alert-dismissible fade show" role="alert" style="display:none">
                <span class="texto-ajax">CPF já tem cadastro.</span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
                Cadastrado com sucesso.
                <a href="<?php echo URL; ?>login/logincliente" class="btn btn-success waves-effect waves-light mr-1">Fazer Login </a>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">Cadastro de Cliente</h4>
                </div>
            </div>
            <form id="formularios">
                <div class="card-box">
                    <h3 class="card-title">Informação básica</h3>
                    <div class="row">
                        <div class="col-md-12">						
                            <div class="profile-img-wrap">
                                <div class="profile" style="background-image: url('<?php echo URL; ?>images/usuario.png');">
                                    <label class="edit">
                                        <span><i class="mdi mdi-upload"></i></span>
                                        <input type="file" size="32" name="cliFoto" id="inputImagem">
                                        <input type="text" name="cliWebcam" id="inputWebcam" hidden>
                                    </label>
                                </div>
                                <button id="editarImagem" type="button" class="btn btn-primary btn-sm">
                                    <i class="fa fa-file-image-o"></i>
                                </button>
                                <!--<button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                                    <i class="fa fa-camera"></i>
                                </button>-->
                            </div>
                            
                            <div class="profile-basic">
                                <div class="row">
                                    <div class="col-sm-5 col-md-4 col-lg-4">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Nome:*</label>
                                            <input type="text" name="cliNome" class="form-control floating" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-4 col-lg-4">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">CPF:*</label>
                                            <input id="cpff" type="text" name="cliCPF" class="ajax-consulta-cpf form-control floating" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-4 col-lg-4">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Email:*</label>
                                            <input type="text" name="cliEmail" class="form-control floating" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-4 col-lg-4">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Senha:*</label>
                                            <input type="password" name="cliSenha" class="form-control floating" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-4 col-lg-4">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Profissão:</label>
                                            <input type="text" name="cliProfissao" class="form-control floating">
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-4 col-lg-4">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Data de Nascimento:</label>
                                            <div class="cal-icon">
                                                <input class="form-control floating" type="text" name="cliNasci">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-4 col-lg-4">
                                        <div class="form-group form-focus select-focus">
                                            <label class="focus-label">Sexo:</label>
                                            <select class="select form-control floating" name="cliSexo">
                                                <option></option>
                                                <option value="Masculino">Masculino</option>
                                                <option value="Feminino">Feminino</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-4 col-lg-4">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">RG:</label>
                                            <input type="text" name="cliRG" class="form-control floating">
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-4 col-lg-4">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Estado Civil:</label>
                                            <input type="text" name="cliEstadoCivil" class="form-control floating">
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-4 col-lg-4">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Telefone:</label>
                                            <input type="text" name="cliFone" class="form-control floating">
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-md-4 col-lg-4">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Celular:*</label>
                                            <input id="cel" type="text" name="cliCel" class="form-control floating" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-box">
                    <h3 class="card-title">Informações de Contato</h3>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group form-focus">
                                <label class="focus-label">Endereço:</label>
                                <input type="text" name="cliEnd" class="form-control floating">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group form-focus">
                                <label class="focus-label">Número:</label>
                                <input type="text" name="cliEndNumero" class="form-control floating">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group form-focus">
                                <label class="focus-label">Complemento:</label>
                                <input type="text" name="cliEndComp" class="form-control floating">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-focus">
                                <label class="focus-label">Cidade:</label>
                                <input type="text" name="cliCidade" class="form-control floating">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-focus">
                                <label class="focus-label">Estado:</label>
                                <input type="text" name="cliEstado" class="form-control floating">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-focus">
                                <label class="focus-label">CEP:</label>
                                <input type="text" name="cliCEP" class="form-control floating">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-box">
                    <h3 class="card-title">Objetivo do Tratamento</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea class="form-control floating cliDepoimento" rows="5" cols="30" name="cliTratamento"></textarea>
                            </div>								
                        </div>                       
                    </div>
                </div>
                <div class="card-box">
                    <h3 class="card-title">Informações Institucionais</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="focus-label">Como conheceu a Clínica Belizes Time</label>
                                <textarea name="cliComoConheceu" class="form-control floating" rows="3" cols="30"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="card-box">
                    <h3 class="card-title">Assinatura Digital</h3>
                    <div class="row">
                        <div class="col">
                            <div class="tabs">
                                <div class="tab">
                                    <input type="checkbox" id="chck1">
                                    <label class="tab-label" for="chck1">Abrir assinatura</label>
                                    <div class="tab-content">
                                        <div class="assinatura-corpo" onselectstart="return false">
                                            <div id="signature-pad" class="signature-pad">
                                                <div class="signature-pad--body">
                                                    <canvas></canvas>
                                                </div>
                                                <div class="signature-pad--footer">
                                                    <div class="description">Assine Acima</div>
                                                    <div class="signature-pad--actions">
                                                        <div>
                                                            <button type="button" class="button clear" data-action="clear">Limpar</button>
                                                            <button type="button" class="button" data-action="undo">Desfazer</button>
                                                        </div>
                                                        <div>
                                                            <button type="button" class="button save" data-action="save-png">SALVAR</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
            </form>
            <div class="row">
                <div class="text-center m-t-20" style="margin-bottom: 30px;display: block;width: 100%;">
                    <button destino="cliente/inserir" class="novo-ajax btn btn-primary submit-btn" style="min-width: 320px;">Cadastrar</button>
                </div>
            </div>		
        </div>
    </div>

    <!-- Modal Web Cam -->
    <div class="modal fade" id="cadastro-ok" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #8ec193;">
                    <h5 class="modal-title" id="exampleModalLongTitle">Cadastro com sucesso!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="margin-top: 15px;padding: 15px;">
                    <p>Grato por fazer o cadatro, seu acesso já está diponível.</p>
                </div>
                <div class="modal-footer">
                    <a href="<?php echo URL; ?>login/logincliente" class="btn btn-primary" style="color:white">Fazer login</a>
                    <a href="<?php echo URL; ?>" class="btn btn-secondary" style="color:white">voltar</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Web Cam -->
   
<div class="sidebar-overlay" data-reff=""></div>