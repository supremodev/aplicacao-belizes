<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
        <div class="row flex-grow">
          <div class="col-lg-6 d-flex align-items-center justify-content-center">
            <div class="auth-form-transparent text-left p-3">
              <div class="brand-logo">
                <img src="<?php echo URL; ?>images/logo.png" alt="logo">
              </div>
              <h4>Seja bem vindo!</h4>
              <h6 class="font-weight-light">Feliz em ver você de novo!</h6>
              <form class="pt-3" id="form-recuperar-cliente">
                <div class="form-group">
                  <label for="exampleInputEmail">Nome ou E-mail</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-account-outline text-primary"></i>
                      </span>
                    </div>
                    <input type="text" name="email" class="form-control form-control-lg border-left-0">
                  </div>
                </div>
                <span class="mensagem" style="display: none;">teste</span>
                
                <div class="my-2 d-flex justify-content-between align-items-center">
                  <a href="<?php echo URL; ?>login" class="auth-link text-black">Voltar ao login?</a>
                </div>
              </form>
              <div class="my-3">
                <button type="submit" name="recuperar" id="recuperar-cliente" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">RECUPERAR</button>
              </div>
            </div>
          </div>
          <div class="col-lg-6 login-half-bg d-flex flex-row">
            <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy; 2018  All rights reserved.</p>
          </div>
        </div>
        <script src="<?php echo URL; ?>assets/js/jquery-3.2.1.min.js"></script>
        <script>
          var url = document.location.origin;
          if (url == "https://belizes.supremodigital.com.br") {
              var url = document.location.origin;
          } else {
              var url = document.location.origin + "/aplicacao-belizes";
          }

          $("#recuperar-cliente").click(function () {
          event.preventDefault();

          var evento = this;
          var form = $('#form-recuperar-cliente');

          $(evento).prop('disabled', true);
          $(evento).toggleClass('button--loading');

          $(form).find('.mensagem').hide('slow');

          $.ajax({
            url: url + "/login/recuperarSenhaCliente",
            method: 'post',
            data: new FormData($('#form-recuperar-cliente')[0]),
            cache: false,
            contentType: false,
            processData: false,
            dataType: "html",
            success: function (retorno) {
              $('.mensagem').html(retorno);
              var msgModal = JSON.parse(retorno);
              if (msgModal) {
                $(form).find('.mensagem').show('slow');
                $(form).find('.mensagem').addClass('sucesso');
                $(form).find('.mensagem').text('Enviamos para seu E-mail');
                $(evento).prop('disabled', false);
              } else {
                $(form).find('.mensagem').show('slow');
                $(form).find('.mensagem').addClass('erro');
                $(form).find('.mensagem').text('NÃ£o existe esse Email cadastrado');
                $(evento).prop('disabled', false);
              }
            },
            beforeSend: function () {

            },
            complete: function (msg) {
              $(evento).toggleClass('button--loading');
            }
          });
        });
          </script>

  