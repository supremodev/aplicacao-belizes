<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth lock-full-bg" style="background-image: url(<?php echo URL;?>images/fundo-login.jpg);">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-transparent text-left p-5 text-center">
              <img src="../../images/cliente/<?php echo $clientes[0]->cliFoto;?>" class="lock-profile-img" alt="img">
              <form class="pt-5" id="form-nova-senha-cliente">
                <span class="mensagem"></span>
                <div class="form-group">
                  <label for="Recuperação de senha">Recuperação de senha</label>
                  <input type="password" name="nova-senha" class="form-control text-center" placeholder="Nova senha" required>
                </div>
                <div class="form-group">
                  <input type="password" name="repita-senha" class="form-control text-center" placeholder="Repita a senha" required>
                  <input type="text" name="id" value="<?php echo $clientes[0]->id;?>" hidden>
                </div>
                <div class="mt-5">
                  <button id="nova-senha-cliente" class="btn btn-block btn-success btn-lg font-weight-medium">Salvar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>