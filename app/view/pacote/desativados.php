<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">Pacotes desativados</h4>
                    </div>
                </div>
				<div class="row doctor-grid">
					<?php foreach ($PacoteLista as $pacote) { ?>
                    <div id="linha<?php echo $pacote->idPacoteServico?>" class="col-md-4 col-sm-4 col-xs-6 col-lg-4">
                        <div class="profile-widget servico">
                            <div class="pacote-img">
							 	<a class="avatar pacoteAvatar" href="<?php echo URL; ?>pacote/detalhe/<?php echo $pacote->idPacoteServico; ?>">
                                	<img alt="<?php if (isset($pacote->pacoteNome)) echo htmlspecialchars($pacote->pacoteNome, ENT_QUOTES, 'UTF-8'); ?>" src="<?php echo URL; ?>assets/img/pacote/<?php if (isset($pacote->pacoteFoto)) echo htmlspecialchars($pacote->pacoteFoto, ENT_QUOTES, 'UTF-8'); ?>">
								</a>
                            </div>
                            <div class="dropdown profile-action">
                                <a class="desativar-ajax dropdown-item" href="#" destino="pacote/ativarDesativar/1" idobjeto="<?php echo $pacote->idPacoteServico; ?>" href="#" data-toggle="modal" data-target="#delete_doctor"><i class="fa fa-check m-r-5"></i>Ativar</a>
                            </div>
                            <h4 class="doctor-name text-ellipsis">
								<a href="profile.html">
								<?php if (isset($pacote->pacoteNome)) echo htmlspecialchars($pacote->pacoteNome, ENT_QUOTES, 'UTF-8'); ?>
								</a>
							</h4>
                            <div class="doc-prof">
								<p>
									<?php 
										//if (isset($pacote->pacoteDescricao)) echo htmlspecialchars($pacote->pacoteDescricao, ENT_QUOTES, 'UTF-8');			
										echo substr($pacote->pacoteDescricao, 0, 200);
									?>
								</p>
							</div>
							<div class="servicosAdminValores">
								<div class="user-country">
									<i class="fa fa-money"></i>
									Valor: R$
									<?php if (isset($pacote->pacoteValor)) echo htmlspecialchars($pacote->pacoteValor, ENT_QUOTES, 'UTF-8'); ?>
								</div>
								<div class="user-country">
									<i class="fa fa-money"></i>
									Valor Promocional: R$
									<?php if (isset($pacote->pacoteValorPromo)) echo htmlspecialchars($pacote->pacoteValorPromo, ENT_QUOTES, 'UTF-8'); ?>
								</div>
							</div>
                        </div>
                    </div>
					<?php } ?>					
                </div>
            </div>

    <div class="sidebar-overlay" data-reff=""></div>