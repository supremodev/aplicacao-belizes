<div class="page-wrapper">
            <div class="content">
				<div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
					<span class="texto-ajax">Erro ao adicionar serviço.</span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
					<span class="texto-ajax">Serviço adicionado com sucesso.</span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
                <div class="row">
                    <div class="col-sm-7 col-4">
                        <h4 class="page-title">Detalhe do Plano : <?php echo htmlspecialchars($detalheLista->pacoteNome, ENT_QUOTES, 'UTF-8'); ?></h4>
                    </div>


                    <div class="col-sm-5 col-8 text-right m-b-30">
                        <a href="<?php echo URL; ?>pacote/editar/<?php echo $detalheLista->idPacoteServico; ?>" class="btn btn-primary btn-rounded">
							<i class="fa fa-plus"></i> Editar Pacote
						</a>
                    </div>
                </div>
                <div class="card-box profile-header">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-view">
                                <div class="profile-img-wrap">
                                    <div class="profile-img">
                                        <a href="#"><img class="avatar" src="<?php echo URL; ?>assets/img/pacote/<?php echo htmlspecialchars($detalheLista->pacoteFoto, ENT_QUOTES, 'UTF-8'); ?>" alt=""></a>
                                    </div>
                                </div>
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="profile-info-left">
                                                <h3 class="user-name m-t-0 m-b-0"><?php echo htmlspecialchars($detalheLista->pacoteNome, ENT_QUOTES, 'UTF-8'); ?></h3>
                                                <div class="staff-id detalheTxt"><?php echo htmlspecialchars($detalheLista->pacoteDescricao, ENT_QUOTES, 'UTF-8'); ?></div>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <ul class="personal-info">
                                                <li>
                                                    <span class="title">Valor:</span>
                                                    <span class="text"><?php echo htmlspecialchars($detalheLista->pacoteValor, ENT_QUOTES, 'UTF-8'); ?></span>
                                                </li>
                                                <li>
                                                    <span class="title">Valor Promocional:</span>
                                                    <span class="text"><?php echo htmlspecialchars($detalheLista->pacoteValorPromo, ENT_QUOTES, 'UTF-8'); ?></span>
                                                </li>
                                                <li>
                                                    <span class="title">Comissão:</span>
                                                    <span class="text"><?php echo htmlspecialchars($detalheLista->pacoteComissao, ENT_QUOTES, 'UTF-8'); ?></span>
                                                </li>
                                                <li>
                                                    <span class="title">Tempo:</span>
                                                    <span class="text"><?php echo htmlspecialchars($detalheLista->pacoteTempo, ENT_QUOTES, 'UTF-8'); ?></span>
                                                </li>
                                                <li>
                                                    <span class="title">Publicado:</span>
                                                    <span class="text">														
														<?php														
															htmlspecialchars($detalheLista->pacotePublicar, ENT_QUOTES, 'UTF-8'); 

															if($detalheLista->pacotePublicar == 1){
																echo "Sim";
															}else{
																echo "Não";
															}																					
														?>													
													</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                    </div>
                </div>
				
				<div class="profile-tabs">
					<ul class="nav nav-tabs nav-tabs-bottom">
						<li class="nav-item"><a class="nav-link active" href="#about-cont" data-toggle="tab">Serviço do Pacote</a></li> 
						<li class="nav-item"><a class="nav-link" href="#bottom-tab2" data-toggle="tab">Serviços da Disponivél</a></li><!--
						<li class="nav-item"><a class="nav-link" href="#bottom-tab3" data-toggle="tab">Opção 02</a></li> -->
					</ul>

					<div class="tab-content">
						<div class="tab-pane show active" id="about-cont">
							<div class="content">
								<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-border table-striped custom-table datatable m-b-0">
											<thead>
												<tr>
													<th>Nome</th>
													<th>Valor</th>
													<th>Valor Promocional</th>
													<th>Comissão</th>
													<th>Tempo</th>
													<th>Remover</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($detalheServico as $servico) { ?>
												<tr id="linha<?php echo $servico->idServico?>">
													<td>
														<img width="28" height="28" src="<?php echo URL; ?>assets/img/servico/<?php 
                                                                                            if ($servico->servicoFoto == "") {
                                                                                                echo 'sem-foto.jpg';
                                                                                            }else{
                                                                                                echo $servico->servicoFoto;
                                                                                            }
                                                                                          ?>" class="rounded-circle m-r-5"> 
														<?php echo $servico->servicoNome; ?>
													</td>
													<td>
														<?php echo $servico->servicoValor; ?>
													</td>
													<td>
														<?php echo $servico->servicoValorPromo; ?> 
													</td>
													<td>
														<?php echo $servico->servicoComissao; ?> 
													</td>
													<td>
														<?php echo $servico->servicoTempo; ?> 
													</td>
													<td>
														<input type="checkbox" class="check-servico" name="checkbox" value="<?php echo $servico->idServico; ?>">															
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-sm-5 col-8 m-b-30">
									<button style="display:none" destino="pacote/removerServico" idObjeto="<?php echo $detalheLista->idPacoteServico;?>" class="add-servico-ajax btn btn-primary btn-rounded">
										<i class="fa fa-minus"></i> Remover Serviço
									</button>
								</div>
							</div>
								
							</div>
						</div>
						<div class="tab-pane" id="bottom-tab2">
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-border table-striped custom-table datatable m-b-0">
											<thead>
												<tr>
													<th>Nome</th>
													<th>Valor</th>
													<th>Valor Promocional</th>
													<th>Comissão</th>
													<th>Tempo</th>
													<th>Adicionar</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($listarServico as $servico) { ?>
												<tr id="linha<?php echo $servico->idServico?>">
													<td>
														<img width="28" height="28" src="<?php echo URL; ?>assets/img/servico/<?php 
                                                                                            if ($servico->servicoFoto == "") {
                                                                                                echo 'sem-foto.jpg';
                                                                                            }else{
                                                                                                echo htmlspecialchars($servico->servicoFoto, ENT_QUOTES, 'UTF-8');
                                                                                            }
                                                                                          ?>" class="rounded-circle m-r-5"> 
														<?php if (isset($servico->servicoNome)) echo htmlspecialchars($servico->servicoNome, ENT_QUOTES, 'UTF-8'); ?>
													</td>
													<td>
														<?php if (isset($servico->servicoValor)) echo htmlspecialchars($servico->servicoValor, ENT_QUOTES, 'UTF-8'); ?>
													</td>
													<td>
														<?php if (isset($servico->servicoValorPromo)) echo htmlspecialchars($servico->servicoValorPromo, ENT_QUOTES, 'UTF-8'); ?> 
													</td>
													<td>
														<?php if (isset($servico->servicoComissao)) echo htmlspecialchars($servico->servicoComissao, ENT_QUOTES, 'UTF-8'); ?> 
													</td>
													<td>
														<?php if (isset($servico->servicoTempo)) echo htmlspecialchars($servico->servicoTempo, ENT_QUOTES, 'UTF-8'); ?> 
													</td>
													<td>
														<input type="checkbox" class="check-servico" name="checkbox" value="<?php if (isset($servico->idServico)) echo htmlspecialchars($servico->idServico, ENT_QUOTES, 'UTF-8'); ?>">															
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-sm-5 col-8 m-b-30">
									<button style="display:none" destino="pacote/inserirServico" idObjeto="<?php echo $detalheLista->idPacoteServico;?>" class="add-servico-ajax btn btn-primary btn-rounded">
										<i class="fa fa-plus"></i> Adicionar Serviço
									</button>
								</div>
							</div>
						</div>
						<!--
						<div class="tab-pane" id="bottom-tab3">
							Tab content 3
						</div> -->
					</div>
				</div>
			</div> 

	
    <div class="sidebar-overlay" data-reff=""></div>