<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Editar Plano de Serviço</h4>
                    </div>
                </div>
				<?php foreach ($PacoteEditar as $linha) { ?>
                <form id="formularios">
                    <div class="card-box">
                        <h3 class="card-title">Informação básica</h3>
                        <div class="row">
                            <div class="col-md-12">
                                
                                
                                <div class="profile-img-wrap">
									<div class="profile" style="background-image: url('<?php							   
                                            if ($linha->pacoteFoto == "") {
												echo URL."assets/img/pacote/sem-foto.jpg";
                                                $imgFoto = "";
											}else{
                                                echo URL."assets/img/pacote/".$linha->pacoteFoto;
                                                $imgFoto = URL."assets/img/pacote/".$linha->pacoteFoto;
										   	} 
											?>');">
										<label class="edit">
											<span><i class="mdi mdi-upload"></i></span>
											<input type="file" size="32" name="pacoteFoto" id="inputImagem" value="<?php echo $imgFoto;?>">
										</label>
									</div>
									
									<button id="editarImagem" type="button" class="btn btn-primary btn-sm">
										<i class="fa fa-file-image-o"></i>
									</button>
								</div>
                                
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Nome do Plano:*</label>
                                                <input type="text" name="pacoteNome" class="form-control floating" value="<?php echo $linha->pacoteNome;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Valor do Plano:*</label>
                                                <input type="text" name="pacoteValor" class="form-control floating" value="<?php echo $linha->pacoteValor;?>" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Valor Promocional:*</label>
                                                <input type="text" name="pacoteValorPromo" class="form-control floating" value="<?php echo $linha->pacoteValorPromo;?>" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Comissão:</label>
                                                <input type="text" name="pacoteComissao" class="form-control floating" value="<?php echo $linha->pacoteComissao;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Tempo:</label>
                                                <input type="text" name="pacoteTempo" class="form-control floating" value="<?php echo $linha->pacoteTempo;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Sessões:</label>
                                                <input type="text" name="pacoteSessoes" class="form-control floating" value="<?php echo $linha->pacoteSessoes;?>" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
                        </div>
                    </div>
                    <div class="card-box">
                        <h3 class="card-title">Descrição</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
									<textarea class="form-control floating" name="pacoteDescricao" rows="6" cols="30"><?php echo $linha->pacoteDescricao;?></textarea>
                                </div>
                            </div>
							<div class="col-md-12">
                                <label class="col-md-4 col-form-label form-group">Publicar? </label>
								<div class="col-md-2">
									<div class="form-group form-check form-check-inline">
										<input class="form-check-input" type="radio" name="pacotePublicar" id="publicar" value="1" checked>
										<label class="form-check-label" for="publicar">Sim</label>
									</div>
									<div class="form-group form-check form-check-inline">
										<input class="form-check-input" type="radio" name="pacotePublicar" id="publicar" value="0">
										<label class="form-check-label" for="publicar">Não</label>
									</div>
								</div>
                            </div>							                         
                        </div>
                    </div>
                    <div class="text-center m-t-20">
                        <button destino="pacote/atualizar/<?php echo $linha->idPacoteServico;?>" class="atualizar-ajax btn-primary submit-btn" type="button">Atualizar pacote</button>
                    </div>
                </form>
				<?php } ?>
            </div>

	
    <div class="sidebar-overlay" data-reff=""></div>   
