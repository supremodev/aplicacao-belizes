<div class="page-wrapper">
            <div class="content">
				<div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
                    <span class="texto-ajax"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
					<span class="texto-ajax"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">Pacotes de Tratamento</h4>
                    </div>
                    <div class="col-sm-8 col-9 text-right m-b-20">
						<a href="<?php echo URL; ?>pacote/novo" class="btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i>Adicionar Pacote</a>
						<a href="<?php echo URL; ?>pacote/desativado" class="btn btn btn-primary btn-rounded float-right">Pacote Desativados</a>
                    </div>
                </div>
				<div class="row doctor-grid">
					<?php foreach ($PacoteLista as $pacote) { ?>
                    <div id="linha<?php echo $pacote->idPacoteServico?>" class="col-md-4 col-sm-4 col-xs-6 col-lg-4">
                        <div class="profile-widget servico">
                            <div class="pacote-img">
							 	<a class="avatar pacoteAvatar" href="<?php echo URL; ?>pacote/detalhe/<?php echo $pacote->idPacoteServico; ?>">
                                	<img alt="<?php if (isset($pacote->pacoteNome)) echo htmlspecialchars($pacote->pacoteNome, ENT_QUOTES, 'UTF-8'); ?>" src="<?php echo URL; ?>assets/img/pacote/<?php if (isset($pacote->pacoteFoto)) echo htmlspecialchars($pacote->pacoteFoto, ENT_QUOTES, 'UTF-8'); ?>">
								</a>
                            </div>
                            <div class="dropdown profile-action">
                                <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="pacote/editar/<?php echo $pacote->idPacoteServico; ?>"><i class="fa fa-pencil m-r-5"></i>Editar</a>
									<a class="dropdown-item" href="<?php echo URL; ?>pacote/detalhe/<?php echo $pacote->idPacoteServico; ?>"><i class="fa fa-trash-o m-r-5"></i>Detalhe</a>
                                    <a class="desativar-ajax dropdown-item" href="#" destino="pacote/ativarDesativar/0" idobjeto="<?php echo $pacote->idPacoteServico; ?>" href="#" data-toggle="modal" data-target="#delete_doctor"><i class="fa fa-trash-o m-r-5"></i>Desativar</a>
                                </div>
                            </div>
                            <h4 class="doctor-name text-ellipsis">
								<a href="profile.html">
								<?php if (isset($pacote->pacoteNome)) echo htmlspecialchars($pacote->pacoteNome, ENT_QUOTES, 'UTF-8'); ?>
								</a>
							</h4>
                            <div class="doc-prof">
								<p>
									<?php 
										//if (isset($pacote->pacoteDescricao)) echo htmlspecialchars($pacote->pacoteDescricao, ENT_QUOTES, 'UTF-8');			
										echo substr($pacote->pacoteDescricao, 0, 200);
									?>
								</p>
							</div>
							<div class="servicosAdminValores">
								<div class="user-country">
									<i class="fa fa-money"></i>
									Valor: R$
									<?php if (isset($pacote->pacoteValor)) echo htmlspecialchars($pacote->pacoteValor, ENT_QUOTES, 'UTF-8'); ?>
								</div>
								<div class="user-country">
									<i class="fa fa-money"></i>
									Valor Promocional: R$
									<?php if (isset($pacote->pacoteValorPromo)) echo htmlspecialchars($pacote->pacoteValorPromo, ENT_QUOTES, 'UTF-8'); ?>
								</div>
							</div>
                        </div>
                    </div>
					<?php } ?>					
                </div>
            </div>

    <div class="sidebar-overlay" data-reff=""></div>