<div class="page-wrapper">
            <div class="content">
            <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
					Erro ao cadastrar o plano.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
					Plano cadastrada com sucesso.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Cadastro Plano de Serviço</h4>
                    </div>
                </div>
                <form id="formularios">
                    <div class="card-box">
                        <h3 class="card-title">Informação básica</h3>
                        <div class="row">
                            <div class="col-md-12">
                                
                                <div class="profile-img-wrap">
									<div class="profile" style="background-image: url('<?php echo URL; ?>assets/img/pacote/sem-foto.jpg');">
										<label class="edit">
											<span><i class="mdi mdi-upload"></i></span>
											<input type="file" size="32" name="pacoteFoto" id="inputImagem">
										</label>
									</div>
									<button id="editarImagem" type="button" class="btn btn-primary btn-sm">
										<i class="fa fa-file-image-o"></i>
									</button>
								</div>
                                
                                
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Nome do Plano:*</label>
                                                <input type="text" name="pacoteNome" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Valor do Plano:*</label>
                                                <input type="text" name="pacoteValor" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Valor Promocional:*</label>
                                                <input type="text" name="pacoteValorPromo" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Comissão:</label>
                                                 <input type="text" name="pacoteComissao" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Tempo (EX.: 02:30:10):</label>
                                                 <input type="text" name="pacoteTempo" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Sessões:</label>
                                                <input type="text" name="pacoteSessoes" class="form-control floating" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
                        </div>
                    </div>
                    <div class="card-box">
                        <h3 class="card-title">Descrição</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
									<textarea class="form-control floating" rows="6" cols="30" name="pacoteDescricao"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label class="col-md-4 col-form-label form-group">Publicar? </label>
								<div class="col-md-2">
									<div class="form-group form-check form-check-inline">
										<input class="form-check-input" type="radio" name="pacotePublicar" id="publicar" value="1" checked>
										<label class="form-check-label" for="publicar">Sim</label>
									</div>
									<div class="form-group form-check form-check-inline">
										<input class="form-check-input" type="radio" name="pacotePublicar" id="publicar" value="0">
										<label class="form-check-label" for="publicar">Não</label>
									</div>
								</div>
                            </div>
							                         
                        </div>
                    </div>
                    <div class="text-center m-t-20">
						<button destino="pacote/inserir" class="novo-ajax btn btn-primary submit-btn" type="button">Cadastrar Plano</button>
                    </div>
                </form>
				
            </div>

    <div class="sidebar-overlay" data-reff=""></div>   
