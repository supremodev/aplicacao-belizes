<div class="page-wrapper">
	<div class="content">
		<div class="row">
			<div class="col-sm-4 col-3">
				<h4 class="page-title">Lista de pesquisa</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table m-b-0 new-patient-table">
						<thead>
							<tr>
								<th>Nome Cliente</th>
								<th>Data</th>
								<th>Celular</th>
								<th class="text-right"></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($listaPesquisa as $pesquisa) { ?>

								<tr>
									<td>
										<img width="28" height="28" class="rounded-circle" src="<?php
																								if (!empty($pesquisa->cliFoto)) {
																									echo URL . "/assets/img/cliente/" . $pesquisa->cliFoto;
																								} elseif (!empty($pesquisa->cliWebcam)) {
																									echo $pesquisa->cliWebcam;
																								} else {
																									echo URL . "/assets/img/user.jpg";
																								}
																								?>" alt="">
									<h2><?php echo $pesquisa->cliNome; ?></h2>
									</td>
									<td><?php echo $pesquisa->pesquisaQualidadeData; ?></td>
									<td><?php echo $pesquisa->cliCel; ?></td>
									<td><a  href="<?php echo URL;?>pesquisa/pesquisacliente/<?php echo $pesquisa->idPesquisaQualidade;?>"><i class="fa fa-eye"></i> Visualizar</a></td>
								</tr>

							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="sidebar-overlay" data-reff=""></div>