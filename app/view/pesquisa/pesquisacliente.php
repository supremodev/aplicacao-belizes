<div class="page-wrapper">
    <div class="content">
        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:<?php if (isset($_SESSION['alerta']) && $_SESSION['alerta'] == "erro") {
                                                                                                    echo 'block';
                                                                                                    $_SESSION['alerta'] = "alerta";
                                                                                                } else {
                                                                                                    echo 'none';
                                                                                                }; ?>">
            Erro ao Atualizar.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="display:<?php if (isset($_SESSION['alerta']) && $_SESSION['alerta'] == "sucesso") {
                                                                                                        echo 'block';
                                                                                                        $_SESSION['alerta'] = "alerta";
                                                                                                    } else {
                                                                                                        echo 'none';
                                                                                                    }; ?>">
            Atualizado com Sucesso.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Pesquisa</h4>
            </div>
        </div>
        <?php foreach ($listaPesquisa as $linha) { ?>
            <form id="formularios">
                <div class="card-box">
                    <h3 class="card-title">Informações do Paciente</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-img-wrap">
                                <img id="img" class="inline-block" src="<?php 
                                                    if (!empty($linha->cliFoto)){
														echo URL."/assets/img/cliente/".$linha->cliFoto;
                                                    }elseif(!empty($linha->cliWebcam)){
                                                        echo $linha->cliWebcam;
                                                    }else{
                                                        echo URL."/assets/img/cliente/user.jpg";
                                                    }
                                                    ?>" alt="user">
                            </div>
                            <div class="profile-basic">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group form-focus select-focus select-focus">
                                            <label class="focus-label">Nome:*</label>
                                            <input id="nome" type="text" class="form-control floating" value="<?php echo $linha->cliNome;?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group form-focus select-focus select-focus">
                                            <label class="focus-label">Pesquisa por CPF:*</label>
                                            <input type="text" name=""  value="<?php echo $linha->cliCPF;?>" class="form-control floating" max="11" maxlength="11" pattern="([0-9]{11})"  required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group form-focus select-focus select-focus">
                                            <label class="focus-label">Data de Nascimento:</label>
                                            <div class="cal-icon">
                                                <input id="nasci" class="form-control floating" type="text" value="<?php echo $linha->cliNasci;?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group form-focus select-focus select-focus">
                                            <label class="focus-label">Profissão:</label>
                                            <input id="profissao" type="text" class="form-control floating" value="<?php echo $linha->cliProfissao;?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group form-focus select-focus select-focus">
                                            <label class="focus-label">Sexo</label>
                                            <input id="sexo" type="text" class="form-control floating" value="<?php echo $linha->cliSexo;?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-box">
                    <h3 class="card-title">Como o serviço foi prestado?</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="focus-label">Avaliação</label>
                                        <div class="star-rating">
                                            <fieldset>
                                                <input type="radio" id="star5" name="rating" value="5" <?php if ($linha->pesquisaQualidadeComoServicoFoiPrestado == "Excelente") {
                                                                                                            echo 'class="ativo-avaliacao"';
                                                                                                        } ?> /><label for="star5" title="Outstanding">5 stars</label>
                                                <input type="radio" id="star4" name="rating" value="4" <?php if ($linha->pesquisaQualidadeComoServicoFoiPrestado == "Bom") {
                                                                                                            echo 'class="ativo-avaliacao"';
                                                                                                        } ?> /><label for="star4" title="Very Good">4 stars</label>
                                                <input type="radio" id="star3" name="rating" value="3" <?php if ($linha->pesquisaQualidadeComoServicoFoiPrestado == "Medio") {
                                                                                                            echo 'class="ativo-avaliacao"';
                                                                                                        } ?> /><label for="star3" title="Good">3 stars</label>
                                                <input type="radio" id="star2" name="rating" value="2" <?php if ($linha->pesquisaQualidadeComoServicoFoiPrestado == "Bom") {
                                                                                                            echo 'class="ativo-avaliacao"';
                                                                                                        } ?> /><label for="star2" title="Poor">2 stars</label>
                                                <input type="radio" id="star1" name="rating" value="1" <?php if ($linha->pesquisaQualidadeComoServicoFoiPrestado == "Muito Mal") {
                                                                                                            echo 'class="ativo-avaliacao"';
                                                                                                        } ?> /><label for="star1" title="Very Poor">1 star</label>
                                            </fieldset>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Comentário</label>
                                            <input type="text" name="servicoTempo" class="form-control floating" value="<?php echo $linha->pesquisaQualidadeObsComoServicoFoiPrestado; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-box">
                        <h3 class="card-title">Atendimento Recepção?</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="focus-label">Avaliação</label>
                                            <div class="star-rating">
                                                <fieldset>
                                                    <input type="radio" id="star5" name="rating" value="5" <?php if ($linha->pesquisaQualidadeAtendimentoRecepcao == "Excelente") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star5" title="Outstanding">5 stars</label>
                                                    <input type="radio" id="star4" name="rating" value="4" <?php if ($linha->pesquisaQualidadeAtendimentoRecepcao == "Bom") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star4" title="Very Good">4 stars</label>
                                                    <input type="radio" id="star3" name="rating" value="3" <?php if ($linha->pesquisaQualidadeAtendimentoRecepcao == "Medio") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star3" title="Good">3 stars</label>
                                                    <input type="radio" id="star2" name="rating" value="2" <?php if ($linha->pesquisaQualidadeAtendimentoRecepcao == "Bom") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star2" title="Poor">2 stars</label>
                                                    <input type="radio" id="star1" name="rating" value="1" <?php if ($linha->pesquisaQualidadeAtendimentoRecepcao == "Muito Mal") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star1" title="Very Poor">1 star</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Comentário</label>
                                            <input type="text" name="servicoTempo" class="form-control floating" value="<?php echo $linha->pesquisaQualidadeObsAtendimentoRecepcao; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-box">
                        <h3 class="card-title">Atendimento Biomédico?</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="focus-label">Avaliação</label>
                                            <div class="star-rating">
                                                <fieldset>
                                                    <input type="radio" id="star5" name="rating" value="5" <?php if ($linha->pesquisaQualidadeAtendimentoBiomedico == "Excelente") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star5" title="Outstanding">5 stars</label>
                                                    <input type="radio" id="star4" name="rating" value="4" <?php if ($linha->pesquisaQualidadeAtendimentoBiomedico == "Bom") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star4" title="Very Good">4 stars</label>
                                                    <input type="radio" id="star3" name="rating" value="3" <?php if ($linha->pesquisaQualidadeAtendimentoBiomedico == "Medio") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star3" title="Good">3 stars</label>
                                                    <input type="radio" id="star2" name="rating" value="2" <?php if ($linha->pesquisaQualidadeAtendimentoBiomedico == "Bom") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star2" title="Poor">2 stars</label>
                                                    <input type="radio" id="star1" name="rating" value="1" <?php if ($linha->pesquisaQualidadeAtendimentoBiomedico == "Muito Mal") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star1" title="Very Poor">1 star</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Comentário</label>
                                            <input type="text" name="servicoTempo" class="form-control floating" value="<?php echo $linha->pesquisaQualidadeObsAtendimentoBiomedico; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-box">
                        <h3 class="card-title">Atendimento Esteticista?</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="focus-label">Avaliação</label>
                                            <div class="star-rating">
                                                <fieldset>
                                                    <input type="radio" id="star5" name="rating" value="5" <?php if ($linha->pesquisaQualidadeAtendimentoEsteticista == "Excelente") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star5" title="Outstanding">5 stars</label>
                                                    <input type="radio" id="star4" name="rating" value="4" <?php if ($linha->pesquisaQualidadeAtendimentoEsteticista == "Bom") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star4" title="Very Good">4 stars</label>
                                                    <input type="radio" id="star3" name="rating" value="3" <?php if ($linha->pesquisaQualidadeAtendimentoEsteticista == "Medio") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star3" title="Good">3 stars</label>
                                                    <input type="radio" id="star2" name="rating" value="2" <?php if ($linha->pesquisaQualidadeAtendimentoEsteticista == "Mal") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star2" title="Poor">2 stars</label>
                                                    <input type="radio" id="star1" name="rating" value="1" <?php if ($linha->pesquisaQualidadeAtendimentoEsteticista == "Muito Mal") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star1" title="Very Poor">1 star</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Comentário</label>
                                            <input type="text" name="servicoTempo" class="form-control floating" value="<?php echo $linha->pesquisaQualidadeObsAtendimentoEsteticista; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-box">
                        <h3 class="card-title">Atendimento Geral?</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="focus-label">Avaliação</label>
                                            <div class="star-rating">
                                                <fieldset>
                                                    <input type="radio" id="star5" name="rating" value="5" <?php if ($linha->pesquisaQualidadeAtendimentoGeral == "Excelente") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star5" title="Outstanding">5 stars</label>
                                                    <input type="radio" id="star4" name="rating" value="4" <?php if ($linha->pesquisaQualidadeAtendimentoGeral == "Bom") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star4" title="Very Good">4 stars</label>
                                                    <input type="radio" id="star3" name="rating" value="3" <?php if ($linha->pesquisaQualidadeAtendimentoGeral == "Medio") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star3" title="Good">3 stars</label>
                                                    <input type="radio" id="star2" name="rating" value="2" <?php if ($linha->pesquisaQualidadeAtendimentoGeral == "Bom") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star2" title="Poor">2 stars</label>
                                                    <input type="radio" id="star1" name="rating" value="1" <?php if ($linha->pesquisaQualidadeAtendimentoGeral == "Muito Mal") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star1" title="Very Poor">1 star</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Comentário</label>
                                            <input type="text" name="servicoTempo" class="form-control floating" value="<?php echo $linha->pesquisaQualidadeObsAtendimentoGeral; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-box">
                        <h3 class="card-title">Limpeza dos Banheiros?</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="focus-label">Avaliação</label>
                                            <div class="star-rating">
                                                <fieldset>
                                                    <input type="radio" id="star5" name="rating" value="5" <?php if ($linha->pesquisaQualidadeLimpezaDosBanheiros == "Excelente") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star5" title="Outstanding">5 stars</label>
                                                    <input type="radio" id="star4" name="rating" value="4" <?php if ($linha->pesquisaQualidadeLimpezaDosBanheiros == "Bom") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star4" title="Very Good">4 stars</label>
                                                    <input type="radio" id="star3" name="rating" value="3" <?php if ($linha->pesquisaQualidadeLimpezaDosBanheiros == "Medio") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star3" title="Good">3 stars</label>
                                                    <input type="radio" id="star2" name="rating" value="2" <?php if ($linha->pesquisaQualidadeLimpezaDosBanheiros == "Mal") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star2" title="Poor">2 stars</label>
                                                    <input type="radio" id="star1" name="rating" value="1" <?php if ($linha->pesquisaQualidadeLimpezaDosBanheiros == "Muito Mal") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star1" title="Very Poor">1 star</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Comentário</label>
                                            <input type="text" name="servicoTempo" class="form-control floating" value="<?php echo $linha->pesquisaQualidadeObsLimpezaDosBanheiros; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-box">
                        <h3 class="card-title">Limpeza das Salas?</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="focus-label">Avaliação</label>
                                            <div class="star-rating">
                                                <fieldset>
                                                    <input type="radio" id="star5" name="rating" value="5" <?php if ($linha->pesquisaQualidadeLimpezaDasSalas == "Excelente") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star5" title="Outstanding">5 stars</label>
                                                    <input type="radio" id="star4" name="rating" value="4" <?php if ($linha->pesquisaQualidadeLimpezaDasSalas == "Bom") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star4" title="Very Good">4 stars</label>
                                                    <input type="radio" id="star3" name="rating" value="3" <?php if ($linha->pesquisaQualidadeLimpezaDasSalas == "Medio") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star3" title="Good">3 stars</label>
                                                    <input type="radio" id="star2" name="rating" value="2" <?php if ($linha->pesquisaQualidadeLimpezaDasSalas == "Mal") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star2" title="Poor">2 stars</label>
                                                    <input type="radio" id="star1" name="rating" value="1" <?php if ($linha->pesquisaQualidadeLimpezaDasSalas == "Muito Mal") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star1" title="Very Poor">1 star</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Comentário</label>
                                            <input type="text" name="servicoTempo" class="form-control floating" value="<?php echo $linha->pesquisaQualidadeObsLimpezaDasSalas; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-box">
                        <h3 class="card-title">Conforto do Espaço?</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="focus-label">Avaliação</label>
                                            <div class="star-rating">
                                                <fieldset>
                                                    <input type="radio" id="star5" name="rating" value="5" <?php if ($linha->pesquisaQualidadeConfortoDoEspaco == "Excelente") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star5" title="Outstanding">5 stars</label>
                                                    <input type="radio" id="star4" name="rating" value="4" <?php if ($linha->pesquisaQualidadeConfortoDoEspaco == "Bom") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star4" title="Very Good">4 stars</label>
                                                    <input type="radio" id="star3" name="rating" value="3" <?php if ($linha->pesquisaQualidadeConfortoDoEspaco == "Medio") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star3" title="Good">3 stars</label>
                                                    <input type="radio" id="star2" name="rating" value="2" <?php if ($linha->pesquisaQualidadeConfortoDoEspaco == "Mal") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star2" title="Poor">2 stars</label>
                                                    <input type="radio" id="star1" name="rating" value="1" <?php if ($linha->pesquisaQualidadeConfortoDoEspaco == "Muito Mal") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star1" title="Very Poor">1 star</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Comentário</label>
                                            <input type="text" name="servicoTempo" class="form-control floating" value="<?php echo $linha->pesquisaQualidadeObsConfortoDoEspaco; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-box">
                        <h3 class="card-title">Seu Tratamento?</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="focus-label">Avaliação</label>
                                            <div class="star-rating">
                                                <fieldset>
                                                    <input type="radio" id="star5" name="rating" value="5" <?php if ($linha->pesquisaQualidadeSeuTratamento == "Excelente") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star5" title="Outstanding">5 stars</label>
                                                    <input type="radio" id="star4" name="rating" value="4" <?php if ($linha->pesquisaQualidadeSeuTratamento == "Bom") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star4" title="Very Good">4 stars</label>
                                                    <input type="radio" id="star3" name="rating" value="3" <?php if ($linha->pesquisaQualidadeSeuTratamento == "Medio") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star3" title="Good">3 stars</label>
                                                    <input type="radio" id="star2" name="rating" value="2" <?php if ($linha->pesquisaQualidadeSeuTratamento == "Mal") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star2" title="Poor">2 stars</label>
                                                    <input type="radio" id="star1" name="rating" value="1" <?php if ($linha->pesquisaQualidadeSeuTratamento == "Muito Mal") {
                                                                                                                echo 'class="ativo-avaliacao"';
                                                                                                            } ?> /><label for="star1" title="Very Poor">1 star</label>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">Comentário</label>
                                            <input type="text" name="servicoTempo" class="form-control floating" value="<?php echo $linha->pesquisaQualidadeObsSeuTratamento; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-box">
                        <h3 class="card-title">Observações</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="row ficha">
                                            <label class="col-md-4 col-form-label form-group">Ficaram esclarecidas todas as dúvidas? </label>
                                            <div class="col-md-2">
                                                <div class="form-group form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="" value="1" <?php if ($linha->pesquisaQualidadeFicaramEsclarecidasTodasAsDuvidas == 1) {echo "checked";} ?>>
                                                    <label class="form-check-label" for=""> Sim <i class="input-helper"></i></label>
                                                </div>
                                                <div class="form-group form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="fichaHistoricoDeDermatiteOuCancerDePele" id="" value="0" <?php if ($linha->pesquisaQualidadeFicaramEsclarecidasTodasAsDuvidas == 0) {
                                                                                                                echo 'checked';
                                                                                                            } ?>>
                                                    <label class="form-check-label" for=""> Não <i class="input-helper"></i></label>
                                                </div>
                                            </div>

                                            <label class="col-md-4 col-form-label form-group">Foi aferida sua pressão arterial? </label>
                                            <div class="col-md-2">
                                                <div class="form-group form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" id="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" value="1" <?php if ($linha->pesquisaQualidadeFoiAferidaSuaPressaoArterial == 1) {echo "checked";} ?>>
                                                    <label class="form-check-label" for="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico"> Sim <i class="input-helper"></i></label>
                                                </div>
                                                <div class="form-group form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" id="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico" value="0" <?php if ($linha->pesquisaQualidadeFoiAferidaSuaPressaoArterial == 0) {echo "checked";} ?>>
                                                    <label class="form-check-label" for="fichaFoiLiberadoPeloMedicoParaTratamentoEstetico"> Não <i class="input-helper"></i></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-focus">
                                            <label class="focus-label">observação</label>
                                            <input type="text" name="servicoTempo" class="form-control floating" value="<?php echo $linha->pesquisaQualidadeDesejaFazerUmaObservacao; ?>" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            </form>

        <?php } ?>
    </div>

    <div class="sidebar-overlay" data-reff=""></div>