<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">Serviço Desativados</h4>
                    </div>
                </div>
				<div class="row doctor-grid">
					<?php foreach ($servicos as $servico) { ?>
                    <div id="linha<?php echo $servico->idServico?>" class="col-md-4 col-sm-4 col-xs-6 col-lg-4">
                        <div class="profile-widget servico">
                            <div class="servico-img">
							 	<a class="avatar" href="<?php echo URL; ?>servicos/detalhe/<?php echo $servico->idServico; ?>">
                                	<img alt="<?php if (isset($servico->servicoNome)) echo htmlspecialchars($servico->servicoNome, ENT_QUOTES, 'UTF-8'); ?>" src="<?php echo URL; ?>assets/img/servico/<?php 
                                                                if ($servico->servicoFoto == "") {
                                                                    echo "sem-foto.jpg";
                                                                }else{
                                                                    echo htmlspecialchars($servico->servicoFoto, ENT_QUOTES, 'UTF-8');
                                                                } 
                                                                
                                                        ?>">
								</a>
                            </div>
                            <div class="dropdown profile-action">
                                
                                <a class="dropdown-item desativar-ajax" destino="servicos/ativarDesativar/1" idobjeto="<?php echo $servico->idServico; ?>" href="#"><i class="fa fa-check m-r-5"></i>Ativar</a>
                            </div>
                            <h4 class="doctor-name text-ellipsis">
								<a href="profile.html">
								<?php if (isset($servico->servicoNome)) echo htmlspecialchars($servico->servicoNome, ENT_QUOTES, 'UTF-8'); ?>
								</a>
							</h4>
                            <div class="doc-prof">
								<p>
									<?php 
										//if (isset($servico->servicoDescricao)) echo htmlspecialchars($servico->servicoDescricao, ENT_QUOTES, 'UTF-8'); 						
										echo substr($servico->servicoDescricao, 0, 200);
									?>
								</p>
							</div>
							<div class="servicosAdminValores">
								<div class="user-country">
									<i class="fa fa-money"></i>
									Valor: R$
									<?php if (isset($servico->servicoValor)) echo htmlspecialchars($servico->servicoValor, ENT_QUOTES, 'UTF-8'); ?>
								</div>
								<div class="user-country">
									<i class="fa fa-money"></i>
									Valor Promocional: R$
									<?php if (isset($servico->servicoValorPromo)) echo htmlspecialchars($servico->servicoValorPromo, ENT_QUOTES, 'UTF-8'); ?>
								</div>
							</div>
                        </div>
                    </div>
					<?php } ?>					
                </div>
            </div>

			
    <div class="sidebar-overlay" data-reff=""></div>