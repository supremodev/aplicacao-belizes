<div class="page-wrapper">
    <div class="content">
        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
				<span class="texto-ajax">Erro ao remover foto serviço.</span>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
			<span class="texto-ajax">Foto removida com sucesso.</span>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
		</div>
        <div class="row">
            <div class="col-sm-7 col-4">
                <h4 class="page-title">Detalhe do Serviço : <?php echo htmlspecialchars($detalheLista->servicoNome, ENT_QUOTES, 'UTF-8'); ?></h4>
            </div>
            <div class="col-sm-5 col-8 text-right m-b-30">
                <a href="<?php echo URL; ?>servicos/editar/<?php echo $detalheLista->idServico; ?>" class="btn btn-primary btn-rounded">
                    <i class="fa fa-plus"></i> Editar Serviço
                </a>
            </div>
        </div>
        <div class="card-box profile-header">
            <div class="row">
                <div class="col-md-12">
                    <div class="profile-view">
                        <div class="profile-img-wrap">
                            <div class="profile-img">
                                <a href="#"><img class="avatar" src="<?php echo URL; ?>assets/img/servico/<?php echo htmlspecialchars($detalheLista->servicoFoto, ENT_QUOTES, 'UTF-8'); ?>" alt=""></a>
                            </div>
                        </div>
                        <div class="profile-basic">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="profile-info-left">
                                        <h3 class="user-name m-t-0 m-b-0"><?php echo htmlspecialchars($detalheLista->servicoNome, ENT_QUOTES, 'UTF-8'); ?></h3>
                                        <div class="staff-id detalheTxt"><?php echo htmlspecialchars($detalheLista->servicoDescricao, ENT_QUOTES, 'UTF-8'); ?></div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <ul class="personal-info">
                                        <li>
                                            <span class="title">Valor:</span>
                                            <span class="text"><?php echo htmlspecialchars($detalheLista->servicoValor, ENT_QUOTES, 'UTF-8'); ?></span>
                                        </li>
                                        <li>
                                            <span class="title">Valor Promocional:</span>
                                            <span class="text"><?php echo htmlspecialchars($detalheLista->servicoValorPromo, ENT_QUOTES, 'UTF-8'); ?></span>
                                        </li>
                                        <li>
                                            <span class="title">Comissão:</span>
                                            <span class="text"><?php echo htmlspecialchars($detalheLista->servicoComissao, ENT_QUOTES, 'UTF-8'); ?></span>
                                        </li>
                                        <li>
                                            <span class="title">Tempo:</span>
                                            <span class="text"><?php echo htmlspecialchars($detalheLista->servicoTempo, ENT_QUOTES, 'UTF-8'); ?></span>
                                        </li>
                                        <li>
                                            <span class="title">Publicado:</span>
                                            <span class="text">
                                                <?php

                                                htmlspecialchars($detalheLista->servicoPublicar, ENT_QUOTES, 'UTF-8');

                                                if ($detalheLista->servicoPublicar == 1) {
                                                    echo "Sim";
                                                } else {
                                                    echo "Não";
                                                }

                                                ?>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="profile-tabs">
            <ul class="nav nav-tabs nav-tabs-bottom">
                <li class="nav-item"><a class="nav-link active" href="#about-cont" data-toggle="tab">Fotos</a></li>
                <!--
						<li class="nav-item"><a class="nav-link" href="#bottom-tab2" data-toggle="tab">Fotos</a></li>
						<li class="nav-item"><a class="nav-link" href="#bottom-tab3" data-toggle="tab">Opção 02</a></li> -->
            </ul>

            <div class="tab-content">
                <div class="tab-pane show active" id="about-cont">
                    <div class="content">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Fotos do Serviço
                                    <button class="btn" data-toggle="modal" data-target="#exampleModalCenter">
                                        <i class="fa fa-plus"></i> Fotos
                                    </button>
                                </h4>
                            </div>
                        </div>
                        <div class="row detalheServico">
                            <?php foreach ($fotoLista as $foto) { ?>
                                <div id="linha<?php echo $foto->idFoto;?>" class="col-xl-3 col-lg-3 col-md-6 col-sm-6 m-b-20">
                                    <img class="img-thumbnail" src="<?php echo URL; ?>assets/img/<?php echo htmlspecialchars($foto->fotoUrl, ENT_QUOTES, 'UTF-8'); ?>" alt="<?php echo htmlspecialchars($foto->fotoNome, ENT_QUOTES, 'UTF-8'); ?>">
                                    <h4><?php echo htmlspecialchars($foto->fotoNome, ENT_QUOTES, 'UTF-8'); ?></h4>
                                    <h5><?php echo htmlspecialchars($foto->fotoDescricao, ENT_QUOTES, 'UTF-8'); ?></h5>
                                    <button type="button" destino="servicos/deleteFoto" idObjeto="<?php echo $foto->idFoto;?>" class="deletar-ajax btn btn-danger"><i class="fa fa-close"></i></button>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!--<div class="tab-pane" id="bottom-tab2">
							
						</div>
						<div class="tab-pane" id="bottom-tab3">
							Tab content 3
						</div> -->
            </div>
        </div>
    </div>

    <div class="sidebar-overlay" data-reff=""></div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Adicionar Foto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
                        Erro ao cadastrar Curso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
                        Curso cadastrado com sucesso.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div> 

                    <form id="cursoform">
                            <div class="card-box">
                                <h3 class="card-title">Imagem</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="profile-img-wrap" style="position: relative;width: 100%;">
                                            <div class="profile" style="background-image: url('<?php echo URL; ?>assets/img/pacote/sem-foto.jpg');">
                                                <label class="edit">
                                                    <span><i class="mdi mdi-upload"></i></span>
                                                    <input type="file" size="32" name="foto" id="inputImagem">
                                                </label>
                                            </div>
                                            <button id="editarImagem" type="button" class="btn btn-primary btn-sm">
                                                <i class="fa fa-file-image-o"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h3 class="card-title">Nome do curso</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-focus">
                                            <input type="text" class="form-control floating" name="idServico" value="<?php echo $id;?>" hidden="">
                                            <input type="text" class="form-control floating" name="fotoTitulo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-box">
                                <h3 class="card-title">Descrição</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-focus">
                                            <textarea class="form-control floating" rows="3" cols="30" name="fotoDescricao" style="min-height: 80px;"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" class="novo-foto-servico-ajax btn btn-primary">Salvar</button>
                </div>
            </div>
        </div>
    </div>