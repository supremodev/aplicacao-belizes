<div class="page-wrapper">
            <div class="content">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:<?php if(isset($_SESSION['alerta']) && $_SESSION['alerta']=="erro"){echo 'block';$_SESSION['alerta']="alerta";}else{echo 'none';}; ?>">
					Erro ao Atualizar. 
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:<?php if(isset($_SESSION['alerta']) && $_SESSION['alerta']=="sucesso"){echo 'block';$_SESSION['alerta']="alerta";}else{echo 'none';}; ?>">
					Atualizado com Sucesso.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Editar Serviço</h4>
                    </div>
                </div>
				<?php foreach ($ServicoEditar as $linha) { ?>
                <form id="formularios">
                    <div class="card-box">
                        <h3 class="card-title">Informação básica</h3>
                        <div class="row">
                            <div class="col-md-12">
                                
                                
                                <div class="profile-img-wrap">
									<div class="profile" style="background-image: url('<?php							   
                                            if ($linha->servicoFoto == "") {
												echo URL."assets/img/servico/sem-foto.jpg";
                                                $imgFoto = "";
											}else{
                                                echo URL."assets/img/servico/".$linha->servicoFoto;
                                                $imgFoto = URL."assets/img/servico/".$linha->servicoFoto;
										   	} 
											?>');">
										<label class="edit">
											<span><i class="mdi mdi-upload"></i></span>
											<input type="file" size="32" name="servicoFoto" id="inputImagem" value="<?php echo $imgFoto;?>">
										</label>
									</div>
									
									<button id="editarImagem" type="button" class="btn btn-primary btn-sm">
										<i class="fa fa-file-image-o"></i>
									</button>
								</div>								
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Nome Serviço:*</label>
                                                <input type="text" name="servicoNome" class="form-control floating" value="<?php echo $linha->servicoNome;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Valor Serviço:*</label>
                                                <input type="text" name="servicoValor" class="form-control floating" value="<?php echo $linha->servicoValor;?>" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Valor Promocional:*</label>
                                                <input type="text" name="servicoValorPromo" class="form-control floating" value="<?php echo $linha->servicoValorPromo;?>" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Comissão:</label>
                                                <input type="text" name="servicoComissao" class="form-control floating" value="<?php echo $linha->servicoComissao;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Tempo:</label>
                                                <input type="text" name="servicoTempo" class="form-control floating" value="<?php echo $linha->servicoTempo;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Sessões:</label>
                                                <input type="text" name="servicoSessoes" class="form-control floating" value="<?php echo $linha->servicoSessoes;?>" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>							
                        </div>
                    </div>
                    <div class="card-box">
                        <h3 class="card-title">Descrição</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
									<textarea name="servicoDescricao" class="form-control floating" rows="6" cols="30"><?php echo $linha->servicoDescricao;?></textarea>
                                </div>
                            </div>							                         
                        </div>
                    </div>
                    
                </form>
                <div class="text-center m-t-20">
                    <button destino="servicos/atualizar/<?php echo $linha->idServico;?>" class="atualizar-ajax btn btn-primary submit-btn" disabled>Editar Serviço</button>
                </div>
				<?php } ?>
            </div>            
			
    <div class="sidebar-overlay" data-reff=""></div>   
