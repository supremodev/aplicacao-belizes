<div class="page-wrapper">
            <div class="content">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
                    <span class="texto-ajax"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
					<span class="texto-ajax"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">Serviços</h4>
                    </div>
                    <div class="col-sm-8 col-9 text-right m-b-20">
                        <a href="<?php echo URL; ?>servicos/novo" class="btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i>Adicionar Serviço</a>
                        <a href="<?php echo URL; ?>servicos/desativado" class="btn btn btn-primary btn-rounded float-right">Serviço Desativados</a>
                    </div>
                </div>
				<div class="row doctor-grid">
					<?php foreach ($ServicosLista as $servico) { ?>
                    <div id="linha<?php echo $servico->idServico?>" class="col-md-2 col-sm-2 col-xs-4 col-lg-2">
                        <div class="profile-widget servico">
                            <div class="servico-img">
							 	<a class="avatar" href="<?php echo URL; ?>servicos/detalhe/<?php echo $servico->idServico; ?>">
                                	<img alt="<?php if (isset($servico->servicoNome)) echo htmlspecialchars($servico->servicoNome, ENT_QUOTES, 'UTF-8'); ?>" src="<?php echo URL; ?>assets/img/servico/<?php 
                                                                if ($servico->servicoFoto == "") {
                                                                    echo "sem-foto.jpg";
                                                                }else{
                                                                    echo htmlspecialchars($servico->servicoFoto, ENT_QUOTES, 'UTF-8');
                                                                } 
                                                                
                                                        ?>">
								</a>
                            </div>
                            <div class="dropdown profile-action">
                                <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="servicos/editar/<?php echo $servico->idServico; ?>"><i class="fa fa-pencil m-r-5"></i>Editar</a>
									
									<a class="desativar-ajax dropdown-item"  href="#" destino="servicos/ativarDesativar/0" idobjeto="<?php echo $servico->idServico; ?>" data-toggle="modal" data-target="#delete_patient">
									<i class="fa fa-trash-o m-r-5"></i>Desativar</a>
                                </div>
                            </div>
                            <h4 class="doctor-name text-ellipsis">
								<a href="profile.html">
								<?php if (isset($servico->servicoNome)) echo htmlspecialchars($servico->servicoNome, ENT_QUOTES, 'UTF-8'); ?>
								</a>
							</h4>
                            <div class="doc-prof">
								<p>
									<?php 
										//if (isset($servico->servicoDescricao)) echo htmlspecialchars($servico->servicoDescricao, ENT_QUOTES, 'UTF-8'); 						
										echo substr($servico->servicoDescricao, 0, 200);
									?>
								</p>
							</div>
							<div class="servicosAdminValores">
								<div class="user-country">
									<i class="fa fa-money"></i>
									Valor: R$
									<?php if (isset($servico->servicoValor)) echo htmlspecialchars($servico->servicoValor, ENT_QUOTES, 'UTF-8'); ?>
								</div>
								<div class="user-country">
									<i class="fa fa-money"></i>
									Valor Promocional: R$
									<?php if (isset($servico->servicoValorPromo)) echo htmlspecialchars($servico->servicoValorPromo, ENT_QUOTES, 'UTF-8'); ?>
								</div>
							</div>
                        </div>
                    </div>
					<?php } ?>					
                </div>
            </div>

			
    <div class="sidebar-overlay" data-reff=""></div>