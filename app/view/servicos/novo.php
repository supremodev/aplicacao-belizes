<div class="page-wrapper">
            <div class="content">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
                    Erro ao cadastrar Serviço.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
                    Serviço cadastrado com sucesso.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Cadastro Serviço</h4>
                    </div>
                </div>
                <form id="formularios">
                    <div class="card-box">
                        <h3 class="card-title">Informação básica</h3>
                        <div class="row">
                            <div class="col-md-12">                                
								<div class="profile-img-wrap">
									<div class="profile" style="background-image: url('<?php echo URL; ?>images/usuario.png');">
										<label class="edit">
											<span><i class="mdi mdi-upload"></i></span>
											<input type="file" size="32" name="servicoFoto" id="inputImagem">
										</label>
									</div>
									<button id="editarImagem" type="button" class="btn btn-primary btn-sm">
										<i class="fa fa-file-image-o"></i>
									</button>
								</div>
								
								
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Nome Serviço:*</label>
                                                <input type="text" name="servicoNome" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Valor Serviço:*</label>
                                                <input type="text" name="servicoValor" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Valor Promocional:*</label>
                                                <input type="text" name="servicoValorPromo" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Comissão:</label>
                                                <input type="text" name="servicoComissao" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Tempo (EX.: 02:30:10):</label>
                                                <input type="text" name="servicoTempo" class="form-control floating" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Sessões:</label>
                                                <input type="text" name="servicoSessoes" class="form-control floating"  required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>							
                        </div>
                    </div>
                    <div class="card-box">
                        <h3 class="card-title">Descrição</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
									<textarea class="form-control floating" rows="6" cols="30" name="servicoDescricao"></textarea>
                                </div>
                            </div>							                         
                        </div>
                    </div>
                </form>
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center m-t-20">
                                <button destino="servicos/inserir" class="novo-ajax btn btn-primary submit-btn">Cadastrar Serviço</button>
                            </div>
                        </div>							                         
                    </div>
                </div>
            </div>


	
    <div class="sidebar-overlay" data-reff=""></div>   
