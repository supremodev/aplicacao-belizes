<div class="various-content">

	<h2>Estamos a disposição</h2>
	<p>Envie uma mensagem dizendo o que vc está procurando, preencha os campos abaixo clique em enviar e entraremos em contato.</p>

	<!-- RESERVATION FORM : begin -->
	<form class="default-form" action="#" method="post">

		<hr class="c-divider">

		<div class="row">
			<div class="col-sm-6">

				<!-- NAME FIELD : begin -->
				<div class="form-field">
					<label for="nome">SEU NOME</label>
					<input id="nome" name="nome">
				</div>
				<!-- NAME FIELD : end -->

				<!-- EMAIL FIELD : begin -->
				<div class="form-field">
					<label for="email">SEU ENDEREÇO DE E-MAIL</label>
					<input id="email" name="email">
				</div>
				<!-- EMAIL FIELD : end -->

				<!-- PHONE FIELD : begin -->
				<div class="form-field">
					<label for="fone">O SEU NÚMERO DE CELULAR</label>
					<input id="fone" name="fone">
				</div>
				<!-- PHONE FIELD : end -->

			</div>
			<div class="col-sm-6">
				
				<!-- DATEPICKER FIELD : begin -->
				<div class="form-field">
					<label for="reservation-date">DATA E PERÍODO</label>
					<div class="row">
						<div class="col-sm-6">
							<div class="datepicker-input" data-date-format="dd/mm/yy" min="2020-06-22" data-first-day="0">
							<input type="date" id="dataReserva" name="dataReserva">
							</div>
						</div>
						<div class="col-sm-6">
							<select class="selectbox-input" id="periodoReserva" name="periodoReserva">
								<option value="Manhã">Manhã</option>
								<option value="Tarde">Tarde</option>
								<option value="Noite">Noite</option>
							</select>
						</div>
					</div>
				</div>
				<!-- DATEPICKER FIELD : end -->

				<!-- NOTE FIELD : begin -->
				<div class="form-field">
					<label for="mens">MENSAGEM</label>
					<textarea id="mens" name="mens"></textarea>
				</div>
				<!-- NOTE FIELD : end -->

				<!-- SUBMIT BUTTON : begin -->
				<div class="form-field">
					<button onclick="enviarFeleConosco()" class="c-button">Enviar mensagem via whatsApp</button>
				</div>
				<!-- SUBMIT BUTTON : end -->

			</div>
		</div>

	</form>
	<!-- RESERVATION FORM : end -->

</div>