<!-- WRAPPER : begin -->
		<div id="wrapper">
			<!-- CORE : begin -->
			<div id="core">
				<!-- PAGE HEADER : begin -->
				<div id="page-header">
					<div class="container">
						<h1>Contato</h1>
						<ul class="breadcrumbs">
							<li><a href="home">Home</a></li>
							<li>Contato</li>
						</ul>
					</div>
				</div>
				<!-- PAGE HEADER : begin -->
				<div class="container">
					<!-- PAGE CONTENT : begin -->
					<div id="page-content">
						<div class="various-content">
							<!-- ADDRESS SECTION : begin -->
							<section>

								<h2>ONDE VOCÊ PODE NOS ENCONTRAR</h2>

								<div class="row">
									<div class="col-lg-8">

										<!-- MAP : begin -->
										<div class="c-map">
											
											<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14635.273041383305!2d-46.4897038!3d-23.5030547!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x324c8dc891c5ba7c!2sBelizes%20Time%20Clinica%20est%C3%A9tica%20avan%C3%A7ada!5e0!3m2!1spt-BR!2sbr!4v1594951703039!5m2!1spt-BR!2sbr"></iframe>
										</div>
										<!-- MAP : end -->

									</div>
									<div class="col-lg-4">

										<p>Somos uma empresa nacional, prestadora de serviços estéticos, há mais de 12 anos no mercado,  promovendo soluções para restauração da beleza e saúde da pele.</p>

										<!-- ICON BLOCK : begin -->
										<div class="c-icon-block">
											<i class="ico fa fa-map-marker"></i>
											<div class="icon-block-inner">
												<p>
													<strong>Belizes Time Clinica estética avançada</strong><br>
													R. Boaventura Rodrigues da Silva, 142 - sala 2 - Parque Boturussu<br>
													São Paulo - SP, 03801-120
													
												</p>
											</div>
										</div>
										<!-- ICON BLOCK : end -->

										<!-- ICON BLOCK : begin -->
										<div class="c-icon-block">
											<i class="ico fa fa-clock-o"></i>
											<div class="icon-block-inner">
												<dl>
													<dt>Seg à Sex:</dt>
													<dd>10:00 - 16:00</dd>
													<dt>Sáb.:</dt>
													<dd>10:00 - 14:00</dd>
													<dt>Dom.:</dt>
													<dd>Fechado</dd>
												</dl>
											</div>
										</div>
										<!-- ICON BLOCK : end -->

									</div>
								</div>

							</section>
							<!-- ADDRESS SECTION : end -->

							<!-- FORM SECTION : begin -->
							<section>

								<h2>ENVIE-NOS UMA MENSAGEM</h2>

								<div class="row">
									<div class="col-lg-8">

										<!-- CONTACT FORM : begin -->
										<form id="contact-form" class="default-form m-ajax-form" action="#" method="post" >
											<input type="hidden" name="contact-form">

											<!-- FORM VALIDATION ERROR MESSAGE : begin -->
											<p class="c-alert-message m-warning m-validation-error" style="display: none;"><i class="ico fa fa-exclamation-circle"></i>Por favor, preencha todos os campos requisitados.</p>
											<!-- FORM VALIDATION ERROR MESSAGE : end -->

											<!-- SENDING REQUEST ERROR MESSAGE : begin -->
											<p class="c-alert-message m-warning m-request-error" style="display: none;"><i class="ico fa fa-exclamation-circle"></i>Houve um problema de conexão. Tente mais tarde.</p>
											<!-- SENDING REQUEST ERROR MESSAGE : end -->

											<div class="row">
												<div class="col-sm-6">

													<!-- NAME FIELD : begin -->
													<div class="form-field">
														<label for="contact-name">Nome: <span>*</span></label>
														<input id="contact-name" name="contact-name" class="m-required">
													</div>
													<!-- NAME FIELD : end -->

													<!-- EMAIL FIELD : begin -->
													<div class="form-field">
														<label for="contact-email">Email: <span>*</span></label>
														<input id="contact-email" name="contact-email" class="m-required m-email">
													</div>
													<p style="display: none;">
														<label for="contact-email-hp">Re Email Address</label>
														<input id="contact-email-hp" name="contact-email-hp">
													</p>
													<!-- EMAIL FIELD : end -->

													<!-- PHONE FIELD : begin -->
													<div class="form-field">
														<label for="contact-phone">Telefone</label>
														<input id="contact-phone" name="contact-phone">
													</div>
													<!-- PHONE FIELD : end -->

												</div>
												<div class="col-sm-6">

													<!-- SUBJECT FIELD : begin -->
													<div class="form-field">
														<label for="contact-subject">Assunto</label>
														<input id="contact-subject" name="contact-subject">
													</div>
													<!-- SUBJECT FIELD : end -->

													<!-- MESSAGE FIELD : begin -->
													<div class="form-field">
														<label for="contact-message">Mensagem <span>*</span></label>
														<textarea id="contact-message" name="contact-message" class="m-required"></textarea>
													</div>
													<!-- MESSAGE FIELD : end -->

													<!-- SUBMIT BUTTON : begin -->
													<div class="form-field">
														<button class="submit-btn c-button" type="submit" data-label="Send Message" data-loading-label="Sending...">Enviar Mensagem</button>
													</div>
													<!-- SUBMIT BUTTON : end -->

												</div>
											</div>

										</form>
										<!-- CONTACT FORM : end -->

									</div>
									<div class="col-lg-4">

										<!-- ICON BLOCK : begin -->
										<div class="c-icon-block">
											<i class="ico fa fa-info-circle"></i>
											<div class="icon-block-inner">
												<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>
												<p><a href="agendamento" class="c-button m-type-2 m-open-ajax-modal">REALIZAR UM AGENDAMENTO</a></p>
											</div>
										</div>
										<!-- ICON BLOCK : end -->

									</div>
								</div>

							</section>
							<!-- FORM SECTION : end -->
							<!-- INSTAGRAM SECTION : begin -->
							<section>
								<!-- SECTION HEADER : begin -->
								<header>
									<div class="container">
										<h2>Instagram</h2>
										<p class="subtitle">#belizes_time/</p>
										<p class="more"><a href="https://www.instagram.com/belizes_time/" target="_blank" class="c-button m-type-2">siga no insta</a></p>
									</div>
								</header>
								<!-- SECTION HEADER : end -->

								<div class="container">
									<div class="row" id="instagram">									
									</div>
								</div>

							</section>
							<!-- INSTAGRAM SECTION : end -->
						</div>
					</div>
					<!-- PAGE CONTENT : end -->
				</div>
			</div>
			<!-- CORE : end -->