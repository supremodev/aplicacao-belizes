		<!-- WRAPPER : begin -->
		<div id="wrapper">
			<!-- MAIN SLIDER : begin -->
			<div id="main-slider" data-interval="8000">
				<div class="slide-list">
					<!-- SLIDE 0 : begin -->
					<div class="slide slide-2" data-label="Belizes Time" style="background-image: url( 'images/slider/slide_02.png' );">
						<div class="slide-bg">
							<div class="container">
								<div class="slide-inner">
									<div class="slide-content various-content textalign-right valign-middle">
										<h3>Bem-Vindo</h3>
										<h1>Belizes Time</h1>
										<h3><br><a href="agendamento" class="m-open-ajax-modal">Faça seu Agendamento</a></h3>

									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- SLIDE 0 : end -->

					<!-- SLIDE 1 : begin -->
					<div class="slide slide-1" data-label="Tratamentos Estéticos" style="background-image: url( 'images/slider/slide_03.png' );">
						<div class="slide-bg">
							<div class="container">
								<div class="slide-inner">
									<div class="slide-content various-content textalign-left valign-middle">
										<h2>Tratamentos Estéticos</h2>
										<h4>50% de desconto em qualquer tratamento ao agendar sua primeira avaliação.</h4>
										<h3><br><a href="agendamento" class="m-open-ajax-modal">Faça seu Agendamento</a></h3>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- SLIDE 1 : end -->

					<!-- SLIDE 2 : begin -->
					<div class="slide slide-2" data-label="Tratamentos Facial" style="background-image: url( 'images/slider/slide_01.png' );">
						<div class="slide-bg">
							<div class="container">
								<div class="slide-inner">
									<div class="slide-content various-content textalign-right valign-middle">										
										<h2>Tratamentos Facial</h2>
										<h4>50% de desconto em qualquer tratamento ao agendar sua primeira avaliação.</h4>
										<h3><br><a href="agendamento" class="m-open-ajax-modal">Faça seu Agendamento</a></h3>

									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- SLIDE 2 : end -->

					<!-- SLIDE 3 : begin -->
					<div class="slide slide-3" data-label="Promoções - @belizes_time" style="background-image: url( 'images/slider/slide_04.png' );">
						<div class="slide-bg">
							<div class="container">
								<div class="slide-inner">
									<div class="slide-content various-content textalign-left valign-middle">
										
										<h3>Promoções</h3>
										<h2>@belizes_time</h2>
										<h4>Siga nosso Instagram e ganhe 01 sessão de massagem modeladora.</h4>
										<h3><br><a href="agendamento" class="m-open-ajax-modal">Faça seu Agendamento</a></h3>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- SLIDE 3 : end -->
					
					<!-- SLIDE 4 : begin -->
					<div class="slide slide-4" data-label="Tratamentos Corporal" style="background-image: url( 'images/slider/slide_05.png' );">
						<div class="slide-bg">
							<div class="container">
								<div class="slide-inner">
									<div class="slide-content various-content textalign-right valign-middle">
										<h2>Tratamentos Corporal</h2>
										<h4>50% de desconto em qualquer tratamento ao agendar sua primeira avaliação.</h4>
										<h3><br><a href="agendamento" class="m-open-ajax-modal">Faça seu Agendamento</a></h3>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- SLIDE 4 : end -->
				</div>
			</div>
			<!-- MAIN SLIDER : end -->

			<!-- CORE : begin -->
			<div id="core">
				<!-- PAGE CONTENT : begin -->
				<div id="page-content">
					<div class="various-content">
						<!-- SERVICES SECTION : begin -->
						<section>

							<!-- SECTION HEADER : begin -->
							<header>
								<div class="container">
									<h2>Serviços</h2>
									<p class="subtitle">Belizes Time</p>
									<p class="more"><a href="servico" class="c-button m-type-2">mais serviços</a></p>
								</div>
							</header>
							<!-- SECTION HEADER : end -->

							<!-- SERVICE LIST : begin -->
							<div class="c-service-list m-paginated" data-items="4" data-items-desktop="4" data-items-desktop-small="3" data-items-tablet="2" data-items-mobile="1">
								<div class="container">
									<div class="service-list-inner">
										<?php foreach ($servicosLista as $linhaServico) { ?>
										<div class="service-list-item">
											<div class="c-service">
												<div class="service-image">
													<a href="servico#corporal"><img src="assets/img/servico/<?php if(!empty($linhaServico->servicoFoto)){ echo $linhaServico->servicoFoto;}else{echo'servico.jpg';}?>" alt="<?php echo $linhaServico->servicoNome;?>"></a>
												</div>
												<h3 class="service-title"><a href="servico#corporal"><?php echo $linhaServico->servicoNome;?></a></h3>
												<div class="service-description">
													<p><?php echo $linhaServico->servicoDescricao;?></p>
												</div>
											</div>
										</div>
										<?php }?>
									</div>
								</div>
							</div>
							<!-- SERVICE LIST : end -->

						</section>
						<!-- SERVICES SECTION : end -->
						
						<!-- GALLERY SECTION : begin -->
						<section>

							<!-- SECTION HEADER : begin -->
							<header>
								<div class="container">
									<h2>Benefícios</h2>
									<p class="subtitle">Conheça alguns Benefícios</p>
								</div>
							</header>
							<!-- SECTION HEADER : end -->

							<!-- GALLERY : begin -->
							<div class="c-gallery m-paginated" data-items="4" data-items-desktop="4" data-items-desktop-small="3" data-items-tablet="2" data-items-mobile="1">
								<div class="thumb-list">
									<div class="thumb">
										<a href="images/galeria/gallery_01_gd.jpg" class="lightbox" data-lightbox-group="gallery">
											<img src="images/galeria/gallery_01.jpg" alt="">
										</a>
										<div>
											<p>Rejuvenescimento facial, pescoço e colo. Eliminação de Manchas.</p>
										</div>
									</div>
									<div class="thumb">
										<a href="images/galeria/gallery_02_gd.jpg" class="lightbox" data-lightbox-group="gallery">
											<img src="images/galeria/gallery_02.jpg" alt="">
										</a>
										<div>
											<p>Eliminação de celulite, estrias e flacidez e gordura localizada</p>
										</div>
									</div>
									<div class="thumb">
										<a href="images/galeria/gallery_03_gd.jpg" class="lightbox" data-lightbox-group="gallery">
											<img src="images/galeria/gallery_03.jpg" alt="">
										</a>
										<div>
											<p>Levantamento de Glúteo, eliminação de gordura localizada, definição dos músculos</p>
										</div>
									</div>
									<div class="thumb">
										<a href="images/galeria/gallery_04_gd.jpg" class="lightbox" data-lightbox-group="gallery">
											<img src="images/galeria/gallery_04.jpg" alt="">
										</a>
										<div>
											<p>Eliminação: linha de Expressão, olheiras, sardas, cicatrizes de acne</p>
										</div>
									</div>
									<div class="thumb">
										<a href="images/galeria/gallery_05_gd.jpg" class="lightbox" data-lightbox-group="gallery">
											<img src="images/galeria/gallery_05.jpg" alt="">
										</a>
										<div>
											<p>Redução de 99% dos pêlos</p>
										</div>
									</div>
									<div class="thumb">
										<a href="images/galeria/gallery_01_gd.jpg" class="lightbox" data-lightbox-group="gallery">
											<img src="images/galeria/gallery_01.jpg" alt="">
										</a>
										<div>
											<p>Rejuvenescimento facial, pescoço e colo. Eliminação de Manchas.</p>
										</div>
									</div>
									<div class="thumb">
										<a href="images/galeria/gallery_02_gd.jpg" class="lightbox" data-lightbox-group="gallery">
											<img src="images/galeria/gallery_02.jpg" alt="">
										</a>
										<div>
											<p>Eliminação de celulite, estrias e flacidez e gordura localizada</p>
										</div>
									</div>
									<div class="thumb">
										<a href="images/galeria/gallery_03_gd.jpg" class="lightbox" data-lightbox-group="gallery">
											<img src="images/galeria/gallery_03.jpg" alt="">
										</a>
										<div>
											<p>Levantamento de Glúteo, eliminação de gordura localizada, definição dos músculos</p>
										</div>
									</div>
									<div class="thumb">
										<a href="images/galeria/gallery_04_gd.jpg" class="lightbox" data-lightbox-group="gallery">
											<img src="images/galeria/gallery_04.jpg" alt="">
										</a>
										<div>
											<p>Eliminação: linha de Expressão, olheiras, sardas, cicatrizes de acne</p>
										</div>
									</div>
									<div class="thumb">
										<a href="images/galeria/gallery_05_gd.jpg" class="lightbox" data-lightbox-group="gallery">
											<img src="images/galeria/gallery_05.jpg" alt="">
										</a>
										<div>
											<p>Redução de 99% dos pêlos</p>
										</div>
									</div>
								</div>
							</div>
							<!-- GALLERY : end -->
						</section>
						<!-- GALLERY SECTION : end -->

						<!-- BLOG SECTION : begin -->
						<section>
							<!-- SECTION HEADER : begin -->
							<header>
								<div class="container">
									<h2>Instagram</h2>
									<p class="subtitle">#belizes_time/</p>
									<p class="more"><a href="https://www.instagram.com/belizes_time/" target="_blank" class="c-button m-type-2">siga no insta</a></p>
								</div>
							</header>
							<!-- SECTION HEADER : end -->

							<div class="container">
								<div class="row" id="instagram">									
								</div>
							</div>

						</section>
						<!-- BLOG SECTION : end -->

						<!-- TESTIMONIALS SECTION : begin -->
						<section>

							<!-- SECTION HEADER : begin -->
							<header>
								<div class="container">
									<h2>Clientes</h2>
									<p class="subtitle">Como foi VIVER o seu MOMENTO</p>
									<!--<p class="more"><a href="contact.html" class="c-button m-type-2">Become Our Client</a></p>-->
								</div>
							</header>
							<!-- SECTION HEADER : end -->

							<!-- TESTIMONIAL LIST : begin -->
							<div class="c-testimonial-list m-paginated">
								<div class="container">
									<div class="testimonial-list-inner">
										<?php foreach ($depoLista as $linhaDepo) { ?>
										<div class="testimonial-list-item">
											<div class="c-testimonial m-has-portrait">
												<div class="testimonial-inner">
													<p class="testimonial-portrait"><span><img src="assets/img/cliente/<?php echo $linhaDepo->cliFoto;?>" alt="<?php echo $linhaDepo->cliNome;?>"></span></p>
													<blockquote>
														<p><?php echo $linhaDepo->cliNome;?></p>
														<footer><?php echo $linhaDepo->depoimentoTexto;?></footer>
													</blockquote>
												</div>
											</div>
										</div>
										<?php }?>
									</div>
								</div>
							</div>
							<!-- TESTIMONIAL LIST : end -->

						</section>
						<!-- TESTIMONIALS SECTION : end -->
					</div>
				</div>
				<!-- PAGE CONTENT : end -->
			</div>
			<!-- CORE : end -->