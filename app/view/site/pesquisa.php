<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Admin - Belize</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">	
	
	<!-- Favicons-->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo URL; ?>assets/img/icon/favicon.ico">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo URL; ?>assets/img/icon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo URL; ?>assets/img/icon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo URL; ?>assets/img/icon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo URL; ?>assets/img/icon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo URL; ?>assets/img/icon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo URL; ?>assets/img/icon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo URL; ?>assets/img/icon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo URL; ?>assets/img/icon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo URL; ?>assets/img/icon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo URL; ?>assets/img/icon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo URL; ?>assets/img/icon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo URL; ?>assets/img/icon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo URL; ?>assets/img/icon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo URL; ?>assets/img/icon/manifest.json">
	<meta name="msapplication-TileColor" content="#3E7943">
	<meta name="msapplication-TileImage" content="<?php echo URL; ?>assets/img/icon/ms-icon-144x144.png">
	<meta name="theme-color" content="#3E7943">
   
    <!-- GOOGLE WEB FONT -->
    <link href="<?php echo URL; ?>assets-pesquisa/css/css" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?php echo URL; ?>assets-pesquisa/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo URL; ?>assets-pesquisa/css/style.css" rel="stylesheet">
	<link href="<?php echo URL; ?>assets-pesquisa/css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?php echo URL; ?>assets-pesquisa/css/custom.css" rel="stylesheet">

</head>

<body class="style_2" style="overflow: visible;">
	<div id="preloader" style="display: none;">
		<div data-loader="circle-side" style="display: none;"></div>
	</div><!-- /Preload -->
	<div id="loader_form">
		<div data-loader="circle-side-2"></div>
	</div><!-- /loader_form -->
	<header>
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-5">
	                <a href="#"><img src="<?php echo URL; ?>assets/img/logo.png" alt="" width="211" height="80"></a>
	            </div>
	            <div class="col-7">
	                <div id="social">
	                    <ul>
	                        <li><a href="#"><i class="social_facebook"></i></a></li>
	                        <li><a href="#"><i class="social_twitter"></i></a></li>
	                        <li><a href="#"><i class="social_instagram"></i></a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
	        <!-- /row -->
	    </div>
	    <!-- /container -->
	</header>
	
    <div class="wrapper_centering">
	    <div class="container_centering">
	        <div class="container">
	            <div class="row justify-content-between">
	                <div class="col-xl-6 col-lg-6 d-flex align-items-center">
	                    <div class="main_title_1">
	                        <h3><img src="<?php echo URL; ?>assets-pesquisa/img/main_icon_1.svg" width="80" height="80" alt=""> Pesquisa de Qualidade</h3>
	                        <p>Muito Obrigado por confiar no nosso trabalho e fazer parte da família Belizes´Time</p>
	                        <p>Pensando sempre em buscar e entregar o melhor atendimento e prestação de serviços, contamos com a sua colaboração em responder esse rápido e objetivo questionário de qualidade!</p>
	                        
	                        
	                    </div>
	                </div>
	                <!-- /col -->
	                <div class="col-xl-5 col-lg-5">
	                    <div id="wizard_container" class="wizard" novalidate="novalidate">
	                        <div id="top-wizard">
	                            <div id="progressbar" class="ui-progressbar ui-widget ui-widget-content ui-corner-all" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="ui-progressbar-value ui-widget-header ui-corner-left" style="display: none; width: 0%;"></div></div>
	                        </div>
	                        <!-- /top-wizard -->
	                        <form id="wrapped" autocomplete="off" class="fl-form fl-style-2 wizard-form" novalidate="novalidate">
	                            <input id="website" name="website" type="text" value="">
                                
	                            <div id="middle-wizard" class="wizard-branch wizard-wrapper">

	                                <!-- PERGUNTA 01 -->
									<div class="step wizard-step current" style="">
	                                    <h3 class="main_question wizard-header"><strong>1 de 10</strong>Como o serviço foi prestado?</h3>
	                                    <div class="review_block_smiles">
	                                    	<ul class="clearfix">
	                                    		<li>
	                                    			 <div class="container_smile">
	                                                    <input type="radio" id="very_bad_1" name="pesquisaQualidadeComoServicoFoiPrestado" class="required" value="Muito Mal">
	                                                    <label class="radio smile_1" for="very_bad_1"><span>Muito Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                    		 <div class="container_smile">
	                                                    <input type="radio" id="bad_1" name="pesquisaQualidadeComoServicoFoiPrestado" class="required" value="Mal">
	                                                    <label class="radio smile_2" for="bad_1"><span>Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="average_1" name="pesquisaQualidadeComoServicoFoiPrestado" class="required" value="Medio">
	                                                    <label class="radio smile_3" for="average_1"><span>Médio</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="good_1" name="pesquisaQualidadeComoServicoFoiPrestado" class="required" value="Bom">
	                                                    <label class="radio smile_4" for="good_1"><span>Bom</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="very_good_1" name="pesquisaQualidadeComoServicoFoiPrestado" class="required" value="Excelente">
	                                                    <label class="radio smile_5" for="very_good_1"><span>Excelente</span></label>
	                                                </div>
	                                            </li>
	                                    	</ul>
	                                    	<div class="row justify-content-between add_bottom_25">
	                                    		<div class="col-4">
	                                    			<em>Muito mal</em>
	                                    		</div>
	                                    		<div class="col-4 text-right">
	                                    			<em>Excelente</em>
	                                    		</div>
	                                    	</div>
	                                    </div>
	                                    <div class="form-group">
	                                        
	                                        <div class="fl-wrap fl-wrap-textarea"><label for="DesejaFazerUmaObservacaolabel" class="fl-label">Deseja fazer uma observação</label><textarea name="pesquisaQualidadeObsComoServicoFoiPrestado" id="DesejaFazerUmaObservacaolabel" class="form-control fl-textarea" style="height:180px;" placeholder="Deseja fazer uma observação"></textarea></div>
	                                    </div>
	                                </div>
									
	                                <!-- PERGUNTA 02 -->
                                    <div class="step wizard-step" style="display: none;">
	                                    <h3 class="main_question wizard-header"><strong>2 de 10</strong>Atendimento Recepção?</h3>
	                                    <div class="review_block_smiles">
	                                    	<ul class="clearfix">
	                                    		<li>
	                                    			 <div class="container_smile">
	                                                    <input type="radio" id="recepcao_very_bad_1" name="pesquisaQualidadeAtendimentoRecepcao" class="required" value="Muito Mal">
	                                                    <label class="radio smile_1" for="recepcao_very_bad_1"><span>Muito Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                    		 <div class="container_smile">
	                                                    <input type="radio" id="recepcao_bad_1" name="pesquisaQualidadeAtendimentoRecepcao" class="required" value="Mal">
	                                                    <label class="radio smile_2" for="recepcao_bad_1"><span>Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="recepcao_average_1" name="pesquisaQualidadeAtendimentoRecepcao" class="required" value="Medio">
	                                                    <label class="radio smile_3" for="recepcao_average_1"><span>Médio</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="recepcao_good_1" name="pesquisaQualidadeAtendimentoRecepcao" class="required" value="Bom">
	                                                    <label class="radio smile_4" for="recepcao_good_1"><span>Bom</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="recepcao_very_good_1" name="pesquisaQualidadeAtendimentoRecepcao" class="required" value="Excelente">
	                                                    <label class="radio smile_5" for="recepcao_very_good_1"><span>Excelente</span></label>
	                                                </div>
	                                            </li>
	                                    	</ul>
	                                    	<div class="row justify-content-between add_bottom_25">
	                                    		<div class="col-4">
	                                    			<em>Muito mal</em>
	                                    		</div>
	                                    		<div class="col-4 text-right">
	                                    			<em>Excelente</em>
	                                    		</div>
	                                    	</div>
	                                    </div>
	                                    <div class="form-group">
	                                        
	                                        <div class="fl-wrap fl-wrap-textarea"><label for="recepcao_DesejaFazerUmaObservacaolabel" class="fl-label">Deseja fazer uma observação</label><textarea name="pesquisaQualidadeObsAtendimentoRecepcao" id="recepcao_DesejaFazerUmaObservacaolabel" class="form-control fl-textarea" style="height:180px;" placeholder="Deseja fazer uma observação"></textarea></div>
	                                    </div>
	                                </div>
                                    
                                    <!-- PERGUNTA 03 -->
                                    <div class="step wizard-step" style="display: none;">
	                                    <h3 class="main_question wizard-header"><strong>3 de 10</strong>Atendimento Biomédico?</h3>
	                                    <div class="review_block_smiles">
	                                    	<ul class="clearfix">
	                                    		<li>
	                                    			 <div class="container_smile">
	                                                    <input type="radio" id="biomedico_very_bad_1" name="pesquisaQualidadeAtendimentoBiomedico" class="required" value="Muito Mal">
	                                                    <label class="radio smile_1" for="biomedico_very_bad_1"><span>Muito Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                    		 <div class="container_smile">
	                                                    <input type="radio" id="biomedico_bad_1" name="pesquisaQualidadeAtendimentoBiomedico" class="required" value="Mal">
	                                                    <label class="radio smile_2" for="biomedico_bad_1"><span>Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="biomedico_average_1" name="pesquisaQualidadeAtendimentoBiomedico" class="required" value="Medio">
	                                                    <label class="radio smile_3" for="biomedico_average_1"><span>Médio</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="biomedico_good_1" name="pesquisaQualidadeAtendimentoBiomedico" class="required" value="Bom">
	                                                    <label class="radio smile_4" for="biomedico_good_1"><span>Bom</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="biomedico_very_good_1" name="pesquisaQualidadeAtendimentoBiomedico" class="required" value="Excelente">
	                                                    <label class="radio smile_5" for="biomedico_very_good_1"><span>Excelente</span></label>
	                                                </div>
	                                            </li>
	                                    	</ul>
	                                    	<div class="row justify-content-between add_bottom_25">
	                                    		<div class="col-4">
	                                    			<em>Muito mal</em>
	                                    		</div>
	                                    		<div class="col-4 text-right">
	                                    			<em>Excelente</em>
	                                    		</div>
	                                    	</div>
	                                    </div>
	                                    <div class="form-group">
	                                        
	                                        <div class="fl-wrap fl-wrap-textarea"><label for="biomedico_DesejaFazerUmaObservacaolabel" class="fl-label">Deseja fazer uma observação</label><textarea name="pesquisaQualidadeObsAtendimentoBiomedico" id="biomedico_DesejaFazerUmaObservacaolabel" class="form-control fl-textarea" style="height:180px;" placeholder="Deseja fazer uma observação"></textarea></div>
	                                    </div>
	                                </div>
                                    
                                    
									<!-- PERGUNTA 04 -->
                                    <div class="step wizard-step" style="display: none;">
	                                    <h3 class="main_question wizard-header"><strong>4 de 10</strong>Atendimento Esteticista?</h3>
	                                    <div class="review_block_smiles">
	                                    	<ul class="clearfix">
	                                    		<li>
	                                    			 <div class="container_smile">
	                                                    <input type="radio" id="esteticista_very_bad_1" name="pesquisaQualidadeAtendimentoEsteticista" class="required" value="Muito Mal">
	                                                    <label class="radio smile_1" for="esteticista_very_bad_1"><span>Muito Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                    		 <div class="container_smile">
	                                                    <input type="radio" id="esteticista_bad_1" name="pesquisaQualidadeAtendimentoEsteticista" class="required" value="Mal">
	                                                    <label class="radio smile_2" for="esteticista_bad_1"><span>Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="esteticista_average_1" name="pesquisaQualidadeAtendimentoEsteticista" class="required" value="Medio">
	                                                    <label class="radio smile_3" for="esteticista_average_1"><span>Médio</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="esteticista_good_1" name="pesquisaQualidadeAtendimentoEsteticista" class="required" value="Bom">
	                                                    <label class="radio smile_4" for="esteticista_good_1"><span>Bom</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="esteticista_very_good_1" name="pesquisaQualidadeAtendimentoEsteticista" class="required" value="Excelente">
	                                                    <label class="radio smile_5" for="esteticista_very_good_1"><span>Excelente</span></label>
	                                                </div>
	                                            </li>
	                                    	</ul>
	                                    	<div class="row justify-content-between add_bottom_25">
	                                    		<div class="col-4">
	                                    			<em>Muito mal</em>
	                                    		</div>
	                                    		<div class="col-4 text-right">
	                                    			<em>Excelente</em>
	                                    		</div>
	                                    	</div>
	                                    </div>
	                                    <div class="form-group">
	                                        
	                                        <div class="fl-wrap fl-wrap-textarea"><label for="esteticista_DesejaFazerUmaObservacaolabel" class="fl-label">Deseja fazer uma observação</label><textarea name="pesquisaQualidadeObsAtendimentoEsteticista" id="esteticista_DesejaFazerUmaObservacaolabel" class="form-control fl-textarea" style="height:180px;" placeholder="Deseja fazer uma observação"></textarea></div>
	                                    </div>
	                                </div>
                                    
									<!-- PERGUNTA 05 -->
                                    
                                    <div class="step wizard-step" style="display: none;">
	                                    <h3 class="main_question wizard-header"><strong>5 de 10</strong>Atendimento Geral?</h3>
	                                    <div class="review_block_smiles">
	                                    	<ul class="clearfix">
	                                    		<li>
	                                    			 <div class="container_smile">
	                                                    <input type="radio" id="geral_very_bad_1" name="pesquisaQualidadeAtendimentoGeral" class="required" value="Muito Mal">
	                                                    <label class="radio smile_1" for="geral_very_bad_1"><span>Muito Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                    		 <div class="container_smile">
	                                                    <input type="radio" id="geral_bad_1" name="pesquisaQualidadeAtendimentoGeral" class="required" value="Mal">
	                                                    <label class="radio smile_2" for="geral_bad_1"><span>Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="geral_average_1" name="pesquisaQualidadeAtendimentoGeral" class="required" value="Medio">
	                                                    <label class="radio smile_3" for="geral_average_1"><span>Médio</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="geral_good_1" name="pesquisaQualidadeAtendimentoGeral" class="required" value="Bom">
	                                                    <label class="radio smile_4" for="geral_good_1"><span>Bom</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="geral_very_good_1" name="pesquisaQualidadeAtendimentoGeral" class="required" value="Excelente">
	                                                    <label class="radio smile_5" for="geral_very_good_1"><span>Excelente</span></label>
	                                                </div>
	                                            </li>
	                                    	</ul>
	                                    	<div class="row justify-content-between add_bottom_25">
	                                    		<div class="col-4">
	                                    			<em>Muito mal</em>
	                                    		</div>
	                                    		<div class="col-4 text-right">
	                                    			<em>Excelente</em>
	                                    		</div>
	                                    	</div>
	                                    </div>
	                                    <div class="form-group">
	                                        
	                                        <div class="fl-wrap fl-wrap-textarea"><label for="geral_DesejaFazerUmaObservacaolabel" class="fl-label">Deseja fazer uma observação</label><textarea name="pesquisaQualidadeObsAtendimentoGeral" id="geral_DesejaFazerUmaObservacaolabel" class="form-control fl-textarea" style="height:180px;" placeholder="Deseja fazer uma observação"></textarea></div>
	                                    </div>
	                                </div>
                                    
									<!-- PERGUNTA 06 -->	
                                    <div class="step wizard-step" style="display: none;">
	                                    <h3 class="main_question wizard-header"><strong>6 de 10</strong>Limpeza dos Banheiros?</h3>
	                                    <div class="review_block_smiles">
	                                    	<ul class="clearfix">
	                                    		<li>
	                                    			 <div class="container_smile">
	                                                    <input type="radio" id="banheiros_very_bad_1" name="pesquisaQualidadeLimpezaDosBanheiros" class="required" value="Muito Mal">
	                                                    <label class="radio smile_1" for="banheiros_very_bad_1"><span>Muito Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                    		 <div class="container_smile">
	                                                    <input type="radio" id="banheiros_bad_1" name="pesquisaQualidadeLimpezaDosBanheiros" class="required" value="Mal">
	                                                    <label class="radio smile_2" for="banheiros_bad_1"><span>Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="banheiros_average_1" name="pesquisaQualidadeLimpezaDosBanheiros" class="required" value="Medio">
	                                                    <label class="radio smile_3" for="banheiros_average_1"><span>Médio</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="banheiros_good_1" name="pesquisaQualidadeLimpezaDosBanheiros" class="required" value="Bom">
	                                                    <label class="radio smile_4" for="banheiros_good_1"><span>Bom</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="banheiros_very_good_1" name="pesquisaQualidadeLimpezaDosBanheiros" class="required" value="Excelente">
	                                                    <label class="radio smile_5" for="banheiros_very_good_1"><span>Excelente</span></label>
	                                                </div>
	                                            </li>
	                                    	</ul>
	                                    	<div class="row justify-content-between add_bottom_25">
	                                    		<div class="col-4">
	                                    			<em>Muito mal</em>
	                                    		</div>
	                                    		<div class="col-4 text-right">
	                                    			<em>Excelente</em>
	                                    		</div>
	                                    	</div>
	                                    </div>
	                                    <div class="form-group">
	                                        
	                                        <div class="fl-wrap fl-wrap-textarea"><label for="banheiros_DesejaFazerUmaObservacaolabel" class="fl-label">Deseja fazer uma observação</label><textarea name="pesquisaQualidadeObsLimpezaDosBanheiros" id="banheiros_DesejaFazerUmaObservacaolabel" class="form-control fl-textarea" style="height:180px;" placeholder="Deseja fazer uma observação"></textarea></div>
	                                    </div>
	                                </div>
                                    
                                    
									<!-- PERGUNTA 07 -->
                                     <div class="step wizard-step" style="display: none;">
	                                    <h3 class="main_question wizard-header"><strong>7 de 10</strong>Limpeza das Salas?</h3>
	                                    <div class="review_block_smiles">
	                                    	<ul class="clearfix">
	                                    		<li>
	                                    			 <div class="container_smile">
	                                                    <input type="radio" id="salas_very_bad_1" name="pesquisaQualidadeLimpezaDasSalas" class="required" value="Muito Mal">
	                                                    <label class="radio smile_1" for="salas_very_bad_1"><span>Muito Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                    		 <div class="container_smile">
	                                                    <input type="radio" id="salas_bad_1" name="pesquisaQualidadeLimpezaDasSalas" class="required" value="Mal">
	                                                    <label class="radio smile_2" for="salas_bad_1"><span>Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="salas_average_1" name="pesquisaQualidadeLimpezaDasSalas" class="required" value="Medio">
	                                                    <label class="radio smile_3" for="salas_average_1"><span>Médio</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="salas_good_1" name="pesquisaQualidadeLimpezaDasSalas" class="required" value="Bom">
	                                                    <label class="radio smile_4" for="salas_good_1"><span>Bom</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="salas_very_good_1" name="pesquisaQualidadeLimpezaDasSalas" class="required" value="Excelente">
	                                                    <label class="radio smile_5" for="salas_very_good_1"><span>Excelente</span></label>
	                                                </div>
	                                            </li>
	                                    	</ul>
	                                    	<div class="row justify-content-between add_bottom_25">
	                                    		<div class="col-4">
	                                    			<em>Muito mal</em>
	                                    		</div>
	                                    		<div class="col-4 text-right">
	                                    			<em>Excelente</em>
	                                    		</div>
	                                    	</div>
	                                    </div>
	                                    <div class="form-group">
	                                        
	                                        <div class="fl-wrap fl-wrap-textarea"><label for="salas_DesejaFazerUmaObservacaolabel" class="fl-label">Deseja fazer uma observação</label><textarea name="pesquisaQualidadeObsLimpezaDasSalas" id="salas_DesejaFazerUmaObservacaolabel" class="form-control fl-textarea" style="height:180px;" placeholder="Deseja fazer uma observação"></textarea></div>
	                                    </div>
	                                </div>
                                    
									<!-- PERGUNTA 08 -->
                                    <div class="step wizard-step" style="display: none;">
	                                    <h3 class="main_question wizard-header"><strong>8 de 10</strong>Conforto do Espaço?</h3>
	                                    <div class="review_block_smiles">
	                                    	<ul class="clearfix">
	                                    		<li>
	                                    			 <div class="container_smile">
	                                                    <input type="radio" id="espaco_very_bad_1" name="pesquisaQualidadeConfortoDoEspaco" class="required" value="Muito Mal">
	                                                    <label class="radio smile_1" for="espaco_very_bad_1"><span>Muito Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                    		 <div class="container_smile">
	                                                    <input type="radio" id="espaco_bad_1" name="pesquisaQualidadeConfortoDoEspaco" class="required" value="Mal">
	                                                    <label class="radio smile_2" for="espaco_bad_1"><span>Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="espaco_average_1" name="pesquisaQualidadeConfortoDoEspaco" class="required" value="Medio">
	                                                    <label class="radio smile_3" for="espaco_average_1"><span>Médio</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="espaco_good_1" name="pesquisaQualidadeConfortoDoEspaco" class="required" value="Bom">
	                                                    <label class="radio smile_4" for="espaco_good_1"><span>Bom</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="espaco_very_good_1" name="pesquisaQualidadeConfortoDoEspaco" class="required" value="Excelente">
	                                                    <label class="radio smile_5" for="espaco_very_good_1"><span>Excelente</span></label>
	                                                </div>
	                                            </li>
	                                    	</ul>
	                                    	<div class="row justify-content-between add_bottom_25">
	                                    		<div class="col-4">
	                                    			<em>Muito mal</em>
	                                    		</div>
	                                    		<div class="col-4 text-right">
	                                    			<em>Excelente</em>
	                                    		</div>
	                                    	</div>
	                                    </div>
	                                    <div class="form-group">
	                                        
	                                        <div class="fl-wrap fl-wrap-textarea"><label for="espaco_DesejaFazerUmaObservacaolabel" class="fl-label">Deseja fazer uma observação</label><textarea name="pesquisaQualidadeObsConfortoDoEspaco" id="espaco_DesejaFazerUmaObservacaolabel" class="form-control fl-textarea" style="height:180px;" placeholder="Deseja fazer uma observação"></textarea></div>
	                                    </div>
	                                </div>
                                    
									<!-- PERGUNTA 09 -->
                                    <div class="step wizard-step" style="display: none;">
	                                    <h3 class="main_question wizard-header"><strong>9 de 10</strong>Seu Tratamento?</h3>
	                                    <div class="review_block_smiles">
	                                    	<ul class="clearfix">
	                                    		<li>
	                                    			 <div class="container_smile">
	                                                    <input type="radio" id="tratamento_very_bad_1" name="pesquisaQualidadeSeuTratamento" class="required" value="Muito Mal">
	                                                    <label class="radio smile_1" for="tratamento_very_bad_1"><span>Muito Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                    		 <div class="container_smile">
	                                                    <input type="radio" id="tratamento_bad_1" name="pesquisaQualidadeSeuTratamento" class="required" value="Mal">
	                                                    <label class="radio smile_2" for="tratamento_bad_1"><span>Mal</span></label>
	                                                </div>
	                                    		</li>
	                                    		<li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="tratamento_average_1" name="pesquisaQualidadeSeuTratamento" class="required" value="Medio">
	                                                    <label class="radio smile_3" for="tratamento_average_1"><span>Médio</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="tratamento_good_1" name="pesquisaQualidadeSeuTratamento" class="required" value="Bom">
	                                                    <label class="radio smile_4" for="tratamento_good_1"><span>Bom</span></label>
	                                                </div>
	                                            </li>
	                                            <li>
	                                                <div class="container_smile">
	                                                    <input type="radio" id="tratamento_very_good_1" name="pesquisaQualidadeSeuTratamento" class="required" value="Excelente">
	                                                    <label class="radio smile_5" for="tratamento_very_good_1"><span>Excelente</span></label>
	                                                </div>
	                                            </li>
	                                    	</ul>
	                                    	<div class="row justify-content-between add_bottom_25">
	                                    		<div class="col-4">
	                                    			<em>Muito mal</em>
	                                    		</div>
	                                    		<div class="col-4 text-right">
	                                    			<em>Excelente</em>
	                                    		</div>
	                                    	</div>
	                                    </div>
	                                    <div class="form-group">
	                                        
	                                        <div class="fl-wrap fl-wrap-textarea"><label for="tratamento_DesejaFazerUmaObservacaolabel" class="fl-label">Deseja fazer uma observação</label><textarea name="pesquisaQualidadeObsSeuTratamento" id="tratamento_DesejaFazerUmaObservacaolabel" class="form-control fl-textarea" style="height:180px;" placeholder="Deseja fazer uma observação"></textarea></div>
	                                    </div>
	                                </div>
                                    
									<!-- PERGUNTA 10 -->
									<div class="submit step wizard-step" disabled="disabled" style="display: none;">
	                                    <h3 class="main_question"><strong>10 de 10</strong></h3>
										
										<div class="row">
	                                        <div class="col-lg-6 col-md-6 col-6">
	                                            <p>Ficaram esclarecidas todas as dúvidas?</p>
	                                        </div>
	                                        <div class="col-6">
	                                            <div class="form-group radio_input">
	                                                <label class="container_radio">Sim
	                                                    <input type="radio" name="pesquisaQualidadeFicaramEsclarecidasTodasAsDuvidas" value="1" class="required">
	                                                    <span class="checkmark"></span>
	                                                </label>
	                                                <label class="container_radio">Não
	                                                    <input type="radio" name="pesquisaQualidadeFicaramEsclarecidasTodasAsDuvidas" value="0" class="required">
	                                                    <span class="checkmark"></span>
	                                                </label>
	                                            </div>
	                                        </div>
	                                    </div>
										<div class="row">
	                                        <div class="col-lg-6 col-md-6 col-6">
	                                            <p>Foi aferida sua pressão arterial?</p>
	                                        </div>
	                                        <div class="col-6">
	                                            <div class="form-group radio_input">
	                                                <label class="container_radio">Sim
	                                                    <input type="radio" name="pesquisaQualidadeFoiAferidaSuaPressaoArterial" value="1" class="required">
	                                                    <span class="checkmark"></span>
	                                                </label>
	                                                <label class="container_radio">Não
	                                                    <input type="radio" name="pesquisaQualidadeFoiAferidaSuaPressaoArterial" value="0" class="required">
	                                                    <span class="checkmark"></span>
	                                                </label>
	                                            </div>
	                                        </div>
	                                    </div>
										<div class="form-group">
	                                        
	                                        <div class="fl-wrap fl-wrap-textarea">
												<label for="DesejaFazerUmaObservacaolabel10" class="fl-label">Deseja fazer uma observação</label>
												<textarea name="pesquisaQualidadeDesejaFazerUmaObservacao" id="pesquisaQualidadeDesejaFazerUmaObservacao" class="form-control fl-textarea" style="height:180px;" onkeyup="getVals(this, &#39;additional_message&#39;);" placeholder="Deseja fazer uma observação"></textarea>
											</div>
	                                    </div>
	                                </div>
	                                
	                            </div>
	                            

	                            <div id="bottom-wizard">
	                                <button type="button" name="backward" class="backward" disabled="disabled">voltar</button>
	                                <button type="button" name="forward" class="forward">proxímo</button>
	                                <button destino="pesquisa/inserir" type="button" name="process" class="submit novo-ajax btn btn-primary submit-btn" disabled="disabled">enviar</button>
								</div>
								<input type="text" name='idCliente' value="<?php echo $id?>" hidden>
								<input type="text" name="idAgenda" value="<?php echo $_GET['idagenda'] ?>" hidden>

								<div class="msg" style="background: rgb(102 204 51);padding: 15px;display: none;margin-top: 15px;"></div>
							</form>
                            
	                    </div>
						<p class="pesquisa">Fique tranquilo, esse e-mail e totalmente confidencial e ira ser entregue exclusivamente e diretamente ao cuidados do gerente da clínica!</p>
	                </div>
	            </div>
	        </div>
	    </div>
		
	    <footer>
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-md-12">
	                    <ul class="clearfix">
	                        <li><a href="#" class="animated_link" target="_parent">Supremo Digital</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
	    </footer>
	</div>
	
	
	<script src="<?php echo URL; ?>assets/js/jquery-3.2.1.min.js"></script>
	
	<script>
		
		var url = document.location.origin;
		if (url == "https://belizes.supremodigital.com.br") {
			var url = document.location.origin;
		} else {
			var url = document.location.origin + "/aplicacao-belizes";
		}

		$(".novo-ajax").click(function () {

		$('.msg').hide("slow");

		var destino = $(this).attr('destino');
		var evento = this;

		$.ajax({

			type: "POST",
			data: new FormData($('#wrapped')[0]),
			cache: false,
			contentType: false,
			processData: false,
			url: url + "/" + destino,
			dataType: "html",
			success: function (retorno) {
				//$('.pesquisa').html(retorno);
				var msgModal = JSON.parse(retorno);
				if (msgModal) {
					$('.msg').show("slow");
                	$('.msg').text('Enviado com Sucesso');
				} else {
					$('.msg').hide("slow");
				}
			},
			beforeSend: function () {

			},
			complete: function (msg) {

			}
		});

		});
	</script>
	
	<!-- COMMON SCRIPTS -->
    <script src="<?php echo URL; ?>assets-pesquisa/js/common_scripts.min.js"></script>
	<!-- Wizard script -->
	<script src="<?php echo URL; ?>assets-pesquisa/js/survey_func.js"></script>

</body>

</html>