		<!-- WRAPPER : begin -->
		<div id="wrapper">
			<!-- CORE : begin -->
			<div id="core" class="core-bg-1">
				<!-- PAGE HEADER : begin -->
				<div id="page-header">
					<div class="container">
						<h1>Serviços</h1>
						<ul class="breadcrumbs">
							<li><a href="home">Home</a></li>
							<li>Serviços</li>
						</ul>
					</div>
				</div>
				<!-- PAGE HEADER : begin -->
				<div class="container">
					<!-- PAGE CONTENT : begin -->
					<div id="page-content">
						<div class="various-content">
							
							<section>

								<div class="row">
									<?php foreach ($servicosLista as $linhaServico) { ?>
									<div class="col-sm-6">
                                        <p><img src="assets/img/servico/<?php echo $linhaServico->servicoFoto;?>" alt="<?php echo $linhaServico->servicoNome;?>"></p>
										<h2><?php echo $linhaServico->servicoNome;?></h2>
										<p><?php echo $linhaServico->servicoDescricao;?></p>
									</div>
									<?php }?>
								</div>

							</section>
								
						</div>
					</div>
					<!-- PAGE CONTENT : end -->
				</div>
			</div>
			<!-- CORE : end -->