		<!-- WRAPPER : begin -->
		<div id="wrapper">
			<!-- CORE : begin -->
			<div id="core" class="core-bg-1">
				<!-- PAGE HEADER : begin -->
				<div id="page-header">
					<div class="container">
						<h1>Sobre</h1>
						<ul class="breadcrumbs">
							<li><a href="home">Home</a></li>
							<li>Sobre</li>
						</ul>
					</div>
				</div>
				<!-- PAGE HEADER : begin -->
				<div class="container">
					<!-- PAGE CONTENT : begin -->
					<div id="page-content">
						<div class="various-content">
							<!-- INTRO SECTION : begin -->
							<section>

								<div class="row">
									<div class="col-sm-6">

										<p>Somos uma empresa nacional, prestadora de serviços estéticos, há mais de 12 anos no mercado,  promovendo soluções para restauração da beleza e saúde da pele.</p>
										<p><strong>The copy warned the Little Blind Text</strong>, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind.</p>

									</div>
									<div class="col-sm-6">

										<div class="c-gallery m-paginated" data-items="1" data-items-desktop="1" data-items-desktop-small="1" data-items-tablet="1" data-items-mobile="1">
											<div class="thumb-list">
												<div class="thumb">
													<a href="images/sobre/espaco01.jpg" class="lightbox" data-lightbox-group="espaco">
														<img src="images/sobre/espaco01.jpg" alt="">
													</a>
													<div>
														<p>Rejuvenescimento facial, pescoço e colo. Eliminação de Manchas.</p>
													</div>
												</div>
												<div class="thumb">
													<a href="images/sobre/espaco02.jpg" class="lightbox" data-lightbox-group="espaco">
														<img src="images/sobre/espaco02.jpg" alt="">
													</a>
													<div>
														<p>Eliminação de celulite, estrias e flacidez e gordura localizada</p>
													</div>
												</div>
												<div class="thumb">
													<a href="images/sobre/espaco03.jpg" class="lightbox" data-lightbox-group="espaco">
														<img src="images/sobre/espaco03.jpg" alt="">
													</a>
													<div>
														<p>Levantamento de Glúteo, eliminação de gordura localizada, definição dos músculos</p>
													</div>
												</div>
												<div class="thumb">
													<a href="images/sobre/espaco04.jpg" class="lightbox" data-lightbox-group="espaco">
														<img src="images/sobre/espaco04.jpg" alt="">
													</a>
													<div>
														<p>Rejuvenescimento facial, pescoço e colo. Eliminação de Manchas.</p>
													</div>
												</div>
												<div class="thumb">
													<a href="images/sobre/espaco05.jpg" class="lightbox" data-lightbox-group="espaco">
														<img src="images/sobre/espaco05.jpg" alt="">
													</a>
													<div>
														<p>Eliminação de celulite, estrias e flacidez e gordura localizada</p>
													</div>
												</div>										
												
											</div>
										</div>

									</div>
								</div>

							</section>
							<!-- INTRO SECTION : end -->
							
							<!-- GALLERY SECTION : begin -->
							<section>

								<!-- SECTION HEADER : begin -->
								<header>
									<div class="container">
										<h2>Equipamentos</h2>
										<p class="subtitle">Conheça alguns Equipamentos</p>
									</div>
								</header>
								<!-- SECTION HEADER : end -->

								<!-- GALLERY : begin -->
								<div class="c-gallery m-paginated" data-items="4" data-items-desktop="4" data-items-desktop-small="3" data-items-tablet="2" data-items-mobile="1">
									<div class="thumb-list">
										<?php foreach ($unidadeLista as $linhaEqui) { ?>
										<div class="thumb">
											<img src="assets/img/equipamento/<?php echo $linhaEqui->equipamentoFoto;?>" alt="<?php echo $linhaEqui->equipamentoFoto;?>">
											<div>
												<p><?php echo $linhaEqui->equipamentoNome;?></p>
											</div>
										</div>
										<?php }?>
									</div>
								</div>
								<!-- GALLERY : end -->
							</section>
							<!-- GALLERY SECTION : end -->
							<section>

								<!-- SECTION LOCAÇÃO : begin -->
								<header>
									<div class="container">
										<h2>Locação</h2>
										<p class="subtitle">Conheça alguns Benefícios</p>
									</div>
								</header>
								<!-- SECTION LOCAÇÃO : end -->
								<div class="row">
									<div class="col-sm-6">

										<p>Somos uma empresa nacional, prestadora de serviços estéticos, há mais de 12 anos no mercado,  promovendo soluções para restauração da beleza e saúde da pele.</p>
										<p><strong>The copy warned the Little Blind Text</strong>, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind.</p>

									</div>
									<div class="col-sm-6">
										<div class="c-gallery m-paginated" data-items="2" data-items-desktop="2" data-items-desktop-small="1" data-items-tablet="1" data-items-mobile="1">
											<div class="thumb-list">
												<div class="thumb">
													<a href="images/sobre/locacao1.jpg" class="lightbox" data-lightbox-group="locacao">
														<img src="images/sobre/locacao1.jpg" alt="">
													</a>
													<div>
														<p>Rejuvenescimento facial, pescoço e colo. Eliminação de Manchas.</p>
													</div>
												</div>
												<div class="thumb">
													<a href="images/sobre/locacao2.jpg" class="lightbox" data-lightbox-group="locacao">
														<img src="images/sobre/locacao2.jpg" alt="">
													</a>
													<div>
														<p>Eliminação de celulite, estrias e flacidez e gordura localizada</p>
													</div>
												</div>
												<div class="thumb">
													<a href="images/sobre/locacao3.jpg" class="lightbox" data-lightbox-group="locacao">
														<img src="images/sobre/locacao3.jpg" alt="">
													</a>
													<div>
														<p>Levantamento de Glúteo, eliminação de gordura localizada, definição dos músculos</p>
													</div>
												</div>
												<div class="thumb">
													<a href="images/sobre/locacao1.jpg" class="lightbox" data-lightbox-group="locacao">
														<img src="images/sobre/locacao1.jpg" alt="">
													</a>
													<div>
														<p>Rejuvenescimento facial, pescoço e colo. Eliminação de Manchas.</p>
													</div>
												</div>
												<div class="thumb">
													<a href="images/sobre/locacao2.jpg" class="lightbox" data-lightbox-group="locacao">
														<img src="images/sobre/locacao2.jpg" alt="">
													</a>
													<div>
														<p>Eliminação de celulite, estrias e flacidez e gordura localizada</p>
													</div>
												</div>
												<div class="thumb">
													<a href="images/sobre/locacao3.jpg" class="lightbox" data-lightbox-group="locacao">
														<img src="images/sobre/locacao3.jpg" alt="">
													</a>
													<div>
														<p>Levantamento de Glúteo, eliminação de gordura localizada, definição dos músculos</p>
													</div>
												</div>
												<div class="thumb">
													<a href="images/sobre/locacao1.jpg" class="lightbox" data-lightbox-group="locacao">
														<img src="images/sobre/locacao1.jpg" alt="">
													</a>
													<div>
														<p>Rejuvenescimento facial, pescoço e colo. Eliminação de Manchas.</p>
													</div>
												</div>
												<div class="thumb">
													<a href="images/sobre/locacao2.jpg" class="lightbox" data-lightbox-group="locacao">
														<img src="images/sobre/locacao2.jpg" alt="">
													</a>
													<div>
														<p>Eliminação de celulite, estrias e flacidez e gordura localizada</p>
													</div>
												</div>
												<div class="thumb">
													<a href="images/sobre/locacao3.jpg" class="lightbox" data-lightbox-group="locacao">
														<img src="images/sobre/locacao3.jpg" alt="">
													</a>
													<div>
														<p>Levantamento de Glúteo, eliminação de gordura localizada, definição dos músculos</p>
													</div>
												</div>
												
												
											</div>
										</div>

									</div>
								</div>
							</section>
							<!-- INSTAGRAM SECTION : begin -->
							<section>
								<!-- SECTION HEADER : begin -->
								<header>
									<div class="container">
										<h2>Instagram</h2>
										<p class="subtitle">#belizes_time/</p>
										<p class="more"><a href="https://www.instagram.com/belizes_time/" target="_blank" class="c-button m-type-2">siga no insta</a></p>
									</div>
								</header>
								<!-- SECTION HEADER : end -->

								<div class="container">
									<div class="row" id="instagram">									
									</div>
								</div>

							</section>
							<!-- INSTAGRAM SECTION : end -->
						</div>
					</div>
					<!-- PAGE CONTENT : end -->
				</div>			
			</div>
			<!-- CORE : end -->			