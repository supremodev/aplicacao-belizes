<!-- FOOTER : begin -->
			<footer id="footer">
				<div class="container">
					<!-- FOOTER BOTTOM : begin -->
					<div class="footer-bottom">
						<div class="row">
							<div class="col-md-6 col-md-push-6">

								<!-- FOOTER MENU : begin -->
								<nav class="footer-menu">
									<ul>
										<li><a href="home">Home</a></li>
										<li><a href="sobre">Sobre</a></li>
										<li><a href="servico">Serviço</a></li>
										<li><a href="contato">Contato</a></li>
										<li><a href="https://supremodigital.com.br" target="_blank">Supremo Digital</a></li>
									</ul>
								</nav>
								<!-- FOOTER MENU : end -->

							</div>
							<div class="col-md-6 col-md-pull-6">

								<!-- FOOTER TEXT : begin -->
								<div class="footer-text">
									<p><a href="https://supremodigital.com.br" target="_blank">Supremo Digital</a></p>
								</div>
								<!-- FOOTER TEXT : end -->

							</div>
						</div>
					</div>
					<!-- FOOTER BOTTOM : end -->

				</div>
			</footer>
			<!-- FOOTER : end -->

		</div>
		<!-- WRAPPER : end -->

		<!-- SCRIPTS : begin -->
		<script src="library/js/jquery-1.9.1.min.js" type="text/javascript"></script>
		<script src="library/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
		<script src="library/js/jquery.ba-outside-events.min.js" type="text/javascript"></script>
		<script src="library/js/owl.carousel.min.js" type="text/javascript"></script><!-- Carousel -->
		<script src="library/js/jquery.magnific-popup.min.js" type="text/javascript"></script><!-- Lightbox -->
		<script src="library/twitter/jquery.tweet.min.js" type="text/javascript"></script><!-- Twitter Feed -->
		<script src="library/js/library.js"></script>
		<script src="library/js/scripts.js"></script>
		<script src="library/js/supremo.js"></script>
		<!-- SCRIPTS : end -->

	</body>
</html>
