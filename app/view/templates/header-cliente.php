<body id="topo">
    <div id="preload" style="display:none">
		<div id="layoutPreload"></div>
	</div>
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left">
                <a href="#" class="logo">
                    <img src="<?php echo URL; ?>assets/img/logo.png" width="100" alt="Belize's Time">
                </a>
            </div>
            <a id="mobile_btn" class="mobile_btn float-left" href="#sidebar"><i class="fa fa-bars"></i></a>
            <ul class="nav user-menu float-right">
                <li class="nav-item dropdown has-arrow">
                    <a href="#" class="dropdown-toggle nav-link user-link" data-toggle="dropdown">
                        <span class="user-img"><img class="rounded-circle" src="<?php 
                                                    if (!empty($_SESSION['cliFoto'])){

                                                        $img = explode('.',$_SESSION['cliFoto']);
                                                        echo URL."/assets/img/cliente/".$img[0] . "_mini." . $img[1];
                                                        
                                                    }elseif(!empty($_SESSION['cliWebcam'])){
                                                        echo $_SESSION['cliWebcam'];
                                                    }else{
                                                        echo URL."/assets/img/cliente/user.jpg";
                                                    }
                                                    ?>" width="40" alt="Admin">
							<span class="status online"></span></span>
                        <span><?php echo $_SESSION['cliNome'];?></span>
                    </a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="<?php echo URL; ?>cliente/editar/<?php echo $_SESSION['idCliente']; ?>">Editar Perfil</a>
						<a class="dropdown-item" href="<?php echo URL; ?>login/sairCliente">Sair</a>
					</div>
                </li>
            </ul>
            <div class="dropdown mobile-user-menu float-right">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="<?php echo URL; ?>cliente/editar/<?php echo $_SESSION['idCliente']; ?>">Editar Perfil</a>
                    <a class="dropdown-item" href="#">Sair</a>
                </div>
            </div>
        </div>
        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                        <li class="menu-title">Menu</li>
						<li>
							<a href="<?php echo URL; ?>cliente/perfil/<?php echo $_SESSION['idCliente']; ?>"><i class="fa fa-id-card-o"></i>Início</a>
						</li>
						<li>
                            <a href="<?php echo URL; ?>login/sairCliente"><i class="fa fa-cube"></i>Sair</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
