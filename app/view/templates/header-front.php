<body>
		<!-- HEADER : begin -->
		<header id="header" class="m-animated">
			<div class="header-bg">
				<div class="header-inner">
					<!-- HEADER BRANDING : begin -->
					<div class="header-branding">
						<a href="home"><img src="<?php echo URL; ?>assets/img/logo.png" alt="Supremo Digital" data-hires="<?php echo URL; ?>assets/img/logo.png" width="274" alt="Belize's Time"></a>
					</div>
					<!-- HEADER BRANDING : end -->

					<!-- HEADER NAVIGATION : begin -->
					<div class="header-navigation">

						<!-- HEADER MENU : begin -->
						<nav class="header-menu">
							<button class="header-menu-toggle" type="button"><i class="fa fa-bars"></i>MENU</button>
							<ul>
								<li>
									<span><a href="home">HOME</a></span>
								</li>
								<li>
									<span><a href="sobre">SOBRE</a></span>
								</li>
								<li>
									<span><a href="servico">SERVIÇO</a></span>
								</li>
								<li>
									<span><a href="contato">CONTATO</a></span>
								</li>
								<li>
									<span><a href="login/logincliente">LOGIN</a></span>
								</li>
							</ul>
						</nav>
						<!-- HEADER MENU : end -->
					</div>
					<!-- HEADER NAVIGATION : end -->

					<!-- HEADER PANEL : begin -->
					<div class="header-panel">

						<button class="header-panel-toggle" type="button"><i class="fa"></i></button>

						<!-- HEADER RESERVATION : begin -->
						<div class="header-reservation">
							<a href="agendamento" class="c-button m-open-ajax-modal">Realizar um Agendamento</a>
						</div>
						<!-- HEADER RESERVATION : end -->

						<!-- HEADER CONTACT : begin -->
						<div class="header-contact">
							<ul>

								<!-- PHONE : begin -->
								<li>
									<div class="item-inner">
										<i class="ico fa fa-phone"></i>
										<strong>11 958 452 521</strong>
									</div>
								</li>
								<!-- PHONE : end -->

								<!-- EMAIL : begin -->
								<li>
									<div class="item-inner">
										<i class="ico fa fa-envelope-o"></i>
										<a href="#">contato@belizestime.com.br</a>
									</div>
								</li>
								<!-- EMAIL : end -->

								<!-- ADDRESS : begin -->
								<li>
									<div class="item-inner">
										<i class="ico fa fa-map-marker"></i>
										<strong>Belizes Time</strong><br>
										Parque Boturussú, 2613<br>
										Zona Leste de São Paulo
									</div>
								</li>
								<!-- ADDRESS : end -->

								<!-- HOURS : begin -->
								<li>
									<div class="item-inner">
										<i class="ico fa fa-clock-o"></i>
										<dl>
											<dt>Seg à Sex:</dt>
											<dd>10:00 - 16:00</dd>
											<dt>Sáb.:</dt>
											<dd>10:00 - 14:00</dd>
											<dt>Dom.:</dt>
											<dd>Fechado</dd>
										</dl>
									</div>
								</li>
								<!-- HOURS : end -->

							</ul>
						</div>
						<!-- HEADER CONTACT : end -->

						<!-- HEADER SOCIAL : begin -->
						<div class="header-social">
							<ul>
								<li><a href="#" title="Instagram"><i class="fa fa-instagram"></i></a></li>
								<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
							</ul>
						</div>
						<!-- HEADER SOCIAL : end -->

					</div>
					<!-- HEADER PANEL : end -->

				</div>
			</div>
		</header>
		<!-- HEADER : end -->