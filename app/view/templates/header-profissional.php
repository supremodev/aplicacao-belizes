<body>
    <div id="preload" style="display:none">
		<div id="layoutPreload"></div>
    </div>
    
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left">
                <a href="#" class="logo">
                    <img src="<?php echo URL; ?>assets/img/logo.png" width="100" alt="Belize's Time">
                </a>
            </div>
            <a id="mobile_btn" class="mobile_btn float-left" href="#sidebar"><i class="fa fa-bars"></i></a>
            <ul class="nav user-menu float-right">
                <li class="nav-item dropdown has-arrow">
                    <a href="#" class="dropdown-toggle nav-link user-link" data-toggle="dropdown">
                        <span class="user-img"><img class="rounded-circle" src="<?php 
                                                    if (!empty($_SESSION['funcFoto'])){

                                                        $img = explode('.',$_SESSION['funcFoto']);
                                                        echo URL."/assets/img/func/".$img[0] . "_mini." . $img[1];
                                                        
                                                    }elseif(!empty($_SESSION['funcWebcam'])){
                                                        echo $_SESSION['funcWebcam'];
                                                    }else{
                                                        echo URL."/assets/img/func/user.jpg";
                                                    }
                                                    ?>" width="40" alt="Admin">
							<span class="status online"></span></span>
                        <span><?php echo $_SESSION['userNome'];?></span>
                    </a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="<?php echo URL; ?>funcionario/editar/<?php echo $_SESSION['idUsuario']; ?>">Editar Perfil</a>
						<a class="dropdown-item" href="<?php echo URL; ?>login/sairUsuario">Sair</a>
					</div>
                </li>
            </ul>
            <div class="dropdown mobile-user-menu float-right">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#">Meu Perfil</a>
                    <a class="dropdown-item" href="<?php echo URL; ?>funcionario/editar/<?php echo $_SESSION['idUsuario']; ?>">Editar Perfil</a>
                    <a class="dropdown-item" href="#">Sair</a>
                </div>
            </div>
        </div>
        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                        <li class="menu-title">Menu</li>
						<li>
							<a href="<?php echo URL; ?>funcionario/perfil/<?php echo $_SESSION['idUsuario']; ?>"><i class="fa fa-id-card-o"></i>Início</a>
						</li>
						<li class="submenu">
							<a href="#"><i class="fa fa-id-card-o"></i> <span> Fichas </span> <span class="menu-arrow"></span></a>
							<ul style="display: none;">
								<li><a href="<?php echo URL; ?>ficha/fichaGeral">Avaliação Geral</a></li>
								<li><a href="<?php echo URL; ?>ficha/fichaAnamneseCriolipolise">Anamnese Criolipólise</a></li>								
							</ul>
                        </li>
                        <li>
                            <a href="<?php echo URL; ?>cliente/listadeclienteprofissional"><i class="fa fa-child"></i>Pacientes / Clientes</a>
                        </li>
						<li>
                            <a href="<?php echo URL; ?>login/sairUsuario"><i class="fa fa-cube"></i>Sair</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>