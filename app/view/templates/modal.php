<!-- Restaurar modal -->
<div class="modal fade" id="restaurarModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-estilo">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Restaurar página</h4>
                <button type="button" class="btn btn-inverse-danger btn-icon" data-dismiss="modal" aria-label="Close">
                    <i class="mdi mdi-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <h5 id="data">Modal Body</h5>
            </div>
            <div class="modal-footer">
                <a href="" id="linkobjeto" class="btn btn-outline-primary">Confirmar</a>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<!-- Apagar modal -->
<div class="modal fade" id="deletarModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-estilo">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Deletar</h4>
                <button type="button" class="btn btn-inverse-danger btn-icon" data-dismiss="modal" aria-label="Close">
                    <i class="mdi mdi-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <h5>Deseja realmente deletar o <span id="descricaodeletar"></span>?</h5>
            </div>
            <div class="modal-footer">
                <button id="linkdeletar" destino="" idobjeto="" type="button" class="link-ajaxxx btn btn-outline-primary" data-dismiss="modal">Confirmar</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>