		<div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">Unidade Clínica desativadas</h4>
                    </div>
                </div>
				<div class="row">
					<?php foreach ($unidadeLista as $unidade) { ?>
					<div id="linha<?php echo $unidade->idUnidade?>" class="col-md-6 col-sm-6 col-lg-6 col-xl-4">					
						<div class="profile-widget profile-widget3">
							<div class="profile-bg blur unidade">
								<img alt="" src="<?php echo URL; ?>assets/img/unidade/<?php
                                                               if (!$unidade->unidadeFoto == ""){
                                                                   echo htmlspecialchars($unidade->unidadeFoto, ENT_QUOTES, 'UTF-8'); 
                                                               }else{
                                                                   echo "local";
                                                               } ?>.jpg" alt="<?php echo htmlspecialchars($unidade->unidadeNomeEmpresa, ENT_QUOTES, 'UTF-8'); ?>">
							</div>
							<div class="dropdown profile-action">
								<a class="desativar-ajax dropdown-item"  href="#" destino="unidade/ativarDesativar/1" idobjeto="<?php echo $unidade->idUnidade; ?>">
								<i class="fa fa-check m-r-5"></i>Ativar</a>
							</div>
							<div>
								<a href="<?php echo URL; ?>unidade/detalhe/<?php echo $unidade->idUnidade; ?>" class="avatar-link unidadeAvatar">
									<img src="<?php echo URL; ?>assets/img/unidade/logo.jpg" alt="<?php echo htmlspecialchars($unidade->unidadeNomeEmpresa, ENT_QUOTES, 'UTF-8'); ?>">
								</a>
								<div class="user-info">
									<div class="username">
										<?php if (isset($unidade->unidadeNomeEmpresa)) echo htmlspecialchars($unidade->unidadeNomeEmpresa, ENT_QUOTES, 'UTF-8'); ?>
									</div>
									<span>
										<?php if (isset($unidade->unidadeEnd)) echo htmlspecialchars($unidade->unidadeEnd, ENT_QUOTES, 'UTF-8'); ?>
									</span>
								</div><!--
								<div class="user-analytics">
									<div class="row">
										<div class="col-sm-4 col-4 border-right">
											<div class="analytics-desc">
												<h5 class="analytics-count">
													<?php echo $unidadeCliente; ?>
												</h5>
												<span class="analytics-title">CLIENTES</span>
											</div>
										</div>
										<div class="col-sm-4 col-4 border-right">
											<div class="analytics-desc">
												<h5 class="analytics-count">
													<?php echo $unidadeAgenda; ?>
												</h5>
												<span class="analytics-title">AGENDAMENTOS</span>
											</div>
										</div>
										<div class="col-sm-4 col-4">
											<div class="analytics-desc">
												<h5 class="analytics-count">
													<?php echo $unidadeFunc; ?>
												</h5>
												<span class="analytics-title">FUNCIONÁRIOS</span>
											</div>
										</div>
									</div>
								</div> -->
							</div>
						</div>
					</div>
					<?php } ?>
                </div>
            </div>
        </div>		
    </div>
    <div class="sidebar-overlay" data-reff=""></div>