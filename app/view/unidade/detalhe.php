        <div class="page-wrapper">
            <div class="content">
                <div class="row">
                    <div class="col-sm-7 col-4">
                        <h4 class="page-title">Detalhe da Unidade: <?php echo htmlspecialchars($detalheLista->unidadeNomeEmpresa, ENT_QUOTES, 'UTF-8'); ?></h4>
                    </div>
                </div>
                <div class="card-box profile-header">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-view">
                                <div class="profile-img-wrap">
                                    <div class="profile-img">
                                        <img class="avatar" src="<?php echo URL; ?>assets/img/unidade/logo.jpg" alt="<?php echo htmlspecialchars($detalheLista->unidadeNomeEmpresa, ENT_QUOTES, 'UTF-8'); ?>">
                                    </div>
                                </div>
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="profile-info-left">
                                                <ul class="personal-info">
													<li>
														<span class="title">Contato:</span>
														<span class="text"><?php echo htmlspecialchars($detalheLista->unidadePessoaContato, ENT_QUOTES, 'UTF-8'); ?></span>
													</li>
													<li>
														<span class="title">Razão Social:</span>
														<span class="text"><?php echo htmlspecialchars($detalheLista->unidadeRazaoSocial, ENT_QUOTES, 'UTF-8'); ?></span>
													</li>
													<li>
														<span class="title">CNPJ:</span>
														<span class="text"><?php echo htmlspecialchars($detalheLista->unidadeCNPJ, ENT_QUOTES, 'UTF-8'); ?></span>
													</li>
													<li>
														<span class="title">Telefone:</span>
														<span class="text"><?php echo htmlspecialchars($detalheLista->unidadeFoneUm, ENT_QUOTES, 'UTF-8'); ?></span>
													</li>
													<li>
														<span class="title">Celular:</span>
														<span class="text"><?php echo htmlspecialchars($detalheLista->unidadeFoneDois, ENT_QUOTES, 'UTF-8'); ?></span>
													</li>
													<li>
														<span class="title">Celular:</span>
														<span class="text"><?php echo htmlspecialchars($detalheLista->unidadeFoneTres, ENT_QUOTES, 'UTF-8'); ?></span>
													</li>
												</ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <ul class="personal-info">
												<li>
													<span class="title">Endereço:</span>
													<span class="text"><?php echo htmlspecialchars($detalheLista->unidadeEnd, ENT_QUOTES, 'UTF-8'); ?>: <?php echo htmlspecialchars($detalheLista->unidadeEndRua, ENT_QUOTES, 'UTF-8'); ?> - <?php echo htmlspecialchars($detalheLista->unidadeEndComp, ENT_QUOTES, 'UTF-8'); ?></span>
												</li>
												<li>
													<span class="title">Bairro:</span>
													<span class="text"><?php echo htmlspecialchars($detalheLista->unidadeEndBairro, ENT_QUOTES, 'UTF-8'); ?></span>
												</li>
												<li>
													<span class="title">Cidade:</span>
													<span class="text"><?php echo htmlspecialchars($detalheLista->unidadeEndCidade, ENT_QUOTES, 'UTF-8'); ?></span>
												</li>
												<li>
													<span class="title">CEP:</span>
													<span class="text"><?php echo htmlspecialchars($detalheLista->unidadeCEP, ENT_QUOTES, 'UTF-8'); ?></span>
												</li>
												<li>
													<span class="title">Email:</span>
													<span class="text"><?php echo htmlspecialchars($detalheLista->unidadeEmail, ENT_QUOTES, 'UTF-8'); ?></span>
												</li>
												<li>
													<span class="title">Email:</span>
													<span class="text"><?php echo htmlspecialchars($detalheLista->unidadeSite, ENT_QUOTES, 'UTF-8'); ?></span>
												</li>
											</ul>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                    </div>
                </div>
				
				<div class="profile-tabs">
					<ul class="nav nav-tabs nav-tabs-bottom">
						<li class="nav-item"><a class="nav-link active" href="#sala" data-toggle="tab">Salas</a></li> 
						<li class="nav-item"><a class="nav-link" href="#equipamentos" data-toggle="tab">Equipamentos</a></li><!--
						<li class="nav-item"><a class="nav-link" href="#bottom-tab3" data-toggle="tab">Opção 02</a></li> -->
					</ul>

					<div class="tab-content">
						<div class="tab-pane show active" id="sala">
							<div class="content">
								
								<div class="row detalheServico">
									<?php foreach ($detalheSala as $sala) { ?>
									<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 m-b-20">										
										<img class="img-thumbnail" src="<?php echo URL; ?>assets/img/sala/<?php 
                                                    if(!$sala->salaFotoUnidade == ""){
                                                        echo $sala->salaFotoUnidade;
                                                    }else{
                                                        echo "sem-foto";        
                                                    } ?>.jpg" alt="<?php echo htmlspecialchars($sala->salaNomeUnidade, ENT_QUOTES, 'UTF-8'); ?>">										
										<h4><?php echo htmlspecialchars($sala->salaNomeUnidade, ENT_QUOTES, 'UTF-8'); ?></h4>
										<h5><?php echo htmlspecialchars($sala->salaDescricaoUnidade, ENT_QUOTES, 'UTF-8'); ?></h5>
									</div>
									<?php } ?>
								</div>
								
							</div>
						</div>
						<div class="tab-pane" id="equipamentos">
							<div class="content">
								<div class="row detalheServico">
									<?php foreach ($listarEquipamento as $equipamento) { ?>
									<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 m-b-20">										
										<img class="img-thumbnail" src="<?php echo URL; ?>assets/img/<?php echo htmlspecialchars($equipamento->equipamentoFoto, ENT_QUOTES, 'UTF-8'); ?>" alt="<?php echo htmlspecialchars($equipamento->equipamentoNome, ENT_QUOTES, 'UTF-8'); ?>">										
										<h4><?php echo htmlspecialchars($equipamento->equipamentoNome, ENT_QUOTES, 'UTF-8'); ?></h4>
										<h5><?php echo htmlspecialchars($equipamento->equipamentoDescricao, ENT_QUOTES, 'UTF-8'); ?></h5>
									</div>
									<?php } ?>
								</div>
								
							</div>
						</div>
						<!--
						<div class="tab-pane" id="bottom-tab3">
							Tab content 3
						</div> -->
					</div>
				</div>
			</div> 
        </div>
    </div>
    <div class="sidebar-overlay" data-reff=""></div>