		<div class="page-wrapper">
            <div class="content">
            <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:<?php if(isset($_SESSION['alerta']) && $_SESSION['alerta']=="erro"){echo 'block';$_SESSION['alerta']="alerta";}else{echo 'none';}; ?>">
					Erro ao Atualizar a Unidade. 
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="alert alert-success alert-dismissible fade show" role="alert" style="display:<?php if(isset($_SESSION['alerta']) && $_SESSION['alerta']=="sucesso"){echo 'block';$_SESSION['alerta']="alerta";}else{echo 'none';}; ?>">
                Unidade Atualizado com Sucesso.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
						<?php foreach ($unidadeLista as $linha) { ?>
                        <form id="formularios">
                            <h3 class="page-title">Editar as Configurações da Clínica</h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">                                        
                                        <div class="profile-unidade">
                                            <div class="profile" style="background-image: url('<?php							   
                                                    if ($linha->unidadeFoto == "") {
                                                        echo URL."assets/img/unidade/local.jpg";
                                                    }else{
                                                        echo URL."assets/img/unidade/".$linha->unidadeFoto;
                                                    } 
                                                    ?>');">
                                                <label class="edit">
                                                    <span><i class="mdi mdi-upload"></i></span>
                                                    <input type="file" size="32" name="unidadeFoto" id="inputImagem">
                                                </label>
                                            </div>
                                            <button id="editarImagem" type="button" class="btn btn-primary btn-sm">
                                                <i class="fa fa-file-image-o"></i>
                                            </button>
                                            
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nome da Empresa <span class="text-danger">*</span></label>
                                        <input class="form-control" name="unidadeNomeEmpresa" type="text" value="<?php echo $linha->unidadeNomeEmpresa;?>" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nome Contato</label>
                                        <input class="form-control" name="unidadePessoaContato" value="<?php echo $linha->unidadePessoaContato	;?>" type="text">
                                    </div>
                                </div>
								<div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Razao Social</label>
                                        <input class="form-control" name="unidadeRazaoSocial" value="<?php echo $linha->unidadeRazaoSocial;?>" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>CNPJ <span class="text-danger">*</span></label>
                                        <input class="form-control" name="unidadeCNPJ" value="<?php echo $linha->unidadeCNPJ;?>" type="text" required>
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Telefone</label>
                                        <input class="form-control" name="unidadeFoneUm" value="<?php echo $linha->unidadeFoneUm;?>" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Celular</label>
                                        <input class="form-control" name="unidadeFoneDois" value="<?php echo $linha->unidadeFoneDois;?>" type="text">
                                    </div>
                                </div>
								<div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Celular</label>
                                        <input class="form-control" name="unidadeFoneTres" value="<?php echo $linha->unidadeFoneTres;?>" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Endereço</label>
                                        <input class="form-control" name="unidadeEnd" value="<?php echo $linha->unidadeEnd;?>" type="text">
                                    </div>
                                </div>
								<div class="col-sm-1">
                                    <div class="form-group">
                                        <label>Número</label>
                                        <input class="form-control" name="unidadeEndRua" value="<?php echo $linha->unidadeEndRua;?>" type="text">
                                    </div>
                                </div>
								<div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Complemento</label>
                                        <input class="form-control" name="unidadeEndComp" value="<?php echo $linha->unidadeEndComp;?>" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Bairro</label>
                                        <input class="form-control" name="unidadeEndBairro" value="<?php echo $linha->unidadeEndBairro;?>" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Cidade</label>
                                        <input class="form-control" name="unidadeEndCidade" value="<?php echo $linha->unidadeEndCidade;?>" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>CEP</label>
                                        <input class="form-control" name="unidadeCEP" value="<?php echo $linha->unidadeCEP;?>" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" name="unidadeEmail" value="<?php echo $linha->unidadeEmail;?>" type="email">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Site</label>
                                        <input class="form-control" name="unidadeSite" value="<?php echo $linha->unidadeSite;?>" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">                                
								<div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Facebook</label>
                                        <input class="form-control" name="unidadeFace" value="<?php echo $linha->unidadeFace;?>" type="text">
                                    </div>
                                </div>
								<div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Instagram</label>
                                        <input class="form-control" name="unidadeInsta" value="<?php echo $linha->unidadeInsta;?>" type="text">
                                    </div>
                                </div>
								<div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Twitter</label>
                                        <input class="form-control" name="unidadeTwitter" value="<?php echo $linha->unidadeTwitter;?>" type="text">
                                    </div>
                                </div>
								<div class="col-sm-3">
                                    <div class="form-group">
                                        <label>YouTUBE</label>
                                        <input class="form-control" name="unidadeYouTUBE" value="<?php echo $linha->unidadeYouTUBE;?>" type="text">
                                    </div>
                                </div>
                            </div>  
                        </form>
                        <div class="row">
                            <div class="col-sm-12 text-center m-t-20">
                                <button destino="unidade/atualizar/<?php echo $linha->idUnidade;?>" class="atualizar-ajax btn btn-primary submit-btn" disabled>Editar Registro</button>
                            </div>
                        </div>
                    	<?php } ?>
					</div>                    
                </div>
            </div>

        </div>
    </div>
    <div class="sidebar-overlay" data-reff=""></div>   
