<div class="page-wrapper">
            <div class="content">
            <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:<?php if(isset($_SESSION['alerta']) && $_SESSION['alerta']=="erro"){echo 'block';$_SESSION['alerta']="alerta";}else{echo 'none';}; ?>">
                Erro ao Atualizar equipamento. 
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="alert alert-success alert-dismissible fade show" role="alert" style="display:<?php if(isset($_SESSION['alerta']) && $_SESSION['alerta']=="sucesso"){echo 'block';$_SESSION['alerta']="alerta";}else{echo 'none';}; ?>">
                Equipamento Atualizado com Sucesso.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Cadastro Equipamento</h4>
                    </div>
                </div>
				<?php foreach ($equipamentoLista as $linha) { ?>
                <form id="formularios">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-md-12">
                                
                                <div class="profile-img-wrap sala">
                                    <div class="profile" style="background-image: url('<?php							   
                                            if ($linha->equipamentoFoto == "") {
                                                echo URL."assets/img/equipamento/sem-foto.jpg";
                                                $imgFoto = "";
                                            }else{
                                                echo URL."assets/img/equipamento/$linha->equipamentoFoto";
                                                $imgFoto = URL."assets/img/equipamento/".$linha->equipamentoFoto;
                                            } 
                                            ?>');">
                                        <label class="edit">
                                            <span><i class="mdi mdi-upload"></i></span>
                                            <input type="file" size="32" name="equipamentoFoto" id="inputImagem" >
                                        </label>
                                    </div>
                                    <button id="editarImagem" type="button" class="btn btn-primary btn-sm">
                                        <i class="fa fa-file-image-o"></i>
                                    </button>
                                </div>
                                
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-focus">
                                                <select class="select" name="idUnidade">
                                                    <option>Selecionar Unidade</option>
                                                    <?php foreach ($unidadeLista as $listaa) { ?>
                                                        <option value="<?php echo $listaa->idUnidade; ?>" <?php if($linha->idUnidade == $listaa->idUnidade) {echo "selected";};?>><?php echo $listaa->unidadeNomeEmpresa; ?></option>
                                                    <?php } ?>
                                                    
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Nome Equipamento:<span class="text-danger">*</span></label>
                                                <input type="text" name="equipamentoNome" class="form-control floating" value="<?php echo $linha->equipamentoNome;?>" required>
                                            </div>
                                        </div>
										<div class="col-md-12">
											<div class="form-group">
												<label class="focus-label">Descrição</label>
												<textarea name="equipamentoDescricao" class="form-control floating" rows="3" cols="30"><?php echo $linha->equipamentoDescricao;?></textarea>
											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>							
                        </div>
                    </div>
                    
                </form>
            	<?php } ?>
                <div class="text-center m-t-20">
                    <button destino="unidade/atualizarEquipamento/<?php echo $linha->idEquipamento;?>" class="atualizar-ajax btn btn-primary submit-btn" disabled>Editar Registro</button>
                </div>
			</div>
            
        </div>
    </div>
    <div class="sidebar-overlay" data-reff=""></div>