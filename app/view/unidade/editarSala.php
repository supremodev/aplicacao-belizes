<div class="page-wrapper">
            <div class="content">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:<?php if(isset($_SESSION['alerta']) && $_SESSION['alerta']=="erro"){echo 'block';$_SESSION['alerta']="alerta";}else{echo 'none';}; ?>">
                    Erro ao Atualizar sala. 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display:<?php if(isset($_SESSION['alerta']) && $_SESSION['alerta']=="sucesso"){echo 'block';$_SESSION['alerta']="alerta";}else{echo 'none';}; ?>">
                    Sala Atualizada com Sucesso.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Editar Sala</h4>
                    </div>
                </div>
				<?php foreach ($salaUnidadeLista as $linha) { ?>
                <form id="formularios">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-img-wrap sala">
                                    <div class="profile" style="background-image: url('<?php							   
                                            if ($linha->salaFotoUnidade == "") {
                                                echo URL."assets/img/sala/sem-foto.jpg";
                                            }else{
                                                echo URL."assets/img/sala/$linha->salaFotoUnidade";
                                            } 
                                            ?>');">
                                        <label class="edit">
                                            <span><i class="mdi mdi-upload"></i></span>
                                            <input type="file" size="32" name="salaFotoUnidade" id="inputImagem">
                                        </label>
                                    </div>
                                    <button id="editarImagem" type="button" class="btn btn-primary btn-sm">
                                        <i class="fa fa-file-image-o"></i>
                                    </button>
                                </div>
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-focus">
                                                <select class="select" name="idUnidade">
                                                    <option>Selecionar</option>
                                                    <?php foreach ($unidadeLista as $listaa) { ?>
                                                        <option value="<?php echo $listaa->idUnidade; ?>" <?php if($linha->idUnidade == $listaa->idUnidade) {echo "selected";};?>><?php echo $listaa->unidadeNomeEmpresa; ?></option>
                                                    <?php } ?>
                                                    
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Nome Serviço:<span class="text-danger">*</span></label>
                                                <input name="salaNomeUnidade" type="text" class="form-control floating" value="<?php echo $linha->salaNomeUnidade;?>" required>
                                            </div>
                                        </div>
										<div class="col-md-12">
											<div class="form-group">
												<label class="focus-label">Descrição</label>
												<textarea name="salaDescricaoUnidade" class="form-control floating" rows="3" cols="30"><?php echo $linha->salaDescricaoUnidade;?></textarea>
											</div>
                                        </div>										
                                    </div>
                                </div>
                            </div>							
                        </div>
                    </div>
                </form>
                <div class="text-center m-t-20">
                    <button destino="unidade/atualizarSala/<?php echo $linha->idSalaUnidade;?>" class="atualizar-ajax btn btn-primary submit-btn" disabled>Editar Registro</button>
                </div>
                
            	<?php } ?>
			</div>

        </div>
    </div>
    <div class="sidebar-overlay" data-reff=""></div>