		<div class="page-wrapper">
            <div class="content">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
                    <span class="texto-ajax"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
					<span class="texto-ajax"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">Equipamento</h4>
                    </div>
                    <div class="col-sm-8 col-9 text-right m-b-20">
                        <a href="<?php echo URL; ?>unidade/novoEquipamento" class="btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i>Adicionar Equipamento</a>
                        <a href="<?php echo URL; ?>unidade/equipamentodesativados" class="btn btn btn-primary btn-rounded float-right">Desativados</a>
                    </div>
                </div>
				<div class="row doctor-grid">
					<?php foreach ($equipamentoLista as $equipamento) { ?>
                    <div id="linha<?php echo $equipamento->idEquipamento?>" class="col-md-4 col-sm-4 col-xs-6 col-lg-3">
                        <div class="profile-widget sala">
                            <div class="servico-img salaImg">
                                <img alt="<?php if (isset($equipamento->equipamentoNome)) echo htmlspecialchars($equipamento->equipamentoNome, ENT_QUOTES, 'UTF-8'); ?>" src="<?php echo URL; ?>assets/img/equipamento/<?php echo $equipamento->equipamentoFoto; ?>">							
                            </div>
                            <div class="dropdown profile-action">
                                <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="editarEquipamento/<?php echo $equipamento->idEquipamento; ?>"><i class="fa fa-pencil m-r-5"></i>Editar</a>
                                    <a class="desativar-ajax dropdown-item" href="#" destino="unidade/ativarDesativarEquipamento/0" idobjeto="<?php echo $equipamento->idEquipamento; ?>" ><i class="fa fa-trash-o m-r-5"></i>Desativar</a>
                                </div>
                            </div>
                            <h4 class="doctor-name text-ellipsis">
								<?php if (isset($equipamento->equipamentoNome)) echo htmlspecialchars($equipamento->equipamentoNome, ENT_QUOTES, 'UTF-8'); ?>
							</h4>
                            <div class="doc-prof">
								<p>
									<?php echo substr($equipamento->equipamentoDescricao, 0, 200); ?>
								</p>
							</div>
                        </div>
                    </div>
					<?php } ?>
					
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar-overlay" data-reff=""></div>
</body>
</html>