		<div class="page-wrapper">
            <div class="content">
				<div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
                    <span class="texto-ajax"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
					<span class="texto-ajax"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">Unidade Clínica</h4>
                    </div>
                    <div class="col-sm-8 col-9 text-right m-b-20">
						<a href="<?php echo URL; ?>unidade/novo" class="btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i>Adicionar Unidade</a>
						<a href="<?php echo URL; ?>unidade/desativados" class="btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i>Unidade desativadas</a>
                    </div>
                </div>
				<div class="row">
					<?php foreach ($unidadeLista as $unidade) { ?>
					<div id="linha<?php echo $unidade->idUnidade?>" class="col-md-6 col-sm-6 col-lg-6 col-xl-4">					
						<div class="profile-widget profile-widget3">
							<div class="profile-bg blur unidade">
								<img alt="" src="<?php echo URL; ?>assets/img/unidade/<?php
                                                               if ($unidade->unidadeFoto == ""){
																	echo "local.jpg"; 
                                                               }else{
																	echo $unidade->unidadeFoto;
                                                               } ?>" alt="<?php echo $unidade->unidadeNomeEmpresa; ?>">
							</div>
							<div class="dropdown profile-action">
								<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="<?php echo URL; ?>unidade/editar/<?php echo $unidade->idUnidade; ?>">
										<i class="fa fa-pencil m-r-5"></i>Editar
									</a>
									<a class="dropdown-item" href="<?php echo URL; ?>unidade/detalhe/<?php echo $unidade->idUnidade; ?>"><i class="fa fa-id-card-o m-r-5"></i>Detalhes</a>
									<a class="desativar-ajax dropdown-item"  href="#" destino="unidade/ativarDesativar/0" idobjeto="<?php echo $unidade->idUnidade; ?>" data-toggle="modal" data-target="#delete_patient">
									<i class="fa fa-trash-o m-r-5"></i>Desativar</a>
								</div>
							</div>
							<div>
								<a href="<?php echo URL; ?>unidade/detalhe/<?php echo $unidade->idUnidade; ?>" class="avatar-link unidadeAvatar">
									<img src="<?php echo URL; ?>assets/img/unidade/logo.jpg" alt="<?php echo htmlspecialchars($unidade->unidadeNomeEmpresa, ENT_QUOTES, 'UTF-8'); ?>">
								</a>
								<div class="user-info">
									<div class="username">
										<?php if (isset($unidade->unidadeNomeEmpresa)) echo htmlspecialchars($unidade->unidadeNomeEmpresa, ENT_QUOTES, 'UTF-8'); ?>
									</div>
									<span>
										<?php if (isset($unidade->unidadeEnd)) echo htmlspecialchars($unidade->unidadeEnd, ENT_QUOTES, 'UTF-8'); ?>
									</span>
								</div><!--
								<div class="user-analytics">
									<div class="row">
										<div class="col-sm-4 col-4 border-right">
											<div class="analytics-desc">
												<h5 class="analytics-count">
													<?php echo $unidadeCliente; ?>
												</h5>
												<span class="analytics-title">CLIENTES</span>
											</div>
										</div>
										<div class="col-sm-4 col-4 border-right">
											<div class="analytics-desc">
												<h5 class="analytics-count">
													<?php echo $unidadeAgenda; ?>
												</h5>
												<span class="analytics-title">AGENDAMENTOS</span>
											</div>
										</div>
										<div class="col-sm-4 col-4">
											<div class="analytics-desc">
												<h5 class="analytics-count">
													<?php echo $unidadeFunc; ?>
												</h5>
												<span class="analytics-title">FUNCIONÁRIOS</span>
											</div>
										</div>
									</div>
								</div> -->
							</div>
						</div>
					</div>
					<?php } ?>
                </div>
            </div>
        </div>		
    </div>
    <div class="sidebar-overlay" data-reff=""></div>