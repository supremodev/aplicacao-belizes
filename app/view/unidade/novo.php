<div class="page-wrapper">
    <div class="content">
        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
			Erro ao cadastrar a unidade.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			</div>
			<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
			Unidade cadastrada com sucesso.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
		</div>
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <form id="formularios">
                    <h3 class="page-title">Configurações da Clínica</h3>
                    <div class="row">                        
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="profile-unidade">
									<div class="profile" style="background-image: url('<?php echo URL; ?>images/usuario.png');">
										<label class="edit">
											<span><i class="mdi mdi-upload"></i></span>
											<input type="file" size="32" name="unidadeFoto" id="inputImagem">
										</label>
									</div>
									<button id="editarImagem" type="button" class="btn btn-primary btn-sm">
										<i class="fa fa-file-image-o"></i>
									</button>
								</div>
                            </div>
                        </div>                        
                        
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Nome da Empresa <span class="text-danger">*</span></label>
                                <input class="form-control" name="unidadeNomeEmpresa" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Nome Contato</label>
                                <input class="form-control" name="unidadePessoaContato" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Razao Social</label>
                                <input class="form-control" name="unidadeRazaoSocial" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>CNPJ <span class="text-danger">*</span></label>
                                <input class="form-control" name="unidadeCNPJ" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Telefone</label>
                                <input class="form-control" name="unidadeFoneUm" type="text">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Celular</label>
                                <input class="form-control" name="unidadeFoneDois" type="text">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Celular</label>
                                <input class="form-control" name="unidadeFoneTres" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Endereço</label>
                                <input class="form-control" name="unidadeEnd" type="text">
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label>Número</label>
                                <input class="form-control" name="unidadeEndRua" type="text">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Complemento</label>
                                <input class="form-control" name="unidadeEndComp" type="text">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Bairro</label>
                                <input class="form-control" name="unidadeEndBairro" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Cidade</label>
                                <input class="form-control" name="unidadeEndCidade" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>CEP</label>
                                <input class="form-control" name="unidadeCEP" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" name="unidadeEmail" type="email">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Site</label>
                                <input class="form-control" name="unidadeSite" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Facebook</label>
                                <input class="form-control" name="unidadeFace" type="text">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Instagram</label>
                                <input class="form-control" name="unidadeInsta" type="text">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Twitter</label>
                                <input class="form-control" name="unidadeTwitter" type="text">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>YouTUBE</label>
                                <input class="form-control" name="unidadeYouTUBE" type="text">
                            </div>
                        </div>
                    </div>
                    
                    
                </form>
                <div class="row">
                    <div class="col-sm-12 text-center m-t-20">
                        <button destino="unidade/inserirUnidade" class="novo-ajax btn btn-primary submit-btn" type="button">Cadastrar Unidade</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="sidebar-overlay" data-reff=""></div>
