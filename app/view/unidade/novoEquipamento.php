<div class="page-wrapper">
            <div class="content">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
					Erro ao cadsatrar a Equipamento.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
					Equipamento cadastrada com sucesso.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Cadastro Equipamento</h4>
                    </div>
                </div>
                <form id="formularios">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-img-wrap sala">
                                    <div class="profile" style="background-image: url('<?php echo URL; ?>images/usuario.png');">
                                        <label class="edit">
                                            <span><i class="mdi mdi-upload"></i></span>
                                            <input type="file" size="32" name="equipamentoFoto" id="inputImagem">
                                        </label>
                                    </div>
                                    <button id="editarImagem" type="button" class="btn btn-primary btn-sm">
                                        <i class="fa fa-file-image-o"></i>
                                    </button>
                                </div>
                                
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-focus">
                                                <select class="select" name="idUnidade">
                                                    <option>Selecionar a unidade</option>
                                                    <?php foreach ($unidadeLista as $lista) { ?>
                                                    <option value="<?php echo $lista->idUnidade; ?>"><?php echo $lista->unidadeNomeEmpresa; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Nome Equipamento:<span class="text-danger">*</span></label>
                                                <input type="text" name="equipamentoNome" class="form-control floating" required>
                                            </div>
                                        </div>
										<div class="col-md-12">
											<div class="form-group">
												<label class="focus-label">Descrição</label>
												<textarea class="form-control floating" rows="3" cols="30" name="equipamentoDescricao"></textarea>
											</div>
                                        </div>										
                                    </div>
                                </div>
                            </div>							
                        </div>
                    </div>
                    <div class="text-center m-t-20">
                        <button destino="unidade/inserirEquipamento" class="novo-ajax btn btn-primary submit-btn" type="button">Cadastrar Equipamento</button>                        
                    </div>
                </form>
				
            </div>
        </div>
    </div>
    <div class="sidebar-overlay" data-reff=""></div>