<div class="page-wrapper">
            <div class="content">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
                    <span class="texto-ajax"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show" role="alert" style="display:none;width: 100%;">
					<span class="texto-ajax"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <h4 class="page-title">Salas desativadas</h4>
                    </div>
                </div>
				<div class="row doctor-grid">
					<?php foreach ($salaUnidadeLista as $sala) { ?>
                    <div id="linha<?php echo $sala->idSalaUnidade?>" class="col-md-4 col-sm-4 col-xs-6 col-lg-3">
                        <div class="profile-widget sala">
                            <div class="servico-img salaImg">
                                <img alt="<?php if (isset($sala->salaNomeUnidade)) echo htmlspecialchars($sala->salaNomeUnidade, ENT_QUOTES, 'UTF-8'); ?>" src="<?php echo URL; ?>assets/img/sala/<?php 
                                    if(!$sala->salaFotoUnidade == ""){
                                        echo $sala->salaFotoUnidade;
                                    }else{
                                        echo "sem-foto.jpg";        
                                    }
                                ?>">							
                            </div>
                            <div class="dropdown profile-action">
								<a class="desativar-ajax dropdown-item"  href="#" destino="unidade/ativarDesativarSala/1" idobjeto="<?php echo $sala->idSalaUnidade; ?>">
								<i class="fa fa-check m-r-5"></i>Ativar</a>
							</div>
                            <h4 class="doctor-name text-ellipsis">
								<?php if (isset($sala->salaNomeUnidade)) echo htmlspecialchars($sala->salaNomeUnidade, ENT_QUOTES, 'UTF-8'); ?>
							</h4>
                            <div class="doc-prof">
								<p>
									<?php 					
										echo substr($sala->salaDescricaoUnidade, 0, 200);
									?>
								</p>
							</div>
                        </div>
                    </div>
					<?php } ?>					
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar-overlay" data-reff=""></div>