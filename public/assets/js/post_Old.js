
var url = document.location.origin;
if (url == "https://belizes.supremodigital.com.br") {
    var url = document.location.origin;
} else {
    var url = document.location.origin + "/aplicacao-belizes";
}

//==================== LIMPAR CAMPOS =======================
function LimparCampos() {
    $('#formularios input').val("");
    $('#formularios textarea').val("");
    $('#formularios select').empty();
    $('#formularios .profile').attr('style',"");
    $('#formularios .assinatura img').attr('src',url+"/assets/img/assinatura.png");
    $(evento).prop('disabled', true);
}
//==================== NOVO DOCUMENTO =======================

$(".novo-ajax").click(function () {

    $('#preload').show();
    var destino = $(this).attr('destino');
    var evento = this;

    $.ajax({

        type: "POST",
        data: new FormData($('#formularios')[0]),
        cache: false,
        contentType: false,
        processData: false,
        url: url + "/" + destino,
        dataType: "html",
        success: function (retorno) {
            $('#preload').hide('slow');
            $('.page-title').html(retorno);
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                $('.alert-success').show("slow");
                LimparCampos();
            } else {
                $('.alert-danger').show("slow");
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });

});

//==================== FACIL DOCUMENTO =======================

$(".facial-ajax").click(function () {

    $('#preload').show();
    var destino = $(this).attr('destino');
    var evento = this;

    $.ajax({

        type: "POST",
        data: new FormData($('#formFacial')[0]),
        cache: false,
        contentType: false,
        processData: false,
        url: url + "/" + destino,
        dataType: "html",
        success: function (retorno) {
            $('#preload').hide('slow');
            $('.page-title').html(retorno);
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                $('.alert-success').show("slow");
                LimparCampos();
            } else {
                $('.alert-danger').show("slow");
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });

});

//==================== CORPORAL DOCUMENTO =======================

$(".corporal-ajax").click(function () {

    $('#preload').show();
    var destino = $(this).attr('destino');
    var evento = this;

    $.ajax({

        type: "POST",
        data: new FormData($('#formulario-corporal')[0]),
        cache: false,
        contentType: false,
        processData: false,
        url: url + "/" + destino,
        dataType: "html",
        success: function (retorno) {
            $('#preload').hide('slow');
            $('.page-title').html(retorno);
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                $('.alert-success').show("slow");
                LimparCampos();
            } else {
                $('.alert-danger').show("slow");
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });

});

//==================== MULTIPLOS FORM NOVO DOCUMENTO =======================

$(".multi-novo-ajax").click(function () {

    $('#preload').show();
    var destino = $(this).attr('destino');
    var evento = this;

    $.ajax({

        type: "POST",
        data: new FormData($('#formularioss')[0]),
        cache: false,
        contentType: false,
        processData: false,
        url: url + "/" + destino,
        dataType: "html",
        success: function (retorno) {
            $('#preload').hide('slow');
            $('.page-title').html(retorno);
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                $('.alert-success').show("slow");
                LimparCampos();
            } else {
                $('.alert-danger').show("slow");
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });

});

//==================== FORM CORPORAL =======================

$(".corporal-ajax").click(function () {

    $('#preload').show();
    var destino = $(this).attr('destino');
    var evento = this;

    $.ajax({

        type: "POST",
        data: new FormData($('#formulario-corporal')[0]),
        cache: false,
        contentType: false,
        processData: false,
        url: url + "/" + destino,
        dataType: "html",
        success: function (retorno) {
            $('#preload').hide('slow');
            $('.page-title').html(retorno);
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                $('.alert-success').show("slow");
                LimparCampos();
            } else {
                $('.alert-danger').show("slow");
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });

});

//==================== ATUALIAZAR DOCUMENTO =======================

$(".atualizar-ajax").click(function () {
    $('#preload').show();
    var destino = $(this).attr('destino');
    var evento = this;

    $.ajax({

        type: "POST",
        data: new FormData($('#formularios')[0]),
        cache: false,
        contentType: false,
        processData: false,
        url: url + "/" + destino,
        dataType: "html",
        success: function (retorno) {
            $('#preload').hide('slow');
            $('.page-title').html(retorno);
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                $('.alert-success').show("slow");
            } else {
                $('.alert-danger').show("slow");
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });

});
//==================== Consulta FICHA =======================
$(document).ready(function () {
    $(".ajax-consulta-ficha-necessario").keyup(function () {
        $(".ajax-consulta-ficha-necessario").val(this.value.match(/[0-9]*/));
    });
    var strokeCount = 0;

    $(function () {

        $(".ajax-consulta-ficha-necessario").keyup(function () {
            strokeCount = $(this).val().length;
            if (strokeCount == 11) {
                ajaxx();
            }
        });
    });
});

function ajaxx() {

    var evento = this;
    var cpf = $('#cpf').val();

    $.ajax({
        url: url + "/ficha/consultaCpfNecessario",
        method: 'post',
        dataType: "html",
        data: {
            cpf: cpf,
        },
        success: function (retorno) {
            objeto = JSON.parse(retorno);
            $('#nome').val(objeto[0][0]["cliNome"]);
            $('#nasci').val(objeto[0][0]["cliNasci"]);
            $('#sexo').val(objeto[0][0]["cliSexo"]);
            $('#profissao').val(objeto[0][0]["cliProfissao"]);
            $('#idCliente').val(objeto[0][0]["idCliente"]);
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });

};

$(document).ready(function () {
    $(".ajax-consulta-ficha").keyup(function () {
        $(".ajax-consulta-ficha").val(this.value.match(/[0-9]*/));
    });
    var strokeCount = 0;

    $(function () {

        $(".ajax-consulta-ficha").keyup(function () {
            strokeCount = $(this).val().length;
            if (strokeCount == 11) {
                ajax();
                
            }
        });
    });
});

function ajax() {

    var evento = this;
    var cpf = $('#cpf').val();

    $.ajax({
        url: url + "/ficha/consultaCpfFichaGeral",
        method: 'post',
        dataType: "html",
        data: {
            cpf: cpf,
        },
        success: function (retorno) {
            objeto = JSON.parse(retorno);
            $('.nav-item').show('slow');
            Corpo();
            var msgModal = JSON.parse(retorno);
            $('#nome').val(objeto[0][0]["cliNome"]);
            $('#nasci').val(objeto[0][0]["cliNasci"]);
            $('#sexo').val(objeto[0][0]["cliSexo"]);
            $('#profissao').val(objeto[0][0]["cliProfissao"]);

            //CHECKBOX
            if (objeto[0][0]['fichaHistoricoDeDermatiteOuCancerDePele']) {
                var input = $("input[name='fichaHistoricoDeDermatiteOuCancerDePele']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaHistoricoDeDermatiteOuCancerDePele']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaFoiLiberadoPeloMedicoParaTratamentoEstetico']) {
                var input = $("input[name='fichaFoiLiberadoPeloMedicoParaTratamentoEstetico']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaFoiLiberadoPeloMedicoParaTratamentoEstetico']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaFoiLiberadoPeloMedicoParaTratamentoEstetico']) {
                var input = $("input[name='fichaFoiLiberadoPeloMedicoParaTratamentoEstetico']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaFoiLiberadoPeloMedicoParaTratamentoEstetico']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaHipertireoidismo']) {
                var input = $("input[name='fichaHipertireoidismo']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaHipertireoidismo']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaHipotireoidismo']) {
                var input = $("input[name='fichaHipotireoidismo']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaHipotireoidismo']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaDiabetes']) {
                var input = $("input[name='fichaDiabetes']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaDiabetes']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaHipertensao']) {
                var input = $("input[name='fichaHipertensao']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaHipertensao']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaEplepsia']) {
                var input = $("input[name='fichaEplepsia']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaEplepsia']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaPsoriase']) {
                var input = $("input[name='fichaPsoriase']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaPsoriase']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaTemImplantesMetalicos']) {
                var input = $("input[name='fichaTemImplantesMetalicos']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaTemImplantesMetalicos']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaPortadorDeMarcapasso']) {
                var input = $("input[name='fichaPortadorDeMarcapasso']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaPortadorDeMarcapasso']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaPortadorDeHIV']) {
                var input = $("input[name='fichaPortadorDeHIV']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaPortadorDeHIV']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaPortadorDeHepatite']) {
                var input = $("input[name='fichaPortadorDeHepatite']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaPortadorDeHepatite']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaAlergiaAlgumAlimento']) {
                var input = $("input[name='fichaAlergiaAlgumAlimento']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaAlergiaAlgumAlimento']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaAlergiaAlgumMedicamento']) {
                var input = $("input[name='fichaAlergiaAlgumMedicamento");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaAlergiaAlgumMedicamento");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaFazUsoDeMedicamento']) {
                var input = $("input[name='fichaFazUsoDeMedicamento']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaFazUsoDeMedicamento']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaPresencaDeQueloides']) {
                var input = $("input[name='fichaPresencaDeQueloides']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaPresencaDeQueloides']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaGestante']) {
                var input = $("input[name='fichaGestante']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaGestante']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaFilhos']) {
                var input = $("input[name='fichaFilhos']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaFilhos']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaFezAlgumaCirurgiaPlastica']) {
                var input = $("input[name='fichaFezAlgumaCirurgiaPlastica']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaFezAlgumaCirurgiaPlastica']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaRealizouProcedimentosEsteticosAnteriores']) {
                var input = $("input[name='fichaRealizouProcedimentosEsteticosAnteriores']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaRealizouProcedimentosEsteticosAnteriores']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaFazUsoDeMaquiagem']) {
                var input = $("input[name='fichaFazUsoDeMaquiagem']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaFazUsoDeMaquiagem']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaFazAtividadeFisica']) {
                var input = $("input[name='fichaFazAtividadeFisica']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaFazAtividadeFisica']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaIngereBebidaAlcoolica']) {
                var input = $("input[name='fichaIngereBebidaAlcoolica']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaIngereBebidaAlcoolica']");
                input[1].checked = false
            }

            if (objeto[0][0]['fichaAlteracaoHormonal'] == 1) {
                var input = $("input[name='fichaAlteracaoHormonal']");
                input[0].checked = true
            } else {
                var input = $("input[name='fichaAlteracaoHormonal']");
                input[1].checked = true
            }


            $("input[name='fichaEmQualLocalDoCorpo']").val(objeto[0][0]['fichaEmQualLocalDoCorpo']);
            $("input[name='fichaFazQuantoTempo']").val(objeto[0][0]['fichaFazQuantoTempo']);        
            $("input[name='fichaTratamentoHipertireoidismo']").val(objeto[0][0]['fichaTratamentoHipertireoidismo']);   
            $("input[name='fichaTratamentoHipotireoidismo']").val(objeto[0][0]['fichaTratamentoHipotireoidismo']);
            $("input[name='fichaTratamentoDiabetes']").val(objeto[0][0]['fichaTratamentoDiabetes']);
            $("input[name='fichaTratamentoHipertensao']").val(objeto[0][0]['fichaTratamentoHipertensao']); 
            $("input[name='fichaTratamentoEplepsia']").val(objeto[0][0]['fichaTratamentoEplepsia']);     
            $("input[name='fichaTratamentoPsoriase']").val(objeto[0][0]['fichaTratamentoPsoriase']);
            $("input[name='fichaOutrasDoencas']").val(objeto[0][0]['fichaOutrasDoencas']);  
            $("input[name='fichaLocalTemImplantesMetalicos']").val(objeto[0][0]['fichaLocalTemImplantesMetalicos']);
            $("input[name='fichaQuantoTempoPortadorDeMarcapasso']").val(objeto[0][0]['fichaQuantoTempoPortadorDeMarcapasso']);
            $("input[name='fichaTratamentoPortadorDeHIV']").val(objeto[0][0]['fichaTratamentoPortadorDeHIV']);
            $("input[name='fichaQualPortadorDeHepatite']").val(objeto[0][0]['fichaQualPortadorDeHepatite']); 
            $("input[name='fichaQualAlergiaAlgumAlimento']").val(objeto[0][0]['fichaQualAlergiaAlgumAlimento']);
            $("input[name='fichaQualAlergiaAlgumMedicamento']").val(objeto[0][0]['fichaQualAlergiaAlgumMedicamento']);
            $("input[name='fichaQualFazUsoDeMedicamento']").val(objeto[0][0]['fichaQualFazUsoDeMedicamento']);
            $("input[name='fichaQueLocalPresencaDeQueloide']").val(objeto[0][0]['fichaQueLocalPresencaDeQueloide']);
            $("input[name='fichaQuantoTempoGestante']").val(objeto[0][0]['fichaQuantoTempoGestante']);
            $("input[name='fichaQuantosFilhosIdade']").val(objeto[0][0]['fichaQuantosFilhosIdade']);
            $("input[name='fichaQualFezAlgumaCirurgiaPlastica']").val(objeto[0][0]['fichaQualFezAlgumaCirurgiaPlastica']);
            $("input[name='fichaQualRealizouProcedimentosEsteticosAnteriores']").val(objeto[0][0]['fichaQualRealizouProcedimentosEsteticosAnteriores']);
            $("input[name='fichaQualFazUsoDeMaquiagem']").val(objeto[0][0]['fichaQualFazUsoDeMaquiagem ']);     
            $("input[name='fichaQuantasVezesPorSemanaAtividadeFisica']").val(objeto[0][0]['fichaQuantasVezesPorSemanaAtividadeFisica']);
            $("input[name='fichaConsumoAguaIngereQuantosCoposDia']").val(objeto[0][0]['fichaConsumoAguaIngereQuantosCoposDia']);
            


        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });

}


//==================== CARREGAR CORPO =======================
function Corpo() {
    let sexo = $("#sexo").val();

    var fd = new FormData();
    fd.append('sexo', sexo);

    $.ajax({

        type: "POST",
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        url: url + "/" + "ficha/carregarcorpo",
        dataType: "html",
        success: function (retorno) {
            $('#corpo-ajax').html(retorno);
            anatomica();
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });

};

//==================== Permissão Atualização =======================
$(".permissao-atualizacao-ajax").click(function () {
    var evento = this;
    var destino = $(this).attr('destino');
    var id = $(this).attr('idobjeto');
    var boleano = $(this).attr('boleano');

    var fd = new FormData();
    fd.append('id', id);
    fd.append('boleano', boleano);

    $.ajax({

        type: "POST",
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        url: url + "/" + destino,
        dataType: "html",
        success: function (retorno) {
            $('.page-title').html(retorno);
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                if (boleano == 1) {
                    $(evento).attr('boleano',"0");
                    $(evento).html("<i class='fa fa-check m-r-5'></i>Permissão Atualizar");
                }else{
                    $(evento).attr('boleano',"1");
                    $(evento).html("<i class='fa fa-close m-r-5'></i>Bloquear Atualizar");
                }
            } else {

            }
        },
        beforeSend: function () {
        },
        complete: function (msg) {

        }
    });

});

//==================== DESATIVAR =======================

$(".desativar-ajax").click(function () {

    var destino = $(this).attr('destino');
    var id = $(this).attr('idobjeto');
    var evento = this;

    var fd = new FormData();
    fd.append('id', id);

    $.ajax({

        type: "POST",
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        url: url + "/" + destino,
        dataType: "html",
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                var linha = $('#linha' + id);
                linha.hide('slow', function () { linha.remove(); });
                $('.alert-success').show("slow");
                $('.alert-success').text('Desativado com sucesso');
            } else {
                $('.alert-danger').show("slow");
                $('.alert-danger').text('Erro ao desativar');
            }
        },
        beforeSend: function () {
        },
        complete: function (msg) {

        }
    });

});


//==================== F =======================

$("#formularios input").keydown(function () {
    $('#atualizado-sucesso').addClass('d-none');
    $('#erro-atualizar').addClass('d-none');
    $('.novo-ajax').prop('disabled', false);
    $('.atualizar-ajax').prop('disabled', false);
});
$("#formularios textarea").keydown(function () {
    $('#atualizado-sucesso').addClass('d-none');
    $('#erro-atualizar').addClass('d-none');
    $('.novo-ajax').prop('disabled', false);
    $('.atualizar-ajax').prop('disabled', false);
    $('.perfil-atualizar').prop('disabled', false);
});
$("#formularios select").change(function () {
    $('#atualizado-sucesso').addClass('d-none');
    $('#erro-atualizar').addClass('d-none');
    $('.novo-ajax').prop('disabled', false);
    $('.atualizar-ajax').prop('disabled', false);
    $('.perfil-atualizar').prop('disabled', false);
});
$(".profile-img-wrap").click(function () {
    $('#atualizado-sucesso').addClass('d-none');
    $('#erro-atualizar').addClass('d-none');
    $('.novo-ajax').prop('disabled', false);
    $('.atualizar-ajax').prop('disabled', false);
    $('.perfil-atualizar').prop('disabled', false);
});
$(".salvar-assinatura").click(function () {
    $('#atualizado-sucesso').addClass('d-none');
    $('#erro-atualizar').addClass('d-none');
    $('.novo-ajax').prop('disabled', false);
    $('.atualizar-ajax').prop('disabled', false);
});
