//==================== CPF =======================
$("#cpf").inputmask({
    mask: "99999999999"
  });
//==================== CPF =======================
$("#cel").inputmask({
    mask: "99999999999"
  });
//==================== IMC =======================
$("#imc").inputmask({
    mask: "9.99"
  });
$(".imc").keypress(function (e) { 
    var peso = $("input[name='fichaCorporalBiotipoPeso']").val();
    var altura = $("input[name='fichaCorporalBiotipoAltura']").val();

    var imc = peso / (altura * altura);
    var valorImc = imc.toFixed(2);
    $("input[name='fichaCorporalBiotipoIMC']").val(valorImc); 

    if(valorImc < 18.5) {
        $("input[name='fichaCorporalBiotipoIMCSituacao']").val("Baixo peso");
    } else if(valorImc >= 18 && valorImc <= 24.9) {
        $("input[name='fichaCorporalBiotipoIMCSituacao']").val("Peso normal");
    } else if(valorImc >= 25 && valorImc <= 29.9) {
        $("input[name='fichaCorporalBiotipoIMCSituacao']").val("Excesso de peso");
    } else if(valorImc >= 30 && valorImc <= 34.9) {
        $("input[name='fichaCorporalBiotipoIMCSituacao']").val("Obesidade de Classe 1");
    } else if(valorImc >= 35 && valorImc <= 39.9) {
        $("input[name='fichaCorporalBiotipoIMCSituacao']").val("Obesidade de Classe 2");
    } else if(valorImc >= 40) {
        $("input[name='fichaCorporalBiotipoIMCSituacao']").val("Obesidade de Classe 3");
    }

});

$('#inputImagem').click(function () {
    $('#inputWebcam').val("");
});

$('#inputWebcam').click(function () {
    $('#inputImagem').val("");
    $("#inputImagem").replaceWith($("#inputImagem").clone());
});

// upload imagem

var input       = document.querySelector('#inputImagem');

if (typeof input !== 'undefined') {

    var imagem = document.querySelector('.profile');

    input.addEventListener('change', function (event) {
        var reader = new FileReader();
        reader.onload = function (e) {
            setImageUrl(e.target.result);
        };
        reader.readAsDataURL(event.target.files[0]);
    });

    function setImageUrl(url) {
        imagem.style.backgroundImage = 'url(' + url + ')';
    }

    $('#editarImagem').click(function () {
        $('#inputImagem').click();
    });

    $('#removerImagem').click(function () {
        setImageUrl('../images/img-upload.png');
    });
};

// Webcam

function showImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#pic')
                .attr('src', e.target.result)
                .width(150);
        };

        reader.readAsDataURL(input.files[0]);

    }
}
(function () {
    // The width and height of the captured photo. We will set the
    // width to the value defined here, but the height will be
    // calculated based on the aspect ratio of the input stream.

    var width = 320; // We will scale the photo width to this
    var height = 0; // This will be computed based on the input stream

    // |streaming| indicates whether or not we're currently streaming
    // video from the camera. Obviously, we start at false.

    var streaming = false;

    // The various HTML elements we need to configure or control. These
    // will be set by the startup() function.

    var video = null;
    var canvas = null;
    var photo = null;
    var startbutton = null;

    function startup() {
        video = document.getElementById('video');
        canvas = document.getElementById('canvas');
        photo = document.getElementById('photo');
        startbutton = document.getElementById('startbutton');

        navigator.mediaDevices.getUserMedia({
                video: true,
                audio: false
            })
            .then(function (stream) {
                video.srcObject = stream;
                video.play();
            })
            .catch(function (err) {
                console.log("An error occurred: " + err);
            });

        video.addEventListener('canplay', function (ev) {
            if (!streaming) {
                height = video.videoHeight / (video.videoWidth / width);

                // Firefox currently has a bug where the height can't be read from
                // the video, so we will make assumptions if this happens.

                if (isNaN(height)) {
                    height = width / (4 / 3);
                }

                streaming = true;
            }
        }, false);

        startbutton.addEventListener('click', function (ev) {
            takepicture();
            ev.preventDefault();
        }, false);
    }

    // Fill the photo with an indication that none has been
    // captured.


    // Capture a photo by fetching the current contents of the video
    // and drawing it into a canvas, then converting that to a PNG
    // format data URL. By drawing it on an offscreen canvas and then
    // drawing that to the screen, we can change its size and/or apply
    // other changes before drawing it.

    function takepicture() {
        var context = canvas.getContext('2d');


        if (width && height) {
            canvas.width = width;
            canvas.height = height;
            context.drawImage(video, 0, 0, width, height);

            var dataURL = canvas.toDataURL();

            //Atualiza SRC da tag IMG
            $('.profile').css('background-image', 'url(' + dataURL + ')');

            //Add value do canvas no input foto webcam
            $('#inputWebcam').val(dataURL);
        }
    }

    // Set up our event listener to run the startup process
    // once loading is complete.
    window.addEventListener('load', startup, false);
})();
