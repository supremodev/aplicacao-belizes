// JavaScript Document

// Bar Chart

	var barChartData = {
		labels: ['jan', 'fev', 'mar', 'abr', 'maio', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'],

		
		datasets: [{
			label: '2020',
			backgroundColor: 'rgba(62, 121, 67, 0.5)',
			borderColor: 'rgba(62, 121, 67, 1)',
			borderWidth: 1,
			data: [35, 59, 80, 81, 56, 55, 40, 59, 80, 81, 56, 55, 40, 50]
		}, {
			label: '2019',
			backgroundColor: 'rgba(228, 228, 228, 0.5)',
			borderColor: 'rgba(228, 228, 228, 1)',
			borderWidth: 1,
			data: [28, 48, 40, 19, 86, 27, 90, 48, 40, 19, 86, 27, 90, 20]
		}]

	};

	var ctx = document.getElementById('bargraph').getContext('2d');
	window.myBar = new Chart(ctx, {
		type: 'bar',
		data: barChartData,
		options: {
			responsive: true,
			legend: {
				display: false,
			}
		}
	});

var data = {
    labels: [
        "Red",
        "Blue",
        "Yellow"
    ],
    datasets: [
        {
            data: [300, 50, 100],
            backgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ],
            hoverBackgroundColor: [
                "#FF4394",
                "#36A2EB",
                "#FFCE56"
            ]
						
						
        }]
};

var options = { 
	cutoutPercentage:40,
};


/*var myDoughnutChart = new Chart(ctx, {
    type: 'doughnut',
    data: data,
    options: options
});*/
	
		/*var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};*/

		var config = {
			type: 'doughnut',
			data: {
				datasets: [{
					data: [300, 50, 100, 20, 70],
					backgroundColor: [
						"#FF6384",
						"#36A2EB",
						"#01b5c7",
						"#FFCE56",
						"#737AFC"
					],
					label: 'Dataset 1'
				}],
				labels: [
					'Red',
					'Orange',
					'Yellow',
					'Green',
					'Blue'
				]
			},
			options: {
				responsive: true,
				legend: {
					position: 'top',
				},
				animation: {
					animateScale: true,
					animateRotate: true
				}
			}
		};
		/*
		window.onload = function() {
			var ctx = document.getElementById('chart-area').getContext('2d');
			window.myDoughnut = new Chart(ctx, config);
		}; */


		
		// BEGIN LINE CHART

		/*var lineChartData = {
			labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],

			datasets: [{
				label: 'My First dataset',
				backgroundColor: 'rgba(102, 126, 234, 0.2)',
				borderColor: 'rgba(102, 126, 234, 1)',
				pointBackgroundColor: 'rgba(102, 126, 234, 1)',
				borderWidth: 2,
				data: [35, 59, 80, 81, 56, 55, 40],
				
			}, {
				label: 'My Second dataset',
				backgroundColor: 'rgba(252, 96, 117, 0.2)',
				borderColor: 'rgba(252, 96, 117, 1)',
				pointBackgroundColor: 'rgba(252, 96, 117, 1)',
				borderWidth: 2,
				data: [28, 48, 40, 19, 86, 27, 90],
			}]

		};*/
		
		var lineChartData = {
		  labels: ['jan', 'fev', 'mar', 'abr', 'maio', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'],
		  datasets: [{
			label: "Pacientes 2020",
			backgroundColor: 'rgba(62, 121, 67, 0.5)',
			/*strokeColor: "none",
			pointColor: "rgba(41, 128, 185, 0.9)",
			pointStrokeColor: "rgba(41, 128, 185, 0)",
			pointHighlightFill: "rgba(41, 128, 185, 0.9)",
			pointHighlightStroke: "rgba(41, 128, 185, 0)",*/
			data: [100, 70, 20, 100, 120, 50, 70, 50, 50, 100, 50, 90]
		  }, {
			label: "Pacientes 2019",
			backgroundColor: "rgba(228, 228, 228, 1)",
			/*strokeColor: "none",
			pointColor: "rgba(155, 89, 182, 0.9)",
			pointStrokeColor: "rgba(231, 76, 60, 255, 0)",
			pointHighlightFill: "rgba(155, 89, 182, 0.9)",
			pointHighlightStroke: "rgba(231, 76, 60, 0)",*/
			fill: true,
			data: [28, 48, 40, 19, 86, 27, 20, 90, 50, 20, 90, 20]
		  }]
		};
	
		var linectx = document.getElementById('canvas').getContext('2d');
		window.myLine = new Chart(linectx, {
		type: 'line',
		data: lineChartData,
		options: {
			responsive: true,
			legend: {
				display: false,
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			}
		}
		});
		


		$(document).ready(function(){
			barChart();

			$(window).resize(function(){
				barChart();
			});

			function barChart(){
				$('.bar-chart').find('.item-progress').each(function(){
					var itemProgress = $(this),
					itemProgressWidth = $(this).parent().width() * ($(this).data('percent') / 100);
					itemProgress.css('width', itemProgressWidth);
				});
			};
		});

