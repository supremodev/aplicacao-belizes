function anatomica() {
    

var
    svg = document.getElementById('humanAnatomy'),
    //NS = svg.getAttribute('xmlns'),
    NS = $('svg').attr('xmlns'),
    pinForm = $('#pinForm'),
    facialForm = $('#facialForm'),
    btnOK = $('#btnSuccess'),
    c = $('#btnDanger'),
    pinConfirm = $('#pinConfirm'),
    btnConfirmTrue = $('#btnConfirmTrue'),
    btnConfirmCancel = $('#btnConfirmCancel'),
    pinConfirmSucces = $('#pinConfirmSucces'),
    pinConfirmBtns = $('#pinConfirmBtns');


/*$(document).on('click', '#humanInner', function(e) {
    var
        t = e.target,
        x = e.clientX,
        y = e.clientY,
        target = (t == svg ? svg : t.parentNode),
        pin = pinCenter(target, x, y),
        newCircIdParam = "newcircle" + Math.round(pin.x) + '-' + Math.round(pin.y),
        circle = document.createElementNS(NS, 'circle');

    circle.setAttributeNS(null, 'cx', Math.round(pin.x));
    circle.setAttributeNS(null, 'cy', Math.round(pin.y));
    circle.setAttributeNS(null, 'r', 20);
    circle.setAttributeNS(null, 'class', "newcircle");
    circle.setAttributeNS(null, 'id', newCircIdParam);
    circle.setAttributeNS(null, 'data-x', Math.round(pin.x));
    circle.setAttributeNS(null, 'data-y', Math.round(pin.y));
    target.after(circle);

    pinForm.show();
    pinConfirmBtns.css({
        "left": (x + 30) + 'px',
        "top": (y) + 'px'
    });

    /* Confirme a posição do Círculo adicionado 
    btnConfirmTrue.click(function() {
        pinConfirm.hide();
        pinForm.show();
    });

    /* After confirmation completion
    pinConfirmSucces.click(function() {
        pinConfirmSucces.hide();
    });
});*/

var idMarcacaoCorpo;
var posicaox;
var posicaoy;

$(document).on('click', '#versoHuman', function(e) {
    var
        t = e.target,
        x = e.clientX,
        y = e.clientY,
        target = (t == svg ? svg : t.parentNode),
        pin = pinCenter(target, x, y),
        newCircIdParam = "newcircle" + Math.round(pin.x) + '-' + Math.round(pin.y),
        circle = document.createElementNS(NS, 'circle');
    circle.setAttributeNS(null, 'cx', Math.round(pin.x));
    circle.setAttributeNS(null, 'cy', Math.round(pin.y));
    circle.setAttributeNS(null, 'r', 20);
    circle.setAttributeNS(null, 'class', "newcircle");
    circle.setAttributeNS(null, 'id', newCircIdParam);
    circle.setAttributeNS(null, 'data-x', Math.round(pin.x));
    circle.setAttributeNS(null, 'data-y', Math.round(pin.y));
    target.after(circle);
    idMarcacaoCorpo = newCircIdParam;
    posicaox = Math.round(pin.x),
    posicaoy = Math.round(pin.y),

    pinForm.show();
    pinConfirmBtns.css({
        "left": (x + 30) + 'px',
        "top": (y) + 'px'
    });

    /* Confirme a posição do Círculo adicionado */
    btnConfirmTrue.click(function() {
        pinConfirm.hide();
        pinForm.show();
    });

    /* Preencha o formulário, envie ou desista */

    /*btnOK.click(function(event) {
        event.preventDefault();
        pinForm.hide();
        var comentario = $('#comentario').val();
        console.log(comentario +" "+ newCircIdParam);
        //$('.list-number-rounded').append('<li class="obsMarcacao" ><a href="#index">'+comentario+'</a><span class="deletarMarca"><i class="fa fa-close"></i></span></li>');
        $('#comentario').val("");
        $('.deletarMarca').attr('idobjeto', newCircIdParam);
    });*/




    /* After confirmation completion*/
    pinConfirmSucces.click(function() {
        pinConfirmSucces.hide();
    });
});

$('#btnSuccess').click(function (e) { 
    e.preventDefault();
    pinForm.hide();
    var comentario = $('#comentario');
    $('.lista-corpo').append('<li class="obsMarcacao" x="'+posicaox+'" y="'+posicaoy+'" descricao="'+comentario.val()+'"><a>'+comentario.val()+'</a><span class="deletarMarca" idObjeto="'+idMarcacaoCorpo+'"><i class="fa fa-close"></i></span></li>');
    //$('.deletarMarca').attr('idobjeto', idMarcacaoCorpo);
    comentario.val('');
});

$(document).on("click",".deletarMarca", function(){
    var idMarcacao = $(this).attr('idObjeto');
    $('#'+idMarcacao).remove();
    //var listaMarcado = $(this).parent(".obsMarcacao").parent(".lista-corpo").children(this).remove();
    $('.deletarMarca[idobjeto='+idMarcacao+']').parent(".obsMarcacao").remove();
});

function pinCenter(element, x, y) {
    var pt = svg.createSVGPoint();
    pt.x = x;
    pt.y = y;
    return pt.matrixTransform(element.getScreenCTM().inverse());
}

$(document).on('click', '.newcircle', function() {
    alert("Marcação: x: " + $(this).data('x') + " y: " + $(this).data("y"));
});


btnConfirmCancel.click(function() {
 	$("#humanInner + .newcircle").remove();
    pinConfirm.hide();
});



}

function anatomicafacial() {

    var
    svg = document.getElementById('humanAnatomyFacial'),
    NS = $('svg').attr('xmlns'),
    pinForm = $('#pinForm'),
    facialForm = $('#facialForm'),
    btnOK = $('#btnSuccess'),
    c = $('#btnDanger'),
    pinConfirm = $('#pinConfirm'),
    btnConfirmTrue = $('#btnConfirmTrue'),
    btnConfirmCancel = $('#btnConfirmCancel'),
    pinConfirmSucces = $('#pinConfirmSucces'),
    pinConfirmBtns = $('#pinConfirmBtns');

    var idMarcacaoCorpo;
    var posicaox;
    var posicaoy;


    $(document).on('click', '#rostoHomem_2_, #imagemRostoMulher ', function(e) {
        console.log("teste");
        var
            t = e.target,
            x = e.clientX,
            y = e.clientY,
            target = (t == svg ? svg : t.parentNode),
            pin = pinCenter(target, x, y),
            newCircIdParam = "newcircle" + Math.round(pin.x) + '-' + Math.round(pin.y),
            circle = document.createElementNS(NS, 'circle');
        circle.setAttributeNS(null, 'cx', Math.round(pin.x));
        circle.setAttributeNS(null, 'cy', Math.round(pin.y));
        circle.setAttributeNS(null, 'r', 20);
        circle.setAttributeNS(null, 'class', "newcircle");
        circle.setAttributeNS(null, 'id', newCircIdParam);
        circle.setAttributeNS(null, 'data-x', Math.round(pin.x));
        circle.setAttributeNS(null, 'data-y', Math.round(pin.y));
        target.after(circle);
        idMarcacaoCorpo = newCircIdParam;
        posicaox = Math.round(pin.x),
        posicaoy = Math.round(pin.y),

        facialForm.show();
        pinConfirmBtns.css({
            "left": (x + 30) + 'px',
            "top": (y) + 'px'
        });

        /* Confirme a posição do Círculo adicionado */
        btnConfirmTrue.click(function() {
            pinConfirm.hide();
            facialForm.show();
        });

        /* Preencha o formulário, envie ou desista */

        /*btnOK.click(function(event) {
            event.preventDefault();
            pinForm.hide();
            var comentario = $('#comentario').val();
            console.log(comentario +" "+ newCircIdParam);
            //$('.list-number-rounded').append('<li class="obsMarcacao" ><a href="#index">'+comentario+'</a><span class="deletarMarca"><i class="fa fa-close"></i></span></li>');
            $('#comentario').val("");
            $('.deletarMarca').attr('idobjeto', newCircIdParam);
        });*/

        /* After confirmation completion*/
        pinConfirmSucces.click(function() {
            pinConfirmSucces.hide();
        });
    });

    function pinCenter(element, x, y) {
        var pt = svg.createSVGPoint();
        pt.x = x;
        pt.y = y;
        return pt.matrixTransform(element.getScreenCTM().inverse());
    }

    $('.btnSuccess').click(function (e) { 
        e.preventDefault();
        facialForm.hide();
        var comentario = $('#comentarioFacial');
        $('.lista-facial').append('<li class="obsMarcacao" x="'+posicaox+'" y="'+posicaoy+'" descricao="'+comentario.val()+'"><a>'+comentario.val()+'</a><span class="deletarMarcaFacial" idObjeto="'+idMarcacaoCorpo+'"><i class="fa fa-close"></i></span></li>');
        //$('.deletarMarca').attr('idobjeto', idMarcacaoCorpo);
        comentario.val('');
    });

    $(document).on("click",".deletarMarcaFacial", function(){
        var idMarcacao = $(this).attr('idObjeto');
        $('#'+idMarcacao).remove();
        //$(this).parent(".obsMarcacao").parent(".lista-facial").children(this).remove();
        $('.deletarMarcaFacial[idobjeto='+idMarcacao+']').parent(".obsMarcacao").remove();


        
    });

}


