/* Galeria INSTA  */

var url = "https://www.instagram.com/belizes_time/?__a=1";
var http = new XMLHttpRequest();
var inst;
var bloco = document.getElementById("instagram");

http.open("GET", url);
http.responseType = "json";
http.addEventListener("readystatechange", function(){
	if(http.readyState == 4){
		if(http.status == 200){
			inst = http.response.graphql.user.edge_owner_to_timeline_media.edges;
			//console.log(inst.length);
			for(let i = 0; i < inst.length; i++){
				//console.log(inst[i].node.thumbnail_resources[0].src);	
				bloco.innerHTML += `<div class="col-sm-3">
										<article class="c-article">
											<div class="article-image">
												<a href="https://www.instagram.com/belizes_time/" target="_blank">
													<img src="${inst[i].node.thumbnail_resources[2].src}" alt="Estética Avançada! Tratamentos personalizados. Há mais de 12 anos no mercado estético. ">
												</a>
											</div>
										</article>
									</div>`;
			}			
		} 
   }
});

http.send();


function enviarFeleConosco() {


    /*  Var do FORM: 
            nome
            email
            fone
			dataReserva
			periodoReserva
            mens
    */

    var imagem = "*Belize's Time - Realizar um Agendamento*";
    var nome = "*Nome:* " + document.querySelector("#nome").value;
    var email = "*E-mail:* " + document.querySelector("#email").value;
    var fone = "*Telefone:* " + document.querySelector("#fone").value;
    var dataReserva = "*Data Reserva:* " + document.querySelector("#dataReserva").value;
    var periodoReserva = "*Período Reserva:* " + document.querySelector("#periodoReserva").value;
    var mens = "*Mensagem:* " + document.querySelector("#mens").value;

    var whats = '11987865200';
    whats = whats.replace(/\D/g, '');
    if (whats.length < 13) {
        whats = "55" + whats;
    }

    var quebraDeLinha = "%0A";

    nome = window.encodeURIComponent(nome);
    email = window.encodeURIComponent(email);
    fone = window.encodeURIComponent(fone);
    dataReserva = window.encodeURIComponent(dataReserva);
    periodoReserva = window.encodeURIComponent(periodoReserva);
    mens = window.encodeURIComponent(mens);


    window.open("https://api.whatsapp.com/send?phone=" + whats + "&text=" + imagem + quebraDeLinha + quebraDeLinha + nome + quebraDeLinha + email + quebraDeLinha + fone + quebraDeLinha + dataReserva + quebraDeLinha + periodoReserva + quebraDeLinha + quebraDeLinha + mens, "_blank");

}